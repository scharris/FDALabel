'use strict';
var packName = '/fdalabel-r';
window.subApp = 'ldt';//'main';
console.log('window.location.href=' + window.location.href + ', window.subApp=' + window.subApp);

// All service and view url's should be built by functions in this object.
var UrlBuilder = {

  // services
  meddraTermsContaining: function(termFragmentsString) {
    return packName+'/services/meddra/containing/' + encodeURIComponent(termFragmentsString);
  },
  meddraLowLevelTermsHavingPreferredTerm: function(preferredTerm) {
    return packName+'/services/meddra/pt/' + encodeURIComponent(preferredTerm) + '/llts';
  },
  meddraLowLevelTermsRelatedToLowLevelTerm: function(lowLevelTerm) {
    return packName+'/services/meddra/llt/' + encodeURIComponent(lowLevelTerm) + '/related-llts';
  },

  pharmClassesContaining: function(termFragmentsString) {
    return packName+'/services/pharm-classes/containing/' + encodeURIComponent(termFragmentsString);
  },

  productNamesContaining: function(nameFragmentsString) {
    return packName+'/services/product/names-containing/' + encodeURIComponent(nameFragmentsString);
  },

  productNamesContainingLDT: function(nameFragmentsString) {
    return packName+'/services/product/ldt/names-containing/' + encodeURIComponent(nameFragmentsString);
  },

  splSummariesForPostedCriterion: function() {
    return packName+'/services/spl/ldt/summaries';
  },

  splSummariesForCriterionId: function(criterionId) {
    return packName+'/services/spl/ldt/summaries/json/criteria/'+criterionId;
  },

  splSummariesCSVForIdentifiedSearch: function(criterionId) {
    return packName+'/services/spl/ldt/summaries/csv/criteria/'+criterionId;
  },

  splsCountForCriterionId: function(criterionId) {
    return packName+'/services/spl/ldt/count/criteria/'+criterionId;
  },

  criterion: function(criterionId) {
    return packName+'/services/query-store/criteria/'+criterionId;
  },

  // Spl document served from application services, optionally with strings matching highlightStringsRegex highlighted.
  splDocumentForSetId: function(setId, highlightStringsRegex) {
    return packName+'/services/spl/set-ids/' + setId + '/spl-doc' +
        (highlightStringsRegex ? '?hl=' + encodeURIComponent(highlightStringsRegex) : '');
  },

  sectionTypes: function() {
    return packName+'/services/spl/section-types';
  },

  sectionTypesTree: function() {
    return packName+'/services/spl/section-types-tree';
  },

  sectionTypesTreeLDT: function() {
    return packName+'/services/spl/section-types-tree-ldt';
  },

  documentTypes: function() {
    return packName+'/services/spl/document-types';
  },

  documentTypesMain: function() {
    return packName+'/services/spl/document-types-main';
  },

  documentTypesLDT: function() {
    return packName+'/services/spl/document-types-ldt';
  },

  marketingCategoriesMain: function() {
    return packName+'/services/spl/marketing-categories-main';
  },

  marketingCategoriesLDT: function() {
    return packName+'/services/spl/marketing-categories-ldt';
  },

  routesOfAdmin: function() {
    return packName+'/services/spl/routes-of-admin';
  },

  dosageForms: function() {
    return packName+'/services/spl/dosage-forms';
  },

  dataLoaderRecords: function() {
    return packName+'/services/spl/data-loader-records';
  },

  // views

  mainSearchView: function() {
    console.log('mainSearchView window.subApp =' + window.subApp);
    return '/search';
  },

  identifiedSearchView: function(target, criterionId) {
    return '/search/'+target+'/criteria/'+criterionId;
  },

  splSummariesView: function(criterionId) {
    return '/spl-summaries/criteria/' + criterionId;
  },

  homeView: function() {
    return '/home';
  },

  aboutView: function() {
    return '/about';
  },

  databaseUpdatesView: function() {
    return '/database-updates';
  },

  disclaimerView: function() {
    return '/disclaimer';
  },

  contactView: function() {
    return '/contact';
  }
};

angular.module('appServices', ['ngResource']);

angular.module('FDALabelApp', ['ngRoute', 'ngSanitize', 'ngDialog','ui.bootstrap', 'appServices','chart.js', 'smart-table'])
  .constant('UrlBuilder', UrlBuilder)
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider.
      when(UrlBuilder.mainSearchView(), {
        templateUrl: 'views/main-search.html'
      }).
      when(UrlBuilder.identifiedSearchView(':target', ':criterionId'), {
        templateUrl: 'views/main-search.html'
      }).
      when(UrlBuilder.splSummariesView(':criterionId'), {
        templateUrl: 'views/spl-summaries.html'
      }).
      when(UrlBuilder.homeView(), {
        templateUrl: 'views/home.html',
        redirectTo: 'views/main-search.html'
      }).
      //when(UrlBuilder.aboutView(), {
      //  templateUrl: 'views/about.html'
        //templateUrl: 'http://www.fda.gov/ScienceResearch/BioinformaticsTools/ucm289739.htm'
      //}).
      when(UrlBuilder.databaseUpdatesView(), {
        templateUrl: 'views/databaseUpdates.html'
      }).
      when(UrlBuilder.disclaimerView(), {
        templateUrl: 'views/disclaimer.html'
      }).
      when(UrlBuilder.contactView(), {
        templateUrl: 'views/contact.html'
      }).
      otherwise({
        redirectTo: UrlBuilder.mainSearchView()
      });
    $locationProvider.html5Mode(true);
  })
  .controller('AppAboutCtrl', ['$scope','$location', function ($scope,$location) {
    $scope.getAboutURL = function () {
      if(window.subApp === 'ldt' || $location.url().indexOf('ldt') !== -1) {
        return 'http://inside.fda.gov:9003/CDER/OfficeofTranslationalSciences/OfficeofComputationalScience/ucm491609';
      } else {
        return 'http://www.fda.gov/ScienceResearch/BioinformaticsTools/ucm289739.htm';
      }
    };
  }]);

