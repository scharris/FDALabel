'use strict';

/* criterionEditor directive

 This directive provides a search criteria panel, which allows the user to edit the criterion object provided
 in the object referenced by the criterion attribute.

 Criteria Model
 --------------

 Criterion:
   EntityCriterion | GroupCriterion

 EntityCriterion
 {
   sourceEntity:('meddra'|'product'|...),

   <entity-specific criteria fields...>
 }

 GroupCriterion
 {
   criteria: Criterion*,
   logOps:   ('a'|'o')*
 }

 Note: The model above is more general than what is actually produced or consumed via this component currently.
 Here we only allow a collection of GroupCriterion objects (criterion.criteria) which can each only contain
 EntityCriterion objects (criterion.criteria[groupIx].criteria).
*/


angular.module('FDALabelApp').directive('criterionEditor', function($compile, $http, $templateCache, _) {

  var controller = function($scope) {

    var allCriteriaSourceEntitiesMap = {
      'spl-text' : 'Labeling Full Text Search',
      'product' : 'Product Name(s)',
      'section' : 'Labeling Section(s)',
      'document-type' : 'Labeling Types',
      'pharm-class' : 'Pharmacologic Class(es)',
      'marketing-category' : 'Application Types or Marketing Categories',
      'market-status' : 'Market Status',
      'meddra' : 'MedDRA Terms',
      'chemical-structure' : 'Chemical Structure',
      'identifier' : 'Labeling, Product and Ingredient Identifiers',
      'deduplication':'Deduplication Option(s)'
    };

    var AND_OP = 'a';
    var OR_OP = 'o';

    console.log('criterionEditor.mainView=' + $scope.mainView);
    console.log('criterionEditor.ldtView=' + $scope.ldtView);

    // functions to add and remove group criteria
    $scope.newGroupCriterion = function() {
      var groups = $scope.combinedCriterion.criteria;
      groups.push({criteria: [], logOps: []});

      if (groups.length > 1)
      {
        var logOps = $scope.combinedCriterion.logOps;
        if($scope.ldtView === true) {
          logOps.push(OR_OP);
        } else {
          logOps.push(logOps.length === 0 ? AND_OP : _.last(logOps));
        }
      }
    };


    $scope.removeGroupCriterionAt = function(groupIx) {
      $scope.combinedCriterion.criteria.splice(groupIx, 1);

      var opIx = groupIx > 0 ? groupIx - 1 : 0;
      $scope.combinedCriterion.logOps.splice(opIx, 1);
    };

    $scope.toggleCriterionPanel = function() {
      $scope.criterionPanelVisible = !$scope.criterionPanelVisible;
    };

    // functions to add and remove entity criteria within group criteria

    $scope.newEntityCriterionInGroupOfType = function(groupIx, entity) {
      var groupCriterion = $scope.combinedCriterion.criteria[groupIx];
      groupCriterion.criteria.push({sourceEntity: entity});

      if (groupCriterion.criteria.length > 1)
      {
        var groupLogOps = groupCriterion.logOps;
        if($scope.ldtView === true) {
          groupLogOps.push(AND_OP);
        } else {
          groupLogOps.push(groupLogOps.length === 0 ? AND_OP : _.last(groupLogOps));
        }
      }
    };

    $scope.removeEntityCriterionInGroupAt = function(groupIx, entCritIx) {
      var groupCriterion = $scope.combinedCriterion.criteria[groupIx];
      groupCriterion.criteria.splice(entCritIx, 1);

      var opIx = entCritIx > 0 ? entCritIx - 1 : 0;
      groupCriterion.logOps.splice(opIx, 1);
    };

    $scope.criterionTypeName = function(criterionType) {
      //console.log('all cri ent:' + allCriteriaSourceEntities);
      return allCriteriaSourceEntitiesMap[criterionType];
    };

    // logical operators between groups

    $scope.logOpBeforeGroupAt = function(groupIx) {
      if($scope.ldtView === true) {
        return groupIx === 0 ? undefined : OR_OP ;
      } else {
        return groupIx === 0 ? undefined : $scope.combinedCriterion.logOps[groupIx - 1];
      }
    };

    $scope.toggleLogOpBeforeGroupAt = function(groupIx) {
      if (groupIx > 0)
      {
        var oldOp = $scope.logOpBeforeGroupAt(groupIx);
        var newOp = oldOp === AND_OP ? OR_OP : AND_OP;
        $scope.combinedCriterion.logOps[groupIx - 1] = newOp;
      }
    };

    // display text for logical operators
    $scope.logOpDisplayText = function(logOp) {
      if($scope.ldtView === true) {
        return logOp === 'a' ? 'and' : 'or';
      } else {
        return logOp === 'a' ? '&' : 'or';
      }
    };


    // logical operators within groups

    $scope.logOpInGroupBeforeEntityAt = function(groupIx, entCritIx) {
      if($scope.ldtView === true) {
        ($scope.combinedCriterion.criteria[groupIx].logOps[entCritIx - 1]) = AND_OP ;
        return entCritIx === 0 ? undefined : AND_OP;
      } else {
        return entCritIx === 0 ? undefined : ($scope.combinedCriterion.criteria[groupIx].logOps[entCritIx - 1]);
      }
    };

    $scope.toggleLogOpInGroupBeforeEntityAt = function(groupIx, entCritIx) {
      if (entCritIx > 0)
      {
        var oldOp = $scope.logOpInGroupBeforeEntityAt(groupIx, entCritIx);
        var newOp = oldOp === AND_OP ? OR_OP : AND_OP;
        $scope.combinedCriterion.criteria[groupIx].logOps[entCritIx - 1] = newOp;
      }
    };

  }; // controller


  return {
    restrict: 'AE',
    scope: {
      combinedCriterion: '=ngModel',
      addableEntityCriterionTypes: '=addableEntityCriterionTypes',
      editingDisabled: '=editingDisabled',
      mainView: '=mainView',
      ldtView: '=ldtView'
    },
    replace: true,
    templateUrl: 'views/criterion-editor.html',
    controller: ['$scope', controller]
  };
});

