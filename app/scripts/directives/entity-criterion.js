'use strict';

/* entity-criterion directive

 This directive just loads its content from a template determined by the "sourceEntity"
 field of the object referenced by its ng-model attribute, and inserts the referenced object
 as "criterion" in the template's scope.
*/
angular.module('FDALabelApp').directive('entityCriterion', function($compile, $http, $templateCache) {

  var linker = function(scope, element) {
    var templateUrl = 'views/' + scope.criterion.sourceEntity + '-criterion.html';
    var templateLoader = $http.get(templateUrl, {cache: $templateCache});
    templateLoader.success(function(html) {
      element.html(html);
    }).then(function () {
      element.replaceWith($compile(element.html())(scope));
    });
    console.log('entityCriterion.mainView=' + scope.mainView);
    console.log('entityCriterion.ldtView=' + scope.ldtView);
  };

  return {
    restrict: 'AE',
    scope: {
      criterion: '=ngModel',
      editingDisabled: '=editingDisabled',
      mainView: '=mainView',
      ldtView: '=ldtView'
    },
    link: linker
  };
});
