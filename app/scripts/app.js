'use strict';
var firstword='/fdalabel';
window.subApp = 'main';
console.log('window.location.href=' + window.location.href);

console.log('window.location.href=' + window.location.href + ', window.subApp=' + window.subApp);

// All service and view url's should be built by functions in this object.
var UrlBuilder = {

  // services
  meddraTermsContaining: function(termFragmentsString) {
    return firstword+'/services/meddra/containing/' + encodeURIComponent(termFragmentsString);
  },
  meddraLowLevelTermsHavingPreferredTerm: function(preferredTerm) {
    return firstword+'/services/meddra/pt/' + encodeURIComponent(preferredTerm) + '/llts';
  },
  meddraLowLevelTermsRelatedToLowLevelTerm: function(lowLevelTerm) {
    return firstword+'/services/meddra/llt/' + encodeURIComponent(lowLevelTerm) + '/related-llts';
  },

  pharmClassesContaining: function(termFragmentsString) {
    return firstword+'/services/pharm-classes/containing/' + encodeURIComponent(termFragmentsString);
  },

  productNamesContaining: function(nameFragmentsString) {
    return firstword+'/services/product/names-containing/' + encodeURIComponent(nameFragmentsString);
  },

  productNamesContainingLDT: function(nameFragmentsString) {
    return firstword+'/services/product/ldt/names-containing/' + encodeURIComponent(nameFragmentsString);
  },

  splSummariesForPostedCriterion: function() {
    //window.alert('splSummariesForPostedCriterion subApp =' + subApp);
    if(window.subApp === 'ldt') {
      return firstword+'/services/spl/ldt/summaries';
    }
    else {
      return firstword+'/services/spl/summaries';
    }
  },

  splSummariesForCriterionId: function(criterionId) {
    //window.alert('splSummariesForCriterionId subApp =' + subApp);
    if(window.subApp === 'ldt') {
      return firstword+'/services/spl/ldt/summaries/json/criteria/'+criterionId;
    }
    else {
      return firstword+'/services/spl/summaries/json/criteria/'+criterionId;
    }
  },

  splSummariesCSVForIdentifiedSearch: function(criterionId) {
    return firstword+'/services/spl/summaries/csv/criteria/'+criterionId;
  },

  ldtSplSummariesCSVForIdentifiedSearch: function(criterionId) {
    return firstword+'/services/spl/ldt/summaries/csv/criteria/'+criterionId;
  },

  splsCountForCriterionId: function(criterionId) {
    if(window.subApp === 'ldt') {
      return firstword+'/services/spl/ldt/count/criteria/'+criterionId;
    }
    else {
      return firstword+'/services/spl/count/criteria/'+criterionId;
    }
  },

  criterion: function(criterionId) {
    return firstword+'/services/query-store/criteria/'+criterionId;
  },

  // Spl document served from application services, optionally with strings matching highlightStringsRegex highlighted.
  splDocumentForSetId: function(setId, highlightStringsRegex) {
    return firstword+'/services/spl/set-ids/' + setId + '/spl-doc' +
        (highlightStringsRegex ? '?hl=' + encodeURIComponent(highlightStringsRegex) : '');
  },

  sectionTypes: function() {
    return firstword+'/services/spl/section-types';
  },

  sectionTypesTree: function() {
    return firstword+'/services/spl/section-types-tree';
  },

  sectionTypesTreeLDT: function() {
    return firstword+'/services/spl/section-types-tree-ldt';
  },

  documentTypes: function() {
    return firstword+'/services/spl/document-types';
  },

  documentTypesMain: function() {
    return firstword+'/services/spl/document-types-main';
  },

  documentTypesLDT: function() {
    return firstword+'/services/spl/document-types-ldt';
  },

  marketingCategoriesMain: function() {
    return firstword+'/services/spl/marketing-categories-main';
  },

  marketingCategoriesLDT: function() {
    return firstword+'/services/spl/marketing-categories-ldt';
  },

  routesOfAdmin: function() {
    return firstword+'/services/spl/routes-of-admin';
  },

  dosageForms: function() {
    return firstword+'/services/spl/dosage-forms';
  },

  dataLoaderRecords: function() {
    return firstword+'/services/spl/data-loader-records';
  },

  // views

  mainSearchView: function() {
    console.log('mainSearchView window.subApp =' + window.subApp);
    //window.alert('mainSearchView window.subApp =' + window.subApp);
    return '/search';
  },

  ldtSearchView: function() {
    console.log('ldtSearchView window.subApp =' + window.subApp);
    //window.alert('ldtSearchView window.subApp =' + window.subApp);
    return '/ldt/search';
  },

  homeSearchView: function() {
    console.log('homeSearchView window.subApp =' + window.subApp);
    //window.alert('homeSearchView window.subApp =' + window.subApp);
    if(window.subApp === 'ldt') {
      return this.ldtSearchView();
    }
    else {
      return this.mainSearchView();
    }
  },

  identifiedSearchView: function(target, criterionId) {
    return '/search/'+target+'/criteria/'+criterionId;
  },

  ldtIdentifiedSearchView: function(target, criterionId) {
    return '/ldt/search/'+target+'/criteria/'+criterionId;
  },

  splSummariesView: function(criterionId) {
    return '/spl-summaries/criteria/' + criterionId;
  },

  ldtSplSummariesView: function(criterionId) {
    return '/ldt/spl-summaries/criteria/' + criterionId;
  },

  fdalabelLinksView: function() {
    return '/fdalabel-links';
  },

  homeView: function() {
    return '/home';
  },

  aboutView: function() {
    return '/about';
  },

  ldtAboutView: function() {
    return '/ldt/about';
  },

  dbUpdatesView: function() {
    return '/dbupdates';
  },

  mainDatabaseUpdatesView: function() {
    return '/database-updates';
  },

  ldtDatabaseUpdatesView: function() {
    return '/ldt/database-updates';
  },

  disclaimerView: function() {
    return '/disclaimer';
  },

  mainDisclaimerView: function() {
    return '/disclaimer';
  },

  ldtDisclaimerView: function() {
    return '/ldt/disclaimer';
  },

  contactView: function() {
    return '/contact';
  },

  mainContactView: function() {
    return '/contact';
  },

  ldtContactView: function() {
    return '/ldt/contact';
  }

};

angular.module('appServices', ['ngResource']);

angular.module('FDALabelApp', ['ngRoute', 'ngSanitize', 'ngDialog','ui.bootstrap', 'appServices','chart.js' ])
  .constant('UrlBuilder', UrlBuilder)
  .config(function ($routeProvider) {
    $routeProvider.
      when(UrlBuilder.mainSearchView(), {
        templateUrl: 'views/main-search.html'
      }).
      when(UrlBuilder.ldtSearchView(), {
        templateUrl: 'views/main-search.html'
      }).
      when(UrlBuilder.identifiedSearchView(':target', ':criterionId'), {
        templateUrl: 'views/main-search.html'
      }).
      when(UrlBuilder.ldtIdentifiedSearchView(':target', ':criterionId'), {
        templateUrl: 'views/main-search.html'
      }).
      when(UrlBuilder.splSummariesView(':criterionId'), {
        templateUrl: 'views/spl-summaries.html'
      }).
      when(UrlBuilder.ldtSplSummariesView(':criterionId'), {
        templateUrl: 'views/spl-summaries.html'
      }).
      when(UrlBuilder.fdalabelLinksView(), {
        templateUrl: 'views/fdalabel-links.html'
      }).
      when(UrlBuilder.homeView(), {
        templateUrl: 'views/home.html'
      }).
      //when(UrlBuilder.aboutView(), {
      //  templateUrl: 'views/about.html'
        //templateUrl: 'http://www.fda.gov/ScienceResearch/BioinformaticsTools/ucm289739.htm'
      //}).
      when(UrlBuilder.dbUpdatesView(), {
        templateUrl: 'views/dbUpdates.html'
      }).
      when(UrlBuilder.mainDatabaseUpdatesView(), {
        templateUrl: 'views/databaseUpdates.html'
      }).
      when(UrlBuilder.ldtDatabaseUpdatesView(), {
        templateUrl: 'views/databaseUpdates.html'
      }).
      when(UrlBuilder.disclaimerView(), {
        templateUrl: 'views/disclaimer.html'
      }).
      when(UrlBuilder.mainDisclaimerView(), {
        templateUrl: 'views/disclaimer.html'
      }).
      when(UrlBuilder.ldtDisclaimerView(), {
        templateUrl: 'views/disclaimer.html'
      }).
      when(UrlBuilder.contactView(), {
        templateUrl: 'views/contact.html'
      }).
      when(UrlBuilder.mainContactView(), {
        templateUrl: 'views/contact.html'
      }).
      when(UrlBuilder.ldtContactView(), {
        templateUrl: 'views/contact.html'
      }).
      otherwise({
        redirectTo: UrlBuilder.mainSearchView()
      });
  })
  .controller('AppAboutCtrl', ['$scope','$location', function ($scope,$location) {
    $scope.getAboutURL = function () {
      if(window.subApp === 'ldt') {
        return 'http://inside.fda.gov:9003/CDER/OfficeofTranslationalSciences/OfficeofComputationalScience/ucm491609';
      } else if ($location.url().indexOf('ldt') !== -1) {
        return 'http://inside.fda.gov:9003/CDER/OfficeofTranslationalSciences/OfficeofComputationalScience/ucm491609';
      } else {
        return 'http://www.fda.gov/ScienceResearch/BioinformaticsTools/ucm289739.htm';
      }
    };
  }]);

