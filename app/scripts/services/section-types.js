'use strict';

angular.module('appServices').factory('SectionTypes', function($http, UrlBuilder) {

  return {
    sectionTypesPromise: $http.get(UrlBuilder.sectionTypes())
  };

});

