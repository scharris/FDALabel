'use strict';

angular.module('appServices').factory('SectionTypesTree', function($http, UrlBuilder) {

  return {
    sectionTypesPromise: $http.get(UrlBuilder.sectionTypesTree())
  };

});

