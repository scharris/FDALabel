'use strict';

angular.module('appServices').factory('InterViewData', function() {

  var data = null;

  return {
    set: function(newData) {
      data = newData;
    },
    take: function() {
      var retData = data;
      data = null;
      return retData;
    }
  };
});
