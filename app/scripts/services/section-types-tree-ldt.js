'use strict';

angular.module('appServices').factory('SectionTypesTreeLDT', function($http, UrlBuilder) {

  return {
    sectionTypesPromise: $http.get(UrlBuilder.sectionTypesTreeLDT())
  };

});

