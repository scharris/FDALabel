'use strict';

angular.module('appServices').factory('MarketingCategoriesforSealds', function($http, UrlBuilder) {

  return {
    marketingCategoriesPromise: $http.get(UrlBuilder.marketingCategoriesLDT())
  };

});

