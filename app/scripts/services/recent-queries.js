'use strict';

angular.module('appServices').factory('RecentQueries', function() {

  var queries = [];
  var maxSize = 20;
  var displayOption = 'last';//Options: ['last', 'empty', 'list']

  // http://stackoverflow.com/questions/4459928/how-to-deep-clone-in-javascript
  function cloneQuery(item) {
    if (!item) { return item; }

    var types = [ Number, String, Boolean ], result;

    // normalizing primitives if someone did new String('aaa'), or new Number('444');
    types.forEach(function(type) {
      if (item instanceof type) {
        result = type(item);
      }
    });

    if (typeof result === 'undefined') {
      if (Object.prototype.toString.call( item ) === '[object Array]') {
        result = [];
        item.forEach(function(child, index) {
          result[index] = cloneQuery(child);
        });
      } else if (typeof item === 'object') {
        // testing that this is DOM
        if (item.nodeType && typeof item.cloneNode === 'function') {
          result = item.cloneNode( true );
        } else if (!item.prototype) { // check that this is a literal
          if (item instanceof Date) {
            result = new Date(item);
          } else {
            // it is an object literal
            result = {};
            for (var i in item) {
              result[i] = cloneQuery(item[i]);
            }
          }
        } else {
          result = item;
        }
      } else {
        result = item;
      }
    }

    return result;
  }

  return {
    last: function() {
      var lastQuery = queries[queries.length-1];
      return cloneQuery(lastQuery);
    },

    add: function(query) {
      queries.push(cloneQuery(query));
      this.setDisplayOpt('last');
      if (queries.length > maxSize)
      {
        queries.splice(0,1);
      }
    },

    addUnlessSameAsLast: function(query) {
      var lastQuery = this.last();
      // Add the query unless both it and the last query have identified criteria and the criteria id's and targets are equal.
      if (!lastQuery || !query.criterionId || !lastQuery.criterionId ||
          query.criterionId !== lastQuery.criterionId || query.target !== lastQuery.target)
      {
        this.add(query);
      }
    },

    setDisplayOpt: function(dispOpt) {
      displayOption = dispOpt;
    },

    getDisplayOpt: function() {
      return displayOption;
    }
  };

});

