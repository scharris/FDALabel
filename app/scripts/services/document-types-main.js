'use strict';

angular.module('appServices').factory('DocumentTypesMain', function($http, UrlBuilder) {

  return {
    documentTypesPromise: $http.get(UrlBuilder.documentTypesMain())
  };

});

