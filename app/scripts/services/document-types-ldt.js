'use strict';

angular.module('appServices').factory('DocumentTypesLDT', function($http, UrlBuilder) {

  return {
    documentTypesPromise: $http.get(UrlBuilder.documentTypesLDT())
  };

});

