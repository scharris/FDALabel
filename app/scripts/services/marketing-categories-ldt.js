'use strict';

angular.module('appServices').factory('MarketingCategoriesLDT', function($http, UrlBuilder) {

  return {
    marketingCategoriesPromise: $http.get(UrlBuilder.marketingCategoriesLDT())
  };

});

