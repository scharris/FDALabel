'use strict';

angular.module('appServices').factory('RoutesOfAdmin', function($http, UrlBuilder) {

  return {
    routesOfAdminPromise: $http.get(UrlBuilder.routesOfAdmin())
  };

});

