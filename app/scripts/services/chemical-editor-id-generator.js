'use strict';

angular.module('appServices').factory('ChemicalEditorIdGenerator', function() {

  var next = 1;

  return {
    nextId: function () {
      return next++;
    }
  };

});

