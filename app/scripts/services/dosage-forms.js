'use strict';

angular.module('appServices').factory('DosageForms', function($http, UrlBuilder) {

  return {
    dosageFormsPromise: $http.get(UrlBuilder.dosageForms())
  };

});

