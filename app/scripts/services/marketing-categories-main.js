'use strict';

angular.module('appServices').factory('MarketingCategoriesMain', function($http, UrlBuilder) {

  return {
    marketingCategoriesPromise: $http.get(UrlBuilder.marketingCategoriesMain())
  };

});

