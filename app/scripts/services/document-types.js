'use strict';

angular.module('appServices').factory('DocumentTypes', function($http, UrlBuilder) {

  return {
    documentTypesPromise: $http.get(UrlBuilder.documentTypes())
  };

});

