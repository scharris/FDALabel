'use strict';

angular.module('appServices').factory('DataLoaderRecords', function($http, UrlBuilder) {

  return {
    dataLoaderRecordsPromise: $http.get(UrlBuilder.dataLoaderRecords())
  };

});

