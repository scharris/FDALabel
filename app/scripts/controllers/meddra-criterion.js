'use strict';

angular.module('FDALabelApp').controller('MeddraCriterionCtrl', function($scope, $http, _, UrlBuilder) {

  // If necessary, initialize any missing fields in our criterion object, which object should be reachable in our scope.
  if ($scope.criterion.terms === undefined)
  {
    $scope.criterion.terms = [];
  }

  // ephemeral scope items
  $scope.preferredTermsOnly = false;
  $scope.includeUnlabeled = false;
  $scope.enteredTerm = null;
  $scope.messages = [];

  var typeaheadResultsLimit = 100;
  var splMeddraIndexingAuthority = 0;


  $scope.termsMatching = function(termFragmentsString) {
    var params = {tt:    $scope.preferredTermsOnly? 'pt': 'llt',
                  a:     splMeddraIndexingAuthority,
                  unlab: $scope.includeUnlabeled? 1: 0,
                  l:     typeaheadResultsLimit};

    var serviceUrl = UrlBuilder.meddraTermsContaining(termFragmentsString);

    return $http.get(serviceUrl, {params: params}).
                 then(function(response) { return response.data; });
  };


  function addTerm(termInfo) {
    if (!_.findWhere($scope.criterion.terms, {term: termInfo.term}))
    {
      $scope.criterion.terms.push({term: termInfo.term, isPt: termInfo.isPt});
    }
  }

  /* When the enteredTerm model value changes, add the term to terms.  Only valid terms from the typeahead
     suggestions can make it into enteredTerm, because of the typeahead-editable=false attribute on the input element.
   */
  $scope.$watch('enteredTerm', function() {
    if ($scope.enteredTerm)
    {
      addTerm($scope.enteredTerm);
      $scope.enteredTerm = null;
    }
  });

  $scope.removeTerm = function(index) {
    $scope.criterion.terms.splice(index, 1);
  };

  $scope.removeAllTerms = function() {
    $scope.criterion.terms.length = 0;
  };

  $scope.termTypeFullName = function(termInfo) {
    return termInfo.isPt === 0 ? 'low level term' : 'preferred term';
  };

  $scope.termTypeAbbrev = function(termInfo) {
    return termInfo.isPt === 0 ? 'llt' : 'pt';
  };

  $scope.removeMessage = function(msgIx) {
    $scope.messages.splice(msgIx, 1);
  };

  /*$scope.clearMessages = function() {
    $scope.messages.length = 0;
  }*/


  ///////////////////////////////////////////////////////
  // viewing and adding of terms related to a given term

  function relatedLowLevelTermsPromise(termInfo) {
    if (termInfo.isPt)
    {
      return $http.get(UrlBuilder.meddraLowLevelTermsHavingPreferredTerm(termInfo.term),
                       {params: {a:     splMeddraIndexingAuthority,
                                 unlab: $scope.includeUnlabeled? 1: 0}});
    }
    else
    {
      return $http.get(UrlBuilder.meddraLowLevelTermsRelatedToLowLevelTerm(termInfo.term),
                       {params: {a:     splMeddraIndexingAuthority,
                                 unlab: $scope.includeUnlabeled? 1: 0}});
    }
  }

  function insertRelatedLowLevelTerms(termInfo) {
    relatedLowLevelTermsPromise(termInfo).then(function(response) {
      if (response.data.length === 0 || response.data.length === 1 && response.data[0].term === termInfo.term)
      {
        var noneFoundMsg =  termInfo.isPt?
            ('No ' + ($scope.includeUnlabeled?'':'labeled ') + 'low level terms were found for preferred term \'' + termInfo.term + '\'.') :
            ('No ' + ($scope.includeUnlabeled?'':'labeled ') + 'low level terms were found having the same preferred term as \'' + termInfo.term + '\'.');
        $scope.messages.push({type: 'info', text: noneFoundMsg});
      }
      else
      {
        _.each(response.data, function(relTermInfo) {
          addTerm(relTermInfo);
        });
      }
    });
  }

  function showRelatedLowLevelTerms(termInfo) {
    relatedLowLevelTermsPromise(termInfo).then(function(response) {
      if (response.data.length === 0 || response.data.length === 1 && response.data[0].term === termInfo.term)
      {
        var noneFoundMsg =  termInfo.isPt?
            ('No ' + ($scope.includeUnlabeled?'':'labeled ') + 'low level terms were found for preferred term \'' + termInfo.term + '\'.') :
            ('No ' + ($scope.includeUnlabeled?'':'labeled ') + 'low level terms were found having the same preferred term as \'' + termInfo.term + '\'.');
        $scope.messages.push({type: 'info', text: noneFoundMsg});
      }
      else
      {
        var msgText = 'Terms related to \'' + termInfo.term + '\'' + ($scope.includeUnlabeled? '' :' which appear in at least one label') + ': ';
        _.each(response.data, function(relTermInfo) {
          msgText += '\n   ' + relTermInfo.term + ' ' + (relTermInfo.isPt?' (← preferred term)':'') + ' ';
        });
        $scope.messages.push({type:'success', text:msgText, classes: 'preformatted-message'});
      }
    });
  }

  function insertPreferredTerm(termInfo) {
    relatedLowLevelTermsPromise(termInfo).then(function(response) {
      var pt = _.findWhere(response.data, {isPt: 1});
      if (pt)
      {
        addTerm(pt);
      }
      else
      {
        var noneFoundMsg =  ('No ' + ($scope.includeUnlabeled?'':'labeled ') + 'preferred term was found for term \'' + termInfo.term + '\'.');
        $scope.messages.push({type: 'info', text: noneFoundMsg});
      }
    });
  }

  // actions for preferred terms
  var ptActions = [
    {text:   'Insert Low Level Terms',
     action: insertRelatedLowLevelTerms},
    {text:   'Show Low Level Terms',
     action: showRelatedLowLevelTerms}
  ];

  // actions for low level terms
  var lltActions = [
    {text:   'Insert Preferred Term',
     action: insertPreferredTerm},
    {text:   'Insert Low Level Terms Having Same Preferred Term',
     action: insertRelatedLowLevelTerms},
    {text:   'Show Low Level Terms Having Same Preferred Term',
     action: showRelatedLowLevelTerms}
  ];

  $scope.termActions = function(termInfo) {
    return termInfo.isPt ? ptActions : lltActions;
  };

  //
  ///////////////////////////////////////////////////////

});
