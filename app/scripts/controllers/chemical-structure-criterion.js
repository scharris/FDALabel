'use strict';

angular.module('FDALabelApp').controller('ChemicalStructureCriterionCtrl', function($scope, $http, $log, $window, $rootScope,
                                                                                    ChemicalEditorIdGenerator) {

  /* VAR values the number in Java */
  var ACT = 'ACT';
  var NACT = 'NACT';
  var ALL = 'ALL';

  $scope.editorNum = ChemicalEditorIdGenerator.nextId();

  $scope.editorId = function() {
    return 'chem-ed-' + $scope.editorNum;
  };

  $scope.structureSearchTypeOptions = ['substructure', 'similarity'/*, 'exact'*/];
  $scope.ingredientTypes = [
    {value: ACT,   displayAs: 'Active'},
    {value: NACT,  displayAs: 'Inactive'},
    {value: ALL,   displayAs: 'Both'}
  ];

  // Install global mol change handler for JSDraw editor elements.
  if (!$window.molChanged) {
    $window.molChanged = function(ed) {
      var ctrlScope = angular.element(document.getElementById('ctrl-el-' + ed.id)).scope();
      ctrlScope.$apply(function() {
        ctrlScope.criterion.acceptedMol = 'q' + ed.getMolfile(); // ('q' necessary because blank name line causes error in Bingo.)
      });
    };
  }

  /* Initialize criterion fields.

   If there's a criterion object reachable from our scope (which there should be when used inside the
   criterion directive), then initialize the (non-constant) scope items from the criterion object.
   */


  if (!$scope.criterion.acceptedMol) {
    $scope.criterion.acceptedMol = null;
  }

  if (!$scope.criterion.structureSearchType) {
    $scope.criterion.structureSearchType = 'substructure';
  }

  if (!$scope.criterion.minimumSimilarity) {
    $scope.criterion.minimumSimilarity = '0.5';
  }

  if (!$scope.criterion.ingredientTypes)
  {
    $scope.criterion.ingredientTypes = ACT;
  }

  $scope.$watch('$viewContentLoaded', function() {
    $window.dojo.ready(function() {
      $scope.$evalAsync(function() {
        $window.JSDraw.init();
      });
    });
  });

});
