'use strict';

angular.module('FDALabelApp').controller('DocumentTypeCriterionCtrl', function($scope, $log, _, DocumentTypesLDT, DocumentTypesMain) {

  $scope.documentTypeChoices = [];
  console.log('DocumentTypeCriterionCtrl.mainView=' + $scope.mainView);
  console.log('DocumentTypeCriterionCtrl.ldtView=' + $scope.ldtView);

  if($scope.ldtView) {
    DocumentTypesLDT.documentTypesPromise.success(function(data) {
      console.log('data.documentTypesSealds=' + data.documentTypesLDT);
      $scope.documentTypeChoices = [{code: '', displayName: '',count:0,lev:0}].concat(data.documentTypesLDT);
    }).error(function() {
      $log.error('Could not load document types, promise failed.');
    });
  }
  else {
    DocumentTypesMain.documentTypesPromise.success(function(data) {
      console.log('data.documentTypesMain=' + data.documentTypesMain);
      $scope.documentTypeChoices = [{code: '', displayName: '',count:0}].concat(data.documentTypesMain);
    }).error(function() {
      $log.error('Could not load document types, promise failed.');
    });
  }

  // Holds selection state of the "other document type" select list.
  $scope.selectedOtherDocumentTypeCode = null;

  $scope.documentTypeDisplayName = function(typeCode) {
    var docTypeInfo = _.findWhere($scope.documentTypeChoices, {code: typeCode});
    return docTypeInfo ? docTypeInfo.displayName : '?';
  };

  $scope.displayDocumentType = function(type) {
    if (type.lev > 0 && type.count>0)
    {
      return '•    '+type.displayName+' ('+type.count+' labeling)';
    }
    else if (type.count > 0 )
    {
      return type.displayName+' ('+type.count+' labeling)';
    }
    return ' ';

  };

  $scope.addDocumentType = function(docTypeCode) {
    if ($scope.criterion.documentTypeCodes.indexOf(docTypeCode) === -1) {
      $scope.criterion.documentTypeCodes.push(docTypeCode);
    }
  };

  $scope.removeDocumentTypeAt = function(ix) {
    $scope.criterion.documentTypeCodes.splice(ix, 1);
  };

  $scope.removeAll = function() {
    $scope.criterion.documentTypeCodes.length = 0;
  };

  // When a document type is selected via the select list, add it and un-select it again in the select list.
  $scope.$watch('selectedOtherDocumentTypeCode', function() {
    if ($scope.selectedOtherDocumentTypeCode)
    {
      $scope.addDocumentType($scope.selectedOtherDocumentTypeCode);
      $scope.selectedOtherDocumentTypeCode = null;
    }
  });

  // Added by JY for tooltip text.
  $scope.labelingCountByType = function(docTypeCode) {
    var type;
    var nTypes = $scope.documentTypeChoices.length;
    for (var i = 0; i < nTypes; i++) {
      type = $scope.documentTypeChoices[i];
      if (type.code === docTypeCode) {
        return type.count + ' labeling';
      }
    }
    return '0 labeling';
  };

  /* Initialize criterion fields.

   If there's a criterion object reachable from our scope (which there should be when used inside the
   criterion directive), then initialize the (non-constant) scope items from the criterion object.
   */

  if (!$scope.criterion.documentTypeCodes)
  {
    $scope.criterion.documentTypeCodes = [];
  }

});
