'use strict';

angular.module('FDALabelApp').controller('HomepageCtrl', function($scope, $http, $location, $window, UrlBuilder, RecentQueries) {

  console.log('HomepageCtrl $location=' + $location.url());
  RecentQueries.setDisplayOpt('empty');

  //$scope.changeRoute('#/search');

  $scope.$on('$routeChangeSuccess', function(event, current, previous) {
    console.log('routeChangeSuccess $location=' + $location.url());
    console.log('routeChangeSuccess current=' + angular.toJson(current));
    console.log('routeChangeSuccess previous=' + angular.toJson(previous));
  });

});
