'use strict';

angular.module('FDALabelApp').controller('SplTextCriterionCtrl', function($scope, $rootScope) {

  /* Initialize variable scope items.

     If there's a criterion object reachable from our scope, which there should be when used inside the
     criterion directive, then initialize the non-constant scope items from the criterion object.
   */
  if (!$scope.criterion.textQuery)
  {
    $scope.criterion.textQuery = '';
  }

  $scope.removeAll = function() {
    $scope.criterion.textQuery = '';
  };

  $scope.doFullTextSearch = function() {
    console.log('s;l-text-criterion.doFullTextSearch()');
    $rootScope.$broadcast('main', 'doSearch');
  };

});
