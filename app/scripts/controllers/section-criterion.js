'use strict';

angular.module('FDALabelApp').controller('SectionCriterionCtrl', function($scope, $rootScope, $http, $log, SectionTypesTree, SectionTypesTreeLDT) {

  $scope.sectionTypeChoices = [];

  console.log('SectionCriterionCtrl.mainView=' + $scope.mainView);
  console.log('SectionCriterionCtrl.ldtView=' + $scope.ldtView);

  if($scope.ldtView) {
    SectionTypesTreeLDT.sectionTypesPromise.success(function(data) {
      console.log('data.sectionTypesTreeLDT=' + data);
      $scope.sectionTypeChoices = [{code: ' ', displayName: ' ',typeFormat:' ',viewNo:' ',lev:0,count:0}].concat(data.sectionTypesTreeLDT);
    }).error(function() {
      $log.error('Could not load section types for LDT, promise failed.');
    });
  }
  else {
    SectionTypesTree.sectionTypesPromise.success(function(data) {
      $scope.sectionTypeChoices = [{code: ' ', displayName: ' ',typeFormat:' ',viewNo:' ',lev:0,count:0}].concat(data.sectionTypesTree);
    }).error(function() {
      $log.error('Could not load section types, promise failed.');
    });
  }

  $scope.displaySectionAsTree = function(sectiontype) {
    var strType = '';
    if ($scope.startsWith(sectiontype.code,'1-') || $scope.startsWith(sectiontype.code,'0-')) {
      strType='PLR,';
    } else if ($scope.startsWith(sectiontype.code,'2-')) {
      strType='Non-PLR,';
    } else if ($scope.startsWith(sectiontype.code,'3-')) {
      strType='OTC,';
    } else if ($scope.startsWith(sectiontype.code,'4-')) {
      strType='Other,';
    }

    if(sectiontype.count > 0 ) {
      if(sectiontype.lev > 0 ) {
        if($scope.ldtView === true) {
          return '•    '+sectiontype.displayName+' ('+strType+ ' ' + sectiontype.count+' labeling)';
        }
        else {
          return sectiontype.displayName+' ('+sectiontype.count+' labeling)';
        }
      }
      else {
        if($scope.ldtView === true) {
          return sectiontype.displayName+' ('+strType+ ' '+sectiontype.count+' labeling)';
        }
        else {
          return sectiontype.displayName+' ('+sectiontype.count+' labeling)';
        }
      }
    }
    else {
      return ' ';
    }
  };

  $scope.startsWith = function(actual, expected) {
    var lowerStr = (actual + '').toLowerCase();
    return lowerStr.indexOf(expected.toLowerCase()) === 0;
  };

  /* Initialize criterion fields.

   If there's a criterion object reachable from our scope (which there should be when used inside the
   criterion directive), then initialize the (non-constant) scope items from the criterion object.
   */

  if (!$scope.criterion.textQuery)
  {
    $scope.criterion.textQuery = '';
  }

  if (!$scope.criterion.sectionTypeCode)
  {
    $scope.criterion.sectionTypeCode = '';
  }


  $scope.selectSectionType = function(secTypeCode) {
    $scope.criterion.sectionTypeCode = secTypeCode;
  };

  $scope.removeAll = function() {
    $scope.criterion.textQuery = '';
    $scope.criterion.sectionTypeCode = '';
    $scope.criterion.sectionTypeName = '';
  };

  $scope.doFullTextSearch = function() {
    console.log('section-criterion.doFullTextSearch()');
    $rootScope.$broadcast('main', 'doSearch');
  };

});
