'use strict';

angular.module('FDALabelApp').controller('DosageFormCriterionCtrl', function($scope, $log, _, DosageForms) {

  $scope.dosageFormChoices = [];

  DosageForms.dosageFormsPromise.success(function(data) {
    $scope.dosageFormChoices = [{code: '', displayName: ''}].concat(data.dosageForms);
  }).error(function() {
    $log.error('Could not load dosage forms, promise failed.');
  });

  // Holds selection state of the complete dosage forms select list.
  $scope.selectedOtherDosageFormCode = null;

  $scope.dosageFormDisplayName = function(code) {
    var info = _.findWhere($scope.dosageFormChoices, {code: code});
    return info ? info.displayName : '?';
  };

  $scope.addDosageFormWithSubtypes = function(baseName) {
    // Add all dosage forms specializing baseName.
    _.each($scope.dosageFormChoices, function(dosageForm) {
      if (dosageForm.displayName === baseName || dosageForm.displayName.indexOf(baseName + ',') === 0) {
        $scope.addDosageForm(dosageForm.code);
      }
    });
  };

  $scope.addDosageForm = function(code) {
    if ($scope.criterion.dosageFormCodes.indexOf(code) === -1) {
      $scope.criterion.dosageFormCodes.push(code);
    }
  };

  $scope.removeDosageFormAt = function(ix) {
    $scope.criterion.dosageFormCodes.splice(ix, 1);
  };

  $scope.removeAll = function() {
    $scope.criterion.dosageFormCodes.length = 0;
  };

  // When a dosage form is selected via the select list, add it and un-select it again in the select list.
  $scope.$watch('selectedOtherDosageFormCode', function() {
    if ($scope.selectedOtherDosageFormCode)
    {
      $scope.addDosageForm($scope.selectedOtherDosageFormCode);
      $scope.selectedOtherDosageFormCode = null;
    }
  });


  /* Initialize criterion fields.

   If there's a criterion object reachable from our scope (which there should be when used inside the
   criterion directive), then initialize the (non-constant) scope items from the criterion object.
   */

  if (!$scope.criterion.dosageFormCodes)
  {
    $scope.criterion.dosageFormCodes = [];
  }

});
