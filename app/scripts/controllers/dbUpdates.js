'use strict';

angular.module('FDALabelApp').controller('DbUpdatesCtrl', function($scope, $http, $location, $window, UrlBuilder) {

  console.log('DbUpdatesCtrl $location=' + $location.url());
  console.log('DbUpdatesCtrl window.subApp=' + window.subApp + ',$window.subApp=' + $window.subApp);

  if(window.subApp === 'ldt') {
    $location.path(UrlBuilder.ldtDatabaseUpdatesView());
  } else if ($location.url().indexOf('ldt') !== -1) {
    $scope.mainView = false;
    $scope.ldtView = true;
    $window.subApp = 'ldt';
    $location.path(UrlBuilder.ldtDatabaseUpdatesView());
  } else {
    $location.path(UrlBuilder.mainDatabaseUpdatesView());
  }

  $scope.$on('$routeChangeSuccess', function(event, current, previous) {
    console.log('routeChangeSuccess $location=' + $location.url());
    //console.log('routeChangeSuccess $rootScope=' + $rootScope);
    console.log('routeChangeSuccess current=' + angular.toJson(current));
    console.log('routeChangeSuccess previous=' + angular.toJson(previous));
  });

});
