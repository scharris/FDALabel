'use strict';

angular.module('FDALabelApp').controller('ProductCriterionCtrl', function($scope, $http, UrlBuilder) {

  // product name types, which should match enumeration values of ProductNameType in Products.java
  var GEN =   'GEN';
  var TRADE = 'TRADE';
  var ANY =   'ANY';

  // product name pattern types, which should match enumeration values of StringPatternType in DAO.java
  var SUBSTR = 'SUBSTR';
  var GLOB =   'GLOB';
  var REGEX =  'REGEX';

  var typeaheadResultsLimit = 100;

  // Initialize constant scope items.

  $scope.nameTypes = [
    {value: ANY,   displayAs: 'Trade or generic/proper name'},
    {value: TRADE, displayAs: 'Trade name'},
    {value: GEN,   displayAs: 'Generic/proper name'}
  ];

  $scope.namePatternTypes = [
    {value: SUBSTR, displayAs: 'contains'},
    {value: GLOB,   displayAs: 'matches (may use * for wildcard functions)'}
    //{value: REGEX,  displayAs: 'matches regex'}
  ];


  /* Initialize variable scope items.

     If there's a criterion object reachable from our scope (which there should be when used inside the
     criterion directive), then initialize the (non-constant) scope items from the criterion object.
   */
  console.log('ProductCriterionCtrl.mainView=' + $scope.mainView);
  console.log('ProductCriterionCtrl.ldtView=' + $scope.ldtView);

  if (!$scope.criterion.nameType)
  {
    $scope.criterion.nameType = ANY;
  }

  if (!$scope.criterion.namePatternType)
  {
    $scope.criterion.namePatternType = SUBSTR;
  }

  if (!$scope.criterion.namePattern)
  {
    $scope.criterion.namePattern = '';
  }


  /* functions */


  $scope.productNamesMatching = function(nameFragmentsString) {
    var serviceUrl = UrlBuilder.productNamesContaining(nameFragmentsString);
    if($scope.ldtView) {
      serviceUrl = UrlBuilder.productNamesContainingLDT(nameFragmentsString);
    }

    var params = {
      t: $scope.criterion.nameType,
      l: typeaheadResultsLimit
    };

    return $http.get(serviceUrl, {params: params}).
        then(function(response) { return response.data; });
  };


  $scope.namePatternInputPlaceholder = function(patternType) {
    if (patternType === SUBSTR)
    {
      return 'Enter any part(s) of product name';
    }
    else if (patternType === GLOB)
    {
      return 'Example: Allegra* (finds “Allegra”, “Allegra-D”, etc.)';
    }
    else if (patternType === REGEX)
    {
      return 'Enter regex for product name';
    }
    else
    {
      return undefined;
    }
  };

  $scope.removeAll = function() {
    $scope.criterion.namePattern = '';
    $scope.criterion.nameType = ANY;
    $scope.criterion.namePatternType = SUBSTR;
  };

});
