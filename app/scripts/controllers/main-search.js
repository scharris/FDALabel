'use strict';

angular.module('FDALabelApp').controller('MainSearchCtrl', function($scope, $routeParams, $http, $location, $window, $log, $anchorScroll,
                                                                    _, UrlBuilder, InterViewData, RecentQueries) {
/*
  var allCriteriaSourceEntities = [
    'spl-text',
    'product',
    'section',
    'document-type',
    'pharm-class',
    'marketing-category',
    'market-status',
    'meddra',
    'chemical-structure',
    //'dosage-form',
    //'route-of-admin',
    'identifier'
  ];
*/
  var mainCriteriaSourceEntities = [
    'spl-text',
    'product',
    'section',
    'document-type',
    'pharm-class',
    'marketing-category',
    'market-status',
    'meddra',
    'chemical-structure',
    //'dosage-form',
    //'route-of-admin',
    'identifier'
  ];

  var ldtCriteriaSourceEntities = [
    'marketing-category',
    'spl-text',
    'identifier',
    'section',
    'document-type',
    'market-status',
    'pharm-class',
    'product'
  ];

  var defaultQueryPanels;

  var defaultMainQueryPanels = [
    'document-type',
    'marketing-category',
    'product',
    'spl-text',
    'section',
    'pharm-class',
    'identifier'
  ];

  var defaultLDTQueryPanels = [
    'document-type',
    'marketing-category',
    'spl-text',
    'section',
    'product',
    'pharm-class',
    'identifier'
  ];


  console.log('1) abs location=' + $location.absUrl());
  console.log('2) location=' + $location.url());
  console.log('3) $window.subApp=' + $window.subApp);

  if($window.subApp === 'ldt') {
    $scope.mainView = false;
    $scope.ldtView = true;
    defaultQueryPanels = defaultLDTQueryPanels;
  }
  else {
    $scope.mainView = true;
    $scope.ldtView = false;
    defaultQueryPanels = defaultMainQueryPanels;
  }

/*
  if($location.url().indexOf('ldt') !== -1) {
    $scope.mainView = false;
    $scope.ldtView = true;
    $window.subApp = 'ldt';
    defaultQueryPanels = defaultLDTQueryPanels;
  }
  else {
    $scope.mainView = true;
    $scope.ldtView = false;
    $window.subApp = 'main';
    defaultQueryPanels = defaultMainQueryPanels;
  }
*/
  console.log('4) $window.subApp=' + $window.subApp);

  function defaultQuery() {
    console.log('$scope.mainView=' + $scope.mainView);
    console.log('$scope.ldtView=' + $scope.ldtView);
    return {
      target: 'spl-summaries',
      criterion: {
        criteria: [
          {
            criteria: _.map(defaultQueryPanels, function(panelName) {return {sourceEntity: panelName};}),
            logOps: _.map(defaultQueryPanels, function() {return 'a';})
          }
        ],
        logOps: []
      }
    };
  }

  // Set the query.

  // If the query (criterion and target) are specified in the url then these take precedence.
  if ($routeParams.criterionId)
  {
    var criterionId = parseInt($routeParams.criterionId, 10);
    if (criterionId)
    {
      $scope.editingDisabled = true;

      var target = $routeParams.target ? $routeParams.target: 'spl-summaries';

      $http.get(UrlBuilder.criterion(criterionId)).
        success(function(criterion) {
          $scope.query = {target: target, criterion: criterion, criterionId: criterionId};
        }).
        error(function() {
          $log.log('Error accessing criterion by id, returning home.');
          //$location.path(UrlBuilder.homeSearchView()); //Back to homepage?
        });
    }
    else
    {
      $log.log('Could not parse criterion id, returning home.');
      //$location.path(UrlBuilder.homeSearchView()); //Back to homepage?
    }
  }
  else if (RecentQueries.getDisplayOpt() === 'last' && RecentQueries.last()) // No query specified in the url, use the last query if any.
  {
    $scope.query = RecentQueries.last();
  }
  else // default query
  {
    $scope.query = defaultQuery();
  }

  $scope.messages = [];

  if($scope.ldtView === true) {
    $scope.addableEntityCriterionTypes = ldtCriteriaSourceEntities;
  } else {
    $scope.addableEntityCriterionTypes = mainCriteriaSourceEntities;
  }

  // callbacks

  $scope.doSearch = function() {

    RecentQueries.add($scope.query);

/*
    if($scope.ldtView === true) {
      $window.subApp = 'ldt';
    } else {
      $window.subApp = 'main';
    }
*/
    if ($scope.query.target === 'spl-summaries')
    {
      var serviceUrl = UrlBuilder.splSummariesForPostedCriterion();
      var queryCriterionJson = angular.toJson($scope.query.criterion);
      //console.log('queryCriterionJson=');
      //console.dir(queryCriterionJson);
      console.log('queryCriterionJson==' + JSON.stringify(queryCriterionJson));
      $scope.resultsLoading = true;

      $http.post(serviceUrl, queryCriterionJson).
        success(function(data) {
          $scope.resultsLoading = false;

          // Allow identifying the query in RecentQueries by setting its criterion id field now that we have its value.
          // This is not critical but allows preventing unnecessary duplication in the history list.
          $scope.query.criterionId = data.criterionId;

          InterViewData.set({
            criterionId:       data.criterionId,
            resultsArray:      data.resultsArray,
            resultsLimit:      data.resultsLimit,
            totalResultsCount: data.totalResultsCount,
            searchTerms:       data.searchTerms
          });

          var viewUrl = UrlBuilder.splSummariesView(data.criterionId);
/*
          if($scope.ldtView === true) {
            viewUrl = UrlBuilder.ldtSplSummariesView(data.criterionId);
          }
*/
          //window.alert('mainSearchView viewUrl =' + viewUrl);
          $location.path(viewUrl);
        }).
        error(function(msg) {
          $scope.resultsLoading = false;
          $scope.messages.push({type: 'error', text: msg});
          // Scroll to the top so the messages are visible.
          $location.hash('');
          $anchorScroll();
        });
    }
  };

  $scope.$on('main', function(event, data) {
      console.log('main->'+data);
      $scope.doSearch();
    }
  );

  $scope.editQuery = function() {
    RecentQueries.addUnlessSameAsLast($scope.query);
    $location.path(UrlBuilder.mainSearchView());
  };

  $scope.clearQueryCriteria = function() {
    function withClearedCriteria(crit) {
      if (crit.sourceEntity) {
        return { sourceEntity: crit.sourceEntity };
      } else {
        return {
          criteria: crit.criteria.map(withClearedCriteria),
          logOps: crit.logOps.slice(0)
        };
      }
    }

    $scope.query.criterion = withClearedCriteria($scope.query.criterion);
  };

  $scope.revertToLastQueryCriteria = function() {
    var last = RecentQueries.last();
    if (last) {
      $scope.query = last;
      RecentQueries.setDisplayOpt('last');
    }
  };

  $scope.removeMessage = function(msgIx) {
    $scope.messages.splice(msgIx, 1);
  };

});
