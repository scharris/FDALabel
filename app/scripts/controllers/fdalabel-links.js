'use strict';

angular.module('FDALabelApp').controller('FDALabelLinksCtrl', function($scope, $http, $location, UrlBuilder) {

  $scope.goFDALabelLinks = function() {
    $location.path(UrlBuilder.fdalabelLinksView());
  };

});
