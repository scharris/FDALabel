'use strict';

angular.module('FDALabelApp').controller('MarketStatusCriterionCtrl', function($scope) {

  $scope.statusChoices = ['', 'active', 'completed'];

  $scope.dateFormat = 'yyyy/MM/dd';

  /* Initialize variable scope items.

     If there's a criterion object reachable from our scope, which there should be when used inside the
     criterion directive, then initialize the non-constant scope items from the criterion object.
   */
  if (!$scope.criterion.status)
  {
    $scope.criterion.status = '';
  }
  if (!$scope.criterion.startMin)
  {
    $scope.criterion.startMin = '';
  }
  if (!$scope.criterion.startMax)
  {
    $scope.criterion.startMax = '';
  }
  if (!$scope.criterion.endMin)
  {
    $scope.criterion.endMin = '';
  }
  if (!$scope.criterion.endMax)
  {
    $scope.criterion.endMax = '';
  }

  $scope.removeAll = function() {
    $scope.criterion.status = '';
    $scope.criterion.startMin = '';
    $scope.criterion.startMax = '';
    $scope.criterion.endMin = '';
    $scope.criterion.endMax = '';
  };

  // When 'active' status is selected, clear the end date bounds, since active records never have end dates.
  $scope.$watch('criterion.status', function() {
    if ($scope.criterion.status === 'active')
    {
      $scope.criterion.endMin = '';
      $scope.criterion.endMax = '';
    }
  });

});
