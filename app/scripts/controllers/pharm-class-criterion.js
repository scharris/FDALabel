'use strict';

angular.module('FDALabelApp').controller('PharmClassCriterionCtrl', function($scope, $http, _, UrlBuilder) {

  // If necessary, initialize any missing fields in our criterion object, which object should be reachable in our scope.
  if (!$scope.criterion.pharmClasses)
  {
    $scope.criterion.pharmClasses = [];
  }

  // ephemeral scope items
  $scope.enteredPharmClass = null;
  $scope.typeaheadPharmClassType = 'EPC';
  $scope.typeaheadPharmClassTypes = [
    {value: 'MoA',  displayAs: 'Mechanisms of Action [MoA]'},
    {value: 'PE',   displayAs: 'Physiologic Effects [PE]'},
    {value: 'CI',   displayAs: 'Chemical Ingredient by Structure [CI]'},
    {value: 'EPC',  displayAs: 'Established Pharmacologic Class [EPC]'},
    {value: 'ANY',  displayAs: 'Any'}
  ];


  $scope.pharmClassesMatching = function(nameFragmentsString) {
    var params = {t: $scope.typeaheadPharmClassType,
                  l: 100 }; // typeahead results limit

    var serviceUrl = UrlBuilder.pharmClassesContaining(nameFragmentsString);

    return $http.get(serviceUrl, {params: params}).
                 then(function(response) { return response.data; });
  };


  function addPharmClass(classInfo) {
    if (!_.findWhere($scope.criterion.pharmClasses, {className: classInfo.className}))
    {
      $scope.criterion.pharmClasses.push({className: classInfo.className, classType: classInfo.classType});
    }
  }

  /* When the enteredPharmClass model value changes to some value, add the pharm class.
     Only complete valid terms from the typeahead suggestions can make it into enteredPharmClass,
     because of the typeahead-editable=false attribute on the input element.
   */
  $scope.$watch('enteredPharmClass', function() {
    if ($scope.enteredPharmClass)
    {
      addPharmClass($scope.enteredPharmClass);
      $scope.enteredPharmClass = null;
    }
  });

  $scope.removePharmClassAt = function(index) {
    $scope.criterion.pharmClasses.splice(index, 1);
  };

  $scope.removeAllPharmClasses= function() {
    $scope.criterion.pharmClasses.length = 0;
  };

  $scope.pharmClassActions = function(classInfo) {
    console.log(classInfo);
    return null;
   /* return [
      { text: '',
        action: function() { } }
    ];*/
  };

});
