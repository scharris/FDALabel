'use strict';

angular.module('FDALabelApp').controller('RouteOfAdminCriterionCtrl', function($scope, $log, _, RoutesOfAdmin) {

  $scope.routeOfAdminChoices = [];

  RoutesOfAdmin.routesOfAdminPromise.success(function(data) {
    $scope.routeOfAdminChoices = [{code: '', displayName: ''}].concat(data.routesOfAdmin);
  }).error(function() {
    $log.error('Could not load routes of administration, promise failed.');
  });

  // Holds selection state of the complete routes select list.
  $scope.selectedOtherRouteOfAdminCode = null;

  $scope.routeOfAdminDisplayName = function(code) {
    var info = _.findWhere($scope.routeOfAdminChoices, {code: code});
    return info ? info.displayName : '?';
  };

  $scope.addRouteOfAdmin = function(code) {
    if ($scope.criterion.routeOfAdminCodes.indexOf(code) === -1) {
      $scope.criterion.routeOfAdminCodes.push(code);
    }
  };

  $scope.removeRouteOfAdminAt = function(ix) {
    $scope.criterion.routeOfAdminCodes.splice(ix, 1);
  };

  $scope.removeAll = function() {
    $scope.criterion.routeOfAdminCodes.length = 0;
  };

  // When a route is selected via the select list, add it and un-select it again in the select list.
  $scope.$watch('selectedOtherRouteOfAdminCode', function() {
    if ($scope.selectedOtherRouteOfAdminCode)
    {
      $scope.addRouteOfAdmin($scope.selectedOtherRouteOfAdminCode);
      $scope.selectedOtherRouteOfAdminCode = null;
    }
  });


  /* Initialize criterion fields.

   If there's a criterion object reachable from our scope (which there should be when used inside the
   criterion directive), then initialize the (non-constant) scope items from the criterion object.
   */

  if (!$scope.criterion.routeOfAdminCodes)
  {
    $scope.criterion.routeOfAdminCodes = [];
  }

});
