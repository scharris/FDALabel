'use strict';

angular.module('FDALabelApp').controller('MarketingCategoryCriterionCtrl', function($scope, $log, _, MarketingCategoriesMain, MarketingCategoriesLDT) {

  $scope.marketingCategoryChoices = [];

  console.log('DocumentTypeCriterionCtrl.mainView=' + $scope.mainView);
  console.log('DocumentTypeCriterionCtrl.ldtView=' + $scope.ldtView);

  if($scope.ldtView) {
    MarketingCategoriesLDT.marketingCategoriesPromise.success(function(data) {
      $scope.marketingCategoryChoices = [{code: '', displayName: '', count: ''}].concat(data.marketingCategoriesLDT);
    }).error(function() {
      $log.error('Could not load marketing categories, promise failed.');
    });
  }
  else {
    MarketingCategoriesMain.marketingCategoriesPromise.success(function(data) {
      $scope.marketingCategoryChoices = [{code: '', displayName: '', count: ''}].concat(data.marketingCategoriesMain);
    }).error(function() {
      $log.error('Could not load marketing categories, promise failed.');
    });
  }

  $scope.displayMaketType = function(type) {
    if (type.count > 0 )
    {
      return type.displayName+' ('+type.count+' labeling)';
    }
    return ' ';
  };

  // Holds selection state of the "other marketing category type" select list.
  $scope.selectedOtherMarketingCategoryCode = null;

  $scope.marketingCategoryDisplayName = function(code) {
    var catInfo = _.findWhere($scope.marketingCategoryChoices, {code: code});
    return catInfo ? catInfo.displayName : '?';
  };

  $scope.addMarketingCategory = function(code) {
    if ($scope.criterion.marketingCategoryCodes.indexOf(code) === -1) {
      $scope.criterion.marketingCategoryCodes.push(code);
    }
  };

  $scope.removeMarketingCategoryAt = function(ix) {
    $scope.criterion.marketingCategoryCodes.splice(ix, 1);
  };

  $scope.removeAll = function() {
    $scope.criterion.marketingCategoryCodes.length = 0;
    $scope.criterion.repacker = false;
    $scope.criterion.authorizedGeneric = false;
    $scope.criterion.productConcept = false;
  };

  // When a marketing category is selected via the select list, add it and un-select it again in the select list.
  $scope.$watch('selectedOtherMarketingCategoryCode', function() {
    if ($scope.selectedOtherMarketingCategoryCode)
    {
      $scope.addMarketingCategory($scope.selectedOtherMarketingCategoryCode);
      $scope.selectedOtherMarketingCategoryCode = null;
    }
  });

  // Added by JY for tooltip text.
  $scope.labelingCountByCategory = function(code) {
    var category;
    var nCategories = $scope.marketingCategoryChoices.length;
    for (var i = 0; i < nCategories; i++) {
      category = $scope.marketingCategoryChoices[i];
      if (category.code === code) {
        return category.count + ' labeling';
      }
    }
    return '0 labeling';
  };

  /* Initialize criterion fields.

   If there's a criterion object reachable from our scope (which there should be when used inside the
   criterion directive), then initialize the (non-constant) scope items from the criterion object.
   */

  if (!$scope.criterion.marketingCategoryCodes)
  {
    $scope.criterion.marketingCategoryCodes = [];
  }
  if (!$scope.criterion.repacker)
  {
    $scope.criterion.repacker = false;
  }
  if (!$scope.criterion.authorizedGeneric)
  {
    $scope.criterion.authorizedGeneric = false;
  }
  if (!$scope.criterion.productConcept)
  {
    $scope.criterion.productConcept = false;
  }

});
