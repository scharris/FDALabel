'use strict';
var packName = '/fdalabel';
window.subApp = 'redirect';
console.log('window.location.href=' + window.location.href + ', window.subApp=' + window.subApp);

// All service and view url's should be built by functions in this object.
var UrlBuilder = {

  // services
  meddraTermsContaining: function(termFragmentsString) {
    return packName+'/services/meddra/containing/' + encodeURIComponent(termFragmentsString);
  },
  meddraLowLevelTermsHavingPreferredTerm: function(preferredTerm) {
    return packName+'/services/meddra/pt/' + encodeURIComponent(preferredTerm) + '/llts';
  },
  meddraLowLevelTermsRelatedToLowLevelTerm: function(lowLevelTerm) {
    return packName+'/services/meddra/llt/' + encodeURIComponent(lowLevelTerm) + '/related-llts';
  },

  pharmClassesContaining: function(termFragmentsString) {
    return packName+'/services/pharm-classes/containing/' + encodeURIComponent(termFragmentsString);
  },

  productNamesContaining: function(nameFragmentsString) {
    return packName+'/services/product/names-containing/' + encodeURIComponent(nameFragmentsString);
  },

  productNamesContainingLDT: function(nameFragmentsString) {
    return packName+'/services/product/ldt/names-containing/' + encodeURIComponent(nameFragmentsString);
  },

  splSummariesForPostedCriterion: function() {
    return packName+'/services/spl/summaries';
  },

  splSummariesForCriterionId: function(criterionId) {
    return packName+'/services/spl/summaries/json/criteria/'+criterionId;
  },

  splSummariesCSVForIdentifiedSearch: function(criterionId) {
    return packName+'/services/spl/summaries/csv/criteria/'+criterionId;
  },

  ldtSplSummariesCSVForIdentifiedSearch: function(criterionId) {
    return packName+'/services/spl/ldt/summaries/csv/criteria/'+criterionId;
  },

  splsCountForCriterionId: function(subApp, criterionId) {
    return packName+'/services/spl/count/criteria/'+criterionId;
  },

  criterion: function(criterionId) {
    return packName+'/services/query-store/criteria/'+criterionId;
  },

  // Spl document served from application services, optionally with strings matching highlightStringsRegex highlighted.
  splDocumentForSetId: function(setId, highlightStringsRegex) {
    return packName+'/services/spl/set-ids/' + setId + '/spl-doc' +
        (highlightStringsRegex ? '?hl=' + encodeURIComponent(highlightStringsRegex) : '');
  },

  sectionTypes: function() {
    return packName+'/services/spl/section-types';
  },

  sectionTypesTree: function() {
    return packName+'/services/spl/section-types-tree';
  },

  sectionTypesTreeLDT: function() {
    return packName+'/services/spl/section-types-tree-ldt';
  },

  documentTypes: function() {
    return packName+'/services/spl/document-types';
  },

  documentTypesMain: function() {
    return packName+'/services/spl/document-types-main';
  },

  documentTypesLDT: function() {
    return packName+'/services/spl/document-types-ldt';
  },

  marketingCategoriesMain: function() {
    return packName+'/services/spl/marketing-categories-main';
  },

  marketingCategoriesLDT: function() {
    return packName+'/services/spl/marketing-categories-ldt';
  },

  routesOfAdmin: function() {
    return packName+'/services/spl/routes-of-admin';
  },

  dosageForms: function() {
    return packName+'/services/spl/dosage-forms';
  },

  dataLoaderRecords: function() {
    return packName+'/services/spl/data-loader-records';
  },

  // views

  mainSearchView: function() {
    console.log('mainSearchView window.subApp =' + window.subApp);
    //window.alert('mainSearchView window.subApp =' + window.subApp);
    return '/search';
  },

  ldtSearchView: function() {
    console.log('ldtSearchView window.subApp =' + window.subApp);
    //window.alert('ldtSearchView window.subApp =' + window.subApp);
    return '/ldt/search';
  }
};

angular.module('appServices', ['ngResource']);

angular.module('FDALabelApp', ['ngRoute', 'ngSanitize', 'ui.bootstrap', 'appServices' ])
  .constant('UrlBuilder', UrlBuilder)
  .config(function ($routeProvider) {
    $routeProvider.
      when(UrlBuilder.mainSearchView(), {
        templateUrl: 'views/redirect.html'
      }).
      when(UrlBuilder.ldtSearchView(), {
        templateUrl: 'views/redirect.html'
      }).
      otherwise({
        templateUrl: 'views/redirect.html'
      });
  })
  .controller('AppHomeCtrl', ['$scope','$location', function ($scope,$location) {
    $scope.getAboutURL = function () {
      if($location.url().indexOf('ldt') !== -1) {
        return 'http://192.168.221.128:8080/fdalabel_redirect/ui/#/ldt/search';
      } else {
        return 'http://192.168.221.128:8080/fdalabel_redirect/ui/#/search';
      }
    };
  }]);

