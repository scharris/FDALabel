'use strict';

angular.module('FDALabelApp').controller('SplSummariesCtrl', function($scope, $routeParams, $http, $location, $window, InterViewData, UrlBuilder) {

  console.log('SplSummariesCtrl $location=' + $location.url());
  // Get criterion id, balk if no (integer-parsable) criterion id is specified as a route parameter.

  var criterionId = parseInt($routeParams.criterionId, 10);

  if($window.subApp === 'ldt') {
    $scope.mainView = false;
    $scope.ldtView = true;
  }
  else {
    $scope.mainView = true;
    $scope.ldtView = false;
  }

  if (!criterionId)
  {
    console.log('No valid criterion id found in url route parameters, returning home.');
    $location.path(UrlBuilder.mainSearchView());
    return;
  }

  console.log('SplSummariesCtrl.subApp=' + $window.subApp);

  // Initialize original scope data.
  $scope.summarySortable = false;

  var interViewData = InterViewData.take();
  //$window.alert('interViewData=' + interViewData);
  if (interViewData)
  {
    if (interViewData.criterionId !== criterionId) // sanity check
    {
      // Pre-fetched data doesn't match what is specified in the url (probably a bug).
      console.log('Inter-view data criterion id or query target name does not match expected value, returning home.');
      $location.path(UrlBuilder.mainSearchView());
    }

    $scope.criterionId = interViewData.criterionId;
    $scope.splSummaries = interViewData.resultsArray;
    $scope.resultsLimit = interViewData.resultsLimit;
    $scope.totalResultsCount = interViewData.totalResultsCount;
    $scope.combinedSearchTerms= interViewData.searchTerms.join('|');
    $scope.displaySplSummaries = [].concat($scope.splSummaries);
    $scope.displaySplSummariesExp = [].concat($scope.splSummaries);


    if ($scope.splSummaries !== null && $scope.splSummaries !== undefined) {
      if ($scope.resultsLimit > $scope.splSummaries.length) {
        $scope.summarySortable = true;
      }
    }
  }
  else
  { // Results are not being passed from the previous view, we need to fetch the results by id from the server.
    $scope.resultsLoading = true;
    $http.get(UrlBuilder.splSummariesForCriterionId(criterionId)).
      success(function(returnedData) {
        $scope.criterionId = returnedData.criterionId;
        $scope.splSummaries = returnedData.resultsArray;
        $scope.resultsLimit = returnedData.resultsLimit;
        $scope.totalResultsCount = returnedData.totalResultsCount;
        $scope.combinedSearchTerms= returnedData.searchTerms.join('|');
        $scope.displaySplSummaries = [].concat($scope.splSummaries);
        $scope.displaySplSummariesExp = [].concat($scope.splSummaries);


        if ($scope.splSummaries !== null && $scope.splSummaries !== undefined) {
          if ($scope.resultsLimit > $scope.splSummaries.length) {
            $scope.summarySortable = true;
          }
        }

        $scope.resultsLoading = false;
      }).
      error(function() {
        console.log('Error accessing spl summaries by id, returning home.');
        $scope.resultsLoading = false;
        $location.path(UrlBuilder.mainSearchView());
      });
  }

  $scope.resultsHeaderText = function() {
    if ($scope.splSummaries !== null && $scope.splSummaries !== undefined) {
//      return ($scope.totalResultsCount !== null ? $scope.totalResultsCount+' ' : '') + 'Labeling Results' +
//             ($scope.resultsLimit === $scope.splSummaries.length ? ' (only ' + $scope.resultsLimit + ' labeling displayed, click on "Download Full Results" to access all)' : '');
      return ($scope.totalResultsCount !== null ? $scope.totalResultsCount : '') + ' labeling results' +
      ($scope.resultsLimit === $scope.splSummaries.length ? ' (only ' + $scope.resultsLimit + ' listed, sorting not available for >2000 results, click "Download Full Results" to access all)' : '');
    }
    else {
      return 'Loading...';
    }
  };

  $scope.dateToDisplay = function(splEffDate) {
    var strDt = splEffDate.trim();
    if(strDt.length > 7) {
      strDt = strDt.substring(0,4).concat('/', strDt.substring(4,6), '/', strDt.substring(6,8));
    }
    return strDt;
  };

  $scope.countResults = function() {
    $http.get(UrlBuilder.splsCountForCriterionId(criterionId)).
      success(function(returnedData) {
        $scope.totalResultsCount = returnedData.totalResultsCount;
      }).
      error(function() {
        console.log('Error counting spl summaries.');
      });
  };

  $scope.searchUrl = UrlBuilder.identifiedSearchView('spl-summaries', criterionId);
  $scope.downloadUrl = UrlBuilder.splSummariesCSVForIdentifiedSearch(criterionId);

  $scope.defaultView = true;

  // functions / callbacks
  $scope.humanDrug = function(mktcat) {
    if(mktcat==='ANADA' || mktcat==='NADA'){
      return false;
    }else{
      return true;
    }
  };

  // Spl Urls
  $scope.splDocumentUrlForSetId = function(setId) {
    return UrlBuilder.splDocumentForSetId(setId, $scope.combinedSearchTerms);
  };
  $scope.dailymedSplHtmlUrlForSetId = function(setId) {
    return 'http://dailymed.nlm.nih.gov/dailymed/lookup.cfm?setid='+setId;
  };
  $scope.dailymedSplPdfUrlForSetId = function(setId) {
    return 'http://dailymed.nlm.nih.gov/dailymed/downloadpdffile.cfm?setId='+setId;
  };
  $scope.dailymedSplZipUrlForSetId = function(setId) {
    return 'http://dailymed.nlm.nih.gov/dailymed/downloadzipfile.cfm?setId='+setId;
  };
  // Orange Book Urls
  // Modified by JY. 
  // Removed OrangeBook link for BLAs. 
  // Also updated OrangeBook link URL, added new parameter Appl_Type derived from Marketing Categories.
  $scope.orangeBookUrl = function(applType, applNo) {
    var urls=[];
    //console.log('input:'+applNo);
    if (applType !== null && applType.indexOf('BLA') === -1 && applNo !== null && applNo.trim().length>5) {
      var appls=applNo.split(';');
      //console.log('appls:'+appls);
      for (var i=0;i<appls.length;i++) {
        urls.push(appls[i]+'$$http://www.accessdata.fda.gov/scripts/cder/ob/results_product.cfm?Appl_Type=' + applType.charAt(0) + '&Appl_No=' + appls[i]);
      }
    }
    $scope.orangeBookUrls=urls;
    //console.log('output'+urls);
    return urls;
  };
  // AninmalDrugs@FDA Urls
  $scope.AninmalDrugsatfdaUrl = function(applNo) {
    var urls=[];
    //console.log('input:'+applNo);
    if (applNo !== null && applNo.trim().length>5) {
      var appls=applNo.split(';');
      //console.log('appls:'+appls);
      for (var i=0;i<appls.length;i++) {
        urls.push(appls[i]+'$$https://animaldrugsatfda.fda.gov/adafda/views/#/home/previewsearch/'+appls[i]);
      }
    }
    $scope.orangeBookUrls=urls;
    //console.log('output'+urls);
    return urls;
  };
  // Drugs@FDA Urls
  // Modified by JY.  
  // Update the link to Drugs@FDA by passing only Application Number applNo.
  // However, FDA update it's link again and this version will available only before Nov 18 2016
  // The new URL is:http://www.accessdata.fda.gov/scripts/cder/daf/index.cfm?event=overview.process&varApplNo=125291
  // So I change the URL to the latest one.
  $scope.drugsatFDAUrl = function(applNo) {
    var urls=[];
    //console.log('input:'+applNo);
    if (applNo !== null && applNo.trim().length>5) {
      var appls=applNo.split(';');
      //console.log('appls:'+appls);
      for (var i=0;i<appls.length;i++) {
        urls.push(appls[i]+'$$http://www.accessdata.fda.gov/scripts/cder/daf/index.cfm?event=overview.process&varApplNo='+appls[i]);
      }
    }
    $scope.drugsatFDAUrls=urls;
    //console.log('output'+urls);
    return urls;
  };
})
.directive('pageSelect', function() {
  return {
    restrict: 'E',
    template: '<input type="text" class="select-page" ng-model="inputPage" ng-change="selectPage(inputPage)">',
    link: function(scope) {
      scope.$watch('currentPage', function(c) {
        scope.inputPage = c;
      });
    }
  };
})
.directive('stRatio', function() {
  return {
    link:function(scope, element, attr) {
      var ratio=+(attr.stRatio);
      element.css('width',ratio+'%');
    }
  };
});
