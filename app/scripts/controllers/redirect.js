'use strict';

angular.module('FDALabelApp').controller('RedirectCtrl', function($scope, $http, $location, $window) {

  console.log('RedirectCtrl $location=' + $location.url());
  console.log('RedirectCtrl window.subApp=' + window.subApp + ',$window.subApp=' + $window.subApp);


  $scope.go2FDA = function () {
    $window.location.href = 'http://ncsvmlabelapp.fda.gov/fdalabel/ui/#/search';//'http://ncsvmweb01.fda.gov/fda/ui/';
  };

  $scope.go2LDT = function () {
    $window.location.href = 'http://ncsvmlabelapp.fda.gov/fdalabel/ui/#/ldt/search';//'http://ncsvmweb01.fda.gov/cder/ui/';
  };

  if($location.url().indexOf('ldt') !== -1) {
    $scope.mainView = false;
    $scope.ldtView = true;
    //$scope.go2LDT();
    //$location.path(UrlBuilder.ldtDatabaseUpdatesView());
    //$window.location.href = 'http://localhost:8080/fdalabel_testing/ui/#/ldt/search';
  }
  /*else if ($location.url().indexOf('ldt') !== -1) {
    $scope.mainView = false;
    $scope.ldtView = true;
    $window.subApp = 'ldt';
    $location.path(UrlBuilder.ldtDatabaseUpdatesView());
  } 
  */
  else {
    $scope.mainView = true;
    $scope.ldtView = false;
    //$scope.go2FDA();
    //$window.location.href = 'http://ncsvmweb01.fda.gov/fdalabel/ui/#/search';
    //$location.path(UrlBuilder.mainDatabaseUpdatesView());
  }

  $scope.$on('$routeChangeSuccess', function(event, current, previous) {
    console.log('routeChangeSuccess $location=' + $location.url());
    //console.log('routeChangeSuccess $rootScope=' + $rootScope);
    console.log('routeChangeSuccess current=' + angular.toJson(current));
    console.log('routeChangeSuccess previous=' + angular.toJson(previous));
  });

});
