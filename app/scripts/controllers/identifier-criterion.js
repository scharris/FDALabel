'use strict';

angular.module('FDALabelApp').controller('IdentifierCriterionCtrl', function($scope) {

  /* VAR values the number in Java */
  var ACT = 'ACT';
  var NACT = 'NACT';
  var ALL = 'ALL';

  /* Initialize variable scope items.

     If there's a criterion object reachable from our scope, which there should be when used inside the
     criterion directive, then initialize the non-constant scope items from the criterion object.
   */
  console.log('IdentifierCriterionCtrl.mainView=' + $scope.mainView);
  console.log('IdentifierCriterionCtrl.ldtView=' + $scope.ldtView);

  $scope.ingredientTypes = [
    {value: ACT,   displayAs: 'Active'},
    {value: NACT,  displayAs: 'Inactive'},
    {value: ALL,   displayAs: 'Both'}
  ];

  if (!$scope.criterion.ingredientTypes)
  {
    $scope.criterion.ingredientTypes = ACT;
  }

  if (!$scope.criterion.identifiers)
  {
    $scope.criterion.identifiers = '';
  }

  $scope.removeAll = function() {
    $scope.criterion.identifiers = '';
  };

});
