'use strict';

angular.module('FDALabelApp').controller('DatabaseUpdatesCtrl',  function($scope, $http, $location, $window, UrlBuilder, $log) {

  $scope.labels = [];
  $scope.series = ['Added','Resurrected', 'Removed','Updated','Total'];
  $scope.data = [];

  console.log('DatabaseUpdatesCtrl $location=' + $location.url());
  console.log('DatabaseUpdatesCtrl $window.subApp=' + $window.subApp);

  if ($window.subApp === 'ldt') {
    $scope.mainView = false;
    $scope.ldtView = true;
    $scope.verFdalabel = '2.2';
  } else {
    $scope.mainView = true;
    $scope.ldtView = false;
    $scope.verFdalabel = '2.2';
  }

  $http.get(UrlBuilder.dataLoaderRecords()).success(function(data) {
/*
    var added=[];
    var removed=[];
    var updated=[],total=[];
    var res=[];
    var lab=[],row;
*/
    if ($scope.mainView) {
      $scope.recentFiveSplDataLoaderRecords = data.recentFiveSplDataLoaderRecords;//[{loaddate: '', added: '', resurrected: '', removed: '', updated: '', total: ''}].concat(data.recentFiveSplDataLoaderRecords);
    }
    else if ($scope.ldtView){
      $scope.recentFiveSplDataLoaderRecords = data.recentFiveSplDataLoaderRecordsLDT;//[{loaddate: '', total: ''}].concat(data.recentFiveSplDataLoaderRecordsLDT);
    }

    $scope.recentDictMaintanceRecords = [{maindate: '', contents: ''}].concat(data.recentDictMaintanceRecords);
/*

    for (var i=0;i< data.dataLoaderofTotalSPL.length;i++){
      row=data.dataLoaderofTotalSPL[i];
      lab.push(row.loaddate);
      added.push(row.added*10);
      removed.push(row.removed*10);
      updated.push(row.updated*10);
      total.push(row.total);
      res.push(row.resurrected*10);
    }
    $scope.labels =lab;
    $scope.data.push(added);
    $scope.data.push(res);
    $scope.data.push(removed);
    $scope.data.push(updated);
    $scope.data.push(total);
    $log.error('label:'+$scope.labels+'series:'+$scope.series ); */
  }).error(function() {
    $log.error('Could not load SPL loader information, promise failed.');
//    $log.error('label:'+$scope.labels+'series:'+$scope.series );
  });
/*
  $scope.goDatabaseUpdates = function() {
    $location.path(UrlBuilder.databaseUpdatesView());
  };
*/
});
