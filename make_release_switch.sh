#!/bin/sh

usage ()
{
  echo 'Usage: ./make_release_switch.sh [fda|cder] [release|testing|development]'
  echo 'For example:'
  echo './make_release_switch.sh cder development'
}

if [ "$#" -ne 2 ];
then
  usage
  exit
fi

set -x

if [ $1 = 'fda' ]
then
echo 'built for fdalabel.war'
echo $1
echo $2
cp -f Gruntfile.fda Gruntfile.js
cp -f make_release.fda make_release.sh
cp -f services/pom.fda services/pom.xml
sh make_release.sh $2
exit 0
fi

if [ $1 = 'cder' ]
then
echo 'built for fdalabel-r.war'
echo $1
echo $2
cp -f Gruntfile.cder Gruntfile.js
cp -f make_release.cder make_release.sh
cp -f services/pom.cder services/pom.xml
sh make_release.sh $2
fi

