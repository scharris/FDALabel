#!/bin/sh
set -e

# ./make-loaders-dist.sh ~/AppData/Local/Programs/elasticsearch ~/Downloads/meddra_20_1_english/MedAscii

die () {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 2 ] || die "expected arguments: <elasticsearch-dist-file-or-dir> <meddra-medascii-dir>"
ELASTICSEARCH_DIST="$1"
MEDDRA_MEDASCII_DIR="$2"

[ -d "$ELASTICSEARCH_DIST" ] || [ -f "$ELASTICSEARCH_DIST" ] || die "Elasticsearch dist file or directory not found."
[ -d "$MEDDRA_MEDASCII_DIR" ] || die "MedDRA MedAscii directory not found."
[ -f "$MEDDRA_MEDASCII_DIR/llt.asc" ] || die "MedDRA MedAscii directory does not contain file llt.asc."


SCRIPT_DIR="$(dirname "$(realpath "$0")")"
cd "$SCRIPT_DIR"

mvn clean package && (cd ../../labels-es-loader && mvn clean package)

mkdir -p target/loaders

tar xzvf target/fdalabel-loader-dist.tgz -C target/loaders
tar xzvf ../../labels-es-loader/target/labels-es-loader-dist.tgz -C target/loaders
mv target/loaders/labels-es-loader target/loaders/es-loader

LOADERS_HOME="target/loaders"
IO_DIR="$LOADERS_HOME/io"
mkdir "$IO_DIR"
mkdir "$IO_DIR/dailymed-data"
mkdir "$IO_DIR/meddra-release"
mkdir "$IO_DIR/logs"
mkdir "$IO_DIR/db-conn-props"
mkdir "$IO_DIR/es-loader-output"
mkdir "$IO_DIR/fdalabel-loader-storage"

cat <<EOF > "$IO_DIR/db-conn-props/example.jdbc.properties"
jdbc.driverClassName=oracle.jdbc.OracleDriver
jdbc.url=jdbc:oracle:thin:@host:port:sid
jdbc.username=druglabel
jdbc.password=whatever
EOF

if [ -d "$ELASTICSEARCH_DIST" ]; then
    cp -r "$ELASTICSEARCH_DIST" target/loaders/elasticsearch
else
    mkdir target/loaders/elasticsearch
    tar xvf "$ELASTICSEARCH_DIST" --strip-components=1 -C target/loaders/elasticsearch
fi

cp -r "$MEDDRA_MEDASCII_DIR" target/loaders/io/meddra-release/


cd target/
tar czvf loaders-dist.tgz loaders

echo
echo
echo "SUCCESS: Distribution written to target/loaders-dist.tgz."
echo
echo "Before running, provide one of more database connection files in directory"
echo "io/db-conn-props (example provided), with name of the form: "
echo "    <db-name>.jdbc.properties"
echo "DailyMed archives can be provided in io/dailymed-data, or else choose to "
echo "download archives via --download-dailymed option to the start script."
echo "Launch the loader via the start script:"
echo "    fdalabel-loader/load.sh dev"
echo "where 'dev' is a database name defined by properties file "
echo "    io/db-conn-props/dev.jdbc.properties."

