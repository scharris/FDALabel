#!/bin/sh
set -e

SCRIPT_DIR="$(dirname "$(realpath "$0")")"
cd "$SCRIPT_DIR"

[ -d dailymed-downloads ] || mkdir dailymed-downloads

java -jar fdalabel-loader.jar --download-archives dailymed-downloads
