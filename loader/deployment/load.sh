#!/bin/sh
set -e

die () {
    echo >&2 "$@"
    exit 1
}

if [ "$1" == "--download-dailymed" ]; then
  DOWNLOAD_DAILYMED=true
  shift
fi

[ "$#" -eq 1 ] || die "expected arguments: [--download-dailymed] <target-db-name>"
DB_TGT="$1"
echo "$(date): Starting load process for FDALabel database '$DB_TGT', using Elasticsearch load process first for MedDRA occurrences."

# Define environment variables configuring the location of various loader resources.
# In a Docker container these should be defined in the Docker build file.
if [ "$LOADERS_HOME" == "" ]; then
    # The LOADERS_HOME is the directory ABOVE this script's directory.
    # The fdalabel-loader and labels-es-loader directories should be siblings under LOADERS_HOME.
    export LOADERS_HOME="$(realpath "$(dirname "$(realpath "$0")")/..")"
    export IO_DIR="$LOADERS_HOME/io"
    export DAILYMED_DATA_DIR="$IO_DIR/dailymed-data"
    export MEDDRA_RELEASE_DIR="$IO_DIR/meddra-release"
    export LOGS_DIR="$IO_DIR/logs"
    export DB_CONN_PROPS_DIR="$IO_DIR/db-conn-props"
    export ES_LOADER_DIST_DIR="$LOADERS_HOME/es-loader"
    export ES_LOADER_JAR="$ES_LOADER_DIST_DIR/labels-es-loader.jar"
    export ES_INDEX_DEFS_DIR="$ES_LOADER_DIST_DIR/elasticsearch/index-defs"
    export ES_LOADER_OUTPUT_DIR="$IO_DIR/es-loader-output"
    export FDALABEL_LOADER_DIST_DIR="$LOADERS_HOME/fdalabel-loader"
    export FDALABEL_LOADER_JAR="$FDALABEL_LOADER_DIST_DIR/fdalabel-loader.jar"
    export FDALABEL_LOADER_STORAGE_DIR="$IO_DIR/fdalabel-loader-storage"
    export ELASTICSEARCH_HOME="$LOADERS_HOME/elasticsearch"
    export ELASTICSEARCH_START="$LOADERS_HOME/elasticsearch/bin/elasticsearch"
    export DAILYMED_FETCHER="$ES_LOADER_DIST_DIR/fetch-dailymed-files-java.sh"
    export JAVA_EXE="/usr/bin/java"
    # NOTE: ES_PATH_CONF is recognized by Elasticsearch, not just the application.
    export ES_PATH_CONF="$ES_LOADER_DIST_DIR/elasticsearch/config"
fi

# Start Elasticsearch and wait for it to be ready by watching its log file.
ELASTICSEARCH_LOG="$LOGS_DIR/elasticsearch.log"
echo "Starting Elasticsearch."
cd "$ELASTICSEARCH_HOME"
"$ELASTICSEARCH_START" > "$ELASTICSEARCH_LOG" &
echo "Waiting for Elasticsearch to be ready."
# See https://superuser.com/questions/270529/monitoring-a-file-until-a-string-is-found .
tail -f "$ELASTICSEARCH_LOG" | while read LOGLINE
do
   [[ "${LOGLINE}" == *"recovered "*" indices into cluster_state"* ]] && pkill -P $$ tail
done
echo "Verified that Elasticsearch is ready."

# Fetch label archives from DailyMed if indicated.
if [ "$DOWNLOAD_DAILYMED" == true  ]; then
  rm "$DAILYMED_DATA_DIR"/* || true
  "$DAILYMED_FETCHER" "$DAILYMED_DATA_DIR"
else
  DM_FILES_COUNT=$(/bin/ls -1U "$DAILYMED_DATA_DIR" | wc -l)
  if [ "$DM_FILES_COUNT" -lt 11 ]; then
    die "Expected at least 11 files in DailyMed directory, aborting loading process."
  fi
  echo "Loading from $DM_FILES_COUNT existing DailyMed archives in the DailyMed volume."
fi

# Clean es loader output directory.
: "${ES_LOADER_OUTPUT_DIR?Required env var ES_LOADER_OUTPUT_DIR is not defined.}"
rm -rf "$ES_LOADER_OUTPUT_DIR"/*


MEDDRA_LLT_OCCS_FILE="$ES_LOADER_OUTPUT_DIR/meddra_llt_spl_coded_sec.csv" # to be created below

# Index the label sections with Elasticsearch and generate term occurrences file for the FDALabel loader to use.
"$ES_LOADER_DIST_DIR"/load-sections-exporting-meddra-occs.sh "$MEDDRA_LLT_OCCS_FILE"

echo "Shutting down Elasticsearch."
kill $(jobs -p)

echo "Starting FDALabel loader."

"$FDALABEL_LOADER_DIST_DIR"/load-with-meddra-occs-file.sh "$DB_TGT" "$MEDDRA_LLT_OCCS_FILE"

echo "Loading process complete."
