#!/bin/sh
set -e

SCRIPT_DIR="$(dirname "$(realpath "$0")")"

die () {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 2 ] || die "expected arguments: <db short name (dev,test,etc)> <meddra-term-occurrences-file>"
DB_TGT="$1"
MEDDRA_LLT_OCCS_FILE="$2"
[ -f "$MEDDRA_LLT_OCCS_FILE" ] || die "Meddra term occurrences file not found."

# Check that required env vars are defined.
: "${DB_CONN_PROPS_DIR?Need to set DB_CONN_PROPS_DIR.}"
: "${DAILYMED_DATA_DIR?Need to set DAILYMED_DATA_DIR.}"
: "${FDALABEL_LOADER_STORAGE_DIR?Need to set FDALABEL_LOADER_STORAGE_DIR.}"
: "${LOGS_DIR?Need to set LOGS_DIR.}"
: "${JAVA_EXE?Need to set JAVA_EXE.}"

JDBC_PROPS_FILE="$DB_CONN_PROPS_DIR"/"$DB_TGT.jdbc.props"
[ -f "$JDBC_PROPS_FILE" ] ||
    die "No database connection properties file '$DB_TGT.jdbc.props' found in database connections directory."

# Write a custom log configuration for our database target based on the template config file.
LOG_CONF_DIR="$(mktemp -d)"
LOG4J2_CONF="$LOG_CONF_DIR/log4j2.xml"
LOG_CONF_TEMPLATE=$(cat "$SCRIPT_DIR/log4j2-template.xml")
tmp=$(echo ${LOG_CONF_TEMPLATE//||LOGS_DIR||/$LOGS_DIR})
echo ${tmp//||TARGET_DB_SHORT_NAME||/$DB_TGT} > $LOG4J2_CONF
echo "Wrote custom log config for this target database to $LOG4J2_CONF."

"$JAVA_EXE" -Dlog4j.configurationFile="$LOG4J2_CONF" -jar fdalabel-loader.jar \
    --import-terminologies \
    --max-removals 2000 \
    --max-failures 20 \
    --keep-removed \
    --meddra-occurrences-file "$MEDDRA_LLT_OCCS_FILE" \
    "$DAILYMED_DATA_DIR" \
    "$JDBC_PROPS_FILE" \
    "$FDALABEL_LOADER_STORAGE_DIR"

