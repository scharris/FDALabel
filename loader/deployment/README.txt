To setup a new environment such as "cert", "prod" etc., first create a
configuration directory for its configuration files at conf/<env-name>.
Two environment-specific files should be present in this directory:

1) log4j2.xml
Contains logging options specific to the environment.  Usually the environment
name (e.g. "dev") in the "filePattern" attribute should be changed to
distinguish log files by environment. Logs are written in a single
directory <LOADERDIST>/logs/ as configured in the example log4j2.xml here.

2) jdbc.properties
The environment-specific database connection information. Expected property
names are as follows:
#
jdbc.driverClassName=oracle.jdbc.OracleDriver
jdbc.url=jdbc:oracle:thin:@<fq-hostname>:<port>:<sid>
jdbc.username=X
jdbc.password=X

See dev/conf as an example.

Next, create a copy of the load-dev.bat/sh launch script with the required
environment name in place of "dev", e.g. "load-cert.bat" for a new "cert"
environment. The environment name will be derived from the launch script
name when the script is run. Customize import parameters in the launch file
as needed for the target environment.

