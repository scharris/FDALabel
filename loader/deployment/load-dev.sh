#!/bin/sh

SCRIPT_DIR="$(dirname "$(realpath "$0")")"
cd "$SCRIPT_DIR"

# If FDALABEL_ENV is unset, set it to the part of the script name after the first "-" (excluding file extension).
if [ -z "$FDALABEL_ENV" ]; then
    SCRIPT_NAME="$(basename "$0")"
    SCRIPT_NAME_WO_EXT="${SCRIPT_NAME%.*}"
    FDALABEL_ENV="${SCRIPT_NAME_WO_EXT#*-}"
fi

# [ -d loader-work-area ] && rm -rf loader-work-area

# Use Java within JAVA_HOME if defined, else expect java from PATH.
if [ -z "$JAVA_HOME" ]; then JAVA="java"; else JAVA="$JAVA_HOME"/bin/java; fi

"$JAVA" -Dlog4j.configurationFile="conf/$FDALABEL_ENV/log4j2.xml" -jar fdalabel-loader.jar \
    --max-removals 2000 \
    --max-failures 20 \
    --keep-removed \
    --import-terminologies \
    dailymed-downloads \
    "conf/$FDALABEL_ENV/jdbc.properties" \
    loader-work-area

# To see all available loader options:
#   java -jar fdalabel-loader.jar -h
