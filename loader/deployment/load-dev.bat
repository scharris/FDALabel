cd %~dp0

rem Set FDALABEL_ENV to the part of the script name (without extension) after the first "-".
set "SCRIPT_NAME=%~n0"
set "FDALABEL_ENV=%SCRIPT_NAME:*-=%"

:: if exist "loader-work-area" rmdir /s /q loader-work-area

java "-Dlog4j.configurationFile=conf\%FDALABEL_ENV%\log4j2.xml" -jar fdalabel-loader.jar ^
    --max-removals 2000 ^
    --max-failures 20 ^
    --keep-removed ^
    --import-terminologies ^
    --msxsl-exe fdalabel1-html-gen\msxsl.exe ^
    --stylesheet fdalabel1-html-gen\Stylesheet\spl.xsl ^
    dailymed-downloads ^
    "conf\%FDALABEL_ENV%\jdbc.properties" ^
    loader-work-area

