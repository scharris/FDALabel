<?xml version="1.0" encoding="us-ascii"?>
<!--
The contents of this file are subject to the Health Level-7 Public
License Version 1.0 (the "License"); you may not use this file
except in compliance with the License. You may obtain a copy of the
License at http://www.hl7.org/HPL/hpl.txt.

Software distributed under the License is distributed on an "AS IS"
basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
the License for the specific language governing rights and
limitations under the License.

The Original Code is all this file.

The Initial Developer of the Original Code is Gunther Schadow.
Portions created by Initial Developer are Copyright (C) 2002-2004
Health Level Seven, Inc. All Rights Reserved.

Contributor(s): Steven Gitterman, Brian Keller, Mingh Chen

Revision: $Id: spl-common.xsl,v 2.0 2006/08/18 04:11:00 sbsuggs Exp $

-->
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:v3="urn:hl7-org:v3">
   <xsl:import href="spl-common.xsl"/>	
   <xsl:template match="/v3:document[starts-with($style, 'Cder')]">
	 <html>
	    <head>
	       <title>
		  <xsl:value-of select="v3:title"/>
	       </title>
	       <link rel="stylesheet" type="text/css" href="{$css}"/>
	       <xsl:if test="boolean($show-subjects-xml)">
		  <xsl:call-template name="xml-verbatim-setup"/>
	       </xsl:if>
	    </head>
	    <body id="spl">
	       
	       <xsl:if test="$style = 'CderListingEntry' or $style = 'CderReview' or $style = 'CderListingRegistration'">		
		  <a target="_blank" href="../{v3:id/@root}/{v3:id/@root}.html?style={$style}2"><u>Print View</u></a>		
	       </xsl:if>	
	       <br/>


	       <xsl:if test="$style = 'CderListingEntry' or $style = 'CderListingEntry2'">
		  <xsl:for-each select="v3:component/v3:structuredBody/v3:component[v3:section/v3:code/@code = '51945-4']/descendant::v3:renderMultiMedia">							
		     <xsl:apply-templates mode="mixed" select="."/>
		     </xsl:for-each><br/>
		     <xsl:apply-templates mode="report1-subjects" select="."/>
		     <h1/>
	       </xsl:if>
	       <xsl:if test="$style = 'CderReview' or $style = 'CderReview2'">
		  <table width="100%"><tbody><tr><td>		
		     <xsl:apply-templates select="v3:component/v3:structuredBody/v3:component[v3:section/v3:code/@code = '51945-4']"/>
		  </td></tr></tbody></table>		
		  <xsl:apply-templates select="v3:component/v3:structuredBody/v3:component[v3:section/v3:code/@code = '34067-9']"/>
		  <br/>
		  <xsl:apply-templates mode="report2-subjects" select="."/>
		  <h1/>
	       </xsl:if>
	       <xsl:if test="$style = 'CderListingRegistration' or $style = 'CderListingRegistration2'">						
		  <xsl:if test="boolean($gSr4)">
		     <xsl:for-each select="//v3:author/v3:assignedEntity/v3:representedOrganization/v3:assignedEntity/v3:assignedOrganization/v3:assignedEntity/v3:assignedOrganization">							
			<h1 style="font-size: 150%;font-weight: normal;margin-top: 0pt;margin-bottom: 0pt;text-align: center;text-indent: 0em;">Establishment <xsl:value-of select="position()"/><a><xsl:attribute name="name">establishment<xsl:value-of select="position()"/></xsl:attribute></a></h1>
			
			<xsl:apply-templates mode="subjects" select="//v3:author/v3:assignedEntity/v3:representedOrganization/v3:assignedEntity/v3:assignedOrganization"/>	
			<xsl:apply-templates mode="subjects" select="."/>							
		     </xsl:for-each>
		     <h1/><br/>		
		  </xsl:if>
	       </xsl:if>
	    </body>
	 </html>
   </xsl:template>
   
   
   <xsl:template mode="report1-subjects" match="/v3:document">
      <xsl:choose>			
	 <xsl:when test="count(//v3:manufacturedProduct)=0">
	    <table class="contentTablePetite" cellSpacing="0" cellPadding="3" width="100%">
	       <tbody>
		  <xsl:apply-templates mode="piMedNames" select="v3:component/v3:structuredBody/v3:component/v3:section/v3:subject/v3:manufacturedProduct/v3:manufacturedMedicine|v3:component/v3:structuredBody/v3:component/v3:section/v3:subject/v3:manufacturedProduct/v3:manufacturedProduct"/>
		  
		  <xsl:apply-templates mode="Sr4ProductInfoBasic" select="v3:component/v3:structuredBody/v3:component/v3:section/v3:subject/v3:manufacturedProduct|v3:code/@code"/>
	       </tbody>
	    </table>
	 </xsl:when>		
	 <xsl:otherwise>
	    <!-- loop into the medicine portion of the label. This template will display all of the product information.  The following will be displayed within this section
		 1. product code
		 2. dosage form
		 3. route of administration
		 4. ingredients
		 5. imprint information
		 6. packaging information
	    -->
	    <xsl:apply-templates mode="report1-subjects" select="v3:component/v3:structuredBody/v3:component//v3:section/v3:subject"/>
	 </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template mode="report1-subjects" match="v3:section/v3:subject">
      <xsl:param name="sr4"/>
      <xsl:variable name="nsr4" select="not(boolean(count(./v3:manufacturedProduct/v3:manufacturedMedicine)))"/>		
      <h1 style="font-size: 150%;font-weight: normal;margin-top: 0pt;margin-bottom: 0pt;text-align: center;text-indent: 0em;">Product <xsl:value-of select="position()"/><a><xsl:attribute name="name">product<xsl:value-of select="position()"/></xsl:attribute></a></h1>
      <table width="100%"><tbody><tr><td>
	 <xsl:apply-templates select="/v3:document/v3:component/v3:structuredBody/v3:component[v3:section/v3:code/@code = '34069-5']"/>		
      </td></tr></tbody></table>
      <table class="contentTablePetite" cellSpacing="0" cellPadding="3" width="100%">
	 <tbody>
	    <xsl:apply-templates mode="piMedNames" select="./v3:manufacturedProduct/v3:manufacturedMedicine|./v3:manufacturedProduct/v3:manufacturedProduct"/>
	    <xsl:choose>
	       <xsl:when test="boolean($nsr4)">
		  <xsl:apply-templates mode="report1-subjects" select="v3:manufacturedProduct/v3:manufacturedProduct"/>
	       </xsl:when>
	       <xsl:otherwise>
		  <xsl:apply-templates mode="report1-subjects" select="v3:manufacturedProduct/v3:manufacturedMedicine"/>
	       </xsl:otherwise>
	    </xsl:choose>	
	 </tbody>
      </table>
      <xsl:if test="boolean($gSr4)">			
	 <xsl:apply-templates mode="subjects"
			      select="/v3:document/v3:author/v3:assignedEntity/v3:representedOrganization/v3:assignedEntity/v3:assignedOrganization/v3:assignedEntity/v3:assignedOrganization"/>			
      </xsl:if>
      <hr style="margin-top:2ex;width: 130%;text-align: left;"></hr>
   </xsl:template>
   <xsl:template name="report1-sr4ProductInfo" mode="report1-subjects" match="v3:subject/v3:manufacturedProduct/v3:manufacturedProduct">	
      <xsl:apply-templates mode="Sr4ProductInfoBasic" select=".."/>
      <xsl:choose>
	 <!-- if this is a multi-component subject then call to parts template -->
	 <xsl:when test="./v3:part">
	    <xsl:apply-templates mode="report1-subjects" select="./v3:part">			
	       <xsl:with-param name="sr4" select="true()"/>
	    </xsl:apply-templates>
	 </xsl:when>
	 <!-- otherwise it is a single product and we simply need to display the ingredients, imprint and packaging. -->
	 <xsl:otherwise>
	    <xsl:call-template name="report1-Sr4ProductInfoIng">			
	       <xsl:with-param name="sr4" select="true()"/>
	    </xsl:call-template>
	 </xsl:otherwise>
      </xsl:choose>
   </xsl:template>		
   <xsl:template name="report1-Sr4ProductInfoIng">		
      <xsl:param name="sr4"/>		
      <tr>
	 <td>
	    <xsl:call-template name="packaging">
	       <xsl:with-param name="path" select="."/>
	    </xsl:call-template>
	 </td>
      </tr>
      <tr>
	 <td>
	    <xsl:apply-templates mode="Sr4MarketingInfo" select=".."/>
	 </td>
      </tr>
      <xsl:choose>
	 <xsl:when test="$sr4">
	    <tr>
	       <td>
		  <xsl:call-template name="sr4ActiveIngredients"/>
	       </td>
	    </tr>
	    <tr>
	       <td>
		  <xsl:call-template name="sr4InactiveIngredients"/>
	       </td>
	    </tr>
	 </xsl:when>
	 <xsl:otherwise>
	    <tr>
	       <td>
		  <xsl:call-template name="ingredients"/>
	       </td>
	    </tr>
	 </xsl:otherwise>
      </xsl:choose>	
   </xsl:template>	
   <!-- multi-component packaging information will be displayed here.  -->
   <xsl:template mode="report1-subjects" match="v3:part">
      <xsl:param name="sr4"/>
      <!-- only display the outer part packaging once -->
      <xsl:if test="count(preceding-sibling::v3:part) = 0">
	 <tr>
	    <td>
	       <xsl:call-template name="packaging">
		  <xsl:with-param name="path" select=".."/>
	       </xsl:call-template>
	    </td>
	 </tr>
	 <tr>
	    <td>
	       <xsl:call-template name="partQuantity">
		  <xsl:with-param name="path" select=".."/>
	       </xsl:call-template>
	    </td>
	 </tr>
	 <tr>
	    <td class="normalizer">
	       <xsl:apply-templates mode="Sr4MarketingInfo" select="."/>	
	    </td>
	 </tr>
      </xsl:if>
      <tr>
	 <td>
	    <table width="100%" cellspacing="0" cellpadding="5">
	       <tr>
		  <td class="contentTableTitle">Part <xsl:value-of select="count(preceding-sibling::v3:part)+1"/>&#160;of&#160;<xsl:value-of select="count(../v3:part)"/></td>
	       </tr>
	       <xsl:apply-templates mode="piMedNames" select="v3:partMedicine|v3:partProduct"/>					
	    </table>
	 </td>
      </tr>
      <xsl:apply-templates mode="Sr4ProductInfoBasic" select="."/>				
      <xsl:for-each select="v3:partMedicine|v3:partProduct">			
	 <xsl:call-template name="report1-Sr4ProductInfoIng">			
	    <xsl:with-param name="sr4" select="$sr4"/>
	 </xsl:call-template>
      </xsl:for-each>	
   </xsl:template>
   
   
   
   <xsl:template mode="report2-subjects" match="/v3:document">
      <xsl:choose>			
	 <xsl:when test="count(//v3:manufacturedProduct)=0">
	    <table class="contentTablePetite" cellSpacing="0" cellPadding="3" width="100%">
	       <tbody>
		  <xsl:apply-templates mode="piMedNames" select="v3:component/v3:structuredBody/v3:component/v3:section/v3:subject/v3:manufacturedProduct/v3:manufacturedMedicine|v3:component/v3:structuredBody/v3:component/v3:section/v3:subject/v3:manufacturedProduct/v3:manufacturedProduct"/>
		  
		  <xsl:apply-templates mode="Sr4ProductInfoBasic" select="v3:component/v3:structuredBody/v3:component/v3:section/v3:subject/v3:manufacturedProduct|v3:code/@code"/>
	       </tbody>
	    </table>
	 </xsl:when>		
	 <xsl:otherwise>
	    <!-- loop into the medicine portion of the label. This template will display all of the product information.  The following will be displayed within this section
		 1. product code
		 2. dosage form
		 3. route of administration
		 4. ingredients
		 5. imprint information
		 6. packaging information
	    -->
	    <xsl:apply-templates mode="report2-subjects" select="v3:component/v3:structuredBody/v3:component//v3:section/v3:subject"/>
	 </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="/v3:document/v3:component/v3:structuredBody/v3:component[v3:section/v3:code/@code = '34069-5']"/>					
   </xsl:template>
   <xsl:template mode="report2-subjects" match="v3:section/v3:subject">
      <xsl:param name="sr4"/>
      <xsl:variable name="nsr4" select="not(boolean(count(./v3:manufacturedProduct/v3:manufacturedMedicine)))"/>
      <h1 style="font-size: 150%;font-weight: normal;margin-top: 0pt;margin-bottom: 0pt;text-align: center;text-indent: 0em;">Product <xsl:value-of select="position()"/><a><xsl:attribute name="name">product<xsl:value-of select="position()"/></xsl:attribute></a></h1>
      <table class="contentTablePetite" cellSpacing="0" cellPadding="3" width="100%">
	 <tbody>
	    <xsl:apply-templates mode="piMedNames" select="./v3:manufacturedProduct/v3:manufacturedMedicine|./v3:manufacturedProduct/v3:manufacturedProduct"/>
	    <xsl:choose>
	       <xsl:when test="boolean($nsr4)">
		  <xsl:apply-templates mode="report2-subjects" select="v3:manufacturedProduct/v3:manufacturedProduct"/>
	       </xsl:when>
	       <xsl:otherwise>
		  <xsl:apply-templates mode="report2-subjects" select="v3:manufacturedProduct/v3:manufacturedMedicine"/>
	       </xsl:otherwise>
	    </xsl:choose>	
	 </tbody>
      </table>	
      <hr style="margin-top:2ex;width: 130%;text-align: left;"></hr>
   </xsl:template>
   <xsl:template name="report2-sr4ProductInfo" mode="report2-subjects" match="v3:subject/v3:manufacturedProduct/v3:manufacturedProduct">	
      <xsl:apply-templates mode="Sr4ProductInfoBasic" select=".."/>
      <xsl:choose>
	 <!-- if this is a multi-component subject then call to parts template -->
	 <xsl:when test="./v3:part">
	    <xsl:apply-templates mode="report2-subjects" select="./v3:part">			
	       <xsl:with-param name="sr4" select="true()"/>
	    </xsl:apply-templates>
	 </xsl:when>
	 <!-- otherwise it is a single product and we simply need to display the ingredients, imprint and packaging. -->
	 <xsl:otherwise>
	    <xsl:call-template name="report2-Sr4ProductInfoIng">			
	       <xsl:with-param name="sr4" select="true()"/>
	    </xsl:call-template>
	 </xsl:otherwise>
      </xsl:choose>
   </xsl:template>		
   <xsl:template name="report2-Sr4ProductInfoIng">		
      <xsl:param name="sr4"/>	
      <xsl:choose>
	 <xsl:when test="$sr4">
	    <tr>
	       <td>
		  <xsl:call-template name="sr4ActiveIngredients"/>
	       </td>
	    </tr>
	 </xsl:when>
	 <xsl:otherwise>
	    <tr>
	       <td>
		  <xsl:call-template name="report2-ingredients"/>
	       </td>
	    </tr>
	 </xsl:otherwise>
      </xsl:choose>	
      <tr>
	 <td>
	    <xsl:choose>
	       <xsl:when test="v3:asEntityWithGeneric">
		  <xsl:call-template name="characteristics-old"/>
	       </xsl:when>
	       <xsl:otherwise>
		  <xsl:call-template name="characteristics-new"/>
	       </xsl:otherwise>
	    </xsl:choose>
	 </td>
      </tr>
   </xsl:template>	
   <!-- multi-component packaging information will be displayed here.  -->
   <xsl:template mode="report2-subjects" match="v3:part">
      <xsl:param name="sr4"/>
      <!-- only display the outer part packaging once -->
      <tr>
	 <td>
	    <table width="100%" cellspacing="0" cellpadding="5">
	       <tr>
		  <td class="contentTableTitle">Part <xsl:value-of select="count(preceding-sibling::v3:part)+1"/>&#160;of&#160;<xsl:value-of select="count(../v3:part)"/></td>
	       </tr>
	       <xsl:apply-templates mode="piMedNames" select="v3:partMedicine|v3:partProduct"/>					
	    </table>
	 </td>
      </tr>
      <xsl:apply-templates mode="Sr4ProductInfoBasic" select="."/>				
      <xsl:for-each select="v3:partMedicine|v3:partProduct">			
	 <xsl:call-template name="report2-Sr4ProductInfoIng">			
	    <xsl:with-param name="sr4" select="$sr4"/>
	 </xsl:call-template>
      </xsl:for-each>	
   </xsl:template>
   <!-- display the ingredient information (active only) -->
   <xsl:template name="report2-ingredients">
      <table width="100%" cellpadding="3" cellspacing="0" class="formTablePetite">
	 <tr>
	    <td colspan="4" class="formTitle">INGREDIENTS</td>
	 </tr>
	 <tr>
	    <td class="formTitle">Name (Active Moiety)</td>
	    <td class="formTitle">Type</td>
	    <td class="formTitle">Strength</td>
	 </tr>
	 <xsl:if test="(count(./v3:activeIngredient)=0) and (count(./v3:inactiveIngredient)=0)">
	    <tr>
	       <td colspan="3" class="formItem" align="center">No Ingredients Found</td>
	    </tr>
	 </xsl:if>
	 <xsl:for-each select="./v3:activeIngredient">
	    <tr>
	       <xsl:attribute name="class">
		  <xsl:choose>
		     <xsl:when test="position() mod 2 = 0">formTableRow</xsl:when>
		     <xsl:otherwise>formTableRowAlt</xsl:otherwise>
		  </xsl:choose>
	       </xsl:attribute>
	       <td class="formItem">
		  <strong>
		     <xsl:value-of select="v3:activeIngredientSubstance/v3:name"/>
		  </strong>
		  <xsl:if test="normalize-space(v3:activeIngredientSubstance/v3:activeMoiety/v3:activeMoiety/v3:name)"> (<xsl:value-of
		  select="v3:activeIngredientSubstance/v3:activeMoiety/v3:activeMoiety/v3:name"/>) </xsl:if>
	       </td>
	       <td class="formItem">Active</td>
	       <td class="formItem">
		  <xsl:value-of select="v3:quantity/v3:numerator/v3:translation/@value"/>&#160;<xsl:value-of select="v3:quantity/v3:numerator/v3:translation/@displayName"/>
		  <xsl:if test="normalize-space(v3:quantity/v3:denominator/v3:translation/@value)"> &#160;In&#160;<xsl:value-of select="v3:quantity/v3:denominator/v3:translation/@value"
		  />&#160;<xsl:value-of select="v3:quantity/v3:denominator/v3:translation/@displayName"/>
		  </xsl:if>
	       </td>
	    </tr>
	 </xsl:for-each>
      </table>
   </xsl:template>
</xsl:transform>
