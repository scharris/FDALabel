<?xml version="1.0" encoding="us-ascii"?>
<xsl:transform version="1.0" 
	       xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	       xmlns:saxon="http://saxon.sf.net/"
	       exclude-result-prefixes="saxon">
   <xsl:import href="spl-common.xsl"/>
   <xsl:output method="xml" media-type="text/xhtml" indent="yes" saxon:next-in-chain="../html2fo.xsl"/>
   <xsl:strip-space elements="*"/>
</xsl:transform>
