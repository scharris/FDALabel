package gov.fda.nctr.util;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;
import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DriverManagerDataSource;


public class SpringJdbc
{
    public static DataSource makeDataSource(Path jdbcPropsLoc) throws IOException
    {
        FileReader jdbcPropsRdr = new FileReader(jdbcPropsLoc.toFile());
        Properties jdbcProps = new Properties();
        jdbcProps.load(jdbcPropsRdr);

        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName(jdbcProps.getProperty("jdbc.driverClassName"));
        ds.setUrl(jdbcProps.getProperty("jdbc.url"));
        ds.setUsername(jdbcProps.getProperty("jdbc.username"));
        ds.setPassword(jdbcProps.getProperty("jdbc.password"));

        return ds;
    }

    private SpringJdbc() {}
}
