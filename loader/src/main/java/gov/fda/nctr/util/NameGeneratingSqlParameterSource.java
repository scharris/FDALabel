package gov.fda.nctr.util;

import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class NameGeneratingSqlParameterSource extends MapSqlParameterSource {

    public NameGeneratingSqlParameterSource()
    {
        super();
    }

    public NameGeneratingSqlParameterSource(String param_name, Object value)
    {
        super(param_name, value);
    }

    public NameGeneratingSqlParameterSource(Map<String, ?> values)
    {
        super(values);
    }

    public static NameGeneratingSqlParameterSource params(Object... interleaved_names_and_values)
    {
        if ( interleaved_names_and_values.length % 2 != 0 )
            throw new IllegalArgumentException("Even number of arguments expected for alternating parameter names and values.");

        NameGeneratingSqlParameterSource psrc = new NameGeneratingSqlParameterSource();

        final int num_pairs = interleaved_names_and_values.length/2;
        for(int pair_ix = 0; pair_ix < num_pairs; ++pair_ix)
        {
            Object param_name = interleaved_names_and_values[2*pair_ix];
            if ( !(param_name instanceof String) )
                throw new IllegalArgumentException("Expected string parameter name for name/value pair number " + (pair_ix+1) + "");

            psrc.addValue((String)param_name, interleaved_names_and_values[2*pair_ix + 1]);
        }

        return psrc;
    }

    public String addNewParam(String basename, Object value, Integer jdbc_type)
    {
        String new_name = newParamName(basename);

        if ( jdbc_type != null )
            addValue(new_name, value, jdbc_type);
        else
            addValue(new_name, value);

        return new_name;
    }

    public String addNewParam(String basename, Object value)
    {
        return addNewParam(basename, value, null);
    }

    public String newParamName(final String basename)
    {
        if ( !hasValue(basename) )
            return basename;
        else
        {
            int count = 1;
            String name;
            while(hasValue(name = basename + String.valueOf(count)))
                ++count;
            return name;
        }
    }
}