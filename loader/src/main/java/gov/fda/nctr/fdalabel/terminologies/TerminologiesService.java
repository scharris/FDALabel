package gov.fda.nctr.fdalabel.terminologies;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import nu.xom.*;

import gov.fda.nctr.util.FileUtils;


public class TerminologiesService
{
    private final Path terminologiesDir;

    private final List<TerminologyMetadata> terminologyMetadatas;

    private final Map<String,Terminology> terminologiesByName;


    private static final String SPL_DATA_STANDARDS_BASE_URL = "https://www.fda.gov/downloads/ForIndustry/DataStandards/StructuredProductLabeling/";

    // These are the names (without extension) of the xml files within the downloaded terminology archives.
    private static final String DOSAGE_FORM_TERMINOLOGY_NAME = "dosage_form";
    private static final String ROUTE_OF_ADMINISTRATION_TERMINOLOGY_NAME = "route_of_administration";
    private static final String DEA_SCHEDULE_TERMINOLOGY_NAME = "dea_schedule";
    private static final String MARKETING_CATEGORY_TERMINOLOGY_NAME = "marketing_category";
    private static final String DOCUMENT_TYPE_TERMINOLOGY_NAME = "document_type";
    private static final String SECTION_TYPE_TERMINOLOGY_NAME = "section_heading";

    private static final Logger log = LogManager.getLogger(TerminologiesService.class);


    public TerminologiesService(Path storageDir)
    {
        this.terminologiesDir = storageDir;

        try
        {
            // Several terminologies are in the "SPL Terminology Validation Files" archive.
            URL splTermValFilesUrl = new URL(SPL_DATA_STANDARDS_BASE_URL + "UCM360736.zip");

            this.terminologyMetadatas = Arrays.asList(
                new TerminologyMetadata(
                    DOSAGE_FORM_TERMINOLOGY_NAME,
                    "dosage_form",
                    "nci_thesaurus_code",
                    "spl_acceptable_term",
                    new URL(SPL_DATA_STANDARDS_BASE_URL + "UCM564346.zip")
                ),
                new TerminologyMetadata(
                    ROUTE_OF_ADMINISTRATION_TERMINOLOGY_NAME,
                    "route_of_admin",
                    "nci_thesaurus_code",
                    "spl_acceptable_term",
                    splTermValFilesUrl
                ),
                new TerminologyMetadata(
                    DEA_SCHEDULE_TERMINOLOGY_NAME,
                    "dea_schedule",
                    "nci_thesaurus_code",
                    "spl_acceptable_term",
                    splTermValFilesUrl
                ),
                new TerminologyMetadata(
                    MARKETING_CATEGORY_TERMINOLOGY_NAME,
                    "marketing_category",
                    "nci_thesaurus_code",
                    "spl_acceptable_term",
                    splTermValFilesUrl
                ),
                new TerminologyMetadata(
                    DOCUMENT_TYPE_TERMINOLOGY_NAME,
                    "document_type",
                    "loinc_code",
                    "loinc_name",
                    splTermValFilesUrl
                ),
                new TerminologyMetadata(
                    SECTION_TYPE_TERMINOLOGY_NAME,
                    "section_type",
                    "loinc_code",
                    "loinc_name",
                    splTermValFilesUrl
                )
            );

            this.terminologiesByName = new HashMap<>();
        }
        catch(MalformedURLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void loadTerminologies(boolean replaceExistingInStorage)
    {
        try
        {
            log.info("Ensuring application terminologies storage directory exists at " + this.terminologiesDir + ".");

            for ( TerminologyMetadata tmd : terminologyMetadatas )
            {
                String tname = tmd.getTerminologyName();

                if ( replaceExistingInStorage || !Files.exists(getTerminologyFile(tname)) )
                {
                    URL url = tmd.getDownloadUrl();

                    log.info("Downloading " + tname + " terminology file from " + url + ".");

                    FileUtils.downloadAndExtractFromZip(url, terminologiesDir, true);
                }
                else
                    log.info("Found " + tname + " terminology file, terminology will not be downloaded.");

                Map<String,String> displayNamesByCode = new HashMap<>(loadTerminologyFromStorage(tname));

                terminologiesByName.put(tname, new Terminology(tmd, displayNamesByCode));
            }

            log.info("Loaded " + terminologiesByName.size() + " terminologies.");
        }
        catch (IOException e)
        {
            throw err("Could not initialize terminologies service.", e);
        }
    }

    public List<Terminology> getTerminologies()
    {
        return new ArrayList<>(terminologiesByName.values());
    }

    private Map<String,String> loadTerminologyFromStorage(String terminologyName)
    {
        try
        {
            Builder parser = new Builder();
            Document doc = parser.build(getTerminologyFile(terminologyName).toFile());
            Element rootEl = doc.getRootElement();
            if ( !rootEl.getLocalName().equalsIgnoreCase("codelist") )
                throw err("In " + terminologyName + " terminology file, expected root element to be \"codeList\" (case insensitive).");

            if ( rootEl.getChildElements().size() != 1 )
                throw err("In " + terminologyName + " terminology file, expected only one child element under the document root.");

            Element listEl = rootEl.getChildElements().get(0);

            Elements terms = listEl.getChildElements("choice");
            if (terms.size() == 0)
                throw err("In " + terminologyName + " terminology file, no term elements found.");

            Map<String, String> displayNamesByCode = new HashMap<>();

            for (int i=0; i<terms.size(); ++i)
            {
                Element termEl = terms.get(i);

                Element labelEl = termEl.getFirstChildElement("label");
                Element valueEl = termEl.getFirstChildElement("value");

                if (labelEl == null || valueEl == null)
                    throw err("In " + terminologyName + " terminology file, a required label or value element is missing.");

                displayNamesByCode.put(valueEl.getValue().trim(), labelEl.getValue().trim());
            }

            log.info("Loaded " + terms.size() + " terms from " + terminologyName + " terminology file.");

            return displayNamesByCode;
        }
        catch(ParsingException e)
        {
            throw err("Could not load terminologiesByName due to parsing error.", e);
        }
        catch(IOException e)
        {
            throw err("Could not load terminologiesByName.", e);
        }
    }

    private Path getTerminologyFile(String terminologyName)
    {
        return terminologiesDir.resolve(getTerminologyFileName(terminologyName));
    }

    private String getTerminologyFileName(String terminologyName)
    {
        return terminologyName + ".xml";
    }

    private TerminologiesException err(String msg)
    {
        return new TerminologiesException(msg);
    }

    private TerminologiesException err(String msg, Throwable cause)
    {
        return new TerminologiesException(msg, cause);
    }

    public static class TerminologyMetadata
    {
        private final String terminologyName;
        private final String dbTableName;
        private final String dbCodeFieldName;
        private final String dbDisplayNameFieldName;
        private final URL downloadUrl;

        public TerminologyMetadata(String terminologyName, String dbTableName, String dbCodeFieldName, String dbDisplayNameFieldName, URL downloadUrl)
        {
            this.terminologyName = terminologyName;
            this.dbTableName = dbTableName;
            this.dbCodeFieldName = dbCodeFieldName;
            this.dbDisplayNameFieldName = dbDisplayNameFieldName;
            this.downloadUrl = downloadUrl;
        }

        public String getTerminologyName() { return terminologyName; }

        public String getDbTableName() { return dbTableName; }

        public String getDbCodeFieldName() { return dbCodeFieldName; }

        public String getDbDisplayNameFieldName() { return dbDisplayNameFieldName; }

        public URL getDownloadUrl() { return downloadUrl; }
    }

    public static class Terminology
    {
        private final TerminologyMetadata terminologyMetadata;
        private final Map<String,String> terminologyDisplayNamesByCode;

        public Terminology(TerminologyMetadata terminologyMetadata, Map<String,String> terminologyDisplayNamesByCode)
        {
            this.terminologyMetadata = terminologyMetadata;
            this.terminologyDisplayNamesByCode = terminologyDisplayNamesByCode;
        }

        public TerminologyMetadata getTerminologyMetadata() { return terminologyMetadata; }

        public Map<String, String> getTerminologyDisplayNamesByCode() { return terminologyDisplayNamesByCode; }
    }
}
