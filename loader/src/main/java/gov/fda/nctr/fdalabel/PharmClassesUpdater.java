package gov.fda.nctr.fdalabel;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.JDBCType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.zip.ZipInputStream;
import javax.sql.DataSource;

import com.univocity.parsers.csv.CsvFormat;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import static org.apache.commons.io.FileUtils.copyURLToFile;

import gov.fda.nctr.dailymed.DailyMed;
import gov.fda.nctr.util.XomNavigator;
import gov.fda.nctr.util.DatabaseOps;
import gov.fda.nctr.util.ZipUtils;
import static gov.fda.nctr.util.SpringJdbc.makeDataSource;
import static gov.fda.nctr.util.DatabaseOps.FieldMapping;
import static gov.fda.nctr.util.ZipUtils.DirOption.OMIT_DIRS;
import static gov.fda.nctr.util.ZipUtils.visitZipEntries;


// Update pharmacological classes information in the DRUGLABEL database schema.
// Example usage:
// java -cp target/fdalabel-loader.jar gov.fda.nctr.fdalabel.PharmClassesUpdater --download-source-files ../../../db-conns/druglabel_scidevl2_jdbc.properties /c/Users/Sharris/tmp/pharm-classes-source-files

public class PharmClassesUpdater
{
    private final DataSource dataSource;
    private final Path sourceFilesDir;
    private final boolean downloadSourceFiles;
    private final boolean clearDeprecatedTables;
    private final Path dbUpdateFilesDir;


    public static String DM_PHARMA_INDEXING_ARCHIVE_FILENAME = "pharmacologic_class_indexing_spl_files.zip";
    public static String DM_PHARMA_CLASS_MAPPINGS_ARCHIVE_FILENAME = "pharmacologic_class_mappings.zip";
    public static String DM_SPL_PHARMA_MAPPINGS_ARCHIVE_ENTRY_NAME = "pharmacologic_class_mappings.txt";

    public static String CORE_MEDRT_ZIP_URL = "https://evs.nci.nih.gov/ftp1/MED-RT/Core_MEDRT_XML.zip";
    public static String CORE_MEDRT_ZIP_FILENAME = "Core_MEDRT_XML.zip";
    public static Predicate<String> IS_CORE_MEDRT_NUI_DEFS_ZIPENTRY = Pattern.compile("MEDRT_all_NUIs_[0-9.]+\\.txt").asPredicate();

    public static String DM_PHARMA_UNII_CLASSES_DB_UPDATE_FILENAME = "dm-pharma-unii-classes.csv";
    public static String DM_SPL_PHARMA_MAPPINGS_DB_UPDATE_FILENAME = "dm-spl-pharma-mapping.csv";
    public static String MEDRT_NUIS_DB_UPDATE_FILENAME = "MEDRT_all_NUIs.tsv";

    private static final Logger log = LogManager.getLogger(PharmClassesUpdater.class);


    public PharmClassesUpdater
        (
            DataSource dataSource,
            Path sourceFilesDir,
            boolean downloadSourceFiles,
            boolean clearDeprecatedTables
        )
    {
        try
        {
            this.dataSource = dataSource;
            this.sourceFilesDir = sourceFilesDir;
            this.downloadSourceFiles = downloadSourceFiles;
            this.clearDeprecatedTables = clearDeprecatedTables;
            this.dbUpdateFilesDir = Files.createTempDirectory("fdalabel-pharm-classes-updater");

            log.info("Created staging directory for database update files at " + dbUpdateFilesDir.toString() + ".");
        }
        catch(IOException e)
        {
            throw new IOError(e);
        }
    }

    public void execute()
    {
        if ( downloadSourceFiles )
            downloadSourceFiles();

        generateDbUpdateFilesFromSourceFiles();

        DatabaseOps.inNewTransaction(dataSource, conn ->
            {
                insertMedRTNuiDefs(conn);

                insertDailyMedPharmaUniiClasses(conn);

                insertDailyMedSplPharmaIndexingFileAssociations(conn);

                if ( clearDeprecatedTables )
                    clearDeprecatedTables(conn);
            }
        );
    }

    private void downloadSourceFiles()
    {
        DailyMed.downloadPharmaClassIndexingArchive(sourceFilesDir.resolve(DM_PHARMA_INDEXING_ARCHIVE_FILENAME));
        DailyMed.downloadPharmaClassMappingsArchive(sourceFilesDir.resolve(DM_PHARMA_CLASS_MAPPINGS_ARCHIVE_FILENAME));

        downloadMedRTCoreArchive();
    }

    private void downloadMedRTCoreArchive()
    {
        try
        {
            File medrtCoreZip = sourceFilesDir.resolve(CORE_MEDRT_ZIP_FILENAME).toFile();

            log.info("Starting download of core MED-RT archive from " + CORE_MEDRT_ZIP_URL + ".");

            copyURLToFile(new URL(CORE_MEDRT_ZIP_URL), medrtCoreZip);

            log.info("Finished download of core MED-RT archive " + " (" + medrtCoreZip.length() + " bytes).");
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    private void generateDbUpdateFilesFromSourceFiles()
    {
        Path dmPharmaClassesIndexingArchive = sourceFilesDir.resolve(DM_PHARMA_INDEXING_ARCHIVE_FILENAME);
        Path dmPharmaUniiClassesDbUpdateFile = dbUpdateFilesDir.resolve(DM_PHARMA_UNII_CLASSES_DB_UPDATE_FILENAME);
        generatePharmaUniiClassesDbUpdateFile(dmPharmaClassesIndexingArchive, dmPharmaUniiClassesDbUpdateFile);

        Path dmPharmaClassMappingsArchive = sourceFilesDir.resolve(DM_PHARMA_CLASS_MAPPINGS_ARCHIVE_FILENAME);
        Path dmSplPharmaMappingsDbUpdateFile = dbUpdateFilesDir.resolve(DM_SPL_PHARMA_MAPPINGS_DB_UPDATE_FILENAME);
        generateSplPharmaMappingsDbUpdateFile(dmPharmaClassMappingsArchive, dmSplPharmaMappingsDbUpdateFile);

        Path medrtCoreZip = sourceFilesDir.resolve(CORE_MEDRT_ZIP_FILENAME);
        Path medrtNuiDefsDbUpdateFile = dbUpdateFilesDir.resolve(MEDRT_NUIS_DB_UPDATE_FILENAME);
        extractNuiDefsZipEntryToFile(medrtCoreZip, medrtNuiDefsDbUpdateFile);
    }

    private static void generatePharmaUniiClassesDbUpdateFile
        (
            Path pharmClassesIndexingArchive,
            Path outputFile
        )
    {
        Builder xmlParser = new Builder();

        try ( BufferedWriter bw = Files.newBufferedWriter(outputFile) )
        {
            XomNavigator xom = new XomNavigator("urn:hl7-org:v3", "hl7");

            int[] failures = new int[] { 0 };
            int[] ignored = new int[] { 0 };
            int[] total = new int[] { 0 };

            visitZipEntries(pharmClassesIndexingArchive, OMIT_DIRS, (zippedSplDirZe, zisOuter) -> {
                visitZipEntries(new ZipInputStream(zisOuter), OMIT_DIRS, (ze, zis) -> {
                    try
                    {
                        ++total[0];

                        byte[] xmlBytes = IOUtils.readFully(zis, (int)ze.getSize());
                        Document doc = xmlParser.build(new ByteArrayInputStream(xmlBytes));
                        Element rootEl = doc.getRootElement();

                        String pharmaSetId = xom.requiredChildAttr(rootEl, "setId", "root");

                        Element subsEl = xom.queryForElement(rootEl,
                            "//hl7:subject/hl7:identifiedSubstance/hl7:identifiedSubstance",
                            "inner identified substance element"
                        );

                        String unii = xom.requiredChildAttr(subsEl, "code", "code");

                        List<Element> codeEls = xom.queryForElements(subsEl,
                            "hl7:asSpecializedKind/hl7:generalizedMaterialKind/hl7:code"
                        );

                        if ( codeEls.isEmpty() )
                            log.warn("No pharm class codes found for pharm classes indexing entry " + ze.getName() + ".");

                        for ( Element codeEl : codeEls )
                        {
                            String nuiCode = xom.requiredAttr(codeEl, "code");
                            bw.write(pharmaSetId + "," + unii + "," + nuiCode + "\n");
                        }
                    }
                    catch(XomNavigator.NavigationException ne)
                    {
                        // A lot of these failures (639) currently are caused by billing unit indexing files which
                        // were apparently put in this pharm class indexing archive by mistake, all from 20150721.
                        if ( zippedSplDirZe.getName().startsWith("20150721") )
                        {
                            ++ignored[0];
                        }
                        else
                        {
                            log.warn("Invalid content in pharm class indexing archive entry '" + zippedSplDirZe.getName() +
                                        "':" + ne.getMessage());
                            ++failures[0];
                        }
                    }
                    catch(Exception e)
                    {
                        throw new RuntimeException(e);
                    }
                });
            });

            log.info("Finished processing pharm class indexing archive, with " + failures[0] + " failures " +
                "and " + ignored[0] + " entries ignored of " + total[0] + " entries processed.");
        }
        catch(IOException e)
        {
            throw new IOError(e);
        }
    }

    private void generateSplPharmaMappingsDbUpdateFile(Path dmPharmaClassMappingsArchive, Path dmSplPharmaMappingsDbUpdateFile)
    {
        ZipUtils.extractZipFileEntryToFile(
            dmPharmaClassMappingsArchive,
            ze -> ze.getName().equals(DM_SPL_PHARMA_MAPPINGS_ARCHIVE_ENTRY_NAME),
            dmSplPharmaMappingsDbUpdateFile,
            "pharma mappings",
            "DailyMed pharma mappings"
        );

    }

    private static void extractNuiDefsZipEntryToFile(Path medrtCoreZip, Path nuiDefsUpdateFile)
    {
        ZipUtils.extractZipFileEntryToFile(
            medrtCoreZip,
            ze -> IS_CORE_MEDRT_NUI_DEFS_ZIPENTRY.test(ze.getName()),
            nuiDefsUpdateFile,
            "Core MED-RT",
            "NUI definitions"
        );
    }

    private void insertMedRTNuiDefs(Connection conn)
    {
        Path medrtNuiDefsFile = dbUpdateFilesDir.resolve(MEDRT_NUIS_DB_UPDATE_FILENAME);

        if ( !Files.isRegularFile(medrtNuiDefsFile) )
            throw new RuntimeException("File " + medrtNuiDefsFile.getFileName().toString() + " not found.");

        log.info("Inserting MED-RT concept definitions.");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(1, "nui", JDBCType.VARCHAR),
            new FieldMapping(0, "name", JDBCType.VARCHAR, Optional.of(nameMaybeWithTLvl -> {
                int bracketIx = nameMaybeWithTLvl.lastIndexOf('[');
                if ( bracketIx == -1 ) return nameMaybeWithTLvl;
                else return nameMaybeWithTLvl.substring(0, bracketIx).trim();
            })),
            new FieldMapping(0, "tlevel", JDBCType.VARCHAR, Optional.of(nameWithTLvl -> {
                int bracketIx = nameWithTLvl.lastIndexOf('[');
                if ( bracketIx == -1 ) return "";
                else return nameWithTLvl.substring(bracketIx).trim();
            }))
        );

        long numInserted = DatabaseOps.loadTableFromTsvFile(
            "medrt_concept",
            fieldMappings,
            medrtNuiDefsFile,
            false,
            true,
            3000,
            conn
        );

        log.info("Inserted " + numInserted + " MED-RT concept definitions.");
    }

    private void insertDailyMedPharmaUniiClasses(Connection conn)
    {
        Path uniiClassesFile = dbUpdateFilesDir.resolve(DM_PHARMA_UNII_CLASSES_DB_UPDATE_FILENAME);

        if ( !Files.isRegularFile(uniiClassesFile) )
            throw new RuntimeException("File " + uniiClassesFile.getFileName().toString() + " not found.");

        log.info("Inserting unii / pharm-class associations.");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(0, "pharma_setid", JDBCType.VARCHAR),
            new FieldMapping(1, "unii", JDBCType.VARCHAR),
            new FieldMapping(2, "nui", JDBCType.VARCHAR)
        );

        long numInserted = DatabaseOps.loadTableFromCsvFile(
            "pharm_index",
            fieldMappings,
            uniiClassesFile,
            false,
            true,
            3000,
            conn
        );

        log.info("Inserted " + numInserted + " unii / pharm-class associations.");
    }

    private void insertDailyMedSplPharmaIndexingFileAssociations(Connection conn)
    {
        Path splPharmaMappingsFile = dbUpdateFilesDir.resolve(DM_SPL_PHARMA_MAPPINGS_DB_UPDATE_FILENAME);

        if ( !Files.isRegularFile(splPharmaMappingsFile) )
            throw new RuntimeException("File " + splPharmaMappingsFile.getFileName().toString() + " not found.");

        log.info("Inserting DailyMed spl / pharma indexing file mappings.");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(0, "spl_setid", JDBCType.VARCHAR),
            new FieldMapping(2, "pharma_setid", JDBCType.VARCHAR)
        );

        CsvFormat bsvFormat = new CsvFormat();
        bsvFormat.setDelimiter('|');

        long numInserted = DatabaseOps.loadTableFromDelimitedFile(
            "pharma_mapping",
            fieldMappings,
            splPharmaMappingsFile,
            bsvFormat,
            true,
            true,
            3000,
            conn
        );

        log.info("Inserted " + numInserted + " DailyMed spl / pharma indexing file mappings.");
    }

    private void clearDeprecatedTables(Connection conn)
    {
        DatabaseOps.clearTables(conn, Arrays.asList(
            "ndfrt_concept_properties",
            "ndfrt_concept_spl_count",
            "dgv_ndfrt_concept_spl_count",
            "ndfrt_conceptinf",
            "ingr_ndfrt_concept",
            "ndfrt_concept"
        ));
    }

    public static void main(String[] args) throws Exception
    {
        List<String> remArgs = new ArrayList<>(Arrays.asList(args));

        boolean downloadSourceFiles = remArgs.remove("--download-source-files");
        boolean clearDeprecatedTables = remArgs.remove("--clear-deprecated-tables");

        if ( remArgs.size() != 2 )
            System.err.println("Expected arguments: [--download-source-files] [--clear-deprecated-tables] <jdbc-props-file> <source-files-dir>");

        Path jdbcPropsFile  = Paths.get(remArgs.remove(0));
        Path sourceFilesDir = Paths.get(remArgs.remove(0));

        DataSource dataSource = makeDataSource(jdbcPropsFile);

        if ( downloadSourceFiles )
            Files.createDirectories(sourceFilesDir);

        PharmClassesUpdater pcUpdater =
            new PharmClassesUpdater(dataSource, sourceFilesDir, downloadSourceFiles, clearDeprecatedTables);

        pcUpdater.execute();
    }
}
