package gov.fda.nctr.fdalabel;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;
import java.util.zip.ZipInputStream;
import javax.sql.DataSource;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.commons.io.IOUtils;
import static org.apache.commons.io.FileUtils.cleanDirectory;
import static org.apache.commons.io.FileUtils.copyDirectory;
import static org.apache.commons.io.FileUtils.readFileToByteArray;
import static org.apache.commons.io.FilenameUtils.getBaseName;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.transaction.support.TransactionTemplate;

import gov.fda.nctr.dailymed.DailyMed;
import gov.fda.nctr.dailymed.DailyMedArchives;
import gov.fda.nctr.fdalabel.terminologies.TerminologiesService;
import gov.fda.nctr.util.DatabaseOps.*;
import gov.fda.nctr.util.*;
import static gov.fda.nctr.util.Args.pluckIntOption;
import static gov.fda.nctr.util.Args.pluckStringOption;
import static gov.fda.nctr.util.Optionals.ifPresentElse;
import static gov.fda.nctr.util.FileUtils.*;
import static gov.fda.nctr.fdalabel.terminologies.TerminologiesService.Terminology;
import static gov.fda.nctr.fdalabel.terminologies.TerminologiesService.TerminologyMetadata;
import static gov.fda.nctr.util.CollectionUtils.partition;
import static gov.fda.nctr.util.NameGeneratingSqlParameterSource.params;
import static gov.fda.nctr.util.ZipUtils.visitZipEntries;
import static gov.fda.nctr.util.ZipUtils.DirOption.OMIT_DIRS;


public class FDALabelLoader
{
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final LobHandler lobHandler;

    private final Path badSplsDir;

    private final TerminologiesService terminologiesService;

    private final Optional<HtmlGenerator> htmlGenCtx;

    private final int includeDgv; // include changes to DGV_* tables.

    private final Optional<PharmClassesUpdater> pharmClassesUpdater;

    private static final Logger log = LogManager.getLogger(FDALabelLoader.class);


    public FDALabelLoader
        (
            DataSource ds,
            Path appDataDir,
            TerminologiesService terminologiesService,
            Optional<HtmlGenerator> htmlGenCtx,
            boolean includeDgv,
            Optional<PharmClassesUpdater> pharmClassesUpdater
        )
    {
        try
        {
            this.badSplsDir = appDataDir.resolve("bad_spls");
            this.jdbcTemplate = new NamedParameterJdbcTemplate(ds);
            this.lobHandler = new DefaultLobHandler();
            this.terminologiesService = terminologiesService;
            this.htmlGenCtx = htmlGenCtx;
            this.includeDgv = includeDgv ? 1 : 0;
            this.pharmClassesUpdater = pharmClassesUpdater;

            Files.createDirectories(this.badSplsDir);
        }
        catch(IOException e)
        {
            throw new IOError(e);
        }
    }

    private static String usage()
    {
        return
            "Expected arguments: [options] <archive-files-dir> [<jdbc-props-file> <app-data-dir>]\n" +
            "    archive-files-dir: directory containing zip files, each of which contains zip files of SPL contents, " +
            "as downloadable from DailyMed.\n" +
            "    jdbc-props-file: jdbc properties file location, with properties jdbc.driverClassName, jdbc.url, " +
            "jdbc.username, jdbc.password. Required unless download-archives option is specified.\n" +
            "    app-data-dir: directory in which to save and load files in the local filesystem, such as terminology " +
            "files and SPLs which failed to be imported. Required unless " +
            "download-archives option is specified.\n" +
            "    [Options]\n" +
            "        --download-archives: if present, archive files will be downloaded from DailyMed into archive-files-dir " +
            "prior to importing data.\n" +
            "       --pharm-classes-source-files-dir: if present, pharmacological class information in the database will be " +
            "updated with data derived from the files in this directory. The files can be downloaded via the " +
            "'--download-pharm-classes-source-files' option, which may may either be specified together with this option, or " +
            "else omitted to re-use files downloaded previously.\n" +
            "       --clear-deprecated-pharm-class-tables: if present and option pharm-classes-source-files-dir is specified " +
            "so that pharm classes information is being updated, then deprecated (NDF-RT related) pharm class information " +
            "tables will be cleared.\n" +
            "       --download-pharm-classes-source-files: if present, pharmacological class indexing and NDF-RT files will " +
            "be downloaded to the directory specified via the '--pharm-classes-source-files-dir' option, which is required " +
            "when this option is present.\n" +
            "        --max-removals <int>: specifies the maximum number of labels that can be removed in the import " +
            "process, defaulting to 5000.\n" +
            "        --max-failures: the maximum number of SPLs failing to import before the import is aborted, defaulting " +
            "to 20\n" +
            "        --keep-removed: if present, removed label data will be copied into history tables\n" +
            "        --import-terminologies: if present, terminologies such as dosage forms will be downloaded and any " +
            "new terms will be inserted to the proper database lookup tables prior to importing.\n" +
            "        --meddra-occurrences-file <file>: if present, occurrences of MedDRA low level terms in SPL " +
            "coded sections will be determined from this file, instead of being found via Oracle Text searches.\n" +
            "        --no-dgv-updates: if present, no DGV_* tables will be updated or accessed in any way.\n" +
            "        --msxsl-exe: if present, html will be generated via msxsl.exe for compatibility with FDALabel 1.0 " +
            "(Windows only).\n" +
            "        --stylesheet: specifies the FDA SPL Stylesheet (main file), for use with the msxsl-exe option.\n"
            ;
    }

    public static void main(String[] args)
    {
        boolean helpRequested = args.length == 1 && (args[0].equals("-h") || args[0].equals("--help"));
        if ( helpRequested )
        {
            System.out.println(usage());
            return;
        }

        // Handle download-only case.
        if ( args.length == 2 && args[0].equals("--download-archives") )
        {
            DailyMed.downloadLabelArchives(Paths.get(args[1]));
            return;
        }

        try
        {
            log.info("FDALabel import process started.");

            LoadingSpecification spec = makeLoadingSpecification(args);

            loadAccordingToSpecification(spec);

            log.info("All done.");
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            log.error("Processing finished abnormally.");
        }
    }

    /// Process command line arguments to produce a loading specification, which
    /// fully describes the loading process to be performed.
    private static LoadingSpecification makeLoadingSpecification(String[] args)
    {
        List<String> remArgs = new ArrayList<>(Arrays.asList(args));

        boolean downloadArchives = remArgs.remove("--download-archives");
        boolean keepRemoved = remArgs.remove("--keep-removed");
        boolean importTerminologies = remArgs.remove("--import-terminologies");
        boolean doDgvUpdates = !remArgs.remove("--no-dgv-updates");
        boolean downloadPharmClassesSourceFiles = remArgs.remove("--download-pharm-classes-source-files");
        boolean clearDeprecatedPharmClassTables = remArgs.remove("--clear-deprecated-pharm-class-tables");
        int maxRemoved = pluckIntOption(remArgs, "--max-removals", 5000);
        int maxFailures = pluckIntOption(remArgs, "--max-failures", 20);

        Optional<Path> pharmClassesSourceFilesDir =
            pluckStringOption(remArgs,"--pharm-classes-source-files-dir").map(Paths::get);

        Optional<Path> meddraOccsFile =
            pluckStringOption(remArgs,"--meddra-occurrences-file").map(Paths::get);

        // Options for html generation via msxsl.exe (for DrugLabel / FDALabel 1.x only).
        Optional<Path> msxslExe = pluckStringOption(remArgs, "--msxsl-exe").map(Paths::get);
        Optional<Path> xslFile = pluckStringOption(remArgs, "--stylesheet").map(Paths::get);
        Optional<HtmlGenerator> htmlGen =
            xslFile.map(xsl -> {
                Path msxsl = msxslExe.orElseThrow(() -> new RuntimeException("msxsl-exe is required with stylesheet option"));
                if ( !Files.isRegularFile(msxsl) ) throw new RuntimeException("msxsl executable not found");
                if ( !Files.isRegularFile(xsl) ) throw new RuntimeException("stylesheet not found");
                return new HtmlGenerator(msxsl, xsl);
            });

        // Remaining arguments should be the non-optional ones or a help request.

        if ( remArgs.size() != 3 )
            throw new IllegalArgumentException("Expected three non-option arguments.");

        Path archivesDir = Paths.get(remArgs.remove(0));
        Path jdbcPropsLoc = Paths.get(remArgs.remove(0));
        Path appDataDir = Paths.get(remArgs.remove(0));

        checkFilesExist(new Path[] {jdbcPropsLoc});
        checkDirectoriesExist(new Path[] {archivesDir});

        return
            new LoadingSpecification(
                archivesDir,
                jdbcPropsLoc,
                appDataDir,
                downloadArchives,
                importTerminologies,
                keepRemoved,
                maxRemoved,
                maxFailures,
                meddraOccsFile,
                pharmClassesSourceFilesDir,
                downloadPharmClassesSourceFiles,
                clearDeprecatedPharmClassTables,
                htmlGen,
                doDgvUpdates
            );
    }

    public static void loadAccordingToSpecification(LoadingSpecification spec) throws IOException
    {
        DataSource ds = SpringJdbc.makeDataSource(spec.getJdbcPropsPath());

        TerminologiesService termsSvc = makeTerminologiesService(spec.getAppDataDir().resolve("terminologies"));

        DailyMedArchives labelArchives = getDailymedLabelArchives(spec.getDownloadArchives(), spec.getArchivesDir());

        Optional<PharmClassesUpdater> pharmClassesUpdater =
            spec.getPharmClassesSourceFilesDir()
            .map(pcSourcesDir ->
                new PharmClassesUpdater(
                    ds,
                    pcSourcesDir,
                    spec.getDownloadPharmClassesSourceFiles(),
                    spec.getClearDeprecatedPharmClassTables()
                )
            );

        FDALabelLoader loader =
            new FDALabelLoader(
                ds,
                spec.getAppDataDir(),
                termsSvc,
                spec.getHtmlGenerator(),
                spec.getIncludeDgvUpdates(),
                pharmClassesUpdater
            );

        TransactionTemplate transactionTemplate = new TransactionTemplate(new DataSourceTransactionManager(ds));

        long loadNum = transactionTemplate.execute(txStatus -> {

            log.info("Connected to database.");

            long loadNumber =
                loader.doLoad(
                    labelArchives,
                    spec.getMeddraOccurrencesFile(),
                    spec.getMaxFailures(),
                    spec.getMaxRemoved(),
                    spec.getKeepRemoved(),
                    spec.getImportTerminologies()
                );

            log.info("Loading of SPLs and summary data finished, performing final commit.");

            return loadNumber;
        });

        log.info("Final transaction committed for load " + loadNum + ".");
    }

    private static TerminologiesService makeTerminologiesService(Path storageDir) throws IOException
    {
        Files.createDirectories(storageDir);

        TerminologiesService svc = new TerminologiesService(storageDir);

        log.info("Creating terminologies service.");

        svc.loadTerminologies(true);

        log.info("Terminologies service created.");

        return svc;
    }

    private static DailyMedArchives getDailymedLabelArchives(boolean downloadArchives, Path archivesDir)
    {
        DailyMedArchives archives =
            downloadArchives ? DailyMed.downloadLabelArchives(archivesDir)
                             : DailyMed.readLocalLabelArchivesDirectory(archivesDir);

        if ( archives.getArchiveFiles().isEmpty() )
            throw new RuntimeException("No archive files found in archive files directory.");

        log.info("Last modified date for sample input archive file is " + archives.getLastModified() + ".");

        return archives;
    }

    public long doLoad
        (
            DailyMedArchives dmArchives,
            Optional<Path> meddraOccurrencesFile,
            int maxFailures,
            int maxRemovals,
            boolean keepRemoved,
            boolean importTerminologies
        )
    {
        try
        {
            long loadNum = createLoadRecord(new java.sql.Date(dmArchives.getLastModified().toEpochMilli()));

            if ( importTerminologies ) importTerminologies();

            Map<Md5,SplDirEntry> splDirEntriesByMd5 = registerLoadMd5s(dmArchives.getArchiveFiles(), loadNum);

            Map<SplDirEntry,Md5> newMd5sBySplDirEntry = getNewMd5sBySplDirectoryEntry(loadNum, splDirEntriesByMd5);

            deleteSummaryData();

            removeSplsNotInLoad(loadNum, maxRemovals, keepRemoved);

            importSelectedSpls(newMd5sBySplDirEntry, dmArchives.getArchiveFiles(), loadNum, maxFailures);

            createSummaryData();

            syncContextIndexes(); // (syncing a context index causes a commit)

            updateMeddraTermOccurrences(meddraOccurrencesFile);

            pharmClassesUpdater.ifPresent(PharmClassesUpdater::execute);

            writeRecordCounts(loadNum);

            finalizeLoadRecord(loadNum);

            return loadNum;
        }
        catch(Exception e)
        {
            // Rethrow as runtime exception to trigger rollback.
            throw new RuntimeException(e);
        }
    }

    private void importTerminologies()
    {
        log.info("Inserting any missing terms from standard terminologies into the database.");

        int newTermsCount = 0;
        for ( Terminology t : terminologiesService.getTerminologies() )
        {
            TerminologyMetadata tMd = t.getTerminologyMetadata();
            Map<String,String> tTerms = t.getTerminologyDisplayNamesByCode();

            Set<String> newTermCodes = new HashSet<>(tTerms.keySet());
            newTermCodes.removeAll(
                getStringFieldValues(tMd.getDbTableName(), tMd.getDbCodeFieldName())
            );

            for ( String termCode : newTermCodes )
            {
                String displayName = tTerms.get(termCode);

                insertNewTerminologyTerm(tMd, termCode, tTerms.get(termCode));

                log.info("Inserted " + tMd.getDbTableName() + " record {" +
                    termCode + ", " + displayName + "}.");

                ++newTermsCount;
            }
        }

        log.info(newTermsCount + " missing terms were inserted from standard terminologies.");
    }

    private void insertNewTerminologyTerm(TerminologyMetadata tMd, String code, String displayName)
    {
        jdbcTemplate.update(
            "insert into " + tMd.getDbTableName() +
                "(" + tMd.getDbCodeFieldName() + ", " + tMd.getDbDisplayNameFieldName() + ") " +
                " values(:code, :display_name)",
            params("code", code,
                   "display_name", displayName)
        );
    }

    private void importSelectedSpls
        (
            Map<SplDirEntry,Md5> newMd5sBySplDirEntry,
            List<Path> zipArchives,
            long loadNum,
            int maxSplImportFailures
        )
        throws IOException
    {
        Path tmpDir = Files.createTempDirectory("fdalabel-loader");
        log.info("Using temp directory " + tmpDir + " for expanding spl directory entries.  Examine this directory if an spl fails to load.");

        Set<String> skipEntryNames = getSkipSplDirectoryEntryNames();

        int[] totalCount = {0}, totalSuccessCount = {0};
        int[] failedSplImports = {0};

        for ( Path zipArchive: zipArchives )
        {
            log.info("Processing spls in archive " + fileName(zipArchive) + "");

            int[] count = {0}, successCount = {0};

            visitZipEntries(zipArchive, OMIT_DIRS, (zippedSplDirZe, zis) -> {

                SplDirEntry thisSplDirEntry = new SplDirEntry(zipArchive, zippedSplDirZe.getName());

                if ( newMd5sBySplDirEntry.containsKey(thisSplDirEntry) && !skipEntryNames.contains(zippedSplDirZe.getName()) )
                {
                    ++count[0];

                    Md5 md5 = newMd5sBySplDirEntry.get(thisSplDirEntry);

                    Pair<Path,List<Path>> splAndAtts = writeSplFileEntriesToDir(tmpDir, zippedSplDirZe.getName(), zis);
                    Path splXml = splAndAtts.fst();
                    List<Path> splAtts = splAndAtts.snd();

                    try
                    {
                        if ( splXml != null && Files.isRegularFile(splXml) )
                        {
                            importSpl(splXml, loadNum, md5, splAtts, zippedSplDirZe.getName());

                            ++successCount[0];
                        }
                        else
                            throw new RuntimeException("No spl xml file found for spl directory entry " + zippedSplDirZe.getName() + "");
                    }
                    catch(Throwable t)
                    {
                        ++failedSplImports[0];
                        if ( failedSplImports[0] < maxSplImportFailures )
                        {
                            log.error("Error processing spl directory entry " + zippedSplDirZe.getName() + ": " + t.getMessage());

                            copyDirectory(tmpDir.toFile(), badSplsDir.resolve(zippedSplDirZe.getName()).toFile());
                        }
                        else
                            throw new RuntimeException("Maximum number of spl import failures reached.");
                    }

                    cleanDirectory(tmpDir.toFile());
                } // new spl
            });

            log.info("Processed " + successCount[0] + " of " + count[0] + " new spls successfully in archive " + fileName(zipArchive) + "");

            totalCount[0] += count[0];
            totalSuccessCount[0] += successCount[0];
        }

        log.info("Processed " + totalSuccessCount[0] + " of " + totalCount[0] + " new spls successfully in all archives.");
    }


    private void importSpl
        (
            Path splXml,
            long loadNum,
            Md5 splMd5,
            List<Path> splAtts,
            String splDirEntryName
        )
        throws IOException
    {
      	Optional<String> splHtmlContents =
            createSplHtml(splXml)
            .map(FileUtils::readFileContents);

        String splXmlContents = readFileContents(splXml);

        long splId =
            insertSplAndDependents(
                splXmlContents,
                splHtmlContents,
                getBaseName(splDirEntryName),
                fileName(splXml),
                splMd5.getLcHex(),
                loadNum
            );


        // Insert attachments.
        for ( Path splAtt: splAtts )
        {
            byte[] attBytes = readFileToByteArray(splAtt.toFile());

            String insertSql = "insert into spl_att(spl_id, name, data) values(?, ?, ?)";

            jdbcTemplate.getJdbcOperations().update(conn -> {
                PreparedStatement ps = conn.prepareStatement(insertSql);
                ps.setLong(1, splId);
                ps.setString(2, fileName(splAtt));
                lobHandler.getLobCreator().setBlobAsBytes(ps, 3, attBytes);
                return ps;
            });
        }
    }

    private long insertSplAndDependents
        (
            String splXml,
            Optional<String> splHtml,
            String splDirEntryBsename,
            String splXmlFilename,
            String splMd5LcHex,
            long loadNum
        )
    {
        return
            jdbcTemplate.getJdbcOperations().execute(conn -> {
                CallableStatement cs = conn.prepareCall("{call spl_load_2_0.insert_spl_and_deps(?,?,?,?,?,?,?,?)}");
                lobHandler.getLobCreator().setClobAsString(cs, 1, splXml);
                lobHandler.getLobCreator().setClobAsString(cs, 2, splHtml.orElse(null));
                cs.setString(3, splDirEntryBsename);
                cs.setString(4, splXmlFilename);
                cs.setString(5, splMd5LcHex);
                cs.setLong(6, loadNum);
                cs.setInt(7, includeDgv);
                cs.registerOutParameter(8, Types.NUMERIC);
                return cs;
            },
            (CallableStatementCallback<Long>) cs -> {
                cs.execute();
                return cs.getLong(8);
            });
    }

    // Writes all spl md5s for spls within the passed archives, and returns the *new* md5s (only) by their spl directory entry in the archive.
    private Map<Md5,SplDirEntry> registerLoadMd5s
        (
            List<Path> zipArchives,
            long loadNum
        )
        throws IOException
    {
        log.info("Computing md5s for this load from archive files.");
        Map<Md5,SplDirEntry> splDirEntriesByMd5 = getSplDirectoryEntriesByMd5(zipArchives);

        log.info("Writing spl md5 checksums.");

        int count = 0;
        int batchSize = 1000;

        List<Object[]> flatBatchParams = new ArrayList<>();
        for ( Md5 md5: splDirEntriesByMd5.keySet() )
            flatBatchParams.add(new Object[] {loadNum, md5.getLcHex()});

        for ( List<Object[]> batchParams: partition(flatBatchParams, batchSize) )
        {
            jdbcTemplate.getJdbcOperations().batchUpdate(
                "insert into load_md5(load_num, spl_md5) values(?, ?)",
                batchParams
            );
            count += batchParams.size();
        }

        log.info(count + " md5 checksums written.");

        return splDirEntriesByMd5;
    }


    private Map<SplDirEntry,Md5> getNewMd5sBySplDirectoryEntry
        (
            long loadNum,
            Map<Md5,SplDirEntry> splEntriesByMd5
        )
    {
        Map<SplDirEntry,Md5> m = new HashMap<>();

        for( Md5 md5: getNewMd5s(loadNum) )
            m.put(splEntriesByMd5.get(md5), md5);

        return m;
    }

    private List<Md5> getNewMd5s(long loadNum)
    {
        return jdbcTemplate.query(
            "select spl_md5 from load_md5 where load_num = :load_num minus select spl_md5 from spl",
            params("load_num", loadNum),
            getMd5ColumnRowMapper()
        );
    }

    private Set<String> getSkipSplDirectoryEntryNames()
    {
        List<String> names =
            jdbcTemplate.query(
              "select zip_entry_name from skip_import_entry",
              (rs, rowNum) -> rs.getString(1)
            );
        return new HashSet<>(names);
    }

    private Map<Md5,SplDirEntry> getSplDirectoryEntriesByMd5(List<Path> zipArchives) throws IOException
    {
        Map<Md5,SplDirEntry> splEntriesByMd5 = new HashMap<>();

        for ( Path zipArchive: zipArchives )
        {
            visitZipEntries(zipArchive, OMIT_DIRS, (zippedSplDirZe, zis) ->
                visitZipEntries(new ZipInputStream(zis), OMIT_DIRS, (splFileZe, innerZis) -> {
                    if ( splFileZe.getName().endsWith(".xml") )
                    {
                        Md5 md5 = Md5.fromBytes(IOUtils.toByteArray(innerZis));
                        splEntriesByMd5.put(md5, new SplDirEntry(zipArchive, zippedSplDirZe.getName()));
                    }
                })
            );
        }

        return splEntriesByMd5;
    }

    private Pair<Path,List<Path>> writeSplFileEntriesToDir
        (
            Path tmpDir,
            String splDirEntryName,
            ZipInputStream zis
        )
        throws IOException
    {
        List<Path> splXmlHolder = new ArrayList<>();
        List<Path> attachments = new ArrayList<>();

        visitZipEntries(new ZipInputStream(zis), OMIT_DIRS, (splFileZe, innerZis) -> {
            Path f = tmpDir.resolve(splFileZe.getName());
            if ( fileName(f).toLowerCase().endsWith(".xml") )
                splXmlHolder.add(f);
            else
                attachments.add(f);

            try ( OutputStream fos = new FileOutputStream(f.toFile()) )
            {
                IOUtils.copy(innerZis, fos);
            }
        });

        if ( splXmlHolder.size() == 0 )
            log.error("No spl xml file found for archive entry " + splDirEntryName + ", this entry will be skipped.");
        else if ( splXmlHolder.size() > 1 )
            log.error("Multiple spl xml files found for archive entry " + splDirEntryName + ": spl will be chosen arbitrarily from these.");

        return Pair.make(splXmlHolder.size() == 1 ? splXmlHolder.get(0) : null, attachments);
    }

    private void finalizeLoadRecord(long loadNum)
    {
        log.info("Finalizing load.");
        jdbcTemplate.update(
            "update load set finished_ts = systimestamp where load_num = :load_num",
            params("load_num", loadNum)
        );
    }

    private void writeRecordCounts(long loadNum)
    {
        log.info("Writing record counts.");
        jdbcTemplate.update(
            "call spl_load_2_0.write_record_counts(:load_num)",
            params("load_num", loadNum)
        );
    }

    private void deleteSummaryData()
    {
        log.info("Deleting summary data.");
        jdbcTemplate.update(
            "call spl_load_2_0.delete_summary_data(:include_dgv)",
            params("include_dgv", includeDgv)
        );
    }

    private void createSummaryData()
    {
        log.info("Refreshing summary data.");
        jdbcTemplate.update(
            "call spl_load_2_0.create_summary_data(:include_dgv)",
            params("include_dgv", includeDgv)
        );
    }

    private void removeSplsNotInLoad
        (
            long loadNum,
            int maxRemovals,
            boolean keepRemoved
        )
    {
        if ( keepRemoved )
        {
            log.info("Archiving and removing spls not found in imported files.");
            jdbcTemplate.getJdbcOperations().update(
                "call spl_load_2_0.archive_spls_not_in_load(?,?,?)",
                loadNum, includeDgv, maxRemovals);
        }
        else
        {
            log.info("Removing spls not found in imported files.");
            jdbcTemplate.getJdbcOperations().update(
                "call spl_load_2_0.remove_spls_not_in_load(?,?,?)",
                loadNum, includeDgv, maxRemovals
            );
        }
    }

    private RowMapper<Md5> getMd5ColumnRowMapper()
    {
        return (rs, rowNum) -> new Md5(rs.getString(1));
    }

    private int getNextLoadNumber()
    {
        int num = jdbcTemplate.getJdbcOperations().queryForObject(
            "select case when max(load_num) is not null then max(load_num) + 1 else 1 end from load",
            Integer.class
        );
        log.info("Load number is " + num + "");

        return num;
    }

    private long createLoadRecord(java.sql.Date srcDate)
    {
        long loadNum = getNextLoadNumber();

        jdbcTemplate.update(
            "insert into load(load_num, started_ts, source, source_fetch_dt) values(:load_num, systimestamp, :src, :src_date)",
            params("load_num", loadNum,
                   "src", "dailymed",
                   "src_date", srcDate)
        );

        return loadNum;
    }

    private Optional<Path> createSplHtml(Path splXml)
    {
        if ( !htmlGenCtx.isPresent() )
            return Optional.empty();

   		try
        {
            HtmlGenerator htmlGen = this.htmlGenCtx.get();
            Path msxslExe = htmlGen.getMsxslExe();
            Path splXsl = htmlGen.getSplXsl();

   			Path htmlFile = splXml.getParent().resolve(getBaseName(fileName(splXml)) + ".html");

   			Process procBldr =
                new ProcessBuilder(
                    msxslExe.toAbsolutePath().toString(),
   					fileName(splXml),
                    splXsl.toAbsolutePath().toString(),
                    "-o",
   					fileName(htmlFile)
                )
                .directory(splXml.getParent().toFile())
   				.redirectErrorStream(true)
                .start();

   			InputStreamReader procOutRdr = new InputStreamReader(procBldr.getInputStream());
   			BufferedReader br = new BufferedReader(procOutRdr);
   			String lineRead;
   			while ( (lineRead = br.readLine()) != null )
   				log.info(lineRead);

   			int retc = procBldr.waitFor();
   			if (retc != 0)
   				log.error("Warning: xslt process returned error code for spl " + fileName(splXml));

   			return Files.isRegularFile(htmlFile) ? Optional.of(htmlFile) : Optional.empty();
   		}
   		catch (Exception e)
        {
   			throw new RuntimeException(e);
   		}
   	}

    private void syncContextIndexes()
    {
        log.info("Syncing context indexes.");

        jdbcTemplate.getJdbcOperations().update("call spl_load_2_0.sync_context_indexes()");

        log.info("Finished syncing of context indexes.");
    }

    private void updateMeddraTermOccurrences(Optional<Path> meddraOccurrencesFile)
    {
        ifPresentElse(meddraOccurrencesFile,
            this::loadMeddraTermOccurrencesFromFile,
            this::updateMeddraTermOccurrencesViaOracleText
        );
    }

    private void loadMeddraTermOccurrencesFromFile(Path meddraOccurrencesFile)
    {
        try
        {
            log.info("Loading MedDRA term occurrences from LLT occurrences file \""
                     + meddraOccurrencesFile + "\".");

            List<FieldMapping> fieldMappings = Arrays.asList(
                new FieldMapping(0, "llt_code",     JDBCType.VARCHAR),
                new FieldMapping(1, "spl_set_id",   JDBCType.VARCHAR),
                new FieldMapping(2, "section_type", JDBCType.VARCHAR),
                new FieldMapping(3, "assigned_by",  JDBCType.INTEGER)
            );

            long numLltInserts = DatabaseOps.loadTableFromCsvFile(
                "meddra_llt_spl_coded_sec",
                fieldMappings,
                meddraOccurrencesFile,
                true,
                true,
                3000,
                jdbcTemplate.getJdbcTemplate().getDataSource().getConnection()
            );

            log.info("Inserted " + numLltInserts + " MedDRA term occurrence records into " +
                     "table meddra_llt_spl_coded_sec.");

            log.info("Refreshing MedDRA preferred term occurrences and term occurrence counts.");

            jdbcTemplate.update("call meddralink.refresh_from_sec_llts", params());

            log.info("Finished loading MedDRA term occurrences.");
        }
        catch(SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    private void updateMeddraTermOccurrencesViaOracleText()
    {
        log.info("Starting refresh of MedDRA term occurrences data via Oracle Text.");

        jdbcTemplate.getJdbcOperations().update("call meddralink.refresh_meddra_linkage_data()");

        log.info("Done refreshing MedDRA term ocurrences data via Oracle Text.");
    }

    private List<String> getStringFieldValues(String table, String field)
    {
        return jdbcTemplate.queryForList(
            "select " + field + " from " + table,
            Collections.emptyMap(),
            String.class
        );
    }


    ///////////////////////////////////////////////////////////////////
    // Inner Classes

    public static class LoadingSpecification
    {
        private Path archivesDir;
        private Path jdbcPropsLoc;
        private Path appDataDir;
        private boolean downloadArchives;
        private boolean importTerminologies;
        private boolean keepRemoved;
        private int maxRemoved;
        private int maxFailures;
        private Optional<Path> meddraOccurrencesFile;
        private Optional<Path> pharmClassesSourceFilesDir;
        private boolean downloadPharmClassesSourceFiles;
        private boolean clearDeprecatedPharmClassTables;
        private Optional<HtmlGenerator> htmlGenerator;
        private boolean doDgvUpdates;

        public LoadingSpecification
            (
                Path archivesDir,
                Path jdbcPropsLoc,
                Path appDataDir,
                boolean downloadArchives,
                boolean importTerminologies,
                boolean keepRemoved,
                int maxRemoved,
                int maxFailures,
                Optional<Path> meddraOccurrencesFile,
                Optional<Path> pharmClassesSourceFilesDir,
                boolean downloadPharmClassesSourceFiles,
                boolean clearDeprecatedPharmClassTables,
                Optional<HtmlGenerator> htmlGenerator,
                boolean doDgvUpdates
            )
        {
            this.archivesDir = archivesDir;
            this.jdbcPropsLoc = jdbcPropsLoc;
            this.appDataDir = appDataDir;
            this.downloadArchives = downloadArchives;
            this.importTerminologies = importTerminologies;
            this.keepRemoved = keepRemoved;
            this.maxRemoved = maxRemoved;
            this.maxFailures = maxFailures;
            this.meddraOccurrencesFile = meddraOccurrencesFile;
            this.pharmClassesSourceFilesDir = pharmClassesSourceFilesDir;
            this.downloadPharmClassesSourceFiles = downloadPharmClassesSourceFiles;
            this.clearDeprecatedPharmClassTables = clearDeprecatedPharmClassTables;
            this.htmlGenerator = htmlGenerator;
            this.doDgvUpdates = doDgvUpdates;

            if ( downloadPharmClassesSourceFiles && !pharmClassesSourceFilesDir.isPresent() )
                throw new RuntimeException("Pharm classes source files directory must be specified when downloading source files.");
        }

        public Path getArchivesDir() { return archivesDir; }

        public Path getJdbcPropsPath() { return jdbcPropsLoc; }

        public Path getAppDataDir() { return appDataDir; }

        public boolean getDownloadArchives() { return downloadArchives; }

        public boolean getImportTerminologies() { return importTerminologies; }

        public boolean getKeepRemoved() { return keepRemoved; }

        public int getMaxRemoved() { return maxRemoved; }

        public int getMaxFailures() { return maxFailures; }

        public Optional<Path> getMeddraOccurrencesFile() { return meddraOccurrencesFile; }

        public Optional<Path> getPharmClassesSourceFilesDir() { return pharmClassesSourceFilesDir; }

        public boolean getDownloadPharmClassesSourceFiles() { return downloadPharmClassesSourceFiles; }

        public boolean getClearDeprecatedPharmClassTables() { return clearDeprecatedPharmClassTables; }

        public Optional<HtmlGenerator> getHtmlGenerator() { return htmlGenerator; }

        public boolean getIncludeDgvUpdates() { return doDgvUpdates; }
    }
}


class HtmlGenerator
{
    private final Path msxslExe;
    private final Path splXsl;

    HtmlGenerator(Path msxslExe, Path splXsl)
    {
        this.msxslExe = msxslExe;
        this.splXsl = splXsl;
    }

    public Path getMsxslExe() { return msxslExe; }

    public Path getSplXsl() { return splXsl; }
}
