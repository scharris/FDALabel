package gov.fda.nctr.fdalabel;

import java.nio.file.Path;


public class SplDirEntry {

    private Path archiveFile;

    private String splDirEntryName;

    public SplDirEntry(Path archiveFile, String splDirEntryName)
    {
        super();
        this.archiveFile = archiveFile;
        this.splDirEntryName = splDirEntryName;
    }


    public Path getArchiveFile()
    {
        return archiveFile;
    }


    public String getSplDirEntryName()
    {
        return splDirEntryName;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((splDirEntryName == null) ? 0 : splDirEntryName.hashCode());
        result = prime * result + ((archiveFile == null) ? 0 : archiveFile.hashCode());
        return result;
    }


    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SplDirEntry other = (SplDirEntry) obj;
        if (splDirEntryName == null)
        {
            if (other.splDirEntryName != null)
                return false;
        }
        else if (!splDirEntryName.equals(other.splDirEntryName))
            return false;
        if (archiveFile == null)
        {
            if (other.archiveFile != null)
                return false;
        }
        else if (!archiveFile.equals(other.archiveFile))
            return false;
        return true;
    }
}
