package gov.fda.nctr.fdalabel.terminologies;


public class TerminologiesException extends RuntimeException
{
    public TerminologiesException(String message) { super(message); }

    public TerminologiesException(String message, Throwable cause) { super(message, cause); }
}

