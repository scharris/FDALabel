package gov.fda.nctr.fdalabel;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.JDBCType;
import java.sql.SQLException;
import java.util.*;
import static java.util.Collections.emptyMap;
import javax.sql.DataSource;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import com.univocity.parsers.csv.CsvFormat;

import gov.fda.nctr.util.DatabaseOps.*;
import gov.fda.nctr.util.DatabaseOps;
import static gov.fda.nctr.util.SpringJdbc.makeDataSource;
import static gov.fda.nctr.util.FileUtils.checkDirectoriesExist;
import static gov.fda.nctr.util.FileUtils.checkFilesExist;
import static gov.fda.nctr.util.FileUtils.fileName;


public class MeddraReleaseLoader
{
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final CsvFormat csvFormat;

    private static final Logger log = LogManager.getLogger(MeddraReleaseLoader.class);


    public MeddraReleaseLoader
        (
            DataSource ds
        )
    {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(ds);

        csvFormat = new CsvFormat();
        csvFormat.setDelimiter('$');
        csvFormat.setQuote((char)0x1F); // 1F => ascii "unit separator"
        csvFormat.setQuoteEscape((char)0x1F);
        csvFormat.setCharToEscapeQuoteEscaping((char)0x1F);
    }

    private static String usage()
    {
        return
            "Expected arguments: [options] <meddra-MedAscii-dir> <jdbc-props-file>\n" +
                "    meddra-MedAscii-dir: the MedAscii data directory within the MedDRA release directory.\n" +
                "    jdbc-props-file: jdbc properties file location, with properties jdbc.driverClassName, jdbc.url, " +
                "jdbc.username, jdbc.password. Required unless download-archives option is specified.\n";
    }

    public static void main(String[] args)
    {
        boolean helpRequested = args.length == 1 && (args[0].equals("-h") || args[0].equals("--help"));
        if ( helpRequested )
        {
            System.out.println(usage());
            return;
        }

        try
        {
            log.info("MedDRA release import process started.");

            LoadingSpecification spec = makeLoadingSpecification(args);

            doLoad(spec);

            log.info("All done.");
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            log.error("Processing finished abnormally.");
        }
    }


    /// Process command line arguments to produce a loading specification, which
    /// fully describes the loading process to be performed.
    private static LoadingSpecification makeLoadingSpecification(String[] args)
    {
        List<String> remArgs = new ArrayList<>(Arrays.asList(args));

        // Remaining arguments should be the non-optional ones.

        if ( remArgs.size() != 2 )
            throw new IllegalArgumentException("Expected two non-option arguments.");

        Path asciiDataDir = Paths.get(remArgs.remove(0));
        Path jdbcPropsLoc = Paths.get(remArgs.remove(0));

        checkFilesExist(new Path[] {jdbcPropsLoc});
        checkDirectoriesExist(new Path[] {asciiDataDir});

        if ( !Files.isDirectory(asciiDataDir) )
            throw new RuntimeException("Directory 'MedAscii' not found within MedDRA release directory.");

        return new LoadingSpecification(asciiDataDir, jdbcPropsLoc);
    }

    public static void doLoad(LoadingSpecification spec) throws IOException
    {
        DataSource ds = makeDataSource(spec.getJdbcPropsPath());

        MeddraReleaseLoader loader = new MeddraReleaseLoader(ds);

        TransactionTemplate transactionTemplate = new TransactionTemplate(new DataSourceTransactionManager(ds));

        transactionTemplate.execute(txStatus -> {

            log.info("Connected to database.");

            loader.doDeletes();
            loader.doInserts(spec.getMeddraAsciiDataDir());

            log.info("Loading of MedDRA release data completed, performing final commit.");
            return "";
        });

        log.info("Transaction committed for MedDRA release data loading.");
    }

    private void doDeletes()
    {
        log.info("Deleting existing table records.");

        List<String> tables = Arrays.asList(
            "smq_content",
            "smq_list",
            "meddra_hierarchy",
            "soc_hlgt",
            "hlgt_hlt",
            "hlt_pt",
            "low_level_term",
            "preferred_term",
            "high_level_grouping_term",
            "high_level_term",
            "soc_intl_order",
            "soc_term"
        );

        for ( String table : tables )
        {
            int count = jdbcTemplate.update("delete from " + table, emptyMap());
            log.info("Deleted " + count + " rows from table " + table + ".");
        }
    }

    public void doInserts(Path meddraAsciiDataDir)
    {
        try
        {
            String ver = getMeddraVersion(meddraAsciiDataDir);
            log.info("MedDRA release version is '" + ver + "'.");

            insertSocTerms(meddraAsciiDataDir.resolve("soc.asc"));
            insertSocIntlOrderTerms(meddraAsciiDataDir.resolve("intl_ord.asc"));
            insertHighLevelTerms(meddraAsciiDataDir.resolve("hlt.asc"));
            insertHighLevelGroupingTerms(meddraAsciiDataDir.resolve("hlgt.asc"));
            insertPreferredTerms(meddraAsciiDataDir.resolve("pt.asc"));
            insertLowLevelTerms(meddraAsciiDataDir.resolve("llt.asc"));
            insertHltPtAssociations(meddraAsciiDataDir.resolve("hlt_pt.asc"));
            insertHlgtHltAssociations(meddraAsciiDataDir.resolve("hlgt_hlt.asc"));
            insertSocHlgtAssociations(meddraAsciiDataDir.resolve("soc_hlgt.asc"));
            insertMeddraHierarchyRecords(meddraAsciiDataDir.resolve("mdhier.asc"));
            insertSMQListRecords(meddraAsciiDataDir.resolve("smq_list.asc"));
            insertSMQContentRecords(meddraAsciiDataDir.resolve("smq_content.asc"));
        }
        catch(SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    private void insertSMQListRecords(Path smqListFile) throws SQLException
    {
        if ( !Files.isRegularFile(smqListFile) )
            throw new RuntimeException("File " + fileName(smqListFile) + " not found in MedDRA release.");

        log.info("Inserting SMQ List rows from file " + fileName(smqListFile) + ".");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(0, "smq_code", JDBCType.INTEGER),
            new FieldMapping(1, "smq_name", JDBCType.VARCHAR),
            new FieldMapping(2, "smq_level", JDBCType.INTEGER),
            new FieldMapping(3, "smq_description", JDBCType.VARCHAR),
            new FieldMapping(4, "smq_source", JDBCType.VARCHAR),
            new FieldMapping(5, "smq_note", JDBCType.VARCHAR),
            new FieldMapping(6, "meddra_version", JDBCType.VARCHAR),
            new FieldMapping(7, "status", JDBCType.VARCHAR),
            new FieldMapping(8,"smq_algorithm", JDBCType.VARCHAR)
        );

        long numInserted = DatabaseOps.loadTableFromDelimitedFile(
            "smq_list",
            fieldMappings,
            smqListFile,
            csvFormat,
            false,
            false,
            3000,
            jdbcTemplate.getJdbcTemplate().getDataSource().getConnection()
        );

        log.info("Inserted " + numInserted + " SMQ List rows.");
    }

    private void insertSMQContentRecords(Path smqContentFile) throws SQLException
    {
        if ( !Files.isRegularFile(smqContentFile) )
            throw new RuntimeException("File " + fileName(smqContentFile) + " not found in MedDRA release.");

        log.info("Inserting SMQ Content rows from file " + fileName(smqContentFile) + ".");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(0, "smq_code", JDBCType.INTEGER),
            new FieldMapping(1, "term_code", JDBCType.INTEGER),
            new FieldMapping(2, "term_level", JDBCType.INTEGER),
            new FieldMapping(3, "term_scope", JDBCType.INTEGER),
            new FieldMapping(4, "term_category", JDBCType.VARCHAR),
            new FieldMapping(5, "term_weight", JDBCType.INTEGER),
            new FieldMapping(6, "term_status", JDBCType.VARCHAR),
            new FieldMapping(7, "term_addition_version", JDBCType.VARCHAR),
            new FieldMapping(8, "term_last_modified_version", JDBCType.VARCHAR)
        );

        long numInserted = DatabaseOps.loadTableFromDelimitedFile(
            "smq_content",
            fieldMappings,
            smqContentFile,
            csvFormat,
            false,
            false,
            3000,
            jdbcTemplate.getJdbcTemplate().getDataSource().getConnection()
        );

        log.info("Inserted " + numInserted + " SMQ Content rows.");
    }

    private void insertMeddraHierarchyRecords(Path mdHierFile) throws SQLException
    {
        if ( !Files.isRegularFile(mdHierFile) )
            throw new RuntimeException("File " + fileName(mdHierFile) + " not found in MedDRA release.");

        log.info("Inserting MedDRA hierarchy rows from file " + fileName(mdHierFile) + ".");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(0, "pt_code",        JDBCType.INTEGER),
            new FieldMapping(1, "hlt_code",       JDBCType.INTEGER),
            new FieldMapping(2, "hlgt_code",      JDBCType.INTEGER),
            new FieldMapping(3, "soc_code",       JDBCType.INTEGER),
            new FieldMapping(4, "pt_name",        JDBCType.VARCHAR),
            new FieldMapping(5, "hlt_name",       JDBCType.VARCHAR),
            new FieldMapping(6, "hlgt_name",      JDBCType.VARCHAR),
            new FieldMapping(7, "soc_name",       JDBCType.VARCHAR),
            new FieldMapping(8, "soc_abbrev",     JDBCType.VARCHAR),
            new FieldMapping(9, "pt_soc_code",    JDBCType.INTEGER),
            new FieldMapping(10, "primary_soc_fg", JDBCType.VARCHAR)
        );

        long numInserted = DatabaseOps.loadTableFromDelimitedFile(
            "meddra_hierarchy",
            fieldMappings,
            mdHierFile,
            csvFormat,
            false,
            false,
            3000,
            jdbcTemplate.getJdbcTemplate().getDataSource().getConnection()
        );

        log.info("Inserted " + numInserted + " MedDRA hierarchy rows.");
    }

    private void insertSocHlgtAssociations(Path socHlgtFile) throws SQLException
    {
        if ( !Files.isRegularFile(socHlgtFile) )
            throw new RuntimeException("File " + fileName(socHlgtFile) + " not found in MedDRA release.");

        log.info("Inserting SOC-HLGT term associations from file " + fileName(socHlgtFile) + ".");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(0, "soc_code",     JDBCType.INTEGER),
            new FieldMapping(1, "hlgt_code",    JDBCType.INTEGER)
        );

        long numInserted = DatabaseOps.loadTableFromDelimitedFile(
            "soc_hlgt",
            fieldMappings,
            socHlgtFile,
            csvFormat,
            false,
            false,
            3000,
            jdbcTemplate.getJdbcTemplate().getDataSource().getConnection()
        );

        log.info("Inserted " + numInserted + " SOC-HLGT term associations.");
    }

    private void insertHlgtHltAssociations(Path hlgtHltFile) throws SQLException
    {
        if ( !Files.isRegularFile(hlgtHltFile) )
            throw new RuntimeException("File " + fileName(hlgtHltFile) + " not found in MedDRA release.");

        log.info("Inserting HLGT-HLT term associations from file " + fileName(hlgtHltFile) + ".");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(0, "hlgt_code",     JDBCType.INTEGER),
            new FieldMapping(1, "hlt_code",      JDBCType.INTEGER)
        );

        long numInserted = DatabaseOps.loadTableFromDelimitedFile(
            "hlgt_hlt",
            fieldMappings,
            hlgtHltFile,
            csvFormat,
            false,
            false,
            3000,
            jdbcTemplate.getJdbcTemplate().getDataSource().getConnection()
        );

        log.info("Inserted " + numInserted + " HLGT-HLT term associations.");
    }

    private void insertHltPtAssociations(Path hltPtFile) throws SQLException
    {
        if ( !Files.isRegularFile(hltPtFile) )
            throw new RuntimeException("File " + fileName(hltPtFile) + " not found in MedDRA release.");

        log.info("Inserting HLT-PT term associations from file " + fileName(hltPtFile) + ".");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(0, "hlt_code",     JDBCType.INTEGER),
            new FieldMapping(1, "pt_code",      JDBCType.INTEGER)
        );

        long numInserted = DatabaseOps.loadTableFromDelimitedFile(
            "hlt_pt",
            fieldMappings,
            hltPtFile,
            csvFormat,
            false,
            false,
            3000,
            jdbcTemplate.getJdbcTemplate().getDataSource().getConnection()
        );

        log.info("Inserted " + numInserted + " HLT-PT term associations.");
    }

    private void insertLowLevelTerms(Path lltFile) throws SQLException
    {
        if ( !Files.isRegularFile(lltFile) )
            throw new RuntimeException("File " + fileName(lltFile) + " not found in MedDRA release.");

        log.info("Inserting MedDRA low level terms from file " + fileName(lltFile) + ".");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(0, "llt_code",     JDBCType.INTEGER),
            new FieldMapping(1, "llt_name",     JDBCType.VARCHAR),
            new FieldMapping(2, "pt_code",      JDBCType.INTEGER),
            new FieldMapping(9, "llt_currency", JDBCType.VARCHAR)
        );

        long numInserted = DatabaseOps.loadTableFromDelimitedFile(
            "low_level_term",
            fieldMappings,
            lltFile,
            csvFormat,
            false,
            false,
            3000,
            jdbcTemplate.getJdbcTemplate().getDataSource().getConnection()
        );

        log.info("Inserted " + numInserted + " low level terms.");
    }

    private void insertPreferredTerms(Path ptFile) throws SQLException
    {
        if ( !Files.isRegularFile(ptFile) )
            throw new RuntimeException("File " + fileName(ptFile) + " not found in MedDRA release.");

        log.info("Inserting MedDRA preferred terms from file " + fileName(ptFile) + ".");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(0, "pt_code",     JDBCType.INTEGER),
            new FieldMapping(1, "pt_name",     JDBCType.VARCHAR),
            new FieldMapping(3, "pt_soc_code", JDBCType.INTEGER)
        );

        long numInserted = DatabaseOps.loadTableFromDelimitedFile(
            "preferred_term",
            fieldMappings,
            ptFile,
            csvFormat,
            false,
            false,
            3000,
            jdbcTemplate.getJdbcTemplate().getDataSource().getConnection()
        );

        log.info("Inserted " + numInserted + " preferred terms.");
    }

    private void insertHighLevelGroupingTerms(Path hlgtFile) throws SQLException
    {
        if ( !Files.isRegularFile(hlgtFile) )
            throw new RuntimeException("File " + fileName(hlgtFile) + " not found in MedDRA release.");

        log.info("Inserting MedDRA high level grouping terms from file " + fileName(hlgtFile) + ".");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(0, "hlgt_code",     JDBCType.INTEGER),
            new FieldMapping(1, "hlgt_name",     JDBCType.VARCHAR)
        );

        long numInserted = DatabaseOps.loadTableFromDelimitedFile(
            "high_level_grouping_term",
            fieldMappings,
            hlgtFile,
            csvFormat,
            false,
            false,
            3000,
            jdbcTemplate.getJdbcTemplate().getDataSource().getConnection()
        );

        log.info("Inserted " + numInserted + " high level grouping terms.");
    }

    private void insertHighLevelTerms(Path hltFile) throws SQLException
    {
        if ( !Files.isRegularFile(hltFile) )
            throw new RuntimeException("File " + fileName(hltFile) + " not found in MedDRA release.");

        log.info("Inserting MedDRA high level terms from file " + fileName(hltFile) + ".");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(0, "hlt_code",     JDBCType.INTEGER),
            new FieldMapping(1, "hlt_name",     JDBCType.VARCHAR)
        );

        long numInserted = DatabaseOps.loadTableFromDelimitedFile(
            "high_level_term",
            fieldMappings,
            hltFile,
            csvFormat,
            false,
            false,
            3000,
            jdbcTemplate.getJdbcTemplate().getDataSource().getConnection()
        );

        log.info("Inserted " + numInserted + " high level terms.");
    }

    private void insertSocIntlOrderTerms(Path intlOrdFile) throws SQLException
    {
        if ( !Files.isRegularFile(intlOrdFile) )
            throw new RuntimeException("File " + fileName(intlOrdFile) + " not found in MedDRA release.");

        log.info("Inserting SOC Order rows from file " + fileName(intlOrdFile) + ".");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(0, "intl_ord_code",  JDBCType.INTEGER),
            new FieldMapping(1, "soc_code",       JDBCType.INTEGER)
        );

        long numInserted = DatabaseOps.loadTableFromDelimitedFile(
            "soc_intl_order",
            fieldMappings,
            intlOrdFile,
            csvFormat,
            false,
            false,
            3000,
            jdbcTemplate.getJdbcTemplate().getDataSource().getConnection()
        );

        log.info("Inserted " + numInserted + " SOC order rows.");
    }

    private void insertSocTerms(Path socFile) throws SQLException
    {
        if ( !Files.isRegularFile(socFile) )
            throw new RuntimeException("File " + fileName(socFile) + " not found in MedDRA release.");

        log.info("Inserting MedDRA SOC terms from file " + fileName(socFile) + ".");

        List<FieldMapping> fieldMappings = Arrays.asList(
            new FieldMapping(0, "soc_code",     JDBCType.INTEGER),
            new FieldMapping(1, "soc_name",     JDBCType.VARCHAR),
            new FieldMapping(2, "soc_abbrev",   JDBCType.VARCHAR)
        );

        long numInserted = DatabaseOps.loadTableFromDelimitedFile(
            "soc_term",
            fieldMappings,
            socFile,
            csvFormat,
            false,
            false,
            3000,
            jdbcTemplate.getJdbcTemplate().getDataSource().getConnection()
        );

        log.info("Inserted " + numInserted + " SOC terms.");
    }

    private static String getMeddraVersion(Path medAsciiDir)
    {
        try
        {
            return Files.readAllLines(medAsciiDir.resolve("meddra_release.asc")).get(0).split("\\$")[0];
        }
        catch(Throwable e)
        {
            throw new RuntimeException("Failed to read MedDRA release file to obtain release version.");
        }
    }


    ///////////////////////////////////////////////////////////////////
    // Inner Classes

    public static class LoadingSpecification
    {
        private Path meddraAsciiDataDir;
        private Path jdbcPropsLoc;

        public LoadingSpecification
            (
                Path meddraAsciiDataDir,
                Path jdbcPropsLoc
            )
        {
            this.meddraAsciiDataDir = meddraAsciiDataDir;
            this.jdbcPropsLoc = jdbcPropsLoc;
        }

        public Path getMeddraAsciiDataDir() { return meddraAsciiDataDir; }

        public Path getJdbcPropsPath() { return jdbcPropsLoc; }
    }
}

