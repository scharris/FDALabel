import xml._
import java.io._
import java.util.zip.{ZipEntry, ZipInputStream}


object PharmClassesExtractor extends App {

  val inputArchive = new File(args(0))
  val outputFile = new File(args(1))

  val os = new BufferedWriter(new FileWriter(outputFile))

  ZipUtils.processZipEntries(inputArchive, includeDirs = false) { (zeOuter: ZipEntry, zisOuter: ZipInputStream) =>
    ZipUtils.processZipEntries(new ZipInputStream(zisOuter), includeDirs = false) { (ze: ZipEntry, zis: ZipInputStream) =>

      val xml = XML.loadString(io.Source.fromInputStream(zis).mkString)

      val subs = xml \ "component" \ "structuredBody" \ "component" \ "section" \ "subject" \ "identifiedSubstance" \ "identifiedSubstance"

      val unii = subs \ "code" \ "@code" text

      val codeNodes = subs \ "asSpecializedKind" \ "generalizedMaterialKind" \ "code"

      //if (codeNodes.size == 0)
      //  throw new RuntimeException("No codes found in spl indexing entry " + ze.getName + "")

      if (codeNodes.size != 0) {
        for (codeEl <- codeNodes) {
          val ndfrtCode = (codeEl \ "@code").text
          os.write(unii + "," + ndfrtCode + "\n")
        }
      }
    }
  }

  os.flush()
  os.close()
}


object ZipUtils {

  def processZipEntries(zis: ZipInputStream, includeDirs: Boolean)(entryAction: (ZipEntry, ZipInputStream) => Unit): Unit = {
    var ze: ZipEntry = zis.getNextEntry()
    while (ze != null) {
      if (includeDirs || !ze.isDirectory)
        entryAction(ze, zis)
      ze = zis.getNextEntry()
    }
  }

  def processZipEntries(zipArchive: File, includeDirs: Boolean)(entryAction: (ZipEntry, ZipInputStream) => Unit): Unit = {
    val zis = new ZipInputStream(new FileInputStream(zipArchive))
    processZipEntries(zis, includeDirs)(entryAction)
    zis.close()
  }

}
