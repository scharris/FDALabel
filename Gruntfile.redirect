'use strict';
/*
var lrSnippet = require('grunt-contrib-livereload/lib/utils').livereloadSnippet;
var mountFolder = function (connect, dir) {
  return connect.static(require('path').resolve(dir));
};
*/
module.exports = function (grunt) {
  // load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  // configurable paths
  var yeomanConfig = {
    app: 'app',
    dist: 'services/target/fdalabel/ui'
  };

  try {
    yeomanConfig.app = require('./component.json').appPath || yeomanConfig.app;
  } catch (e) {}

  grunt.initConfig({
    yeoman: yeomanConfig,
    watch: {
//      coffee: {
//        files: ['<%= yeoman.app %>/scripts/{,*/}*.coffee'],
//        tasks: ['coffee:dist']
//      },
//      coffeeTest: {
//        files: ['test/spec/{,*/}*.coffee'],
//        tasks: ['coffee:test']
//      },
//      compass: {
//        files: ['<%= yeoman.app %>/styles/{,*/}*.{scss,sass}'],
//        tasks: ['compass']
//      },
      autobuild: {
        files: [
          '<%= yeoman.app %>/{,*/}*.html',
          '{.tmp,<%= yeoman.app %>}/styles/{,*/}*.css',
          '{.tmp,<%= yeoman.app %>}/scripts/{,*/}*.js',
          '<%= yeoman.app %>/img/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ],
        tasks: ['buildFast']
      }
    },
    open: {
      server: {
        url: 'http://localhost:<%= connect.options.port %>'
      }
    },
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/*',
            '!<%= yeoman.dist %>/.git*'
          ]
        }]
      },
      server: '.tmp'
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'Gruntfile.js',
        '<%= yeoman.app %>/scripts/{,*/}*.js'
      ]
    },
/*    karma: {
      unit: {
        configFile: 'karma.conf.js',
        singleRun: true
      }
    },*/
//    coffee: {
//      dist: {
//        files: [{
//          expand: true,
//          cwd: '<%= yeoman.app %>/scripts',
//          src: '{,*/}*.coffee',
//          dest: '.tmp/scripts',
//          ext: '.js'
//        }]
//      },
//      test: {
//        files: [{
//          expand: true,
//          cwd: 'test/spec',
//          src: '{,*/}*.coffee',
//          dest: '.tmp/spec',
//          ext: '.js'
//        }]
//      }
//    },
//    compass: {
//      options: {
//        sassDir: '<%= yeoman.app %>/styles',
//        cssDir: '.tmp/styles',
//        imagesDir: '<%= yeoman.app %>/img',
//        javascriptsDir: '<%= yeoman.app %>/scripts',
//        fontsDir: '<%= yeoman.app %>/styles/fonts',
//        importPath: '<%= yeoman.app %>/components',
//        relativeAssets: true
//      },
//      dist: {},
//      server: {
//        options: {
//          debugInfo: true
//        }
//      }
//    },
    // compile angular view templates into the angular $templateCache in js to avoid unnecessary ajax calls
    ngtemplates: {
      FDALabelApp: { // module name into which template cache entries will be added
        options: {
          base: '<%= yeoman.dist %>', // $templateCache id's will be relative to this folder
          concat: 'services/target/fdalabel/ui/scripts/scripts.js' // add the dest to the "concat" target to be done later
        },
        src:  '<%= yeoman.dist %>/views/**.html',
        dest: '.tmp/scripts/ngtemplate.js' //'.tmp/scripts/view-templates.js'
      }
    },
// This apparently doesn't do anything.  The actual concat target either default or are generated dynamically somehow.
//    concat: {
//      dist: {
//        files: {
//          '<%= yeoman.dist %>/scripts/scripts.js': [
//            '.tmp/scripts/*.js',
//            '<%= yeoman.app %>/scripts/{,*/}*.js'
//          ]
//        }
//      }
//    },
    useminPrepare: {
      html: '<%= yeoman.app %>/index.html',
      options: {
        dest: '<%= yeoman.dist %>'
      }
    },
    usemin: {
      html: ['<%= yeoman.dist %>/*.html'], // was {,*/}*.html, but we don't need to process views/*.html which will already be in js via ngtemplates
      css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
      options: {
        dirs: ['<%= yeoman.dist %>']
      }
    },
//    No need to minimize images, the images are copied to the target in the copy stage instead.
//    imagemin: {
//      dist: {
//        files: [{
//          expand: true,
//          cwd: '<%= yeoman.app %>/img',
//          src: '{,*/}*.{png,jpg,jpeg}',
//          dest: '<%= yeoman.dist %>/img'
//        }]
//      }
//    },
    cssmin: {
      dist: {
        files: {
          '<%= yeoman.dist %>/styles/main.css': [
            '.tmp/styles/{,*/}*.css',
            '<%= yeoman.app %>/styles/{,*/}*.css'
          ]
        }
      }
    },
    htmlmin: {
      dist: {
        options: {
          /*removeCommentsFromCDATA: true,
          // https://github.com/yeoman/grunt-usemin/issues/44
          //collapseWhitespace: true,
          collapseBooleanAttributes: true,
          removeAttributeQuotes: true,
          removeRedundantAttributes: true,
          useShortDoctype: true,
          removeEmptyAttributes: true,
          removeOptionalTags: true*/
        },
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>',
          src: ['views/*.html'],
          dest: '<%= yeoman.dist %>'
        }]
      }
    },
    cdnify: {
      dist: {
        html: ['<%= yeoman.dist %>/*.html']
      }
    },
    ngmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>/scripts',
          src: '*.js',
          dest: '<%= yeoman.dist %>/scripts'
        }]
      }
    },
    uglify: {
      dist: {
        files: {
          '<%= yeoman.dist %>/scripts/scripts.js': [
            '<%= yeoman.dist %>/scripts/scripts.js'
          ]
        }
      }
    },
    rev: {
      dist: {
        files: {
          src: [
            '<%= yeoman.dist %>/scripts/{,*/}*.js',
            '<%= yeoman.dist %>/styles/{,*/}*.css',
//            '<%= yeoman.dist %>/img/{,*/}*.{png,jpg,jpeg,gif,webp,svg}', // don't rev images, because then they can't practically be referred to in the cached angular templates injected into js.
            '<%= yeoman.dist %>/styles/fonts/*'
          ]
        }
      }
    },
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,txt,html}',
            '.htaccess',
            'components/**/*',
            'img/{,*/}*.{png,jpg,jpeg,gif,webp,svg,gif,webp}',
            'styles/fonts/*'
          ]
        }]
      }
    }
  });

  grunt.renameTask('regarde', 'watch');

  grunt.registerTask('server', [
    'clean:server',
    //'coffee:dist',
    //'compass:server',
    'watch'
  ]);
/*
  grunt.registerTask('test', [
    'clean:server',
    // 'coffee',
    //'compass',
    'karma'
  ]);
*/
  grunt.registerTask('buildFast', [
    'clean:dist',
    //'coffee',
    //'compass:dist',
    'useminPrepare',
//    'imagemin',
    'cssmin',
    'htmlmin',
    'ngtemplates',
    'concat',
    'copy',
    'cdnify',
    'ngmin',
    'uglify',
    'rev',
    'usemin'
  ]);

  grunt.registerTask('build', [
    'clean:dist',
    'jshint',
   // 'test',
    // 'coffee',
    //'compass:dist',
    'useminPrepare',
//    'imagemin',
    'cssmin',
    'htmlmin',
    'ngtemplates',
    'concat',
    'copy',
    'cdnify',
    'ngmin',
    'uglify',
    'rev',
    'usemin'
  ]);

  grunt.registerTask('default', ['build']);
};
