'use strict';

describe('Controller MeddraCriterionCtrl', function () {
/*
  // load the controller's module
  beforeEach(module('FDALabelApp'));

  var $controller, $httpBackend, $rootScope, $scope;

  var oloPrefTerms = [
    {term: "Coloboma", splCount: 10},
    {term: "Colour blindness", splCount: 0},
    {term: "Clot retraction time prolonged", splCount: 2},
    {term: "Nail discolouration", splCount: 234},
    {term: "Psychological abuse", splCount: 0}
  ];
  var eyePrefTerms = [
    {term: "Biopsy eyelid", splCount:1},
    {term: "Hypotony of eye", splCount: 23}
  ];
  var oloLowLevelTerms = [
    {term: "Coloboma", splCount: 10},
    {term: "Colour blindness", splCount: 0},
    {term: "Clot retraction time prolonged", splCount: 2},
    {term: "Nail discolouration", splCount: 234},
    {term: "Psychological abuse", splCount: 0},
    {term: "APTT prolonged", splCount: 1},
    {term: "Acquired color vision deficiencies", splCount: 0}
  ];
  var eyeLowLevelTerms = [
    {term: "Biopsy eyelid", splCount:1},
    {term: "Hypotony of eye", splCount: 23},
    {term: "Bacterial eye infection", splCount: 20}
  ];

  beforeEach(inject(function($injector) {
    $httpBackend = $injector.get('$httpBackend');
    $controller = $injector.get('$controller');
    $rootScope = $injector.get('$rootScope')
    $scope = $rootScope.$new();
    $scope.criterion = {terms: []};

    // mock the meddra terms typeahead service
    $httpBackend.when('GET', '/fdalabel/services/meddra/containing/olo?a=0&l=100&tt=pt&unlab=0').respond(oloPrefTerms);
    $httpBackend.when('GET', '/fdalabel/services/meddra/containing/olo?a=0&l=100&tt=llt&unlab=0').respond(oloLowLevelTerms);
    $httpBackend.when('GET', '/fdalabel/services/meddra/containing/eye?a=0&l=100&tt=pt&unlab=0').respond(eyePrefTerms);
    $httpBackend.when('GET', '/fdalabel/services/meddra/containing/eye?a=0&l=100&tt=llt&unlab=0').respond(eyeLowLevelTerms);
  }));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  // Tests
  it("is initialized properly", function() {

    $controller('MeddraCriterionCtrl', {$scope: $scope});

    expect($scope.criterion.terms.length).toEqual(0);
    expect($scope.enteredTerm).toBeFalsy();
    expect($scope.preferredTermsOnly).toBe(false);
  });


  it("should access typeahead service and add terms correctly for preferred terms containing 'olo'", function() {

    var ctrl = $controller('MeddraCriterionCtrl', {$scope: $scope});

    $scope.preferredTermsOnly = true;
    var choice_ix = 0;

    console.log($scope.termsMatching('olo'));

    $scope.termsMatching('olo').then(function(termInfos) {
      $scope.enteredTerm = termInfos[choice_ix];
    });

    expect($scope.criterion.terms.length).toEqual(0);

    $httpBackend.flush();

    expect($scope.criterion.terms.length).toEqual(1);
    expect($scope.criterion.terms[0].term).toEqual('Coloboma');
  });

  it("should access typeahead service and add terms correctly for preferred terms containing 'eye'", function() {

    var ctrl = $controller('MeddraCriterionCtrl', {$scope: $scope});

    $scope.preferredTermsOnly = true;
    var choice_ix = 1;

    $scope.termsMatching('eye').then(function(termInfos) {
      $scope.enteredTerm = termInfos[choice_ix];
    });

    expect($scope.criterion.terms.length).toEqual(0);

    $httpBackend.flush();

    expect($scope.criterion.terms.length).toEqual(1);
    expect($scope.criterion.terms[0].term).toEqual('Hypotony of eye');
  });

  it("should access typeahead service and add terms correctly for low level terms containing 'olo'", function() {

    var ctrl = $controller('MeddraCriterionCtrl', {$scope: $scope});

    $scope.preferredTermsOnly = false;
    var choice_ix = 5;

    $scope.termsMatching('olo').then(function(termInfos) {
      $scope.enteredTerm = termInfos[choice_ix];
    });

    expect($scope.criterion.terms.length).toEqual(0);

    $httpBackend.flush();

    expect($scope.criterion.terms.length).toEqual(1);
    expect($scope.criterion.terms[0].term).toEqual('APTT prolonged');
  });

  it("should access typeahead service and add terms correctly for low level terms containing 'eye'", function() {

    var ctrl = $controller('MeddraCriterionCtrl', {$scope: $scope});

    $scope.preferredTermsOnly = false;
    var choice_ix = 2;

    $scope.termsMatching('eye').then(function(termInfos) {
      $scope.enteredTerm = termInfos[choice_ix];
    });

    expect($scope.criterion.terms.length).toEqual(0);

    $httpBackend.flush();

    expect($scope.criterion.terms.length).toEqual(1);
    expect($scope.criterion.terms[0].term).toEqual('Bacterial eye infection');
  });
*/
});
