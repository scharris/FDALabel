#!/bin/sh
echo Building client.
echo grunt
echo
grunt && (
  (grunt server)&
  cd services
  echo
  echo
  echo Building and running server.
  echo mvn tomcat7:run-war
  echo
  mvn tomcat7:run-war
)
