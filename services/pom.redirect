<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <name>fdalabel-services</name>

    <groupId>gov.fda.nctr</groupId>
    <artifactId>fdalabel</artifactId>
    <version>1.0-SNAPSHOT</version>

    <packaging>war</packaging>


    <properties>
        <jersey.version>2.1</jersey.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <webapp.filter>development</webapp.filter> <!-- default settings are for development, can be overridden via a profile -->
        <webcontainer.context.docbase.attr>docBase="${project.build.directory}/${project.build.finalName}"</webcontainer.context.docbase.attr> <!-- for development -->
    </properties>

    <build>
        <finalName>fdalabel</finalName>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>2.5.1</version>
                <inherited>true</inherited>
                <configuration>
                    <source>1.7</source>
                    <target>1.7</target>
                </configuration>
            </plugin>
            <plugin>
	        <groupId>com.coderplus.maven.plugins</groupId>
	        <artifactId>copy-rename-maven-plugin</artifactId>
	        <version>1.0.1</version>
	        <executions>
	            <execution>
		        <id>copy-file</id>
		        <phase>generate-sources</phase>
		        <goals>
		            <goal>copy</goal>
		        </goals>
		        <configuration>
		            <sourceFile>${release.homepage}</sourceFile>
		            <destinationFile>../app/index.html</destinationFile>
		        </configuration>
	            </execution>
	       </executions>
	    </plugin>
            <!-- Configure war packaging step to filter the xml files in WEB-INF and META-INF. -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>2.4</version>
                <executions>
                    <execution>
                        <id>war</id>
                        <phase>package</phase>
                        <goals>
                            <goal>war</goal>
                        </goals>
                        <configuration>
                            <webResources>
                                <resource>
                                    <directory>src/main/webapp</directory>
                                    <includes>
                                        <include>META-INF/context.xml</include>
                                    </includes>
                                    <filtering>true</filtering>
                                </resource>
                            </webResources>
                            <filters>
                                <filter>src/main/filter/${webapp.filter}.properties</filter>
                            </filters>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.tomcat.maven</groupId>
                <artifactId>tomcat7-maven-plugin</artifactId>
                <version>2.1</version>
                <configuration>
                    <port>8080</port>
                    <!-- Configure the tomcat7 plugin to run using the build directory instead of the source directory.
                         This allows filtering on the webcontainer configuration file(s) so we can customize for
                         development, and also allows the client build system to have a non-source directory into which
                         it can dump its artifacts to be served ("dist" target in Gruntfile.js). We do this by pointing
                         to the context file in the build directory, which will have been filtered to have the document
                         base for the webcontainer set via the webcontainer.context.docbase.attr property.
                    -->
                    <contextFile>${project.build.directory}/${project.build.finalName}/META-INF/context.xml</contextFile>
                </configuration>
                <dependencies> <!-- Adding jdbc driver here so can access driver via jndi resource -->
                    <dependency>
                        <groupId>com.oracle</groupId>
                        <artifactId>ojdbc6</artifactId>
                        <version>11.2.0.2.0</version>
                        <type>jar</type>
                        <scope>compile</scope>
                    </dependency>
                </dependencies>
            </plugin>
        </plugins>
    </build>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.glassfish.jersey</groupId>
                <artifactId>jersey-bom</artifactId>
                <version>${jersey.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.glassfish.jersey.containers</groupId>
            <artifactId>jersey-container-servlet</artifactId>
            <version>${jersey.version}</version>
        </dependency>
        <dependency>
            <groupId>org.glassfish.jersey.media</groupId>
            <artifactId>jersey-media-json-processing</artifactId>
            <version>${jersey.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>3.2.3.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>com.oracle</groupId>
            <artifactId>ojdbc6</artifactId>
            <version>11.2.0.2.0</version>
            <type>jar</type>
            <scope>compile</scope>
        </dependency>
       <dependency>
          <groupId>redis.clients</groupId>
          <artifactId>jedis</artifactId>
          <version>2.1.0</version>
       </dependency>
       <!-- servlet api for @context usage -->
       <dependency>
          <groupId>org.apache.tomcat</groupId>
          <artifactId>tomcat-servlet-api</artifactId>
          <version>7.0.30</version>
          <scope>provided</scope>
       </dependency>
       <dependency>
          <groupId>org.apache.commons</groupId>
          <artifactId>commons-io</artifactId>
          <version>1.3.2</version>
       </dependency>
       <dependency>
          <groupId>org.apache.commons</groupId>
          <artifactId>commons-lang3</artifactId>
          <version>3.3.2</version>
       </dependency>
    </dependencies>

    <profiles>
        <profile>
            <id>release</id>
            <properties>
                <webapp.filter>release</webapp.filter>
                <webcontainer.context.docbase.attr></webcontainer.context.docbase.attr>
            </properties>
        </profile>
        <profile>
            <id>fda</id>
            <properties>
		<release.homepage>../app/views/index-fda.html</release.homepage>
                <webapp.filter>release</webapp.filter>
                <webcontainer.context.docbase.attr></webcontainer.context.docbase.attr>
            </properties>
        </profile>
        <profile>
            <id>cder</id>
            <properties>
		<release.homepage>../app/views/index-cder.html</release.homepage>
                <webapp.filter>release</webapp.filter>
                <webcontainer.context.docbase.attr></webcontainer.context.docbase.attr>
            </properties>
        </profile>
        <profile>
            <id>redirect</id>
            <properties>
		<release.homepage>../app/views/index-redirect.html</release.homepage>
                <webapp.filter>release</webapp.filter>
                <webcontainer.context.docbase.attr></webcontainer.context.docbase.attr>
            </properties>
        </profile>
        <profile>
            <id>testing</id>
            <properties>
                <webapp.filter>testing</webapp.filter>
                <webcontainer.context.docbase.attr></webcontainer.context.docbase.attr>
            </properties>
        </profile>
        <profile>
            <id>development</id>
            <properties>
                <webapp.filter>development</webapp.filter>
                <webcontainer.context.docbase.attr></webcontainer.context.docbase.attr>
            </properties>
        </profile>
        <profile>
            <id>debug</id>
            <properties>
                <webapp.filter>debug</webapp.filter>
                <webcontainer.context.docbase.attr></webcontainer.context.docbase.attr>
            </properties>
        </profile>
    </profiles>

</project>
