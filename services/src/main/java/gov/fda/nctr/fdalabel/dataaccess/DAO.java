package gov.fda.nctr.fdalabel.dataaccess;

import java.io.*;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Logger;
import javax.json.*;
import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import com.google.common.collect.ImmutableList;

import org.springframework.dao.CleanupFailureDataAccessException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.xml.sax.SAXException;

import static gov.fda.nctr.fdalabel.MeddraTerms.TermsType;
import static gov.fda.nctr.fdalabel.PharmClasses.PharmClassType;
import static gov.fda.nctr.fdalabel.Products.ProductNameType;

/**
 * Created by sharris
 * Date: 7/30/2013
 */

public class DAO {


  public enum StringPatternType { SUBSTR, GLOB, REGEX }

  final Logger log = java.util.logging.Logger.getLogger(DAO.class.getName());

  final NamedParameterJdbcTemplate jdbcTemplate;


  public DAO(DataSource ds)
  {
    this.jdbcTemplate = new NamedParameterJdbcTemplate(ds);
  }


  public JsonArray meddraTermsContaining(String termFragsStr,
                                         TermsType termsType,
                                         boolean includeUnlabeled,
                                         int splMeddraIndexingAuthority,
                                         long resultsLimit)
  {
    boolean ptsOnly = termsType == TermsType.pt;

    NameGeneratingSqlParameterSource sqlParams = new NameGeneratingSqlParameterSource();

    String limParam = sqlParams.addNewParam("lim", resultsLimit);
    String authorityParam = sqlParams.addNewParam("authority", splMeddraIndexingAuthority);

    String fragsCond = condFieldMatchesFragments("llt_name", termFragsStr, sqlParams);

    String qry =
      "select * from ("
      +  "select t.term, case when tc.spl_count is not null then tc.spl_count else 0 end spl_count, t.is_pt\n"
      +  "from (\n"
      +    "select llt_name term, case when llt_code = pt_code then 1 else 0 end is_pt\n"
      +    "from meddra.low_level_term\n"
      +    "where " + (condNonEmpty(fragsCond) ? fragsCond + " and\n": "")
      +    (ptsOnly ? "pt_code = llt_code and\n" : "")
      +    "llt_currency = 'Y'\n"
      +  ") t\n"
      +  (includeUnlabeled?"left ":"") + "join meddra_term_spl_count tc on tc.term = t.term and tc.assigned_by = :"+authorityParam+"\n"
      +  "order by t.term\n"
      +") where rownum <= :"+limParam;

    return queryForJsonArray(qry, sqlParams);
  }


  public JsonArray meddraLowLevelTermsHavingPreferredTerm(String preferredTerm,
                                                          boolean includeUnlabeledTerms,
                                                          int splMeddraIndexingAuthority)
  {
      String qry =
        "select t.term, tc.spl_count, t.is_pt "
        + "from (\n"
        + " select llt_name term, case when llt_code = pt_code then 1 else 0 end is_pt\n"
        + " from meddra.low_level_term\n"
        + " where pt_code = (select pt_code from meddra.preferred_term where pt_name = :pt)\n"
        + " and llt_currency = 'Y'\n"
        + ") t\n"
        + (includeUnlabeledTerms?"left ":"") + "join meddra_term_spl_count tc on tc.term = t.term and tc.assigned_by = :authority\n"
        + "order by t.term";

    return queryForJsonArray(qry, params("pt", preferredTerm, "authority", splMeddraIndexingAuthority));
  }


  public JsonArray meddraLowLevelTermsRelatedToLowLevelTerm(String lowLevelTerm,
                                                            boolean includeUnlabeledTerms,
                                                            int splMeddraIndexingAuthority)
  {
    String qry =
      "select t.term, tc.spl_count, t.is_pt "
      + "from (\n"
      + " select llt_name term, case when llt_code = pt_code then 1 else 0 end is_pt\n"
      + " from meddra.low_level_term\n"
      + " where pt_code = (select pt_code from meddra.low_level_term where llt_name = :llt)\n"
      + " and llt_currency = 'Y'\n"
      + ") t\n"
      + (includeUnlabeledTerms?"left ":"") + "join meddra_term_spl_count tc on tc.term = t.term and tc.assigned_by = :authority\n"
      + "order by t.term";

    return queryForJsonArray(qry, params("llt", lowLevelTerm, "authority", splMeddraIndexingAuthority));
  }


  /* Return a json array of product-name-info objects, of the form { name (String), isGen (0/1) }. */
  public JsonArray productNamesContaining(String nameFragsStr,
                                          ProductNameType productNameType,
                                          long resultsLimit)
  {
    NameGeneratingSqlParameterSource sqlParams = new NameGeneratingSqlParameterSource();

    String limitParam = sqlParams.addNewParam("lim", resultsLimit);

    String prodNameInfosSubquery =
        productNameType == ProductNameType.GEN ? queryForGenericNamesMatchingFragmentsAsProdNameInfos(nameFragsStr, sqlParams) :
        productNameType == ProductNameType.TRADE ? queryForTradeNamesMatchingFragmentsAsProdNameInfos(nameFragsStr, sqlParams) :
          queryForGenericNamesMatchingFragmentsAsProdNameInfos(nameFragsStr, sqlParams)
          + " union all " +
          queryForTradeNamesMatchingFragmentsAsProdNameInfos(nameFragsStr, sqlParams);

    String qry =
      "select * from (" +
        prodNameInfosSubquery +
      ") where rownum <= :" + limitParam;

    return queryForJsonArray(qry, sqlParams);
  }

  public JsonArray productNamesContainingLDT(String nameFragsStr,
                                          ProductNameType productNameType,
                                          long resultsLimit)
  {
    NameGeneratingSqlParameterSource sqlParams = new NameGeneratingSqlParameterSource();

    String limitParam = sqlParams.addNewParam("lim", resultsLimit);

    String prodNameInfosSubquery =
        productNameType == ProductNameType.GEN ? queryForGenericNamesMatchingFragmentsAsProdNameInfos(nameFragsStr, sqlParams) :
        productNameType == ProductNameType.TRADE ? queryForTradeNamesMatchingFragmentsAsProdNameInfosLDT(nameFragsStr, sqlParams) :
          queryForGenericNamesMatchingFragmentsAsProdNameInfos(nameFragsStr, sqlParams)
          + " union all " +
          queryForTradeNamesMatchingFragmentsAsProdNameInfosLDT(nameFragsStr, sqlParams);

    String qry =
      "select * from (" +
        prodNameInfosSubquery +
      ") where rownum <= :" + limitParam;

    return queryForJsonArray(qry, sqlParams);
  }


  // Return generic names as name-info structure
  String queryForGenericNamesMatchingFragmentsAsProdNameInfos(String nameFragsStr, NameGeneratingSqlParameterSource sqlParams)
  {
    String fragsCond = condFieldMatchesFragments("gen_name_comp", nameFragsStr, sqlParams);
    return "select distinct upper(gen_name_comp ) name, 1 is_gen from prod_gen_name_comp" + (condNonEmpty(fragsCond) ? " where " + fragsCond : "");
  }

  String queryForTradeNamesMatchingFragmentsAsProdNameInfos(String nameFragsStr, NameGeneratingSqlParameterSource sqlParams)
  {
    String fragsCond = condFieldMatchesFragments("name", nameFragsStr, sqlParams);
    return "select distinct name, 0 is_gen from spl_prod" + (condNonEmpty(fragsCond) ? " where " + fragsCond : "");
  }

  String queryForTradeNamesMatchingFragmentsAsProdNameInfosLDT(String nameFragsStr, NameGeneratingSqlParameterSource sqlParams)
  {
    String fragsCond = condFieldMatchesFragments("name", nameFragsStr, sqlParams);
    return "select distinct upper(name) name, 0 is_gen from dgv_spl_prod" + (condNonEmpty(fragsCond) ? " where " + fragsCond : "");
  }

  public JsonArray pharmClassesContaining(String pharmClassNameFragsStr,
                                          PharmClassType pharmClassType,
                                          long resultsLimit)
  {
    NameGeneratingSqlParameterSource sqlParams = new NameGeneratingSqlParameterSource();

    String limParam = sqlParams.addNewParam("lim", resultsLimit);

    String fragsCond = condFieldMatchesFragments("c.class_name", pharmClassNameFragsStr, sqlParams);

    String classTypeCond;
    if (pharmClassType != PharmClassType.ANY) {
      String classTypeParam = sqlParams.addNewParam("pc", pharmClassType.name());
      classTypeCond = "c.CLASS_TYPE = :" + classTypeParam;
    } else {
      classTypeCond = "";
    }

    String combinedConds = combineConds("and", fragsCond, classTypeCond);

    String qry =
      "select * from (\n" +
      "  select class_name, class_type,spl_count from  NDFRT_Class_SPL c\n" +
      (condNonEmpty(combinedConds) ? "where "+combinedConds+"\n" : "") +
      "  order by c.class_name\n" +
      ") where rownum <= :"+limParam;

    return queryForJsonArray(qry, sqlParams);
  }

  /** Return  section types with tree strutrue as an array of { code: ..., display_name: ... } objects. */
  public JsonArray sectionTypesTree()
  {
    String qry = "select TYPEFORMAT, groupno||'-'||code code,display_name,view_no ,section_level lev,count from section_type_sealds_view";

    return queryForJsonArray(qry, params());
  }


  /** Return  section types with tree strutrue as an array of { code: ..., display_name: ... } objects. */
  public JsonArray sectionTypesTreeLDT()
  {
    String qry =  "select sts.TYPEFORMAT,sts.groupno||'-'||sts.CODE code,sts.VIEW_NO,sts.DISPLAY_NAME,ssc.COUNTS count,sts.section_level lev from DGV_SECTION_TYPE_SEALDS sts  join DGV_SPL_SECT_COUNT ssc on sts.GROUPNO=ssc.GROUPNO and sts.CODE=ssc.SECT_CODE  order by sts.GROUPNO,to_number(sts.VIEW_NO,'99.99')";
    return queryForJsonArray(qry, params());
  }

  /** Return all section types as an array of { code: ..., display_name: ... } objects. */
  public JsonArray allSectionTypes()
  {
    String qry = "select loinc_code as code, loinc_name as display_name from section_type order by loinc_name";

    return queryForJsonArray(qry, params());
  }

  /** Return all all Spl Data Loader Records as an array objects. */
  public JsonArray allSplDataLoaderRecords()
  {
    String qry = "select to_char(COUNT_TS,'yyyy-MM') yyyymm ,sum(case when COUNT_TYPE = 'new' then RECORD_COUNT else 0 end ) added,\n"+
                 "sum(case when COUNT_TYPE = 'resurrected' then RECORD_COUNT else 0 end ) resurrected, \n"+
                  "sum(case when COUNT_TYPE = 'removed' then RECORD_COUNT else 0 end ) removed, \n"+
                  "sum(case when COUNT_TYPE = 'updated' then RECORD_COUNT else 0 end ) updated \n"+
                  "from load_record_count \n"+ 
                  "where object_name='SPL' and COUNT_TS>add_months(sysdate, -13 * 2) and "+
                    "COUNT_TYPE in ('new','removed','updated','resurrected') \n"+
                  "group by to_char(COUNT_TS,'yyyy-MM') \n"+
                  "order by to_char(COUNT_TS,'yyyy-MM')" ;
    return queryForJsonArray(qry, params());
  }

 /** Return the recent Laoder information of Spl as an array objects. */
  public JsonArray recentFiveSplDataLoaderRecords()
  {
    String qry = "select load_date loaddate,added,resurrected,removed,updated,total from spl_loader_count_view where rownum<6" ;
    return queryForJsonArray(qry, params());
  }
  
  /** Added by JY. Return the recent Loader information of SPL as array object, for LDT only */
  public JsonArray recentFiveSplDataLoaderRecordsLDT()
  {
    String qry = "select load_date loaddate, total from spl_loader_count_view_ldt where rownum<6" ;
    return queryForJsonArray(qry, params());
  }
  

/** Return the recent Dict Maintance information  as an array objects. */
  public JsonArray recentDictMaintanceRecords()
  {
    //String qry = "select to_char(main_date,'yyyy-mm-dd') maindate,contents from dict_maintance where rownum<6 order by main_date desc" ;
    String qry = "select * from (select to_char(main_date,'yyyy-mm-dd') maindate,contents from dict_maintance order by main_date desc) where rownum<6 " ;
    return queryForJsonArray(qry, params());
  }
/** Return the recent Dict Maintance information  as an array objects. */
  public JsonArray dataLoaderofTotalSPL()
  {
    String qry = "select load_date loaddate,added,resurrected,removed,updated,total from SPL_Loader_Count_view  where load_date>to_char(add_months(sysdate,-24),'yyyy-mm-dd') order by loaddate" ;
    return queryForJsonArray(qry, params());
  }


  /** Return all document types as an array of { code: ..., display_name: ... } objects. */
  public JsonArray allDocumentTypes()
  {
    String qry = "select loinc_code as code, loinc_name as display_name from document_type order by loinc_name";

    return queryForJsonArray(qry, params());
  }

  /** Return some document types for SEALDS as an array of { code: ..., display_name: ... } objects. */
  public JsonArray documentTypesMain()
  {
    String qry = "SELECT s.code,REPLACE(display_name,'LABEL','') display_name,c.count count from doc_type_sealds s join  document_type_count c on c.code=s.code order by display_name";

    return queryForJsonArray(qry, params());
  }

  /** Return some document types for SEALDS as an array of { code: ..., display_name: ... } objects. */
  public JsonArray documentTypesLDT()
  {
    String qry = "select tt.FORMAT_GROUP||'='||tt.LOINC_CODE code, tt.LOINC_NAME display_name,cc.counts count,lev from DGV_DOCUMENT_TYPE_SEALDS tt join DGV_FORMAT_DOCUMENT_TYPE_COUNT cc on cc.FORMAT=tt.FORMAT_GROUP and cc.code=LOINC_CODE order by tt.FORMAT_GROUP,tt.VIEW_ORDER";
    return queryForJsonArray(qry, params());
  }

  /** Return all marketing categories as an array of { code: ..., display_name: ... } objects. */
  public JsonArray allMarketingCategories()
  {
    String qry = "select nci_thesaurus_code as code, spl_acceptable_term as display_name from marketing_category order by spl_acceptable_term";

    return queryForJsonArray(qry, params());
  }

  /** Return some marketing categories for Sealds as an array of { code: ..., display_name: ... } objects. */
  public JsonArray marketingCategoriesMain()
  {
    //String qry = "select distinct nci_thesaurus_code as code, spl_acceptable_term as display_name ,mc.count count from marketing_category m join MARKETING_CAT_count mc on m.nci_thesaurus_code=mc.CODE order by display_name";
    String qry = "select distinct m.code, m.display_name ,mc.count count from market_category_SEALDS m join MARKETING_CAT_count mc on m.code=mc.CODE order by display_name";
    return queryForJsonArray(qry, params());
  }

  /** Return some marketing categories for Sealds as an array of { code: ..., display_name: ... } objects. */
  public JsonArray marketingCategoriesLDT()
  {
    //String qry = "select mc.NCI_THESAURUS_CODE code, mc.SPL_ACCEPTABLE_TERM  display_name,mcc.COUNT from MARKETING_CATEGORY mc join dgv_MARKETING_CAT_COUNT mcc on mcc.CODE=mc.NCI_THESAURUS_CODE where mc.ISLDT='Y' order by mc.VIEW_NO";

    String qry = "select mc.code, mc.display_name,mcc.COUNT from MARKET_CATEGORY_SEALDS mc join dgv_MARKETING_CAT_COUNT mcc on mcc.CODE=mc.CODE where mc.ISLDT='Y' order by mc.VIEW_NO";

    return queryForJsonArray(qry, params());
  }

  /** Return all routes of administration as an array of { code: ..., display_name: ... } objects. */
  public JsonArray allRoutesOfAdmin()
  {
    String qry = "select nci_thesaurus_code as code, spl_acceptable_term as display_name from route_of_admin order by spl_acceptable_term";

    return queryForJsonArray(qry, params());
  }

  /** Return all dosage forms as an array of { code: ..., display_name: ... } objects. */
  public JsonArray allDosageForms()
  {
    String qry = "select nci_thesaurus_code as code, spl_acceptable_term as display_name from dosage_form order by spl_acceptable_term";

    return queryForJsonArray(qry, params());
  }

  public JsonArray splSummariesForCriterion(JsonObject criterion, int resultsLimit)
  {
    boolean limitResults = resultsLimit > 0;

    NameGeneratingSqlParameterSource sqlParams = new NameGeneratingSqlParameterSource();

    if (limitResults)
      sqlParams.addNewParam("lim", resultsLimit);

    String cond = condOnSplSum(criterion, sqlParams);

    String unlimQry =
      "select "
      +  "set_id,"
      +  "document_type labeling_type,"
      +  "author_org_normd_name submitter,"
      +  "marketing_acts market_dates,"
      +  "product_names,"
      +  "product_normd_generic_names generic_proper_names,"
      +  "act_ingr_names,"
      +  "act_ingr_uniis,"
      +  "ndc_codes,"
      +  "routes_of_administration,"
      +  "market_categories,"
      +  "dosage_forms,INITIAL_APPROVAL_YEAR year,"
      +  "eff_time spl_eff_date, epc\n"
      +  ",(select LISTAGG(trim(sav.appl_no), ';') WITHIN GROUP (ORDER BY sav.spl_id)  from spl_appl_v sav  where sav.spl_id=sum_spl.spl_id ) application_number \n"
      +"from sum_spl\n"
      +(condNonEmpty(cond) ? "where "+cond+"\n": "")
      +"order by product_normd_generic_names, eff_time desc";
System.out.println("Daojun unlimQry=" + unlimQry);
 
    String qry = limitResults ? "select * from (" + unlimQry + ") where rownum <= :lim" : unlimQry;

    return queryForJsonArray(qry, sqlParams);
  }

  public JsonArray splSummariesForCriterionLDT(JsonObject criterion, int resultsLimit)
  {
    boolean limitResults = resultsLimit > 0;

    NameGeneratingSqlParameterSource sqlParams = new NameGeneratingSqlParameterSource();

    if (limitResults)
      sqlParams.addNewParam("lim", resultsLimit);

    String cond = condOnSplSumLDT(criterion, sqlParams);

    String unlimQry =
      "select "
      +  "set_id,"
      +  "document_type labeling_type,"
      +  "author_org_normd_name submitter,"
      +  "marketing_acts market_dates,"
      +  "product_names,"
      +  "product_normd_generic_names generic_proper_names,"
      +  "act_ingr_names,"
      +  "act_ingr_uniis,"
      +  "ndc_codes,"
      +  "routes_of_administration,"
      +  "market_categories,"
      +  "dosage_forms,INITIAL_APPROVAL_YEAR year,"
      +  "eff_time spl_eff_date, epc\n"
      +  ",(select LISTAGG(trim(sav.appl_no), ';') WITHIN GROUP (ORDER BY sav.spl_id)  from spl_appl_v sav  where sav.spl_id=DGV_SUM_RX_SPL.spl_id ) application_number \n"
      +"from DGV_SUM_RX_SPL\n"
      +(condNonEmpty(cond) ? "where "+cond+"\n": "")
      +"order by product_normd_generic_names asc, eff_time desc";

 
    String qry = limitResults ? "select * from (" + unlimQry + ") where rownum <= :lim" : unlimQry;

    return queryForJsonArray(qry, sqlParams);
  }

  public String splSummariesSQLForCriterion(JsonObject criterion, int resultsLimit)
  {
    boolean limitResults = resultsLimit > 0;

    NameGeneratingSqlParameterSource sqlParams = new NameGeneratingSqlParameterSource();

    if (limitResults)
      sqlParams.addNewParam("lim", resultsLimit);

    String cond = condOnSplSum(criterion, sqlParams);

    String unlimQry =
      "select "
      +  "set_id,"
      +  "document_type labeling_type,"
      +  "author_org_normd_name submitter,"
      +  "marketing_acts market_dates,"
      +  "product_names,"
      +  "product_normd_generic_names generic_proper_names,"
      +  "act_ingr_names,"
      +  "act_ingr_uniis,"
      +  "ndc_codes,"
      +  "routes_of_administration,"
      +  "market_categories,"
      +  "dosage_forms,INITIAL_APPROVAL_YEAR year,"
      +  "eff_time spl_eff_date, epc\n"
      +  ",(select LISTAGG(trim(sav.appl_no), ';') WITHIN GROUP (ORDER BY sav.spl_id)  from spl_appl_v sav  where sav.spl_id=sum_spl.spl_id ) application_number \n"
      +"from sum_spl\n"
      +(condNonEmpty(cond) ? "where "+cond+"\n": "")
      +"order by product_normd_generic_names, eff_time desc";

 
    String qry = limitResults ? "select * from (" + unlimQry + ") where rownum <= :lim" : unlimQry;

    return qry;
  }


  public String splSummariesSQLForCriterionLDT(JsonObject criterion, int resultsLimit)
  {
    boolean limitResults = resultsLimit > 0;

    NameGeneratingSqlParameterSource sqlParams = new NameGeneratingSqlParameterSource();

    if (limitResults)
      sqlParams.addNewParam("lim", resultsLimit);

    String cond = condOnSplSumLDT(criterion, sqlParams);

    String unlimQry =
      "select "
      +  "set_id,"
      +  "document_type labeling_type,"
      +  "author_org_normd_name submitter,"
      +  "marketing_acts market_dates,"
      +  "product_names,"
      +  "product_normd_generic_names generic_proper_names,"
      +  "act_ingr_names,"
      +  "act_ingr_uniis,"
      +  "ndc_codes,"
      +  "routes_of_administration,"
      +  "market_categories,"
      +  "dosage_forms,INITIAL_APPROVAL_YEAR year,"
      +  "eff_time spl_eff_date, epc\n"
      +  ",(select LISTAGG(trim(sav.appl_no), ';') WITHIN GROUP (ORDER BY sav.spl_id)  from spl_appl_v sav  where sav.spl_id=DGV_SUM_RX_SPL.spl_id ) application_number \n"
      +"from DGV_SUM_RX_SPL\n"
      +(condNonEmpty(cond) ? "where "+cond+"\n": "")
      +"order by product_normd_generic_names asc, eff_time desc";

 
    String qry = limitResults ? "select * from (" + unlimQry + ") where rownum <= :lim" : unlimQry;

    return qry;
  }

  public long splsCountForCriterion(JsonObject criterion)
  {
    NameGeneratingSqlParameterSource sqlParams = new NameGeneratingSqlParameterSource();

    String cond = condOnSplSum(criterion, sqlParams);

    String qry = "select count(*) from sum_spl" + (condNonEmpty(cond) ? "\nwhere " + cond : "");

    return jdbcTemplate.queryForObject(qry, sqlParams, Long.class);
  }


  public long splsCountForCriterionLDT(JsonObject criterion)
  {
    NameGeneratingSqlParameterSource sqlParams = new NameGeneratingSqlParameterSource();

    String cond = condOnSplSumLDT(criterion, sqlParams);

    String qry = "select count(*) from DGV_SUM_RX_SPL" + (condNonEmpty(cond) ? "\nwhere " + cond : "");

    return jdbcTemplate.queryForObject(qry, sqlParams, Long.class);
  }

  // 12/23/16 JY added active moiety names and active moiety uniis.
  public void writeSplSummariesCSV(JsonObject criterion, Writer w) throws IOException
  {
    NameGeneratingSqlParameterSource sqlParams = new NameGeneratingSqlParameterSource();

    String cond = condOnSplSum(criterion, sqlParams);

    String qry =
      "select "
      +  "market_categories,"
      +  "(select LISTAGG(trim(sav.appl_no), ';') WITHIN GROUP (ORDER BY sav.spl_id)  from spl_appl_v sav  where sav.spl_id=sum_spl.spl_id ) application_number, \n"
      +  "product_names,"
      +  "product_normd_generic_names generic_proper_names,"
      +  "eff_time spl_eff_date,"
      +  "epc,"
      +  "dosage_forms,"
      +  "routes_of_administration,"
      +  "initial_approval_year,"
      +  "author_org_normd_name submitter,"
      +  "marketing_acts market_dates,"
      +  "ndc_codes,"
      +  "act_ingr_uniis,"
      +  "act_ingr_names,"
      +  "act_moiety_names,"
      +  "act_moiety_uniis,"
      +  "document_type labeling_type,"
      +  "'http://fdalabel.fda.gov/fdalabel/services/spl/set-ids/'||set_id||'/spl-doc' fdalabel_link,"
      +  "('http://dailymed.nlm.nih.gov/dailymed/lookup.cfm?setid='|| set_id) dailymed_link,"
      +  "('http://dailymed.nlm.nih.gov/dailymed/downloadpdffile.cfm?setId='|| set_id) dailymed_pdf_link,"
      +  "set_id\n"
      +"from sum_spl\n"
      +(condNonEmpty(cond) ? "where "+cond+"\n" : "")
      +"order by product_normd_generic_names asc, eff_time desc";

    jdbcTemplate.query(qry, sqlParams, new CSVWriterRowCallback(w));
  }

  public void writeSplSummariesCSVLDT(JsonObject criterion, Writer w) throws IOException
  {
    NameGeneratingSqlParameterSource sqlParams = new NameGeneratingSqlParameterSource();

    String cond = condOnSplSumLDT(criterion, sqlParams);

    String qry =
      "select "
      +  "market_categories,"
      +  "(select LISTAGG(trim(sav.appl_no), ';') WITHIN GROUP (ORDER BY sav.spl_id)  from spl_appl_v sav  where sav.spl_id=DGV_SUM_RX_SPL.spl_id ) application_number, \n"
      +  "product_names,"
      +  "product_normd_generic_names generic_proper_names,"
      +  "eff_time spl_eff_date,"
      +  "epc,"
      +  "dosage_forms,"
      +  "routes_of_administration,"
      +  "initial_approval_year,"
      +  "author_org_normd_name submitter,"
      +  "marketing_acts market_dates,"
      +  "ndc_codes,"
      +  "act_ingr_uniis,"
      +  "act_ingr_names,"
      +  "document_type labeling_type,"
      +  "'http://fdalabel.fda.gov/fdalabel-r/services/spl/set-ids/'||set_id||'/spl-doc' fdalabel_link,"
      +  "('http://dailymed.nlm.nih.gov/dailymed/lookup.cfm?setid='|| set_id) dailymed_link,"
      +  "('http://dailymed.nlm.nih.gov/dailymed/downloadpdffile.cfm?setId='|| set_id) dailymed_pdf_link,"
      +  "set_id\n"
      +"from DGV_SUM_RX_SPL\n"
      +(condNonEmpty(cond) ? "where "+cond+"\n" : "")
      +"order by product_normd_generic_names asc, eff_time desc";

    jdbcTemplate.query(qry, sqlParams, new CSVWriterRowCallback(w));
  }

  String condOnSplSum(JsonObject criterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      if (isEntityCriterion(criterion))
      {
        switch(criterion.getString("sourceEntity"))
        {
          case "spl-text":
            return splTextCondOnSplSum(criterion, sqlParams);
          case "document-type":
            return documentTypeCondOnSplSum(criterion, sqlParams);
          case "section":
            return sectionCondOnSplSum(criterion, sqlParams);
          case "product":
            return productCondOnSplSum(criterion, sqlParams);
          case "marketing-category":
            return marketingCategoryCondOnSplSum(criterion, sqlParams);
          case "market-status":
            return marketStatusCondOnSplSum(criterion, sqlParams);
          case "route-of-admin":
            return routeOfAdminCondOnSplSum(criterion, sqlParams);
          case "dosage-form":
            return dosageFormCondOnSplSum(criterion, sqlParams);
          case "pharm-class":
            return pharmClassCondOnSplSum(criterion, sqlParams);
          case "meddra":
            return meddraCondOnSplSum(criterion, sqlParams);
          case "chemical-structure":
            return chemicalStructureCondOnSplSum(criterion, sqlParams);
          case "identifier":
            return identifierCondOnSplSum(criterion, sqlParams);
          default:
            throw new IllegalArgumentException("In query criterion, source entity \"" + criterion.getString("sourceEntity") + "\" is invalid.");
        }
      }
      else if (isGroupCriterion(criterion))
      {
        JsonArray subcriteria = criterion.getJsonArray("criteria");
        JsonArray logOps = criterion.getJsonArray("logOps");

        StringBuilder combinedCond = new StringBuilder();

        for(int i=0; i<subcriteria.size(); ++i)
        {
          String subcritCond = condOnSplSum(subcriteria.getJsonObject(i), sqlParams);

          if (condNonEmpty(subcritCond))
          {
            boolean firstSubcritCond = combinedCond.length() == 0;
            boolean multipleSubcritConds = subcriteria.size() > 0; // False positives are OK here (some might be empty), we'll just get unnecessary parens.

            if (!firstSubcritCond)
            {
              String op = logOpSql(logOps.getString(i - 1));
              combinedCond.append(' ');
              combinedCond.append(op);
              combinedCond.append(' ');
            }

            if (multipleSubcritConds)
              combinedCond.append('(');

            combinedCond.append(subcritCond);

            if (multipleSubcritConds)
              combinedCond.append(')');
          }
        }

        return combinedCond.toString();
      }
      else
        throw new IllegalArgumentException("The query criterion is invalid.");
    }
    catch(NullPointerException e)
    {
      throw new IllegalArgumentException("Invalid group criterion in query: required field is missing.");
    }
    catch(ClassCastException e)
    {
      throw new IllegalArgumentException("Invalid group criterion in query: field encountered with unexpected type.");
    }
  }


  String condOnSplSumLDT(JsonObject criterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    { System.out.println("condOnSplSumLDT called");
      if (isEntityCriterion(criterion))
      {
        switch(criterion.getString("sourceEntity"))
        {
          case "spl-text":
            return splTextCondOnSplSum(criterion, sqlParams);
          case "document-type":
            return documentTypeCondOnSplSumLDT(criterion, sqlParams);
          case "section":
            return sectionCondOnSplSumLDT(criterion, sqlParams);
          case "product":
            return productCondOnSplSumLDT(criterion, sqlParams);
          case "marketing-category":
            String docRes=marketingCategoryCondOnSplSumLDT(criterion, sqlParams);
            String dedRes=deduplicationCondOnSplSum(criterion);
            if(docRes.length()>2 && dedRes.length()>2)
              return docRes+" and "+dedRes;
            else 
              return docRes+" "+dedRes;
          case "market-status":
            return marketStatusCondOnSplSumLDT(criterion, sqlParams);
          case "route-of-admin":
            return routeOfAdminCondOnSplSumLDT(criterion, sqlParams);
          case "dosage-form":
            return dosageFormCondOnSplSumLDT(criterion, sqlParams);
          case "pharm-class":
            return pharmClassCondOnSplSum(criterion, sqlParams);
          case "meddra":
            return meddraCondOnSplSum(criterion, sqlParams);
          case "chemical-structure":
            return chemicalStructureCondOnSplSum(criterion, sqlParams);
          case "identifier":
            return identifierCondOnSplSumLDT(criterion, sqlParams);
          default:
            throw new IllegalArgumentException("In query criterion, source entity \"" + criterion.getString("sourceEntity") + "\" is invalid.");
        }
      }
      else if (isGroupCriterion(criterion))
      {
        JsonArray subcriteria = criterion.getJsonArray("criteria");
        JsonArray logOps = criterion.getJsonArray("logOps");

        StringBuilder combinedCond = new StringBuilder();

        for(int i=0; i<subcriteria.size(); ++i)
        {
          String subcritCond = condOnSplSumLDT(subcriteria.getJsonObject(i), sqlParams);

          if (condNonEmpty(subcritCond))
          {
            boolean firstSubcritCond = combinedCond.length() == 0;
            boolean multipleSubcritConds = subcriteria.size() > 0; // False positives are OK here (some might be empty), we'll just get unnecessary parens.

            if (!firstSubcritCond)
            {
              String op = logOpSql(logOps.getString(i - 1));
              combinedCond.append(' ');
              combinedCond.append(op);
              combinedCond.append(' ');
            }

            if (multipleSubcritConds)
              combinedCond.append('(');

            combinedCond.append(subcritCond);

            if (multipleSubcritConds)
              combinedCond.append(')');
          }
        }

        return combinedCond.toString();
      }
      else
        throw new IllegalArgumentException("The query criterion is invalid.");
    }
    catch(NullPointerException e)
    {
      throw new IllegalArgumentException("Invalid group criterion in query: required field is missing.");
    }
    catch(ClassCastException e)
    {
      throw new IllegalArgumentException("Invalid group criterion in query: field encountered with unexpected type.");
    }
  }

  String deduplicationCondOnSplSum(JsonObject deduplicationCriterion)
  {
    boolean repacker=deduplicationCriterion.getBoolean("repacker");
    boolean nda=deduplicationCriterion.getBoolean("authorizedGeneric");
    boolean prodIdx=deduplicationCriterion.getBoolean("productConcept");
    String res="";
    if (repacker && nda)    {
      res=
        "spl_id in ("
        + "select spl_id from dgv_spl_prod "
        + "where EQUIV_TO_NDC_CODE is null ) "
        + " and "
        + "spl_id not in ( "
        + "select spl_id from DGV_SUM_SPL_MKT_CAT "
        + "where MARKETING_CAT_NCIT_CODE='C73605' ) " ;
    }
   if (repacker && !nda)    {
      res=
        "spl_id in ("
        + "select spl_id from dgv_spl_prod "
        + "where EQUIV_TO_NDC_CODE is null ) ";
    }
   if (!repacker && nda)    {
      res=
         "spl_id not in ( "
        + "select spl_id from DGV_SUM_SPL_MKT_CAT "
        + "where MARKETING_CAT_NCIT_CODE='C73605' ) " ;
    }
    if ( prodIdx ){
      if (res.length()>3) res=res+" and ";
      res=res+" spl_id in (select distinct rf.REF_ID from dgv_spl_with_ref rf join dgv_sum_rx_spl rx on rx.spl_id=rf.spl_id) ";
    }
      return res;
  }

  String productCondOnSplSum(JsonObject productCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    String prodCond = productCondOnSplProd(productCriterion, sqlParams);

    if (condNonEmpty(prodCond))
    {
      return
        "spl_id in ("
        + "select spl_id from spl_prod "
        + "where " + prodCond +
        ")";
    }
    else
      return "";
  }

  String productCondOnSplSumLDT(JsonObject productCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    String prodCond = productCondOnSplProd(productCriterion, sqlParams);

    if (condNonEmpty(prodCond))
    {
      return
        "spl_id in ("
        + "select spl_id from dgv_spl_prod "
        + "where " + prodCond +
        ")";
    }
    else
      return "";
  }

  String productCondOnSplProd(JsonObject productCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      if (!productCriterion.containsKey("namePattern"))
        return "";

      String namePattern = productCriterion.getString("namePattern");
      if (namePattern.length() == 0)
        return "";

      StringPatternType namePatternType = StringPatternType.valueOf(productCriterion.getString("namePatternType"));
      ProductNameType nameType = ProductNameType.valueOf(productCriterion.getString("nameType"));

      StringBuilder condBuf = new StringBuilder();

      List<ProductNameType> specificNameTypeAlternatives = nameType == ProductNameType.ANY ?
        ImmutableList.of(ProductNameType.GEN, ProductNameType.TRADE) :
        ImmutableList.of(nameType);

      for(ProductNameType specificNameType: specificNameTypeAlternatives)
      {
        if (condBuf.length() > 0)
          condBuf.append(" or ");

        // Database field expression which should yield an uppercase value when evaluated in SQL.
        String ucDbFieldValExpr = specificNameType == ProductNameType.GEN ? "normd_generic_name" : // values already uppercase in db
                                  specificNameType == ProductNameType.TRADE ? "upper(name)" :
                                  illegalArg("Unrecognized or missing product type name in query criterion.");

        switch (namePatternType)
        {
          case SUBSTR:
          {
            String patternParamName = sqlParams.addNewParam("pnpat", namePattern.toUpperCase());

            condBuf.append("instr(")
                   .append(ucDbFieldValExpr)
                   .append(", :")
                   .append(patternParamName)
                   .append(") > 0");
            break;
          }
          case GLOB:
          {
            String patternParamName = sqlParams.addNewParam("pnpat", namePattern.toUpperCase().replace('*', '%'));

            condBuf.append(ucDbFieldValExpr)
                   .append(" like :")
                   .append(patternParamName);
            break;
          }
          case REGEX:
          {
            String patternParamName = sqlParams.addNewParam("pnpat", namePattern.toUpperCase());

            condBuf.append("regexp_like(")
                   .append(ucDbFieldValExpr)
                   .append(", :")
                   .append(patternParamName)
                   .append(",'in')"); // i => case insensitive, n => allow "." to match newlines
            break;
          }
          default:
            throw new IllegalArgumentException("Unrecognized product name match type \"" + namePatternType + "\" in query criterion.");
        }
      }

      return condBuf.toString();
    }
    catch(NullPointerException e)
    {
      throw new IllegalArgumentException("Invalid product criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      throw new IllegalArgumentException("Invalid product criterion in query criterion: field encountered with unexpected type.");
    }
  }


  String meddraCondOnSplSum(JsonObject meddraCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      // Extract terms and determine whether all of the terms are known to be preferred terms (for a more efficient query).
      List<String> terms = new ArrayList<>();
      boolean ptsOnly = true;
      for(JsonValue termInfo_: meddraCriterion.getJsonArray("terms"))
      {
        JsonObject termInfo = (JsonObject)termInfo_;

        terms.add(termInfo.getString("term"));

        if (!termInfo.containsKey("isPt") || termInfo.getInt("isPt") == 0)
          ptsOnly = false;
      }

      if (terms.size() > 0)
      {
        String termsParamName = sqlParams.addNewParam("mts", terms);

        String tt = ptsOnly ? "pt" : "llt";
        String meddraTable = ptsOnly ? "meddra.preferred_term" : "meddra.low_level_term";

        return
          "set_id in (" +
            "select distinct spl_set_id\n" +
            "from meddra_"+tt+"_spl_coded_sec\n" +
            "where " + tt+"_code in (\n" +
            "  select " + tt+"_code\n" +
            "  from " + meddraTable +"\n" +
            "  where " + tt+"_name in (:" + termsParamName + ")" +
            ")" +
          ")";
      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      throw new IllegalArgumentException("Invalid MedDRA criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      throw new IllegalArgumentException("Invalid MedDRA criterion in query criterion: field encountered with unexpected type.");
    }
  }


  String pharmClassCondOnSplSum(JsonObject meddraCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      List<String> pharmClassNames = new ArrayList<>();
      for(JsonValue classInfo_: meddraCriterion.getJsonArray("pharmClasses"))
      {
        JsonObject classInfo = (JsonObject)classInfo_;
        pharmClassNames.add(classInfo.getString("className"));
      }

      if (pharmClassNames.size() > 0)
      {
        String pharmClassesParamName = sqlParams.addNewParam("pcs", pharmClassNames);

        return
            "set_id in (" +
              "select distinct  uu.spl_setid from pharm_index cc \n"+
                "join NDFRT_CONCEPTINF nc on cc.NUI=nc.NUI \n"+
                "join pharma_mapping uu on uu.pharma_setid=cc.pharma_setid \n"+
                " where  cc.NUI in (\n"+
                  "select nui from ndfrt_conceptinf where name in (:" + pharmClassesParamName + ")\n" +
                ")\n" +
              ")\n" ;
      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      throw new IllegalArgumentException("Invalid pharm classes criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      throw new IllegalArgumentException("Invalid pharm classes criterion in query criterion: field encountered with unexpected type.");
    }
  }


  String splTextCondOnSplSum(JsonObject splTextCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      String splTextQuery = splTextCriterion.getString("textQuery");

      if (splTextQuery != null && splTextQuery.trim().length() > 0)
      {
        String termsParamName = sqlParams.addNewParam("tq", splTextQuery);
	/*	
	String termsParamName;
	if (splTextQuery.trim().startsWith("{") && splTextQuery.trim().endsWith("}"))
	{
		termsParamName = sqlParams.addNewParam("tq", splTextQuery);
	} else
	{
		termsParamName = sqlParams.addNewParam("tq", "{" + splTextQuery + "}");
	}
	*/
        return "spl_id in (select l.id from spl l where contains(l.spl_xml, :"+termsParamName+") > 0)";
      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      throw new IllegalArgumentException("Invalid spl text criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      throw new IllegalArgumentException("Invalid spl text criterion in query criterion: field is is of wrong type.");
    }
  }


  String documentTypeCondOnSplSum(JsonObject splDocumentTypeCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      JsonArray docTypeCodesJson = splDocumentTypeCriterion.getJsonArray("documentTypeCodes");
System.out.println("documentTypeCondOnSplSum called");
      if (!docTypeCodesJson.isEmpty())
      {
        List<String> docTypeCodes = asStringList(docTypeCodesJson);
        String docTypeCodesParamName = sqlParams.addNewParam("dts", docTypeCodes);
        return "spl_id in (select id from spl where doc_type_loinc_code in (:" + docTypeCodesParamName + "))";
      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      log.severe("Invalid document type criterion in query criterion: required field is missing");
      throw new IllegalArgumentException("Invalid document type criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting document type condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid document type criterion in query criterion: field is is of wrong type.");
    }
  }

  String documentTypeCondOnSplSumLDT(JsonObject splDocumentTypeCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    { System.out.println("documentTypeCondOnSplSumLDT called");
      JsonArray docTypeCodesJson = splDocumentTypeCriterion.getJsonArray("documentTypeCodes");
System.out.println("docTypeCodesJson =" + docTypeCodesJson.size());
      if (!docTypeCodesJson.isEmpty())
      {
        List<String> docTypeCodes = asStringList(docTypeCodesJson);
        List<String> docTypeGroup = new ArrayList<>();
        List<String> docTypes = new ArrayList<>();
        String docTypesStr="",docTypeGroupStr="";
        Iterator<String> iterator = docTypeCodes.iterator();
        while(iterator.hasNext()){
          String[] code = iterator.next().split("=");
          docTypeGroup.add(code[0]);
          if (docTypeGroup.size()>1) 
            docTypeGroupStr=docTypeGroupStr+","+code[0];
          else 
            docTypeGroupStr=code[0];
          if(code[1].length() >1) {
            docTypes.add(code[1]);
            if (docTypes.size()>1) 
             docTypesStr=docTypesStr+",'"+code[1]+"'";
            else 
              docTypesStr="'"+code[1]+"'";
           }
        }
        String docTypeCodesParamName = sqlParams.addNewParam("dts", docTypes);
        String docTypeGroupParamName = sqlParams.addNewParam("dtg", docTypeGroup);
System.out.println("docTypesStr=" + docTypesStr);
        if(docTypesStr.length()>1) 
          return "spl_id in (select spl_id from DGV_SUM_RX_SPL srs where srs.DOCUMENT_TYPE_LOINC_CODE in (" + docTypesStr + ") or srs.FORMAT_GROUP in ("+docTypeGroupStr+") )";
        else
         return "spl_id in (select spl_id from DGV_SUM_RX_SPL srs where srs.FORMAT_GROUP in ("+docTypeGroupStr+") )";
      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      log.severe("Invalid document type criterion in query criterion: required field is missing");
      throw new IllegalArgumentException("Invalid document type criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting document type condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid document type criterion in query criterion: field is is of wrong type.");
    }
  }

  String sectionCondOnSplSum(JsonObject splSectionCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      String textQuery = splSectionCriterion.getString("textQuery", "");

      String sectionTypeCodewithgroup= splSectionCriterion.getString("sectionTypeCode", "");
      String sectionTypeCode = sectionTypeCodewithgroup;
      String error_msg = "Invalid additional section criterion in query criterion: field is of wrong type.";
      // Must have a section type code to be considered a non-empty condition.
      if (condNonEmpty(sectionTypeCode))
      {
        sectionTypeCode=sectionTypeCodewithgroup.substring(2);
        if (condNonEmpty(textQuery))
        {
          String textQueryParamName = sqlParams.addNewParam("stq", textQuery.trim());
          String sectionCodeParamName = sqlParams.addNewParam("sc", sectionTypeCode.trim());
          if (sectionTypeCode.trim().startsWith("99999")){
          if (sectionTypeCode.trim().equals("99999-1") && textQuery.trim().matches("(18|19|20)([0-9]{2})"))
            return "to_number(INITIAL_APPROVAL_YEAR,'9999')="+textQuery.trim();
          else error_msg = "Invalid query criterion: please input 4-digit year yyyy between 1800-2099";
         
          if (sectionTypeCode.trim().equals("99999-2") && textQuery.trim().length()>1)
            return " spl_id  in (select spl_id from dgv_spl_title where contains(title,'"+textQuery.trim().toUpperCase()+"')>0) ";
            //return "instr(upper(PRODUCT_TITLE),'"+textQuery.trim().toUpperCase()+"')>0";

          if (sectionTypeCode.trim().equals("99999-3") && textQuery.trim().length()>4)
            return "to_date(REVISED_DATE,'month yyyy')=to_date('"+textQuery.trim()+"','mm/yyyy')";
          throw new IllegalArgumentException(error_msg);
        } 
        else return
            "spl_id in (" +
              "select spl_id from spl_sec" +
              " where loinc_code = :" + sectionCodeParamName +
              " and contains(content_xml, :" + textQueryParamName + ") > 0" +
            ")";
        }
        else // No text query: just require existence of the section.
        {
          String sectionCodeParamName = sqlParams.addNewParam("sc", sectionTypeCode.trim());
          if (sectionTypeCode.trim().startsWith("99999")){
            if (sectionTypeCode.trim().equals("99999-1"))
              return " spl_id in (select spl_id from sum_spl where INITIAL_APPROVAL_YEAR is not null)";
            else if (sectionTypeCode.trim().equals("99999-2"))
              return " spl_id in (select spl_id from SUM_SPL where PRODUCT_TITLE is not null)";
            else
              return " 1=1 ";
          }
          else 
            if  (sectionTypeCodewithgroup.trim().equals("1-34076-0"))
               return "spl_id in (select distinct spl_id from spl_sec where contains(content_xml,'PATIENT COUNSELING INFORMATION')>0 and LOINC_CODE='34076-0')";
             else
               return "spl_id in (select spl_id from spl_sec where loinc_code = :" + sectionCodeParamName + ")";
        }
      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      log.severe("Invalid section text criterion in query criterion: required field is missing");
      throw new IllegalArgumentException("Invalid section text criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting section condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid section text criterion in query criterion: field is is of wrong type.");
    }
  }

  String sectionCondOnSplSumLDT(JsonObject splSectionCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      String textQuery = splSectionCriterion.getString("textQuery", "");

      String sectionTypeCodewithgroup= splSectionCriterion.getString("sectionTypeCode", "");
      String sectionTypeCode = sectionTypeCodewithgroup;
      String groupNo="";
      // Must have a section type code to be considered a non-empty condition.
      if (condNonEmpty(sectionTypeCode))
      {
        sectionTypeCode=sectionTypeCodewithgroup.substring(2);
        groupNo=sectionTypeCodewithgroup.substring(0,1);
        if (condNonEmpty(textQuery))
        {
          String textQueryParamName = sqlParams.addNewParam("stq", textQuery.trim());
          String sectionCodeParamName = sqlParams.addNewParam("sc", sectionTypeCode.trim());
         if (sectionTypeCode.trim().startsWith("99999") || sectionTypeCode.trim().startsWith("otc99")  ){
           if (sectionTypeCode.trim().equals("99999-1") && textQuery.trim().length()==4)
             return "FORMAT_GROUP=1 and to_number(INITIAL_APPROVAL_YEAR,'9999')="+textQuery.trim();
          
           if (sectionTypeCode.trim().equals("99999-2") && textQuery.trim().length()>1)
             return "FORMAT_GROUP=1 and spl_id  in (select spl_id from dgv_spl_title where contains(title,'"+textQuery.trim().toUpperCase()+"')>0) ";

           if (sectionTypeCode.trim().equals("otc99-01") && textQuery.trim().length()>1)
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and (contains(CONTENT_XML,'External use')>0 or contains(CONTENT_XML,'Rectal use')>0 or contains(CONTENT_XML,'Vaginal use')>0) and contains(CONTENT_XML,'"+textQuery.trim()+"')>0)";

           if (sectionTypeCode.trim().equals("otc99-02") && textQuery.trim().length()>1)
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Reye’s Syndrome')>0 and contains(CONTENT_XML,'"+textQuery.trim()+"')>0)";

           if (sectionTypeCode.trim().equals("otc99-03") && textQuery.trim().length()>1)
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec whereloinc_code='34071-1' and contains(CONTENT_XML,'Allergy Alert')>0  and contains(CONTENT_XML,'"+textQuery.trim()+"')>0)";

           if (sectionTypeCode.trim().equals("otc99-04") && textQuery.trim().length()>1)
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Flammability Warning')>0  and contains(CONTENT_XML,'"+textQuery.trim()+"')>0)";

           if (sectionTypeCode.trim().equals("otc99-05") && textQuery.trim().length()>1)
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Choking')>0 and contains(CONTENT_XML,'"+textQuery.trim()+"')>0)";

           if (sectionTypeCode.trim().equals("otc99-06") && textQuery.trim().length()>1)
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Alcohol Warning' )>0 and contains(CONTENT_XML,'"+textQuery.trim()+"')>0)";

           if (sectionTypeCode.trim().equals("otc99-07") && textQuery.trim().length()>1)
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Sore Throat Warning')>0 and contains(CONTENT_XML,'"+textQuery.trim()+"')>0)";

           if (sectionTypeCode.trim().equals("otc99-08") && textQuery.trim().length()>1)
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Dosage Warning ')>0 and contains(CONTENT_XML,'"+textQuery.trim()+"')>0)";

           if (sectionTypeCode.trim().equals("otc99-11") && textQuery.trim().length()>1)
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Asthma Alert')>0 and contains(CONTENT_XML,'"+textQuery.trim()+"')>0)";

           if (sectionTypeCode.trim().equals("otc99-12") && textQuery.trim().length()>1)
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Liver Warning')>0 and contains(CONTENT_XML,'"+textQuery.trim()+"')>0)";

           if (sectionTypeCode.trim().equals("otc99-13") && textQuery.trim().length()>1)
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Stomach Bleeding Warning')>0 and contains(CONTENT_XML,'"+textQuery.trim()+"')>0)";

           throw new IllegalArgumentException("Invalid additional section criterion in query criterion: field is of wrong type.");
          } 
          else return
            "FORMAT_GROUP="+groupNo+" and spl_id in (" +
              "select spl_id from spl_sec" +
              " where loinc_code = :" + sectionCodeParamName +
              " and contains(content_xml, :" + textQueryParamName + ") > 0" +
            ")";
        }
        else // No text query: just require existence of the section.
        {
          String sectionCodeParamName = sqlParams.addNewParam("sc", sectionTypeCode.trim());
          if (sectionTypeCode.trim().startsWith("99999") || sectionTypeCode.trim().startsWith("otc99") ){
            if (sectionTypeCode.trim().equals("99999-1"))
              return " spl_id in (select spl_id from DGV_SUM_RX_SPL where FORMAT_GROUP=1 and INITIAL_APPROVAL_YEAR is not null)";
            if (sectionTypeCode.trim().equals("99999-2"))
              return " spl_id in (select spl_id from DGV_SUM_RX_SPL where FORMAT_GROUP=1 and PRODUCT_TITLE is not null)";

           if (sectionTypeCode.trim().equals("otc99-01") )
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and (contains(CONTENT_XML,'External use')>0 or contains(CONTENT_XML,'Rectal use')>0 or contains(CONTENT_XML,'Vaginal use')>0) )";

           if (sectionTypeCode.trim().equals("otc99-02") )
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Reye’s Syndrome')>0)";

           if (sectionTypeCode.trim().equals("otc99-03"))
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Allergy Alert')>0  )";

           if (sectionTypeCode.trim().equals("otc99-04") )
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Flammability Warning')>0 )";

           if (sectionTypeCode.trim().equals("otc99-05") )
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Choking')>0 )";

           if (sectionTypeCode.trim().equals("otc99-06") )
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Alcohol Warning' )>0 )";

           if (sectionTypeCode.trim().equals("otc99-07") )
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Sore Throat Warning')>0 )";

           if (sectionTypeCode.trim().equals("otc99-08") )
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Dosage Warning ')>0 )";

           if (sectionTypeCode.trim().equals("otc99-11") )
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Asthma Alert')>0 )";

           if (sectionTypeCode.trim().equals("otc99-12") )
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Liver Warning')>0 )";

           if (sectionTypeCode.trim().equals("otc99-13") )
             return "FORMAT_GROUP=3 and spl_id in ( select distinct spl_id from spl_sec where loinc_code='34071-1' and contains(CONTENT_XML,'Stomach Bleeding Warning')>0 )";

            return " 1=1 ";
          }
          else 
            return "FORMAT_GROUP="+groupNo+" and spl_id in (select distinct spl_id from spl_sec where loinc_code = :" + sectionCodeParamName + ")";
        }
      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      log.severe("Invalid section text criterion in query criterion: required field is missing");
      throw new IllegalArgumentException("Invalid section text criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting section condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid section text criterion in query criterion: field is is of wrong type.");
    }
  }

  String marketingCategoryCondOnSplSum(JsonObject splMarketingCategoryCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      JsonArray mktCatCodesJson = splMarketingCategoryCriterion.getJsonArray("marketingCategoryCodes");

      if (!mktCatCodesJson.isEmpty())
      {
        List<String> mktCatCodes = asStringList(mktCatCodesJson);
        String mktCatCodesParamName = sqlParams.addNewParam("mcs", mktCatCodes);
        return "spl_id in (select spl_id from prod_appr where marketing_cat_ncit_code in (:" + mktCatCodesParamName + "))";
      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      log.severe("Invalid marketing category criterion in query criterion: required field is missing");
      throw new IllegalArgumentException("Invalid marketing category criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting marketing category condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid marketing category criterion in query criterion: field is is of wrong type.");
    }
  }


  String marketingCategoryCondOnSplSumLDT(JsonObject splMarketingCategoryCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      JsonArray mktCatCodesJson = splMarketingCategoryCriterion.getJsonArray("marketingCategoryCodes");

      if (!mktCatCodesJson.isEmpty())
      {
        List<String> mktCatCodes = asStringList(mktCatCodesJson);
        String mktCatCodesParamName = sqlParams.addNewParam("mcs", mktCatCodes);
        return "spl_id in (select spl_id from dgv_prod_appr where marketing_cat_ncit_code in (:" + mktCatCodesParamName + "))";
      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      log.severe("Invalid marketing category criterion in query criterion: required field is missing");
      throw new IllegalArgumentException("Invalid marketing category criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting marketing category condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid marketing category criterion in query criterion: field is is of wrong type.");
    }
  }


  String marketStatusCondOnSplSum(JsonObject marketStatusCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      List<String> pma_conds = new ArrayList<>();

      String status = marketStatusCriterion.getString("status");
      String startMin = marketStatusCriterion.getString("startMin");
      String startMax = marketStatusCriterion.getString("startMax");
      String endMin = marketStatusCriterion.getString("endMin");
      String endMax = marketStatusCriterion.getString("endMax");

      if (!status.isEmpty() || !startMin.isEmpty() || !startMax.isEmpty() || !endMin.isEmpty() || !endMax.isEmpty())
      {
        if (!status.isEmpty())
        {
          String param = sqlParams.addNewParam("mst", status);
          pma_conds.add("status_code = :"+param);
        }

        if (!startMin.isEmpty())
        {
          String dateStr = getYYYYMMDDFromISO8601TS(startMin);
          String param = sqlParams.addNewParam("smin", dateStr);
          pma_conds.add("low_date >= :"+param);
        }
        if (!startMax.isEmpty())
        {
          String dateStr = getYYYYMMDDFromISO8601TS(startMax);
          String param = sqlParams.addNewParam("smax", dateStr);
          pma_conds.add("low_date <= :"+param);
        }
        if (!endMin.isEmpty())
        {
          String dateStr = getYYYYMMDDFromISO8601TS(endMin);
          String param = sqlParams.addNewParam("emin", dateStr);
          pma_conds.add("high_date >= :"+param);
        }
        if (!endMax.isEmpty())
        {
          String dateStr = getYYYYMMDDFromISO8601TS(endMax);
          String param = sqlParams.addNewParam("emax", dateStr);
          pma_conds.add("high_date <= :"+param);
        }

        return "spl_id in (select pma.spl_id from prod_mkt_act pma where " + combineConds("and", pma_conds) + ")";

      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      log.severe("Invalid market status criterion in query criterion: required field is missing");
      throw new IllegalArgumentException("Invalid market status criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting market status condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid market status criterion in query criterion: field is is of wrong type.");
    }
    catch(ParseException e)
    {
      log.severe("Invalid date format in market status criteria.");
      throw new IllegalArgumentException("Invalid date format in market status criteria.");
    }
  }


  String marketStatusCondOnSplSumLDT(JsonObject marketStatusCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      List<String> pma_conds = new ArrayList<>();

      String status = marketStatusCriterion.getString("status");
      String startMin = marketStatusCriterion.getString("startMin");
      String startMax = marketStatusCriterion.getString("startMax");
      String endMin = marketStatusCriterion.getString("endMin");
      String endMax = marketStatusCriterion.getString("endMax");

      if (!status.isEmpty() || !startMin.isEmpty() || !startMax.isEmpty() || !endMin.isEmpty() || !endMax.isEmpty())
      {
        if (!status.isEmpty())
        {
          String param = sqlParams.addNewParam("mst", status);
          pma_conds.add("status_code = :"+param);
        }

        if (!startMin.isEmpty())
        {
          String dateStr = getYYYYMMDDFromISO8601TS(startMin);
          String param = sqlParams.addNewParam("smin", dateStr);
          pma_conds.add("low_date >= :"+param);
        }
        if (!startMax.isEmpty())
        {
          String dateStr = getYYYYMMDDFromISO8601TS(startMax);
          String param = sqlParams.addNewParam("smax", dateStr);
          pma_conds.add("low_date <= :"+param);
        }
        if (!endMin.isEmpty())
        {
          String dateStr = getYYYYMMDDFromISO8601TS(endMin);
          String param = sqlParams.addNewParam("emin", dateStr);
          pma_conds.add("high_date >= :"+param);
        }
        if (!endMax.isEmpty())
        {
          String dateStr = getYYYYMMDDFromISO8601TS(endMax);
          String param = sqlParams.addNewParam("emax", dateStr);
          pma_conds.add("high_date <= :"+param);
        }

        return "spl_id in (select pma.spl_id from dgv_prod_mkt_act pma where " + combineConds("and", pma_conds) + ")";

      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      log.severe("Invalid market status criterion in query criterion: required field is missing");
      throw new IllegalArgumentException("Invalid market status criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting market status condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid market status criterion in query criterion: field is is of wrong type.");
    }
    catch(ParseException e)
    {
      log.severe("Invalid date format in market status criteria.");
      throw new IllegalArgumentException("Invalid date format in market status criteria.");
    }
  }


  String routeOfAdminCondOnSplSum(JsonObject routeCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      JsonArray routeCodesJson = routeCriterion.getJsonArray("routeOfAdminCodes");

      if (!routeCodesJson.isEmpty())
      {
        List<String> routeCodes = asStringList(routeCodesJson);
        String routeCodesParamName = sqlParams.addNewParam("rs", routeCodes);
        return "spl_id in (select spl_id from prod_route where ncit_route_of_admin_code in (:" + routeCodesParamName + "))";
      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      log.severe("Invalid route criterion in query criterion: required field is missing");
      throw new IllegalArgumentException("Invalid route criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting route condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid route criterion in query criterion: field is is of wrong type.");
    }
  }


  String routeOfAdminCondOnSplSumLDT(JsonObject routeCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      JsonArray routeCodesJson = routeCriterion.getJsonArray("routeOfAdminCodes");

      if (!routeCodesJson.isEmpty())
      {
        List<String> routeCodes = asStringList(routeCodesJson);
        String routeCodesParamName = sqlParams.addNewParam("rs", routeCodes);
        return "spl_id in (select spl_id from dgv_prod_route where ncit_route_of_admin_code in (:" + routeCodesParamName + "))";
      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      log.severe("Invalid route criterion in query criterion: required field is missing");
      throw new IllegalArgumentException("Invalid route criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting route condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid route criterion in query criterion: field is is of wrong type.");
    }
  }


  String dosageFormCondOnSplSum(JsonObject dosageFormCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      JsonArray dosageFormCodesJson = dosageFormCriterion.getJsonArray("dosageFormCodes");

      if (!dosageFormCodesJson.isEmpty())
      {
        List<String> dosageFormCodes = asStringList(dosageFormCodesJson);
        String dosageFormCodesParamName = sqlParams.addNewParam("dfs", dosageFormCodes);
        return "spl_id in (select lp.spl_id from spl_prod lp where lp.ncit_form_code in (:"+dosageFormCodesParamName+"))";
      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      log.severe("Invalid dosage form criterion in query criterion: required field is missing");
      throw new IllegalArgumentException("Invalid dosage form criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting dosage form condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid dosage form criterion in query criterion: field is is of wrong type.");
    }
  }

  String dosageFormCondOnSplSumLDT(JsonObject dosageFormCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      JsonArray dosageFormCodesJson = dosageFormCriterion.getJsonArray("dosageFormCodes");

      if (!dosageFormCodesJson.isEmpty())
      {
        List<String> dosageFormCodes = asStringList(dosageFormCodesJson);
        String dosageFormCodesParamName = sqlParams.addNewParam("dfs", dosageFormCodes);
        return "spl_id in (select lp.spl_id from dgv_spl_prod lp where lp.ncit_form_code in (:"+dosageFormCodesParamName+"))";
      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      log.severe("Invalid dosage form criterion in query criterion: required field is missing");
      throw new IllegalArgumentException("Invalid dosage form criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting dosage form condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid dosage form criterion in query criterion: field is is of wrong type.");
    }
  }

  String chemicalStructureCondOnSplSum(JsonObject chemStructCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      String mol = chemStructCriterion.getString("acceptedMol", "");
      String searchType = chemStructCriterion.getString("structureSearchType", "substructure");
      String minSim = chemStructCriterion.getString("minimumSimilarity", "");
      String ingredientType = chemStructCriterion.getString("ingredientTypes");

      String storedFunc = searchType.equals("substructure") ? "sub" :
                          searchType.equals("similarity") ? "sim" :
                          searchType.equals("exact") ? "exact" :
                          illegalArg("Chemical structure search type " + searchType + " is currently not supported.");

      if (searchType.equals("similarity")) {
        try {
          Double.parseDouble(minSim);
        }
        catch(Exception e) {
          illegalArg("Invalid similarity provided.");
        }
      }

      String comparison = storedFunc.equals("sim") ? (">= " + minSim) : " = 1";

      if (condNonEmpty(mol))
      {
	String molParamName = sqlParams.addNewParam("mol", mol.trim());

	String bingoFunc = storedFunc+"(smiles, :mol) ";	
	String qry = "select unii from srs.unii_data where bingo." + bingoFunc + comparison;
	JsonArray srsUniiResults= queryForJsonArray (qry, params("mol", mol.trim()));

	List<String> srsUniiCodes =new ArrayList<String>();
	String strUniis = "-1";
	String srsUniiParamName = "srsUnii";
	JsonObject jo;
	if (!srsUniiResults.isEmpty()) {
	  for (int i=0; i<srsUniiResults.size(); i++) {
	    jo = srsUniiResults.getJsonObject(i);
	    srsUniiCodes.add(jo.getString("unii"));
	    //strUniis = strUniis + "," + jo.getString("unii");
	  } 
	  srsUniiParamName = sqlParams.addNewParam("srsUnii", srsUniiCodes);
	} else {
	  srsUniiParamName = sqlParams.addNewParam("srsUnii", "-1");
	}

		
	if (ingredientType.isEmpty() || ingredientType.equals("ACT")) {
          return
            "spl_id IN (" +
                "select /*+ NO_REWRITE */ spl_id from prod_ingr pi " +
                "where pi.is_active=1 AND pi.unii in (" +
                  "select unii from srs.unii_data where bingo."+storedFunc+"(smiles, :"+molParamName + ") " + comparison +
                ")" +
            ")";
	} else if (ingredientType.equals("NACT")) {
	  return
            "spl_id IN (" +
                "select /*+ NO_REWRITE */ spl_id from prod_ingr pi " +
                "where pi.is_active=0 AND pi.unii in (" +
                  "select unii from srs.unii_data where bingo."+storedFunc+"(smiles, :"+molParamName + ") " + comparison +
                ")" +
            ")";
	} else {
	  return
            "spl_id IN (" +
                "select /*+ NO_REWRITE */ spl_id from prod_ingr pi " +
                "where pi.unii in (" +
                  "select unii from srs.unii_data where bingo."+storedFunc+"(smiles, :"+molParamName + ") " + comparison +
                ")" +
            ")";
	}
      }
      else
        return "";
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting chemical structure condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid chemical structure criterion: field is is of wrong type.");
    }
  }

/*
  String chemicalStructureCondOnSplSum(JsonObject chemStructCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      String mol = chemStructCriterion.getString("acceptedMol", "");
      String searchType = chemStructCriterion.getString("structureSearchType", "substructure");
      String minSim = chemStructCriterion.getString("minimumSimilarity", "");
      String ingredientType = chemStructCriterion.getString("ingredientTypes");

      String storedFunc = searchType.equals("substructure") ? "sub" :
                          searchType.equals("similarity") ? "sim" :
                          searchType.equals("exact") ? "exact" :
                          illegalArg("Chemical structure search type " + searchType + " is currently not supported.");

      if (searchType.equals("similarity")) {
        try {
          Double.parseDouble(minSim);
        }
        catch(Exception e) {
          illegalArg("Invalid similarity provided.");
        }
      }

      String comparison = storedFunc.equals("sim") ? (">= " + minSim) : " = 1";

      if (condNonEmpty(mol))
      {
	String molParamName = sqlParams.addNewParam("mol", mol.trim());

	String bingoFunc = storedFunc+"(smiles, :mol) ";	
	String qry = "select unii from srs.unii_data where bingo." + bingoFunc + comparison;
//System.out.println("daojun qry=" + qry);
//System.out.println("daojun mol=" + mol.trim());
	JsonArray srsUniiResults= queryForJsonArray (qry, params("mol", mol.trim()));

	List<String> srsUniiCodes =new ArrayList<String>();
	String strUniis = "-1";
	String srsUniiParamName = "srsUnii";
	JsonObject jo;
	String cond_unii = "";

	int unii_count = srsUniiResults.size();
	if (!srsUniiResults.isEmpty()) {
	  if(unii_count > 999) {
	    int r_1k = unii_count/998, i;
	    for (i=0; i<r_1k; i++) {
	      srsUniiCodes =new ArrayList<String>();
	      for(int j=0; j<998; j++) {
		jo = srsUniiResults.getJsonObject(i*998+j);
	        srsUniiCodes.add(jo.getString("unii"));
	      }
	      srsUniiParamName = sqlParams.addNewParam("srsUnii", srsUniiCodes);
	      if(i==0) {
	        cond_unii = "pi.unii in (:" + srsUniiParamName +") ";
	      } else {
	        cond_unii = cond_unii + " OR " + "pi.unii in (:" + srsUniiParamName +") ";
              }
	    }
	    int rt_1k = unii_count - i*998;
	    if(rt_1k > 0) {
	      srsUniiCodes =new ArrayList<String>();
	      for(int j=0; j<rt_1k; j++) {
		jo = srsUniiResults.getJsonObject(i*998+j);
	        srsUniiCodes.add(jo.getString("unii"));
	      }
	      srsUniiParamName = sqlParams.addNewParam("srsUnii", srsUniiCodes);
	      cond_unii = cond_unii + " OR " + "pi.unii in (:" + srsUniiParamName +") ";
	    }
	  } else {
	    for (int i=0; i<srsUniiResults.size(); i++) {
            //for (int i=0; i<900; i++) {
	      jo = srsUniiResults.getJsonObject(i);
	      srsUniiCodes.add(jo.getString("unii"));
	      //strUniis = strUniis + "," + jo.getString("unii");
	    } 
	    srsUniiParamName = sqlParams.addNewParam("srsUnii", srsUniiCodes);
	    cond_unii = "pi.unii in (:" + srsUniiParamName +") ";
	  }
	} else {
	  srsUniiParamName = sqlParams.addNewParam("srsUnii", "-1");
	}
System.out.println("daojun srsUniiCodes=" + srsUniiCodes);
System.out.println("daojun cond_unii=" + cond_unii);

	if (ingredientType.isEmpty()) {
          return
            "spl_id IN (" +
                "select spl_id from prod_ingr pi " +
                "where pi.is_active=1 AND " + //pi.unii in (:" + srsUniiParamName +") " +
		"(" + cond_unii + ")" +
            ")";
	} else if (ingredientType.equals("ACT")) {
	  return
            "spl_id IN (" +
                "select spl_id from prod_ingr pi " +
                "where pi.is_active=1 AND " + //pi.unii in (:" + srsUniiParamName +") " +
		"(" + cond_unii + ")" +
            ")";
	} else if (ingredientType.equals("NACT")) {
	  return
            "spl_id IN (" +
                "select spl_id from prod_ingr pi " +
                "where pi.is_active=0 AND pi.unii in (:" + srsUniiParamName +") " +
            ")";
	} else {
	  return
            "spl_id IN (" +
                "select spl_id from prod_ingr pi " +
                "where pi.unii in (:" + srsUniiParamName +") " +
            ")";
	}	
      }
      else
        return "";
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting chemical structure condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid chemical structure criterion: field is is of wrong type.");
    }
  }
*/

  String identifierCondOnSplSum(JsonObject identifierCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      String identifiersText = identifierCriterion.getString("identifiers");
      String ingredientType = identifierCriterion.getString("ingredientTypes");
      //throw new IllegalArgumentException("identifier '" + identifiersText + "': ingredient: " + ingredientType);

      if (!identifiersText.isEmpty())
      {
        List<String> uniis = new ArrayList<>();
        List<String> ndcs = new ArrayList<>();
        List<String> setIds = new ArrayList<>();
        List<String> deas = new ArrayList<>();
        List<String> appnos = new ArrayList<>();

        for(String id: identifiersText.split("[,;:\\s]+")) {
          if (id.length() == 36)
            setIds.add(id.toLowerCase());
          else if (id.matches("[A-Za-z]{1,5}\\d{6}")) //letters with length 1-5 followed by 6 digits number, considered as valid application number
            appnos.add(id.toUpperCase());
          else if (id.length() > 1 && id.length() < 5 && id.toUpperCase().startsWith("C"))
            deas.add(id.toUpperCase());
          else if (id.length() == 10 && id.indexOf('-') == -1)
            uniis.add(id);
          else if (id.length() >= 8 && id.length() <= 13 && id.indexOf('-') != -1) { // NDC code
            int firstDashIx = id.indexOf('-');
            int lastDashIx = id.lastIndexOf('-');
            if (lastDashIx != firstDashIx) // Remove packaging section if present.
              ndcs.add(id.substring(0, lastDashIx));
            else
              ndcs.add(id);
          }
          else
            throw new IllegalArgumentException("Invalid identifier '" + id + "': identifier must be a valid UNII, NDC, DEA, Application Number or SET ID.");
        }

        List<String> conds = new ArrayList<String>();

        if (!uniis.isEmpty()) {
          String uniisParamName = sqlParams.addNewParam("uniis", uniis);
          //conds.add("spl_id in (select spl_id from prod_ingr where is_active = 1 and unii in (:" + uniisParamName + "))");
		
	  if (ingredientType.isEmpty()) {
             conds.add("spl_id in (select spl_id from prod_ingr where unii in (:" + uniisParamName + "))");
	  } else if (ingredientType.equals("ACT")) {
	      	conds.add("spl_id in (select spl_id from prod_ingr where is_active = 1 and unii in (:" + uniisParamName + "))"); 
	  } else if (ingredientType.equals("NACT")) {
		conds.add("spl_id in (select spl_id from prod_ingr where is_active = 0 and unii in (:" + uniisParamName + "))");
	  } else {
		conds.add("spl_id in (select spl_id from prod_ingr where unii in (:" + uniisParamName + "))");
	  }
	  
        }
        if (!ndcs.isEmpty()) {
          String ndcsParamName = sqlParams.addNewParam("ndcs", ndcs);
          conds.add("spl_id in (select spl_id from spl_prod where ndc_code in (:" + ndcsParamName + "))");
        }
        if (!setIds.isEmpty()) {
          String setIdsParamName = sqlParams.addNewParam("sids", setIds);
          conds.add("spl_id in (select id from spl where set_id in (:" + setIdsParamName + "))");
        }

        if (!appnos.isEmpty()) {
          String appnosParamName = sqlParams.addNewParam("appnos", appnos);
          conds.add("spl_id in (select SPL_ID from PROD_APPR where APPROVAL_NUM in (:" + appnosParamName + "))");
        }

        //TODO: prod_deapolicy doesn't exist yet, need to create the new table and fill with data.
        if (!deas.isEmpty()) {
          String deasParamName = sqlParams.addNewParam("deas", deas);
          conds.add("spl_id in (select SPL_ID from PROD_DEA where NCIT_DEA_NAME in (:" + deasParamName + "))");
        }


        return combineConds("or", conds);
      }
      else
        return ""; 
    } 
    catch(NullPointerException e)
    {
      log.severe("Invalid identifiers criterion in query criterion: required field is missing");
      throw new IllegalArgumentException("Invalid identifiers criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting identifiers condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid identifiers criterion in query criterion: field is is of wrong type.");
    } 
  }

  String identifierCondOnSplSumLDT(JsonObject identifierCriterion, NameGeneratingSqlParameterSource sqlParams)
  {
    try
    {
      String identifiersText = identifierCriterion.getString("identifiers");
      String ingredientType = identifierCriterion.getString("ingredientTypes");

      if (!identifiersText.isEmpty())
      {
        List<String> uniis = new ArrayList<>();
        List<String> ndcs = new ArrayList<>();
        List<String> setIds = new ArrayList<>();
        List<String> deas = new ArrayList<>();
        List<String> appnos = new ArrayList<>();
        for(String id: identifiersText.split("[,;:\\s]+")) {
          if (id.length() >= 2 && id.length() <= 4 && id.toUpperCase().startsWith("C")){
            deas.add(id.toUpperCase());
          }else{
           if (id.length() == 36)
             setIds.add(id.toLowerCase());
           else if (id.length() == 10 && id.indexOf('-') == -1 && !(id.toUpperCase().startsWith("BLA") || id.toUpperCase().startsWith("NDA") || id.toUpperCase().startsWith("ANDA")))
             uniis.add(id);
           else if (id.length() >= 8 && id.length() <= 15  ) { // NDC code or Applicaion No
             if (id.indexOf('-') != -1){ // NDC code
               int firstDashIx = id.indexOf('-');
               int lastDashIx = id.lastIndexOf('-');
               if (lastDashIx != firstDashIx) // Remove packaging section if present.
                 ndcs.add(id.substring(0, lastDashIx));
               else
                 ndcs.add(id);
             }else{ //Applicaion No
                appnos.add(id.toUpperCase());
             } 
           }
           else
            throw new IllegalArgumentException("Invalid identifier '" + id + "': identifier must be a valid UNII, NDC,DEA, Application Number or SET ID.");
         }
        }
        List<String> conds = new ArrayList<String>();

        if (!uniis.isEmpty()) {
          String uniisParamName = sqlParams.addNewParam("uniis", uniis);
          //conds.add("spl_id in (select spl_id from dgv_prod_ingr where is_active = 1 and unii in (:" + uniisParamName + "))");
	
 	  if (ingredientType.isEmpty()) {
             conds.add("spl_id in (select spl_id from dgv_prod_ingr where unii in (:" + uniisParamName + "))");
	  } else if (ingredientType.equals("ACT")) {
	      	conds.add("spl_id in (select spl_id from dgv_prod_ingr where is_active = 1 and unii in (:" + uniisParamName + "))"); 
	  } else if (ingredientType.equals("NACT")) {
		conds.add("spl_id in (select spl_id from dgv_prod_ingr where is_active = 0 and unii in (:" + uniisParamName + "))");
	  } else {
		conds.add("spl_id in (select spl_id from dgv_prod_ingr where unii in (:" + uniisParamName + "))");
	  }
	
        }
        if (!ndcs.isEmpty()) {
          String ndcsParamName = sqlParams.addNewParam("ndcs", ndcs);
          conds.add("spl_id in (select spl_id from dgv_spl_prod where ndc_code in (:" + ndcsParamName + ") or EQUIV_TO_NDC_CODE in (:"+ndcsParamName+"))");
        }
        if (!setIds.isEmpty()) {
          String setIdsParamName = sqlParams.addNewParam("sids", setIds);
          conds.add("spl_id in (select id from spl where set_id in (:" + setIdsParamName + "))");
        }

        if (!appnos.isEmpty()) {
          String appnosParamName = sqlParams.addNewParam("appnos", appnos);
          conds.add("spl_id in (select SPL_ID from DGV_PROD_APPR where APPROVAL_NUM in (:" + appnosParamName + "))");
        }

        if (!deas.isEmpty()) {
          String deasParamName = sqlParams.addNewParam("deas", deas);
          conds.add("spl_id in (select SPL_ID from DGV_PROD_DEAPOLICY where POLICY_NAME in (:" + deasParamName + "))");
        }

      return combineConds("or", conds);
      }
      else
        return "";
    }
    catch(NullPointerException e)
    {
      log.severe("Invalid identifiers criterion in query criterion: required field is missing");
      throw new IllegalArgumentException("Invalid identifiers criterion in query criterion: required field is missing.");
    }
    catch(ClassCastException e)
    {
      log.severe("Error interpreting identifiers condition: " + e.getMessage());
      throw new IllegalArgumentException("Invalid identifiers criterion in query criterion: field is is of wrong type.");
    }
  }

  // Fetch search terms from the passed criterion as a set of regular expressions. Search terms are loosely
  // defined as parts of the criteria that clients may want to highlight in result documents.
  public Set<String> searchTerms(JsonObject criterion) {
    HashSet<String> terms = new HashSet<>();
    try
    {
      if (isEntityCriterion(criterion))
      {
        switch(criterion.getString("sourceEntity"))
        {
          case "spl-text":
            terms.addAll(splTextSearchTerms(criterion));
            break;
          case "section":
            terms.addAll(splTextSearchTerms(criterion));
             break;
          case "product":
            terms.addAll(productSearchTerms(criterion));
            break;
          case "meddra":
            terms.addAll(meddraSearchTerms(criterion));
            break;
          case "pharm-class":
            terms.addAll(pharmClassSearchTerms(criterion));
            break;
          default:
            // no terms to add
        }
      }
      else if (isGroupCriterion(criterion))
      {
        JsonArray subcriteria = criterion.getJsonArray("criteria");

        for(int i=0; i<subcriteria.size(); ++i)
        {
          terms.addAll(searchTerms(subcriteria.getJsonObject(i)));
        }
      }
      else
        throw new IllegalArgumentException("The query criterion is invalid.");

      return terms;
    }
    catch(NullPointerException e)
    {
      throw new IllegalArgumentException("Invalid group criterion in query: required field is missing.");
    }
    catch(ClassCastException e)
    {
      throw new IllegalArgumentException("Invalid group criterion in query: field encountered with unexpected type.");
    }
  }


  Set<String> productSearchTerms(JsonObject productCriterion) {
    HashSet<String> terms = new HashSet<>();
    if (productCriterion.containsKey("namePattern"))
    {
      String namePattern = productCriterion.getString("namePattern").trim();
      if (namePattern.length() > 0 ) {
        StringPatternType namePatternType = StringPatternType.valueOf(productCriterion.getString("namePatternType"));
        switch (namePatternType)
        {
          case SUBSTR:
            terms.add(namePattern);
            break;
          case GLOB:
            break;
          case REGEX:
            break;
          default:
            throw new IllegalArgumentException("Unrecognized product name match type \"" + namePatternType + "\" in query criterion.");
        }
      }
    }
    return terms;
  }


  Set<String> meddraSearchTerms(JsonObject meddraCriterion) {
    HashSet<String> terms = new HashSet<>();
    for(JsonValue termInfo_: meddraCriterion.getJsonArray("terms"))
    {
      JsonObject termInfo = (JsonObject)termInfo_;

      terms.add(termInfo.getString("term"));
    }
    return terms;
  }


  Set<String> pharmClassSearchTerms(JsonObject pharmClassCriterion) {
    HashSet<String> terms = new HashSet<>();
    for(JsonValue classInfo_: pharmClassCriterion.getJsonArray("pharmClasses"))
    {
      JsonObject termInfo = (JsonObject)classInfo_;

      terms.add(termInfo.getString("className"));
    }
    return terms;
  }


  Set<String> splTextSearchTerms(JsonObject splTextCriterion) {
    HashSet<String> terms = new HashSet<>();
    if (splTextCriterion.containsKey("textQuery"))
    {
      String textQuery = splTextCriterion.getString("textQuery").trim();
      if (textQuery.length() > 0 )
      {
        // If a simple list of words then consider as a single phrase.
        if (textQuery.toLowerCase().indexOf(" and ") == -1 && textQuery.toLowerCase().indexOf(" or ") == -1 && textQuery.indexOf("(") == -1)
        {
          terms.add(textQuery.trim());
        }
        else
        {
          String[] words = textQuery.split("[ ,;()`!@#$%^&*=+/.-]");
          for (String word : words) {
            word = word.toLowerCase();
            if (word.length() > 2 && !(word.equals("and") || word.equals("or") || word.equals("not"))) {
              terms.add(word);
            }
          }
        }
      }
    }
    return terms;
  }


  String condFieldMatchesFragments(String field, String fragmentsString, NameGeneratingSqlParameterSource sqlParams)
  {
    return condOneOrMoreFieldsMatchFragments(Arrays.asList(field), fragmentsString, sqlParams);
  }


  /* Return a SQL condition requiring that at least one of the passed alternative fields contains every fragment in the
     fragments string, which are separated by whitespace. */
  String condOneOrMoreFieldsMatchFragments(List<String> alternativeFields,
                                           String fragmentsString,
                                           NameGeneratingSqlParameterSource sqlParams) // mutated to add params
  {
    List<String> fragments = parseStringMatchingFragments(fragmentsString);

    if (fragments.size() == 0)
      return "";

    StringBuilder condBuf = new StringBuilder();

    for(int fieldIx=0; fieldIx<alternativeFields.size(); ++fieldIx)
    {
      if (fieldIx != 0)
        condBuf.append(" or ");

      for (int fragIx=0; fragIx<fragments.size(); ++fragIx)
      {
        String paramName = sqlParams.addNewParam("pat", fragments.get(fragIx).toLowerCase());

        if (fragIx != 0 )
          condBuf.append(" and ");

        condBuf.append("instr(lower(")
               .append(alternativeFields.get(fieldIx))
               .append("), :")
               .append(paramName)
               .append(") > 0");
      }
    }

    return condBuf.toString();
  }


  List<String> parseStringMatchingFragments(String s)
  {
    try
    {
      List<String> frags = new ArrayList<>();
      StreamTokenizer tokenizer = new StreamTokenizer(new StringReader(s));
      while(tokenizer.nextToken() != StreamTokenizer.TT_EOF)
      {
        if (tokenizer.sval != null)
        {
          String frag = tokenizer.sval.trim();
          if (frag.length() > 0)
            frags.add(frag);
        }
      }
      return frags;
    }
    catch(IOException ioe)
    {
      throw new RuntimeException(ioe);
    }
  }


  boolean isEntityCriterion(JsonObject criterion)
  {
    return criterion.containsKey("sourceEntity");
  }


  boolean isGroupCriterion(JsonObject criterion)
  {
    return criterion.containsKey("logOps") && criterion.containsKey("criteria");
  }


  String logOpSql(String logOpCode)
  {
    if (logOpCode.charAt(0) == 'a')
      return "and";
    else if (logOpCode.charAt(0) == 'o')
      return "or";
    else throw new IllegalArgumentException("Invalid logical operator encountered in query criterion.");
  }


  String combineConds(String logicalOp, String... conds) {
    StringBuilder buf = new StringBuilder();
    boolean condWritten = false;
    for(String cond: conds) {
      if (condNonEmpty(cond)) {
        if (condWritten)
          buf.append(" " + logicalOp + " ");
        buf.append("("  + cond + ")");
        condWritten = true;
      }
    }
    return buf.toString();
  }

  String combineConds(String logicalOp, List<String> conds) {
   return combineConds(logicalOp, conds.toArray(new String[conds.size()]));
  }


  JsonArray queryForJsonArray(String qry, NameGeneratingSqlParameterSource sqlParams)
  {
    return queryForJsonArray(qry, sqlParams.getValues());
  }


  JsonArray queryForJsonArray(String qry, Map<String,Object> sqlParams)
  {
    JsonArrayAccumulatorRowCallback jsonArrayAccumulator = new JsonArrayAccumulatorRowCallback();
System.out.println("queryForJsonArray=" + qry);
    //writetofile(qry);
    jdbcTemplate.query(qry, sqlParams, jsonArrayAccumulator);
   
    return jsonArrayAccumulator.getResults();
  }

  void writetofile(String sOut)
  {
      
      try {
	  BufferedWriter out = new BufferedWriter(new FileWriter("output.txt"));
	  out.write(sOut);
	  out.newLine();
 	  out.close();
	} catch (IOException el) {
		
	} finally {
	 
	}

  }

  Map<String, Object> params(Object... namesAndValues)
  {
    if (namesAndValues.length % 2 != 0)
      throw new IllegalArgumentException("Keys and values must occur in pairs.");

    Map<String, Object> m = new HashMap<>();

    for (int i = 0; i < namesAndValues.length; i+=2)
    {
      m.put((String)namesAndValues[i], namesAndValues[i+1]);
    }

    return m;
  }


  boolean condNonEmpty(String cond)
  {
    return cond != null && cond.trim().length() > 0;
  }


  // spl and related files

  public Document getSplWithoutStylesheetInstruction(String set_id)
  {
      Blob xml_blob = getSplUtf8Blob(set_id);

      return getSplFromUtf8Blob(xml_blob, "", true);
  }


  public Document getSplWithOriginalStylesheetInstruction(String set_id)
  {
    Blob xml_blob = getSplUtf8Blob(set_id);

    return getSplFromUtf8Blob(xml_blob, null, true);
  }


  public Document getSplWithAlternateStylesheet(String set_id, String xsl_url)
  {
    Blob xml_blob = getSplUtf8Blob(set_id);

    return getSplFromUtf8Blob(xml_blob, xsl_url, true);
  }


  public InputStream getSplImageStream(String set_id, String image_name)
  {
    Blob img_blob = jdbcTemplate.queryForObject("select data from spl_att where spl_id = (select id from spl where set_id = :set_id) and name = :name",
        params("set_id", set_id, "name", image_name),
        Blob.class);

    return getBlobStream(img_blob);
  }


  private Document getSplFromUtf8Blob(Blob xml_blob, String xsl_url, boolean free_blob) // xsl_url: null => xsl as is, "" => remove xsl decl, else replace with provided url
  {
      InputStream xml_is = null;

      try
      {
        xml_is = getBlobStream(xml_blob);

        DocumentBuilder docbldr = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = docbldr.parse(xml_is);

        if (xsl_url == null)
        {
          // leave stylesheet decl as is
        }
        else if ("".equals(xsl_url)) // remove stylesheet decl
        {
          NodeList nodes = doc.getChildNodes();
          for(int i=nodes.getLength()-1; i>=0; --i)
          {
            if (nodes.item(i) instanceof ProcessingInstruction)
            {
              ProcessingInstruction pi = (ProcessingInstruction)nodes.item(i);
              if ("xml-stylesheet".equals(pi.getTarget()))
                doc.removeChild(pi);
            }
          }
        }
        else // replace stylesheet decl
        {
          NodeList nodes = doc.getChildNodes();
          for(int i=nodes.getLength()-1; i>=0; --i)
          {
            if (nodes.item(i) instanceof ProcessingInstruction)
            {
              ProcessingInstruction pi = (ProcessingInstruction)nodes.item(i);
              if ("xml-stylesheet".equals(pi.getTarget())) {
                pi.setData("href=\"" + xsl_url + "\" type=\"text/xsl\"");
              }
            }
          }
        }
        return doc;
      }
      catch(ParserConfigurationException pce)
      {
          throw new RuntimeException("Could not instantiate document builder.", pce);
      }
      catch(IOException ioe)
      {
          throw new RuntimeException("Communication error occurred while parsing xml.", ioe);
      }
      catch(SAXException se)
      {
          throw new RuntimeException("Error parsing xml from utf8 blob.", se);
      }
      finally
      {
        if (xml_is != null)
          closeStream(xml_is);
        if ( free_blob )
            freeBlob(xml_blob);
      }
  }


  private Blob getSplUtf8Blob(String set_id)
  {
    return jdbcTemplate.queryForObject("select xmlserialize(document spl_xml as blob encoding 'UTF-8') from spl where set_id = :set_id",
        params("set_id", set_id),
        Blob.class);
  }


  private static InputStream getBlobStream(Blob blob)
  {
    try
    {
      return blob.getBinaryStream();
    }
    catch(SQLException e)
    {
      throw new DataRetrievalFailureException("Error getting blob stream", e);
    }
  }


  private static void freeBlob(Blob blob)
  {
    try
    {
      blob.free();
    }
    catch(SQLException e)
    {
      throw new CleanupFailureDataAccessException("Failed to free() blob.", e);
    }
  }


  private static void closeStream(InputStream is)
  {
    try
    {
      if ( is != null )
        is.close();
    }
    catch(Exception e)
    {
      throw new CleanupFailureDataAccessException("Failed to close input stream.", e);
    }
  }


  private List<String> asStringList(JsonArray jsonArray) {
    List<String> list = new ArrayList<String>();
    for (int i=0; i<jsonArray.size(); i++) {
      list.add(jsonArray.getString(i));
    }
    return list;
  }


  private String getYYYYMMDDFromISO8601TS(String isoTimestamp) throws ParseException {
    String[] dateParts = isoTimestamp.split("[-T]");
    if (dateParts.length < 3)
      throw new ParseException("Bad timestamp: " + isoTimestamp, 0);
    else
      return dateParts[0] + dateParts[1] + dateParts[2];
  }


  private String illegalArg(String msg)
  {
    throw new IllegalArgumentException(msg);
  }

}
