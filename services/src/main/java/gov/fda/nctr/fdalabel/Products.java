package gov.fda.nctr.fdalabel;

import gov.fda.nctr.fdalabel.dataaccess.DAO;

import javax.inject.Inject;
import javax.json.JsonArray;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by sharris
 * Date: 8/21/2013
 * Time: 11:15 AM
 */

@Path("product")
public class Products {

    public enum ProductNameType {GEN, TRADE, ANY}

    private final DAO dao;

    @Inject
    public Products(DAO dao)
    {
      this.dao = dao;
    }

    @GET
    @Path("names-containing/{name-fragments}")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray productNamesContaining(@PathParam("name-fragments")
                                            String nameFragsStr,
                                            @QueryParam("t") @DefaultValue("ANY")
                                            ProductNameType productNameType,
                                            @QueryParam("l") @DefaultValue("50")
                                            int resultsLimit)
    {
      return dao.productNamesContaining(nameFragsStr, productNameType, resultsLimit);
    }

    @GET
    @Path("ldt/names-containing/{name-fragments}")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray productNamesContainingLDT(@PathParam("name-fragments")
                                            String nameFragsStr,
                                            @QueryParam("t") @DefaultValue("ANY")
                                            ProductNameType productNameType,
                                            @QueryParam("l") @DefaultValue("50")
                                            int resultsLimit)
    {
      return dao.productNamesContainingLDT(nameFragsStr, productNameType, resultsLimit);
    }

}
