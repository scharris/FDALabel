package gov.fda.nctr.fdalabel.dataaccess;

/**
 * Created by sharris
 * Date: 2014/10/10
 */
public class StringUtils {

  public static String camelCase(String name)
  {
    StringBuilder res = new StringBuilder();
    for (String word : name.split("_"))
    {
      if ( res.length() == 0 )
        res.append(word.toLowerCase());
      else
      {
        res.append(Character.toUpperCase(word.charAt(0)));
        res.append(word.substring(1).toLowerCase());
      }
    }
    return res.toString();
  }
}
