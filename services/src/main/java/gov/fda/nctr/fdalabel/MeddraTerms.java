package gov.fda.nctr.fdalabel;

import gov.fda.nctr.fdalabel.dataaccess.DAO;

import javax.inject.Inject;
import javax.json.JsonArray;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


@Path("meddra")
public class MeddraTerms {

  public enum TermsType { pt, llt }

  private static final String DEFAULT_SPL_MEDDRA_INDEXING_AUTHORITY = "0"; // switch to "1" (FDA Indexing) when available

  private final DAO dao;

  @Inject
  public MeddraTerms(DAO dao)
  {
    this.dao = dao;
  }


  @GET @Path("containing/{term-fragments}")
  @Produces(MediaType.APPLICATION_JSON)
  public JsonArray meddraTermsContaining(@PathParam("term-fragments")
                                           String termFragments,
                                         @QueryParam("tt") @DefaultValue("pt")
                                           TermsType termsType,
                                         @QueryParam("unlab") @DefaultValue("0")
                                           int includeUnlabeledTerms,
                                         @QueryParam("a") @DefaultValue(DEFAULT_SPL_MEDDRA_INDEXING_AUTHORITY)
                                           int splMeddraIndexingAuthority,
                                         @QueryParam("l") @DefaultValue("50")
                                           int resultsLimit)
  {
    return dao.meddraTermsContaining(termFragments, termsType, includeUnlabeledTerms != 0, splMeddraIndexingAuthority, resultsLimit);
  }

  @GET @Path("pt/{pt}/llts")
  @Produces(MediaType.APPLICATION_JSON)
  public JsonArray meddraLowLevelTermsHavingPreferredTerm(@PathParam("pt")
                                                            String preferredTerm,
                                                          @QueryParam("unlab") @DefaultValue("0")
                                                            int includeUnlabeledTerms,
                                                          @QueryParam("a") @DefaultValue(DEFAULT_SPL_MEDDRA_INDEXING_AUTHORITY)
                                                            int splMeddraIndexingAuthority)
  {
    return dao.meddraLowLevelTermsHavingPreferredTerm(preferredTerm, includeUnlabeledTerms != 0, splMeddraIndexingAuthority);
  }

  @GET @Path("llt/{llt}/related-llts")
  @Produces(MediaType.APPLICATION_JSON)
  public JsonArray meddraLowLevelTermsRelatedToLowLevelTerm(@PathParam("llt")
                                                              String lowLevelTerm,
                                                            @QueryParam("unlab") @DefaultValue("0")
                                                              int includeUnlabeledTerms,
                                                            @QueryParam("a") @DefaultValue(DEFAULT_SPL_MEDDRA_INDEXING_AUTHORITY)
                                                              int splMeddraIndexingAuthority)
  {
    return dao.meddraLowLevelTermsRelatedToLowLevelTerm(lowLevelTerm, includeUnlabeledTerms != 0, splMeddraIndexingAuthority);
  }

}
