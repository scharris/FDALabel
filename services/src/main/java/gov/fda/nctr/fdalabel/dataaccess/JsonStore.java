package gov.fda.nctr.fdalabel.dataaccess;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.*;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import java.io.StringReader;
import java.io.StringWriter;


/**
 * Created by sharris
 * Date: 8/29/2013
 * Time: 3:15 PM
 */
public class JsonStore {

  private JedisPool jedisPool;

  public JsonStore()
  {
    jedisPool = new JedisPool(getJedisPoolConfig(), "localhost", 6379);
  }

  JedisPoolConfig getJedisPoolConfig()
  {
    int sec = 1000;
    int min = 60 * sec;

    JedisPoolConfig config = new JedisPoolConfig();
    config.setMaxActive(500);
    config.setWhenExhaustedAction(org.apache.commons.pool.impl.GenericObjectPool.WHEN_EXHAUSTED_FAIL);
    config.setMaxIdle(10);
    config.setMinIdle(5);
    config.setMinEvictableIdleTimeMillis(1*sec);
    config.setSoftMinEvictableIdleTimeMillis(1*sec);
    config.setTestOnBorrow(true);
    config.setTestOnReturn(false);
    config.setTestWhileIdle(true);
    config.setTimeBetweenEvictionRunsMillis(1*min);

    return config;
  }

  public long storeCriterion(JsonObject jsonObj)
  {
    return store(jsonObj, "crit");
  }

  public JsonObject fetchCriterion(long objId)
  {
    return fetch(objId, "crit");
  }

  // Stores an association of the passed json object to an object id, if not already associated with an id.
  // Also stores if necessary an association of the object id and type to the object json, so the json may
  // be fetched by the pair of its id and type. This allows the same json object id to be registered under
  // more than one type. Returns the object id for the json object.
  private long store(JsonObject obj, String type)
  {
    Jedis jedis = jedisPool.getResource();

    try
    {
      String objJson = stringFromJsonObject(obj);

      String objIdStr = jedis.get(objJson);

      if (objIdStr != null) // existing json value
      {
        long objId = Long.valueOf(objIdStr);

        // Add a mapping of (obj-id, type) -> json, if not already present.
        String typed_obj_id_key = keyForObjByTypedId(objId, type);

        if (!jedis.exists(typed_obj_id_key))
          jedis.set(typed_obj_id_key, objJson);

        return objId;
      }
      else // new json value
      {
        long newObjId = jedis.incr(keyForObjIdCounter());

        jedis.set(objJson, String.valueOf(newObjId));

        String typed_obj_id_key = keyForObjByTypedId(newObjId, type);

        jedis.set(typed_obj_id_key, objJson);

        return newObjId;
      }
    }
    finally
    {
      jedisPool.returnResource(jedis);
    }
  }

  private JsonObject fetch(long objId, String objType)
  {
    Jedis jedis = jedisPool.getResource();

    try
    {
      String key = keyForObjByTypedId(objId, objType);
      String json = jedis.get(key);
      return json != null ? jsonObjectFromString(json) : null;
    }
    finally
    {
      jedisPool.returnResource(jedis);
    }
  }

  String keyForObjByTypedId(long objId, String objType)
  {
    return objType+"-id:" + objId;
  }

  String keyForObjIdCounter() {
    return "obj-id-counter";
  }

  String stringFromJsonObject(JsonObject obj)
  {
    StringWriter sw = new StringWriter();
    try (JsonWriter writer = Json.createWriter(sw)) {
      writer.writeObject(obj);
    }
    return sw.toString();
  }

  JsonObject jsonObjectFromString(String json)
  {
    JsonReader jsonReader = Json.createReader(new StringReader(json));
    JsonObject jsonObj = jsonReader.readObject();
    jsonReader.close();
    return jsonObj;
  }
}
