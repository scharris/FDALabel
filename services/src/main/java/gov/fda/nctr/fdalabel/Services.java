package gov.fda.nctr.fdalabel;

import gov.fda.nctr.fdalabel.dataaccess.DAO;
import gov.fda.nctr.fdalabel.dataaccess.JsonStore;
import org.glassfish.hk2.api.DynamicConfiguration;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.internal.inject.Injections;

import javax.inject.Inject;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Created by sharris
 * Date: 7/30/2013
 * Time: 9:54 AM
 */
public class Services extends Application {

  static final Logger logger = Logger.getLogger(Services.class.getName());

  @Inject
  public Services(ServiceLocator serviceLocator)
  {
    // Configure dependency injection.
    DynamicConfiguration dc = Injections.getConfiguration(serviceLocator);
    Injections.addBinding(Injections.newBinder(new DAO(getDataSource())).to(DAO.class), dc);
    Injections.addBinding(Injections.newBinder(new JsonStore()).to(JsonStore.class), dc);
    dc.commit();
    logger.info("Configured dependency injection.");
  }

  @Override
  public Set<Class<?>> getClasses()
  {
    Set<Class<?>> s = new HashSet<>();
    s.add(PharmClasses.class);
    s.add(MeddraTerms.class);
    s.add(Spls.class);
    s.add(Products.class);
    s.add(Criteria.class);
    return s;
  }


  private static DataSource getDataSource()
  {
    try
    {
      Context initCtx = new InitialContext();
      Context envCtx = (Context)initCtx.lookup("java:comp/env");
      DataSource ds = (DataSource)envCtx.lookup("jdbc/fdalabel");
      logger.info("Data source created.");

      return ds;
    }
    catch(NamingException ne)
    {
      throw new RuntimeException(ne);
    }
  }

}
