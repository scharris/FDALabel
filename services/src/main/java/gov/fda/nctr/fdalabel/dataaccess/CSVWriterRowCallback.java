package gov.fda.nctr.fdalabel.dataaccess;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.JdbcUtils;
import static org.apache.commons.lang3.StringEscapeUtils.escapeCsv;

import java.io.IOException;
import java.io.Writer;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;
import java.io.*;


/**
 * Created by sharris
 * Date: 2014/10/10
 */

public class CSVWriterRowCallback implements RowCallbackHandler {

  private final Writer writer;

  private final boolean camelCaseColNames;

  private int numCols = -1;

  private ResultSet resultSet; // Kept to ensure that multiple result sets are not being serviced by the same instance.

  // This map is used to ensure the csv header consistent with the summary table header
  private static final Map<String, String> colNameMap = initialzeMap();
  private static Map<String, String> initialzeMap() 
  {
    Map<String, String> map = new HashMap<String, String>();
    map.put("application_number", "Application Number(s)");
    map.put("market_categories", "Marketing Category");
    map.put("product_names", "Trade Name");
    map.put("generic_proper_names", "Generic/Proper Name(s)");
    map.put("spl_eff_date", "Most Recent SPL Date (YYYY/MM/DD)");
    map.put("epc", "Established Pharmacologic Class(es)");
    map.put("dosage_forms", "Dosage Form(s)");
    map.put("routes_of_administration", "Route(s) of Administration");
    map.put("initial_approval_year", "Initial U.S. Approval");
    map.put("submitter", "Company");
    map.put("market_dates", "Marketing Date(s) (YYYY/MM/DD)");
    map.put("ndc_codes", "NDC(s)");
    map.put("act_ingr_uniis", "Active Ingredient UNII(s)");
    map.put("act_ingr_names", "Active Ingredient(s)");
    map.put("labeling_type", "Labeling Type");
    map.put("fdalabel_link", "FDALabel Link");
    map.put("dailymed_link", "DailyMed Link");
    map.put("dailymed_pdf_link", "DailyMed PDF Link");
    map.put("set_id", "SET ID");
    map.put("act_moiety_names", "Active Moiety Name(s)");
    map.put("act_moiety_uniis", "Active Moiety UNII(s)");

    return map;
  }
  
  private String colName2CSVHeader(String colName)
  {
	  String header = colNameMap.get(colName);

	  return (header != null) ? header : colName;
  }

  public CSVWriterRowCallback(Writer w)
  {
    this(w, false);
  }

  public CSVWriterRowCallback(Writer w, boolean camelCaseColNames)
  {
    this.writer = w;
    this.camelCaseColNames = camelCaseColNames;
  }


  @Override
  public void processRow(ResultSet rs) throws SQLException
  {
    try
    {
      if (resultSet == null)
      {
        resultSet = rs;
        ResultSetMetaData rsmd = rs.getMetaData();

        numCols = rsmd.getColumnCount();

        for (int cix = 0; cix < numCols; ++cix)
        {
          String colName = camelCaseColNames ? StringUtils.camelCase(JdbcUtils.lookupColumnName(rsmd, cix + 1))
                                             : JdbcUtils.lookupColumnName(rsmd, cix + 1);
          if (cix != 0)
            writer.write(',');
          writer.write(colName2CSVHeader(colName.toLowerCase()));
        }
      }
      else if (resultSet != rs)
        throw new IllegalStateException("Single CSVWriterRowCallback instance cannot be used as callback for multiple queries simultaneously.");

      writer.write("\r\n");
      for (int cix = 0; cix < numCols; ++cix)
      {
        if (cix != 0)
          writer.write(',');

        String value = rs.getString(cix + 1);

        if (value != null)
          writer.write(escapeCsv(value));
      }
    }
    catch(IOException ioe)
    {
      throw new RuntimeException("Failed to write csv data to output stream: " + ioe.getMessage());
    }
  }
}
