package gov.fda.nctr.fdalabel;

/**
 * Created by sharris
 * Date: 2013/09/04
 */


import gov.fda.nctr.fdalabel.dataaccess.DAO;
import gov.fda.nctr.fdalabel.dataaccess.JsonStore;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


@Path("query-store")
public class Criteria {

  final DAO dao;
  final JsonStore jsonStore;

  @Inject
  public Criteria(DAO dao, JsonStore jsonStore)
  {
    this.dao = dao;
    this.jsonStore = jsonStore;
  }

  @GET @Path("criteria/{criterionId}")
  @Produces(MediaType.APPLICATION_JSON)
  public JsonObject criterion(@PathParam("criterionId")
                              long criterionId)
  {
    try
    {
      JsonObject criterion = jsonStore.fetchCriterion(criterionId);

      if ( criterion == null )
        throw new NotFoundException("Criterion not found.");

      return criterion;
    }
    catch(IllegalArgumentException e)
    {
      throw new WebApplicationException(e.getMessage(), 400);
    }
  }

}
