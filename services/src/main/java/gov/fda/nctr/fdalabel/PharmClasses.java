package gov.fda.nctr.fdalabel;

import gov.fda.nctr.fdalabel.dataaccess.DAO;

import javax.inject.Inject;
import javax.json.JsonArray;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


@Path("pharm-classes")
public class PharmClasses {

  public enum PharmClassType { MoA, PE, CI, EPC, ANY }

  private final DAO dao;

  @Inject
  public PharmClasses(DAO dao)
  {
    this.dao = dao;
  }


  @GET @Path("containing/{name-fragments}")
  @Produces(MediaType.APPLICATION_JSON)
  public JsonArray pharmClassesContaining(@PathParam("name-fragments")
                                          String pharmClassNameFragments,
                                          @QueryParam("t") @DefaultValue("ANY")
                                          PharmClassType pharmClassType,
                                          @QueryParam("l") @DefaultValue("50")
                                          int resultsLimit)
  {
    return dao.pharmClassesContaining(pharmClassNameFragments, pharmClassType, resultsLimit);
  }

}
