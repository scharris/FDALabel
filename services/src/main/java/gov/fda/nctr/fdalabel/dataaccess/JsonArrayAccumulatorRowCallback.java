package gov.fda.nctr.fdalabel.dataaccess;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.JdbcUtils;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static gov.fda.nctr.fdalabel.dataaccess.StringUtils.camelCase;

/**
 * Created by sharris
 * Date: 7/30/2013
 * Time: 4:27 PM
 */
public class JsonArrayAccumulatorRowCallback implements RowCallbackHandler {

  private final JsonArrayBuilder jsonArrayBuilder;

  private final boolean camelCaseColNames;

  private List<String> outputColNames;
  private ResultSet resultSet; // Kept to ensure that multiple result sets are not being serviced by the same instance.

  public JsonArrayAccumulatorRowCallback()
  {
    this(true);
  }

  public JsonArrayAccumulatorRowCallback(boolean camelCaseColNames)
  {
    this.jsonArrayBuilder = Json.createArrayBuilder();
    this.camelCaseColNames = camelCaseColNames;
  }


  @Override
  public void processRow(ResultSet rs) throws SQLException
  {
    if (resultSet == null)
    {
      resultSet = rs;
      ResultSetMetaData rsmd = rs.getMetaData();

      int numCols = rsmd.getColumnCount();
      outputColNames = new ArrayList<String>(numCols);

      for (int cix = 0; cix < numCols; ++cix) {
        String colName = camelCaseColNames ? camelCase(JdbcUtils.lookupColumnName(rsmd, cix+1)) : JdbcUtils.lookupColumnName(rsmd, cix+1);
        outputColNames.add(colName);
      }
    }
    else if (resultSet != rs)
      throw new IllegalStateException("Single JsonArrayAccumulatorRowCallback instance cannot be used as callback for multiple queries simultaneously.");

    final int numCols = outputColNames.size();

    JsonObjectBuilder bldr = Json.createObjectBuilder();

    for (int cix = 0; cix < numCols; ++cix)
    {
      String colName = outputColNames.get(cix);

      Object value = rs.getObject(cix+1);

      if (value == null)
        bldr.addNull(colName);
      else if (value instanceof String)
        bldr.add(colName, (String)value);
      else if (value instanceof BigDecimal)
        bldr.add(colName, (BigDecimal)value);
      else if (value instanceof Date)
        bldr.add(colName, ((Date)value).getTime());
      else if (value instanceof Integer)
        bldr.add(colName, (Integer)value);
      else if (value instanceof Boolean)
        bldr.add(colName, (Boolean)value);
      else if (value instanceof Long)
        bldr.add(colName, (Long)value);
      else if (value instanceof Double)
        bldr.add(colName, (Double)value);
      else if (value instanceof Float)
        bldr.add(colName, (Float)value);
      else if (value instanceof Byte)
        bldr.add(colName, (Byte)value);
      else
        throw new IllegalArgumentException("Object type could not be mapped in JsonArrayAccumulatorRowCallback: " + value.getClass());
    }

    jsonArrayBuilder.add(bldr.build());
  }

  public JsonArray getResults()
  {
    resultSet = null;
    return jsonArrayBuilder.build();
  }
}
