package gov.fda.nctr.fdalabel;

/**
 * Created by sharris
 * Date: 8/12/2013
 */

import gov.fda.nctr.fdalabel.dataaccess.DAO;
import gov.fda.nctr.fdalabel.dataaccess.JsonStore;
import org.apache.commons.io.IOUtils;
import org.springframework.jdbc.UncategorizedSQLException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

import javax.inject.Inject;
import javax.json.*;
import javax.ws.rs.core.*;
import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.util.Collection;

import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

// Example service url: http://localhost:8080/fdalabel/services/spl/set-ids/354d8b2c-de37-4f88-a04d-5c8d431485ab/spl-doc?hl=Blindness

@Path("spl")
public class Spls {

  final DAO dao;
  final JsonStore jsonStore;

  @Context
  ServletContext servletContext;

  @Inject
  public Spls(DAO dao, JsonStore jsonStore)
  {
    this.dao = dao;
    this.jsonStore = jsonStore;
  }

  // label summaries

  @POST @Path("summaries")
  @Produces(MediaType.APPLICATION_JSON)
  public Response splSummariesForPostedCriterion(JsonObject criterion,
                                                 @QueryParam("l") @DefaultValue("2000")
                                                 int resultsLimit,
                                                 @QueryParam("cr") @DefaultValue("true")
                                                 boolean countResults)
  {
    try
    {
      long criterionId = jsonStore.storeCriterion(criterion);

      JsonArray resultsArray = dao.splSummariesForCriterion(criterion, resultsLimit);

      JsonArray search_terms = toJsonStringsArray(dao.searchTerms(criterion));

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("criterionId", criterionId);
      retObj.add("resultsArray", resultsArray);
      retObj.add("searchTerms", search_terms);
      retObj.add("resultsLimit", resultsLimit);

      if (resultsArray.size() < resultsLimit) {
       retObj.add("totalResultsCount", resultsArray.size());
      }
      else if (countResults)
      {
        long resCount = dao.splsCountForCriterion(criterion);
        retObj.add("totalResultsCount", resCount);
      }
      else {
        retObj.add("totalResultsCount", JsonValue.NULL);
      }

      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
    catch(UncategorizedSQLException e)
    {
      if (e.getMessage().contains("DRG-50901"))
        return Response.status(400).entity("Invalid syntax in text search.").build();
      else
        return Response.status(400).entity("The server encountered an unexpected database access error.").build();
    }
  }

  @POST @Path("ldt/summaries")
  @Produces(MediaType.APPLICATION_JSON)
  public Response splSummariesForPostedCriterionLDT(JsonObject criterion,
                                                 @QueryParam("l") @DefaultValue("2000")
                                                 int resultsLimit,
                                                 @QueryParam("cr") @DefaultValue("true")
                                                 boolean countResults)
  {
    try
    {
      long criterionId = jsonStore.storeCriterion(criterion);

      JsonArray resultsArray = dao.splSummariesForCriterionLDT(criterion, resultsLimit);
System.out.println("ldt/summaries: resultsArray.size()=" + resultsArray.size());
      JsonArray search_terms = toJsonStringsArray(dao.searchTerms(criterion));

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("criterionId", criterionId);
      retObj.add("resultsArray", resultsArray);
      retObj.add("searchTerms", search_terms);
      retObj.add("resultsLimit", resultsLimit);

      if (resultsArray.size() < resultsLimit) {
       retObj.add("totalResultsCount", resultsArray.size());
      }
      else if (countResults)
      {
        long resCount = dao.splsCountForCriterionLDT(criterion);
        retObj.add("totalResultsCount", resCount);
      }
      else {
        retObj.add("totalResultsCount", JsonValue.NULL);
      }

      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
    catch(UncategorizedSQLException e)
    {
      if (e.getMessage().contains("DRG-50901"))
        return Response.status(400).entity("Invalid syntax in text search.").build();
      else
        return Response.status(400).entity("The server encountered an unexpected database access error.").build();
    }
  }


  @GET @Path("summaries/json/criteria/{criterionId}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response splSummariesForCriterionId(@PathParam("criterionId")
                                             long criterionId,
                                             @QueryParam("l") @DefaultValue("2000")
                                             int resultsLimit,
                                             @QueryParam("cr") @DefaultValue("true")
                                             boolean countResults)
  {
    try
    {
      JsonObject criterion = jsonStore.fetchCriterion(criterionId);

      if ( criterion == null )
        throw new NotFoundException("Criterion not found.");

      JsonArray resultsArray = dao.splSummariesForCriterion(criterion, resultsLimit);

      JsonArray search_terms = toJsonStringsArray(dao.searchTerms(criterion));

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("criterionId", criterionId);
      retObj.add("resultsArray", resultsArray);
      retObj.add("searchTerms", search_terms);
      retObj.add("resultsLimit", resultsLimit);

      if (resultsArray.size() < resultsLimit) {
        retObj.add("totalResultsCount", resultsArray.size());
      }
      else if (countResults)
      {
        long resCount = dao.splsCountForCriterion(criterion);
        retObj.add("totalResultsCount", resCount);
      }
      else {
        retObj.add("totalResultsCount", JsonValue.NULL);
      }

      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("ldt/summaries/json/criteria/{criterionId}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response splSummariesForCriterionIdLDT(@PathParam("criterionId")
                                             long criterionId,
                                             @QueryParam("l") @DefaultValue("2000")
                                             int resultsLimit,
                                             @QueryParam("cr") @DefaultValue("true")
                                             boolean countResults)
  {
    try
    {
      JsonObject criterion = jsonStore.fetchCriterion(criterionId);

      if ( criterion == null )
        throw new NotFoundException("Criterion not found.");

      JsonArray resultsArray = dao.splSummariesForCriterionLDT(criterion, resultsLimit);

      JsonArray search_terms = toJsonStringsArray(dao.searchTerms(criterion));

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("criterionId", criterionId);
      retObj.add("resultsArray", resultsArray);
      retObj.add("searchTerms", search_terms);
      retObj.add("resultsLimit", resultsLimit);

      if (resultsArray.size() < resultsLimit) {
        retObj.add("totalResultsCount", resultsArray.size());
      }
      else if (countResults)
      {
        long resCount = dao.splsCountForCriterionLDT(criterion);
        retObj.add("totalResultsCount", resCount);
      }
      else {
        retObj.add("totalResultsCount", JsonValue.NULL);
      }

      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("summaries/csv/criteria/{criterionId}")
  @Produces("text/csv")
  public Response splSummariesCSVForCriterionId(@PathParam("criterionId")
                                                long criterionId)
  {
    try
    {
      final JsonObject criterion = jsonStore.fetchCriterion(criterionId);

      if ( criterion == null )
        throw new NotFoundException("Criterion not found.");

      StreamingOutput stream = new StreamingOutput() {
        public void write(OutputStream os) throws IOException, WebApplicationException {
          Writer w = new BufferedWriter(new OutputStreamWriter(os));
          dao.writeSplSummariesCSV(criterion, w);
          w.flush();
        }
      };

      return Response.ok(stream)
                     .header("Content-Disposition", "attachment; filename=fdalabel-query-" + criterionId + ".csv")
                     .build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("ldt/summaries/csv/criteria/{criterionId}")
  @Produces("text/csv")
  public Response splSummariesCSVForCriterionIdLDT(@PathParam("criterionId")
                                                long criterionId)
  {
    try
    {
      final JsonObject criterion = jsonStore.fetchCriterion(criterionId);

      if ( criterion == null )
        throw new NotFoundException("Criterion not found.");

      StreamingOutput stream = new StreamingOutput() {
        public void write(OutputStream os) throws IOException, WebApplicationException {
          Writer w = new BufferedWriter(new OutputStreamWriter(os));
          dao.writeSplSummariesCSVLDT(criterion, w);
          w.flush();
        }
      };

      return Response.ok(stream)
                     .header("Content-Disposition", "attachment; filename=fdalabel-query-" + criterionId + ".csv")
                     .build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("count/criteria/{criterionId}")
  @Produces(MediaType.APPLICATION_JSON)
  public JsonObject splsCountForCriterionId(@PathParam("criterionId")
                                            long criterionId)
  {
    try
    {
      JsonObject criterion = jsonStore.fetchCriterion(criterionId);

      if ( criterion == null )
        throw new NotFoundException("Criterion not found.");

      long count = dao.splsCountForCriterion(criterion);

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("totalResultsCount", count);
      return retObj.build();
    }
    catch(IllegalArgumentException e)
    {
      throw new WebApplicationException(e.getMessage(), 400);
    }
  }

  @GET @Path("ldt/count/criteria/{criterionId}")
  @Produces(MediaType.APPLICATION_JSON)
  public JsonObject splsCountForCriterionIdLDT(@PathParam("criterionId")
                                            long criterionId)
  {
    try
    {
      JsonObject criterion = jsonStore.fetchCriterion(criterionId);

      if ( criterion == null )
        throw new NotFoundException("Criterion not found.");

      long count = dao.splsCountForCriterionLDT(criterion);

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("totalResultsCount", count);
      return retObj.build();
    }
    catch(IllegalArgumentException e)
    {
      throw new WebApplicationException(e.getMessage(), 400);
    }
  }

  @GET @Path("section-types-tree")
  @Produces(MediaType.APPLICATION_JSON)
  public Response sectionTypesTree()
  {
    try
    {
      JsonArray resultsArray = dao.sectionTypesTree();

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("sectionTypesTree", resultsArray);

      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("section-types-tree-ldt")
  @Produces(MediaType.APPLICATION_JSON)
  public Response sectionTypesTreeLDT()
  {
    try
    {
      JsonArray resultsArray = dao.sectionTypesTreeLDT();

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("sectionTypesTreeLDT", resultsArray);

      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("section-types")
  @Produces(MediaType.APPLICATION_JSON)
  public Response allSplSectionTypes()
  {
    try
    {
      JsonArray resultsArray = dao.allSectionTypes();

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("sectionTypes", resultsArray);

      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("data-loader-records")
  @Produces(MediaType.APPLICATION_JSON)
  public Response allSplDataLoaderRecords()
  {
    try
    {
//      JsonArray resultsArray = dao.allSplDataLoaderRecords();
      JsonArray recentLoaderArray = dao.recentFiveSplDataLoaderRecords();
      JsonArray recentDictMaintanceRecords = dao.recentDictMaintanceRecords();
//      JsonArray dataLoaderofTotalSPL = dao.dataLoaderofTotalSPL();
      JsonArray recentLoaderArrayLDT = dao.recentFiveSplDataLoaderRecordsLDT();

      JsonObjectBuilder retObj = Json.createObjectBuilder();
//      retObj.add("dataLoaderRecords", resultsArray);
      retObj.add("recentFiveSplDataLoaderRecords", recentLoaderArray);
      retObj.add("recentDictMaintanceRecords", recentDictMaintanceRecords);
//      retObj.add("dataLoaderofTotalSPL", dataLoaderofTotalSPL);
      retObj.add("recentFiveSplDataLoaderRecordsLDT", recentLoaderArrayLDT);
      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("document-types")
  @Produces(MediaType.APPLICATION_JSON)
  public Response allSplDocumentTypes()
  {
    try
    {
      JsonArray resultsArray = dao.allDocumentTypes();

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("documentTypes", resultsArray);

      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("document-types-main")
  @Produces(MediaType.APPLICATION_JSON)
  public Response documentTypesMain()
  { System.out.println("document-types-main called...");
    try
    {
      JsonArray resultsArray = dao.documentTypesMain();

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("documentTypesMain", resultsArray);
System.out.println("document-types-main res=" + retObj.toString());
      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("document-types-ldt")
  @Produces(MediaType.APPLICATION_JSON)
  public Response documentTypesLDT()
  { System.out.println("document-types-ldt called...");
    try
    {
      JsonArray resultsArray = dao.documentTypesLDT();

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("documentTypesLDT", resultsArray);
System.out.println("document-types-ldt res=" + retObj.toString());
      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("marketing-categories-ldt")
  @Produces(MediaType.APPLICATION_JSON)
  public Response marketingCategoriesLDT()
  {
    try
    {
      JsonArray resultsArray = dao.marketingCategoriesLDT();

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("marketingCategoriesLDT", resultsArray);

      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("marketing-categories-main")
  @Produces(MediaType.APPLICATION_JSON)
  public Response marketingCategoriesMain()
  {
    try
    {
      JsonArray resultsArray = dao.marketingCategoriesMain();

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("marketingCategoriesMain", resultsArray);

      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("marketing-categories")
  @Produces(MediaType.APPLICATION_JSON)
  public Response allMarketingCategories()
  {
    try
    {
      JsonArray resultsArray = dao.allMarketingCategories();

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("marketingCategoriesAll", resultsArray);

      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("routes-of-admin")
  @Produces(MediaType.APPLICATION_JSON)
  public Response allRoutesOfAdmin()
  {
    try
    {
      JsonArray resultsArray = dao.allRoutesOfAdmin();

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("routesOfAdmin", resultsArray);

      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("dosage-forms")
  @Produces(MediaType.APPLICATION_JSON)
  public Response allDosageForms()
  {
    try
    {
      JsonArray resultsArray = dao.allDosageForms();

      JsonObjectBuilder retObj = Json.createObjectBuilder();
      retObj.add("dosageForms", resultsArray);

      return Response.ok(retObj.build()).build();
    }
    catch(IllegalArgumentException e)
    {
      return Response.status(400).entity(e.getMessage()).build();
    }
  }

  @GET @Path("set-ids/{setId}/spl-doc")
  @Produces(MediaType.APPLICATION_XML)
  public Document splDocument(@PathParam("setId")
                              String setId,
                              @QueryParam("xsl") @DefaultValue("l") // l->local stylesheet (from this service), n->no stylesheet, o->original declaration if any
                              String xsl_opt)
  {
    switch (xsl_opt) {
      case "l":
        return dao.getSplWithAlternateStylesheet(setId, "../../stylesheet/spl.xsl");  // reference to service below
      case "n":
        return dao.getSplWithoutStylesheetInstruction(setId);
      default:
        return dao.getSplWithOriginalStylesheetInstruction(setId);
    }
  }

  // Resource method for images resolved relative to the spl returned by the above method.
  @GET @Path("set-ids/{setId}/{imgFileName: .*\\.jpe?g}")
  @Produces("image/jpeg")
  public InputStream splImage(@PathParam("setId")
                              String setId,
                              @PathParam("imgFileName")
                              String imgFileName)
  {
    return dao.getSplImageStream(setId, imgFileName);
  }

  @GET @Path("stylesheet/{fileName: .*\\.xsl}")
  @Produces("text/xsl")
  public InputStream splStylesheetXSLResource(@PathParam("fileName") String fileName,
                                              @Context UriInfo uriInfo)
  {
    // JY: use modified local spl-common.xsl instead.
    return servletContext.getResourceAsStream("/WEB-INF/spl-stylesheet/" + fileName);
/*
    // Modify spl-common.xsl to
    // 1) Inject our term highlighting javascript into the html header template, and
    // 2) Replace links to external FDA sites with links to resources from this server.

    if (fileName.equals("spl-common.xsl"))
    {
      try(InputStream xsl_is = servletContext.getResourceAsStream("/WEB-INF/spl-stylesheet/" + fileName))
      {
        String xsl_abs_path = uriInfo.getAbsolutePath().getPath();
        String stylesheet_resources_abs_path = xsl_abs_path.substring(0, xsl_abs_path.lastIndexOf('/'));

        DocumentBuilder doc_bldr = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = doc_bldr.parse(xsl_is);

        Node script_template_parent = null;
        NodeList html_els = doc.getElementsByTagName("html");
        for(int k=0; k<html_els.getLength(); k++) {
          Element html_parent = (Element)html_els.item(k).getParentNode();
          Node head = html_parent.getElementsByTagName("head").item(0);

          //Remove the spl.css in head and replaced it with the template and call template functions
          NodeList head_links = html_parent.getElementsByTagName("link");
          for(int i=0; i<head_links.getLength(); ++i) {
            Element head_link = (Element)head_links.item(i);
            if (head_link.getAttribute("href").equals("{$css}")) { 
              head.removeChild(head_links.item(i));
            }
          } // remove spl.css

          // Include jquery in html head.
          String jquery_server_url = servletContext.getContextPath() + "/jquery-1.11.0.min.js";

          Element jquery_el = doc.createElement("script");
          jquery_el.setAttribute("type", "text/javascript");
          jquery_el.setAttribute("src", jquery_server_url);

          // Include term highlighting jquery extension (https://github.com/jbr/jQuery.highlightRegex).
          String hlregex_server_url = servletContext.getContextPath() + "/highlightRegex.min.js";
          Element hlregex_el = doc.createElement("script");
          hlregex_el.setAttribute("type", "text/javascript");
          hlregex_el.setAttribute("src", hlregex_server_url);

          // Replace fully qualified css stylesheet link with our own (augmented) copy of the stylesheet.
          String splcss_server_url = stylesheet_resources_abs_path + "/spl.css";
          Element splcss_el = doc.createElement("link");
          splcss_el.setAttribute("type", "text/css");
          splcss_el.setAttribute("rel", "stylesheet");
          splcss_el.setAttribute("href", splcss_server_url);

          // Replace spl.js script reference with local copy.
          NodeList script_els = doc.getElementsByTagName("script");
          
          for(int i=0; i<script_els.getLength(); ++i) {
            Element script_el = (Element)script_els.item(i);
            if (script_el.getAttribute("src").equals("{$resourcesdir}spl.js"))
            {
              script_el.setAttribute("src", stylesheet_resources_abs_path + "/spl.js");
              Node script_parent = script_el.getParentNode();
              script_parent.appendChild(jquery_el);
              script_parent.appendChild(hlregex_el);
              script_parent.appendChild(splcss_el);
              script_template_parent = script_parent.getParentNode();
            }
          } // add the three scripts and css


          // Add term highlighting javascript to the html body template.
          Node body = html_parent.getElementsByTagName("body").item(0);
          if(body != null) {
            Element new_call_template_el = doc.createElement("xsl:call-template");
            new_call_template_el.setAttribute("name", "include-spl-interactive");
            body.appendChild(new_call_template_el);
          } //add term highlight js

        } //for each html element

        // Add term highlighting template for javascript to document template.
        String js_abs_path = servletContext.getContextPath() + "/spl-interactive.js";

        Element new_template_el = doc.createElement("xsl:template");
        new_template_el.setAttribute("name", "include-spl-interactive");            
        Element new_script_el = doc.createElement("script");
        new_script_el.setAttribute("type", "text/javascript");
        new_script_el.setAttribute("src", js_abs_path); 
        new_template_el.appendChild(new_script_el);
        script_template_parent.appendChild(new_template_el);

        DOMImplementationLS domImplementation = (DOMImplementationLS)doc.getImplementation();
        LSSerializer lsSerializer = domImplementation.createLSSerializer();
        return new ByteArrayInputStream(lsSerializer.writeToString(doc).getBytes("UTF-8"));
      }
      catch(Exception ioe) { throw new RuntimeException(ioe); }
    }
    else
    {
      return servletContext.getResourceAsStream("/WEB-INF/spl-stylesheet/" + fileName);
    }
*/
  }

  //method to convert Document to String
  public String getStringFromDocument(Document doc)
  {
     try
     {
        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.transform(domSource, result);
        return writer.toString();
     } catch(TransformerException ex) {
       ex.printStackTrace();
       return null;
     }
  }

  @GET @Path("stylesheet/{fileName: .*\\.xml}")
  @Produces(MediaType.APPLICATION_XML)
  public InputStream splStylesheetXMLResource(@PathParam("fileName") String fileName)
  {
    return servletContext.getResourceAsStream("/WEB-INF/spl-stylesheet/" + fileName);
  }

  // Resource method for css stylesheet resolved relative to the spl.
  @GET @Path("stylesheet/{cssFileName: .*\\.css}")
  @Produces("text/css")
  public String splStylesheetCSSResource(@PathParam("cssFileName")
                                         String cssFileName)
  {
    // Augment the spl css with our own styles.
    if (cssFileName.equals("spl.css"))
    {
      try(InputStream is_orig = servletContext.getResourceAsStream("/WEB-INF/spl-stylesheet/spl.css");
          InputStream is_adds = servletContext.getResourceAsStream("/WEB-INF/spl-css-addendums.css"))
      {
        String orig_css_str = IOUtils.toString(is_orig, "UTF-8");
        String addendums_css_str = IOUtils.toString(is_adds, "UTF-8");
        return orig_css_str + addendums_css_str;
      }
      catch(IOException ioe) { throw new RuntimeException(ioe); }
    }
    else
    {
      try(InputStream is = servletContext.getResourceAsStream("/WEB-INF/spl-stylesheet/" + cssFileName))
      {
        return IOUtils.toString(is, "UTF-8");
      }
      catch(IOException ioe) { throw new RuntimeException(ioe); }
    }
  }

  @GET @Path("stylesheet/{fileName: .*\\.js}")
  @Produces("application/javascript")
  public InputStream splStylesheetJsResource(@PathParam("fileName") String fileName)
  {
      return servletContext.getResourceAsStream("/WEB-INF/spl-stylesheet/" + fileName);
  }

  private JsonArray toJsonStringsArray(Collection<String> strs) {
    JsonArrayBuilder bldr = Json.createArrayBuilder();
    for (String s: strs)
    {
      bldr.add(s);
    }
    return bldr.build();
  }

}
