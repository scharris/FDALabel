function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,''])[1].replace(/\+/g, '%20'))||null;
}
/*
var hl_strs_str = getURLParameter('hl');


// Do search term highlights.
if (hl_strs_str) {
  var hl_strs = hl_strs_str.split('|');

  // Sort search patterns descending by length, to avoid some failures to match when searching for a term and a substring
  // term (both without wildcards). (This does not solve the general problem of overlapping terms which the match implementation
  // cannot handle.)
  hl_strs.sort(function(el1, el2) { return el2.length - el1.length; });

  for (var h=0; h<hl_strs.length; ++h) {
    $(document.body).highlightRegex(new RegExp(hl_strs[h], 'ig'), {
      className: 'search-term',
      attrs: {'data-pattern': hl_strs[h]}
    });
  }
}

// Attach highlight toggle handler to the highlights.
$(function() {
  $('.search-term').click(function() {
    var match_pattern = $(this).attr('data-pattern');
    $('.search-term[data-pattern="' + match_pattern + '"]').toggleClass('highlight-disabled');
  })
});


*/
// JY: use "jquery.mark.min.js" instead to hightlight search terms.
var hl_strs_str = getURLParameter('hl');

// Do search term highlights.
if (hl_strs_str) {
  var hl_strs = hl_strs_str.split('|');
  
  // Sort search patterns descending by length, to avoid some failures to match when searching for a term and a substring
  // term (both without wildcards).
  hl_strs.sort(function(el1, el2) { return el2.length - el1.length; });
  
  for (var h=0; h<hl_strs.length; ++h) {
    var options = {
      "element": "span",
      "className": "search-term",
      "markedTerm": hl_strs[h],
      "exclude": [],
      "iframes": false,
      "acrossElements": false,
      "each": function(node){
        // node is the marked DOM element
      },
      "filter": function(textNode, foundTerm, totalCounter){
        // textNode is the text node which contains the found term
        // foundTerm is the found search term
        // totalCounter is a counter indicating the total number of all marks
        //              at the time of the function call
        return true; // must return either true or false
      },
      "noMatch": function(term){
        // term is the not found term
      },
      "done": function(counter){
        // counter is a counter indicating the total number of all marks
      },
      "debug": false,
      "log": window.console
    };

    $(document.body).markRegExp(new RegExp(hl_strs[h], 'ig'), options);
  }
}

$(function() {
// Attach highlight toggle handler to the highlights.
  $('.search-term').click(function() {
    var match_pattern = $(this).attr("markedTerm");
    $('.search-term[markedTerm="' + match_pattern + '"]').toggleClass('highlight-disabled');
  })
});



