#!/bin/sh

usage ()
{
  echo 'Run make_release_switch.sh instead'
  echo 'Usage: ./make_release_switch.sh [fda|cder] [release|testing|development]'
  echo 'For example:'
  echo './make_release_switch.sh cder development'
}

if [ "$#" -ne 1 ];
then
  usage
  exit
fi

set -x

today=$(date +"%B %d, %Y")
sed -ri "s/(Updated ).*/Updated $today/g" app/views/databaseUpdates.html

(cd services; mvn -P$1 clean package) && grunt && (

  cd services/target/fdalabel/ui/components

  rm -rf angular-scenario

  rm -rf angular-mocks

  for f in $(find angular -type f ! -name '*.min.js'); do
     rm "$f"
  done

  for f in $(find angular-bootstrap -type f ! -name '*-tpls.min.js'); do
     rm "$f"
  done

  for f in $(find angular-cookies -type f ! -name '*.min.js'); do
     rm "$f"
  done

  for f in $(find angular-resource -type f ! -name '*.min.js'); do
     rm "$f"
  done

  for f in $(find angular-route -type f ! -name '*.min.js'); do
     rm "$f"
  done

  for f in $(find angular-sanitize -type f ! -name '*.min.js'); do
     rm "$f"
  done

  for f in $(find angular-smart-table -type f ! -name '*.min.js'); do
     rm "$f"
  done

  for f in $(find es5-shim -type f ! -name '*shim.min.js'); do
     rm "$f"
  done

  for f in $(find html5shiv -type f ! -name 'html5shiv.js'); do
     rm "$f"
  done

  for f in $(find json3 -type f ! -name '*.min.js'); do
     rm "$f"
  done

  for f in $(find underscore -type f ! -name '*-min.js'); do
     rm "$f"
  done

  cd ../..

  name="fdalabel.war"
  backupname="fdalabel-last.war"

  mv ../../../$name ../../../$backupname 2> /dev/null
  jar cvf ../../../$name *

)
