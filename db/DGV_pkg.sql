create or replace package dgv as

  procedure insert_spl_prod_deps(p_spl_id integer);

  procedure delete_summary_data;

  procedure create_summary_data;

  procedure delete_spl(p_spl_id integer);

end dgv;
