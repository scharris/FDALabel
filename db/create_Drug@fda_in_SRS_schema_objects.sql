/* This table contains  ChemicalType dictionary .  */
create table ChemTypeLookup (
  ChemicalTypeID integer not null,
  ChemicalTypeCode varchar2(3) not null,
  ChemicalTypeDescription varchar2(200) not null,
  constraint pk_ChemTypeLookup primary key (ChemicalTypeID)
);	
/* This table contains  ReviewClass dictionary .  */
create table ReviewClass_Lookup (
  ReviewClassID integer not null,
  ReviewCode varchar2(1) not null,
  LongDescription varchar2(100),
  ShortDescription varchar2(100),
  constraint pk_ReviewClass_Lookup primary key (ReviewClassID)
);	
/* This table contains  DocType dictionary .  Supplement type code and description to the application number.  */
create table DocType_Lookup (
  DocType varchar2(4) not null,
  DocTypeDesc varchar2(50),
  constraint pk_DocType_Lookup primary key (DocType)
);		
		
/* This table contains  AppDocType dictionary .  Type of document that is linked, which relates to the AppDoc table.   */
create table AppDocType_Lookup (
  AppDocType varchar2(50) not null,
  SortOrder integer,
  constraint pk_AppDocType_Lookup primary key (AppDocType)
);		


/* This table contains  Application number and sponsor name. .  */
create table application (
  ApplNo varchar2(6) not null,
  ApplType varchar2(5) not null,
  SponsorApplicant varchar2(50) not null,
  MostRecentLabelAvailableFlag char(1) not null,
  CurrentPatentFlag char(1) not null,
  ActionType varchar2(10) not null,
  Chemical_Type varchar2(3),
  Ther_Potential varchar2(2),
  Orphan_Code varchar2(1),
  constraint pk_application primary key (ApplNo)
);
/* This table contains the products included in each application. Includes form, dosage, and route.  
ProductMktStatus [tinyint, 1] (1=prescription, 2=OTC, 3=discontinued, 4=tentative approval) (Primary Key)
ReferenceDrug [bit, 1] (0=not RLD, 1=RLD, 2=TBD)
*/

create table Product (
  ApplNo varchar2(6) not null,
  ProductNo varchar2(3) not null,
  Form varchar2(255) not null,
  Dosage varchar2(240) ,
  ProductMktStatus integer,
  TECode varchar2(100),
  ReferenceDrug integer,
  drugname varchar2(125),
  activeingred varchar2(255),
  constraint pk_Product primary key (ApplNo,ProductNo,ProductMktStatus)
);
/* This table contains Therapeutic Equivalence Code for Products. 
*/

create table Product_TECode (
  ApplNo varchar2(6) not null,
  ProductNo varchar2(3) not null,
  TECode varchar2(50) not null,
  TESequence integer ,
  ProductMktStatus integer,
  constraint pk_Product_TECode primary key (ApplNo,ProductNo,TESequence,ProductMktStatus)
);								

/* Supplements (RegActionDate): Approval history for each application. Includes supplement number and dates of approval. 
*/

create table RegActionDate (
  ApplNo varchar2(6) not null,
  ActionType varchar2(10) not null,
  InDocTypeSeqNo varchar2(4) not null,
  DuplicateCounter integer ,
  ActionDate timestamp,
  DocType varchar2(4),
  constraint pk_RegActionDate primary key (ApplNo,DuplicateCounter,InDocTypeSeqNo)
);
			

/* Document addresses or URLs to letters, labels, reviews, Consumer Information Sheets, FDA Talk Papers, and other types. 
*/

create table AppDoc (
  AppDocID integer not null,
  ApplNo varchar2(6) not null,
  SeqNo varchar2(4) not null,
  DocType varchar2(50) ,
  DocTitle varchar2(100),
  DocURL varchar2(200),
  DocDate timestamp,
  ActionType varchar2(10),
  DuplicateCounter integer,
  constraint pk_AppDoc primary key (AppDocID)
);
								
grant select, references on AppDoc to DrugLabel;
grant select, references on RegActionDate to DrugLabel;
grant select, references on Product_TECode to DrugLabel;
grant select, references on Product to DrugLabel;
grant select, references on application to DrugLabel;
grant select, references on AppDocType_Lookup to DrugLabel;
grant select, references on DocType_Lookup to DrugLabel;
grant select, references on ReviewClass_Lookup to DrugLabel;
grant select, references on ChemTypeLookup to DrugLabel;

create view spl_appl_v as
select distinct spl_id, MARKETING_CAT_DESCRIPTION,  
substr(trim(APPROVAL_NUM),length(trim(APPROVAL_NUM))-5,length(trim(APPROVAL_NUM))) appl_no
from prod_appr
where trim(MARKETING_CAT_NCIT_CODE) in( 'C73583','C73584','C73585','C73588','C73593','C73594','C73605','C75302','C780438','C80440','C80441','C80442','C92556') and APPROVAL_NUM is not null;

					