
create table coding_authority (
  id integer,
  name varchar(255) not null,
  source_date date,
  constraint pk_codauth primary key (id),
  constraint un_codauth_namedt unique (name, source_date)
);
insert into coding_authority(id, name) values(0, 'Simple Word Search');
insert into coding_authority(id, name) values(1, 'FDA Indexing');

create table meddra_llt_spl_coded_sec (
  llt_code integer,
  spl_set_id varchar(200),
  section_type varchar(20),
  assigned_by integer,
  constraint pk_mlltsplcodedsec primary key (llt_code, spl_set_id, section_type, assigned_by),
  constraint fk_mlltsplcodedsec_llt foreign key (llt_code) references MedDRA.low_level_term (llt_code),
  constraint fk_mlltsplcodedsec_codauth foreign key (assigned_by) references coding_authority (id),
  constraint fk_mlltsplcodedsec_sect foreign key (section_type) references DrugLabel.section_type (loinc_code)
);
create index ix_mlltsplcodedsec_setid on meddra_llt_spl_coded_sec(spl_set_id);
create bitmap index ix_mlltsplcodedsec_assignedby on meddra_llt_spl_coded_sec (assigned_by);
create index ix_mlltsplcodedsec_sect ON meddra_llt_spl_coded_sec (section_type);


create table meddra_llt_spl_uncl_sec (
  llt_code integer,
  spl_set_id varchar(200),
  section_title varchar(4000) not null,
  assigned_by integer,
  constraint pk_mlltsplunclsec primary key (llt_code, spl_set_id, section_title, assigned_by),
  constraint fk_mlltsplunclsec_llt foreign key (llt_code) references MedDRA.low_level_term (llt_code),
  constraint fk_mlltsplunclsec_codauth foreign key (assigned_by) references coding_authority (id)
);
create index ix_mlltsplunclsec_setid on meddra_llt_spl_uncl_sec(spl_set_id);
create bitmap index ix_mlltsplunclsec_assignedby on meddra_llt_spl_uncl_sec (assigned_by);



create table meddra_pt_spl_coded_sec (
  pt_code integer,
  spl_set_id varchar(200),
  section_type varchar(20),
  assigned_by integer,
  constraint pk_mptsplcodedsec primary key (pt_code, spl_set_id, section_type, assigned_by),
  constraint fk_mptsplcodedsec_pt foreign key (pt_code) references MedDRA.preferred_term (pt_code),
  constraint fk_mptsplcodedsec_codauth foreign key (assigned_by) references coding_authority (id),
  constraint fk_mptsplcodedsec_sect foreign key (section_type) references DrugLabel.section_type (loinc_code)
);
create index ix_mptsplcodedsec_setid on meddra_pt_spl_coded_sec(spl_set_id);
create bitmap index ix_mptsplcodedsec_assignedby on meddra_pt_spl_coded_sec (assigned_by);
create index ix_mptsplcodedsec_sect ON meddra_pt_spl_coded_sec (section_type);


create table meddra_pt_spl_uncl_sec (
  pt_code integer,
  spl_set_id varchar(200),
  section_title varchar(4000) not null,
  assigned_by integer,
  constraint pk_mptsplunclsec primary key (pt_code, spl_set_id, section_title, assigned_by),
  constraint fk_mptsplunclsec_pt foreign key (pt_code) references MedDRA.preferred_term (pt_code),
  constraint fk_mptsplunclsec_codauth foreign key (assigned_by) references coding_authority (id)
);
create index ix_mptsplunclsec_setid on meddra_pt_spl_uncl_sec(spl_set_id);
create bitmap index ix_mptsplunclsec_assignedby on meddra_pt_spl_uncl_sec (assigned_by);


create table meddra_term_spl_count (
  term varchar2(100),
  assigned_by int,
  spl_count int not null,
  is_pt int not null check(is_pt = 0 or is_pt = 1),
  constraint pk_mtermsplcount primary key (term, assigned_by)
);
create bitmap index ix_mtermspl_count_ispt on meddra_term_spl_count(is_pt);

create table meddra_pt_spl_count (
  pt_code int,
  assigned_by int,
  spl_count int,
  constraint pk_mptsplcount primary key (pt_code, assigned_by)
);


-- Views

-- TODO: llt views (needs more specific or filtered section information)
select llt_sec.spl_set_id, sect.loinc_name, llt.llt_name, case when llt.llt_code = llt.pt_code then 'Y' else 'N' end is_pt
from meddra_llt_spl_sec llt_sec
join meddra.low_level_term llt on llt_sec.llt_code = llt.llt_code
join section_type sect on llt_sec.section_type = sect.loinc_code
;

-- llt occurrences in all coded sections, joined to meddra hierarchy
create view meddra_llt_hier_spl_coded_sec as
select sect.loinc_name section, llt.llt_name llt, llt_sec.spl_set_id set_id, llt.llt_currency is_llt_current, llt_sec.llt_code, llt_sec.section_type section_code,
       mhier.pt_name pt, mhier.hlt_name hlt, mhier.hlgt_name hlgt, mhier.soc_name soc, mhier.primary_soc_fg is_primary_soc_for_pt,
       mhier.pt_code, mhier.hlt_code, mhier.hlgt_code, mhier.soc_code, mhier.pt_soc_code pt_primary_soc_code
from meddra_llt_spl_coded_sec llt_sec
join meddra.low_level_term llt on llt_sec.llt_code = llt.llt_code
join section_type sect on llt_sec.section_type = sect.loinc_code
join meddra.meddra_hierarchy mhier on llt.pt_code = mhier.pt_code
;
