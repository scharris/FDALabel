create table soc_term (
  soc_code integer not null,
  soc_name varchar2(100) not null,
  soc_abbrev varchar2(5) not null,
/*soc_whoart_code varchar2(7),
  soc_harts_code integer,
  soc_costart_sym varchar2(21),
  soc_icd9_code varchar2(8),
  soc_icd9cm_code varchar2(8),
  soc_icd10_code varchar2(8),
  soc_jart_code varchar2(6)*/
  constraint pk_soc primary key (soc_code)
);
create unique index ix_soc_name on soc_term(soc_name);

create table high_level_grouping_term (
  hlgt_code integer,
  hlgt_name varchar2(100)  not null,
/*hlgt_whoart_code varchar2(7),
  hlgt_harts_code  integer,
  hlgt_costart_sym varchar2(21),
  hlgt_icd9_code      varchar2(8),
  hlgt_icd9cm_code varchar2(8),
  hlgt_icd10_code  varchar2(8),
  hlgt_jart_code   varchar2(6)*/
  constraint pk_hlgt primary key(hlgt_code)
);
create unique index ix_hlgt_name on high_level_grouping_term(hlgt_name);

create table soc_hlgt (
  soc_code integer,
  hlgt_code integer,
  constraint pk_sochlgt primary key(soc_code, hlgt_code),
  constraint fk_sochlgt_soc foreign key (soc_code) references soc_term(soc_code),
  constraint fk_sochlgt_hlgt foreign key (hlgt_code) references high_level_grouping_term(hlgt_code)
);
create index ix_sochlght_hlgtsoc on soc_hlgt(hlgt_code, soc_code);


create table high_level_term (
  hlt_code integer,
  hlt_name varchar2(100 byte) not null,
/*hlt_whoart_code varchar2(7 byte),
  hlt_harts_code integer,
  hlt_costart_sym varchar2(21 byte),
  hlt_icd9_code varchar2(8 byte),
  hlt_icd9cm_code varchar2(8 byte),
  hlt_icd10_code varchar2(8 byte),
  hlt_jart_code varchar2(6 byte)*/
  constraint pk_hlt primary key(hlt_code)
);
create index ix_hlt_name on high_level_term(hlt_name);

create table hlgt_hlt(
  hlgt_code integer,
  hlt_code integer,
  constraint pk_hlgthlt primary key (hlgt_code, hlt_code),
  constraint fk_hlgthlt_hlgt foreign key (hlgt_code) references high_level_grouping_term(hlgt_code),
  constraint fk_hlgthlt_hlt foreign key (hlt_code) references high_level_term(hlt_code)
);
create index ix_hlgthlt_hlthlgt on hlgt_hlt(hlt_code, hlgt_code);


create table preferred_term (
  pt_code integer,
  pt_name varchar2(100) not null,
--null_field varchar2(1),
  pt_soc_code integer,
/*pt_whoart_code varchar2(7),
  pt_harts_code integer,
  pt_costart_sym varchar2(21),
  pt_icd9_code varchar2(8),
  pt_icd9cm_code varchar2(8),
  pt_icd10_code varchar2(8),
  pt_jart_code varchar2(6),*/
  constraint pk_pt primary key(pt_code),
  constraint fk_pt_soc foreign key (pt_soc_code) references soc_term(soc_code)
);
create index ix_pt_name on preferred_term(pt_name);
create index ix_pt_soccode on preferred_term(pt_soc_code);


create table hlt_pt (
  hlt_code integer,
  pt_code  integer,
  constraint pk_hltpcomp primary key (hlt_code, pt_code),
  constraint fk_hltprefcomp_hlt foreign key (hlt_code) references high_level_term(hlt_code),
  constraint fk_hltprefcomp_pt foreign key (pt_code) references preferred_term(pt_code)
);
create index ix_hltpt_pthlt on hlt_pt(pt_code, hlt_code);


create table low_level_term (
  llt_code integer,
  llt_name varchar2(100) not null,
  pt_code integer,
--  llt_whoart_code varchar2(7),
--  llt_harts_code integer,
--  llt_costart_sym varchar2(21),
--  llt_icd9_code varchar2(8),
--  llt_icd9cm_code varchar2(8),
--  llt_icd10_code varchar2(8),
  llt_currency varchar2(1),
--  llt_jart_code varchar2(6),
  constraint pk_llt primary key (llt_code),
  constraint fk_llt_pt foreign key (pt_code) references preferred_term(pt_code)
);
create index ix_llt_name on low_level_term(llt_name);
create index ix_llt_ptcode on low_level_term(pt_code); 


create table meddra_hierarchy (
  pt_code integer not null,
  hlt_code integer not null,
  hlgt_code integer not null,
  soc_code integer not null,
  pt_name varchar2(100) not null,
  hlt_name varchar2(100) not null,
  hlgt_name varchar2(100) not null,
  soc_name varchar2(100) not null,
  soc_abbrev varchar2(5) not null,
--null_field varchar2(1),
  pt_soc_code integer,
  primary_soc_fg varchar2(1),
  constraint pk_meddrahier primary key (pt_code, soc_code),
  constraint fk_medrahier_pt foreign key (pt_code) references preferred_term(pt_code),
  constraint fk_medrahier_hlt foreign key (hlt_code) references high_level_term(hlt_code),
  constraint fk_medrahier_hlgt foreign key (hlgt_code) references high_level_grouping_term(hlgt_code),
  constraint fk_medrahier_soc foreign key (soc_code) references soc_term(soc_code)
);
create index ix_mdhier_hlt on meddra_hierarchy(hlt_code);
create index ix_mdhier_hlgt on meddra_hierarchy(hlgt_code);
create index ix_mdhier_soc on meddra_hierarchy(soc_code);
create index ix_mdhier_ptnm on meddra_hierarchy(pt_name);
create index ix_mdhier_hltnm on meddra_hierarchy(hlt_name);
create index ix_mdhier_hlgtnm on meddra_hierarchy(hlgt_name);
create index ix_mdhier_socnm on meddra_hierarchy(soc_name);


create table soc_intl_order (
  intl_ord_code integer,
  soc_code integer,
  constraint pk_socintlorder primary key (intl_ord_code, soc_code)
);
create index ix_socintlorder_ordsoc on soc_intl_order(soc_code, intl_ord_code);


create table smq_list (
  smq_code integer not null,
  smq_name varchar2(100) not null,
  smq_level integer not null,
  smq_description varchar(4000) not null,
  smq_source varchar(4000),
  smq_note varchar(4000),
  meddra_version varchar2(5) not null,
  status varchar2(1) not null,
  smq_algorithm varchar2(256) not null,
  constraint pk_smqlist primary key (smq_code)
);

create table smq_content (
  smq_code integer,
  term_code integer not null,
  term_level integer not null,
  term_scope integer not null,
  term_category varchar2(1) not null,
  term_weight integer not null,
  term_status varchar2(1) not null,
  term_addition_version varchar2(5) not null,
  term_last_modified_version varchar2(5) not null,
  constraint pk_smqcontent primary key (smq_code, term_code),
  constraint fk_smqcontent_smq foreign key (smq_code) references smq_list (smq_code)
);
create index ix_smqcontent_termcode on smq_content(term_code);


grant select, references on SOC_TERM to DrugLabel;
grant select, references on HIGH_LEVEL_GROUPING_TERM to DrugLabel;
grant select, references on SOC_HLGT to DrugLabel;
grant select, references on HIGH_LEVEL_TERM to DrugLabel;
grant select, references on HLGT_HLT to DrugLabel;
grant select, references on PREFERRED_TERM to DrugLabel;
grant select, references on HLT_PT to DrugLabel;
grant select, references on LOW_LEVEL_TERM to DrugLabel;
grant select, references on SOC_INTL_ORDER to DrugLabel;
grant select, references on MEDDRA_HIERARCHY to DrugLabel;
grant select, references on SMQ_LIST to DrugLabel;
grant select, references on SMQ_CONTENT to DrugLabel;

grant select, references on SOC_TERM to FDALabel;
grant select, references on HIGH_LEVEL_GROUPING_TERM to FDALabel;
grant select, references on SOC_HLGT to FDALabel;
grant select, references on HIGH_LEVEL_TERM to FDALabel;
grant select, references on HLGT_HLT to FDALabel;
grant select, references on PREFERRED_TERM to FDALabel;
grant select, references on HLT_PT to FDALabel;
grant select, references on LOW_LEVEL_TERM to FDALabel;
grant select, references on SOC_INTL_ORDER to FDALabel;
grant select, references on MEDDRA_HIERARCHY to FDALabel;
grant select, references on SMQ_LIST to FDALabel;
grant select, references on SMQ_CONTENT to FDALabel;
