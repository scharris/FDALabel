create or replace package body meddralink as

procedure load_sec_llts_via_text_search is
  cleaned_escaped_llt varchar2(500);
begin
  for llt in (select llt_name, llt_code from meddra.low_level_term order by llt_code)
  loop
    begin
      cleaned_escaped_llt := '{' || regexp_replace(llt.llt_name, '([%{}-])',' ') || '}';
      
      insert into meddra_llt_spl_coded_sec (llt_code, spl_set_id, section_type, assigned_by)
        select distinct llt.llt_code, l.set_id, ls.loinc_code, 0
          from spl l
          join spl_sec ls on ls.spl_id = l.id
          where contains(ls.content_xml, cleaned_escaped_llt) > 0
            and ls.loinc_code is not null and ls.loinc_code <> '42229-5'; -- SPL UNCLASSIFIED SECTION
            
    exception when others then
       dbms_output.put_line('Error for llt code ' || llt.llt_code);
       raise;
    end;
  end loop;
end;


procedure refresh_term_spl_counts is
begin
  delete from meddra_term_spl_count;

  insert into meddra_term_spl_count
    select llt.llt_name "term", counts.assigned_by "assigned_by", counts.spl_count "spl_count", case when llt.pt_code = llt.llt_code then 1 else 0 end "is_pt"
    from 
      (select llt_code, assigned_by, count(distinct spl_set_id) spl_count
       from meddra_llt_spl_coded_sec llt_sec
       group by llt_code, assigned_by) counts
      join meddra.low_level_term llt on counts.llt_code = llt.llt_code;
end;


procedure refresh_from_sec_llts is
begin

    delete from meddra_pt_spl_coded_sec;

    insert into meddra_pt_spl_coded_sec (pt_code, spl_set_id, section_type, assigned_by)
      select tls.llt_code, tls.spl_set_id, tls.section_type, tls.assigned_by
      from meddra_llt_spl_coded_sec tls
      where tls.llt_code in (select pt.pt_code from meddra.preferred_term pt);

    refresh_term_spl_counts();

end refresh_from_sec_llts;


procedure refresh_meddra_linkage_data is
begin

  delete from meddra_llt_spl_coded_sec;

  load_sec_llts_via_text_search();

  refresh_from_sec_llts();

end refresh_meddra_linkage_data;


end meddralink;
/
