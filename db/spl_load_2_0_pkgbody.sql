create or replace package body spl_load_2_0 as

  procedure delete_spl(p_spl_id integer,
                       p_include_dgv integer) is
    begin
      delete from sum_spl_gen_prod_ndc where spl_id = p_spl_id;

      delete from sum_spl_gen_prod_act_ingr_unii where spl_id = p_spl_id;

      delete from sum_spl_gen_prod_mkt_act where spl_id = p_spl_id;

      delete from sum_spl_gen_prod where spl_id = p_spl_id;

      delete from sum_spl_ndc where spl_id = p_spl_id;

      delete from sum_spl_prodname where spl_id = p_spl_id;

      delete from sum_spl_dosageform where spl_id = p_spl_id;

      delete from sum_spl_route where spl_id = p_spl_id;

      delete from sum_spl_mkt_cat where spl_id = p_spl_id;

      delete from sum_spl_act_ingr_unii where spl_id = p_spl_id;

      delete from sum_spl_act_ingr_name where spl_id = p_spl_id;

      delete from sum_spl_mkt_act where spl_id = p_spl_id;

      delete from sum_spl where spl_id = p_spl_id;

      delete spl_tag where spl_id = p_spl_id;

      delete spl_ann where spl_id = p_spl_id;

      delete spl_att where spl_id = p_spl_id;

      delete spl_html where spl_id = p_spl_id;

      delete spl_sec where spl_id = p_spl_id;

      delete prod_dea where spl_id = p_spl_id;

      delete prod_route where spl_id = p_spl_id;

      delete prod_ingr where spl_id = p_spl_id;

      delete prod_appr where spl_id = p_spl_id;

      delete prod_mkt_act where spl_id = p_spl_id;

      delete prod_gen_name_comp where spl_id = p_spl_id;

      delete spl_prod where spl_id = p_spl_id;

      delete spl_prod_staging where spl_id = p_spl_id;

      if p_include_dgv = 1 then
        dgv.delete_spl(p_spl_id);
      end if;

      delete spl where id = p_spl_id;

  end delete_spl;

  procedure insert_spl(p_spl_xml xmltype,
                       p_spl_entry_dir varchar2,
                       p_spl_xml_file_name varchar2,
                       p_spl_xml_md5 varchar2,
                       p_load_num integer,
                       p_include_dgv integer,
                       po_spl_id out integer) is
    l_set_id varchar(200) := null;
    l_old_splid_having_setid  integer := null;
  begin

    -- No spl in the spl table should have this spl's set-id, but often the current set of spls from dailymed includes a few spls with duplicate set ids.
    -- If we are trying to insert a new spl which would have caused a duplicate set id, then delete the original spl having the same set id first.
    select lower(xmlcast(xmlquery('declare default element namespace "urn:hl7-org:v3"; $spl/document/setId/@root' passing p_spl_xml as "spl" returning content) as varchar2(200)))
           into l_set_id from dual;
    begin
      select id into l_old_splid_having_setid from spl where set_id = l_set_id;
      delete_spl(l_old_splid_having_setid, p_include_dgv);
    exception when NO_DATA_FOUND THEN null;
    end;

    -- Insert the drug label.

    insert into spl(id,
                    archive_dir,
                    filename,
                    spl_xml,
                    spl_md5,
                    loaded_ts,
                    created_load_num,
                    author_org,
                    title,
                    normd_org,
                    doc_type_loinc_code,
                    doc_type_name_from_mfr,
                    eff_time,
                    version_num,
                    set_id,
                    guid)
      values(s_spl.nextval,
             p_spl_entry_dir,
             p_spl_xml_file_name,
             p_spl_xml,
             p_spl_xml_md5,
             systimestamp,
             p_load_num,
             xmlcast(xmlquery('declare default element namespace "urn:hl7-org:v3"; $spl/document/fn:string-join(author/assignedEntity/representedOrganization/name/text(),"|")' passing p_spl_xml as "spl" returning content) as varchar2(500)),
             xmlcast(xmlquery('declare default element namespace "urn:hl7-org:v3"; $spl/document/fn:string-join(title/text()," ")' passing p_spl_xml as "spl" returning content) as varchar2(500)),
             trim(regexp_replace(replace(upper(xmlcast(xmlquery('declare default element namespace "urn:hl7-org:v3"; $spl/document/fn:string-join(author/assignedEntity/representedOrganization/name/text(),"|")' passing p_spl_xml as "spl" returning content) as varchar2(500))),'&','AND'), '[^[:alpha:]]+', ' ')),
             xmlcast(xmlquery('declare default element namespace "urn:hl7-org:v3"; $spl/document/code[@codeSystem="2.16.840.1.113883.6.1"]/@code' passing p_spl_xml as "spl" returning content) as varchar2(500)),
             xmlcast(xmlquery('declare default element namespace "urn:hl7-org:v3"; $spl/document/code[@codeSystem="2.16.840.1.113883.6.1"]/@displayName' passing p_spl_xml as "spl" returning content) as varchar2(500)),
             xmlcast(xmlquery('declare default element namespace "urn:hl7-org:v3"; $spl/document/effectiveTime/@value' passing p_spl_xml as "spl" returning content) as varchar2(500)),
             xmlcast(xmlquery('declare default element namespace "urn:hl7-org:v3"; $spl/document/versionNumber/@value' passing p_spl_xml as "spl" returning content) as integer),
             l_set_id,
             lower(xmlcast(xmlquery('declare default element namespace "urn:hl7-org:v3"; $spl/document/id/@root' passing p_spl_xml as "spl" returning content) as varchar2(500))))
      returning id into po_spl_id;

  end insert_spl;


  procedure insert_spl_html(p_spl_id integer,
                            p_spl_html clob) is
  begin

    insert into spl_html(spl_id, sec_num, html)
      values(p_spl_id, 0, p_spl_html);

  end insert_spl_html;


  procedure insert_spl_secs(p_spl_id integer) is
  begin
    insert into spl_sec (id, spl_id, loinc_code, content_xml, guid, title, parent_sec_guid)
      select /*+ NO_XML_QUERY_REWRITE */
        s_spl_sec.nextval,
        l.id spl_id,
        xt.*
      from
        spl l,
        xmltable(
          xmlnamespaces(default 'urn:hl7-org:v3'),
          'for $sec at $n in /document/component/structuredBody/component//section[code[@codeSystem="2.16.840.1.113883.6.1"]]
             return element r {$sec, element parent-sec {$sec/ancestor::section[1]}}'
          passing l.spl_xml
          columns
            loinc_code varchar2(20)
              path 'section/code[@codeSystem="2.16.840.1.113883.6.1"]/@code',
            content_xml xmltype
              path 'section',
            guid varchar2(36)
              path 'fn:lower-case(section/id/@root)',
            title varchar2(2000)
              path 'section/code[@codeSystem="2.16.840.1.113883.6.1"]/@displayName',
            parent_sec_guid varchar2(36)
              path 'fn:lower-case(parent-sec/section/id/@root)'
        ) xt
      where l.id = p_spl_id;

  end insert_spl_secs;


  procedure insert_spl_prod_deps(p_spl_id integer,
                                 p_include_dgv integer) is
  begin
    delete from spl_prod_staging where spl_id = p_spl_id;

    insert into spl_prod_staging (id, spl_id, normd_generic_name, name, generic_name, mfr_org_name, ndc_code,
        ncit_form_code, ncit_form_name, gen_mat_kinds_xml, equiv_to_ndc_code, packagings_xml, ingredients_xml, characteristics_xml,
        approvals_xml, marketing_acts_xml, issues_xml, consumed_ins_xml, participations_xml, ncit_dea_code, ncit_dea_name, doc_order)
      select /*+ NO_XML_QUERY_REWRITE */
        s_spl_prod.nextval id,
        l.id spl_id,
        trim(upper(xt.generic_name)) normd_generic_name,
        xt.*,
        rownum
      from
        spl l,
        xmltable(xmlnamespaces(default 'urn:hl7-org:v3'),
          '/document/component/structuredBody/component/section/subject/manufacturedProduct[manufacturedMedicine|manufacturedProduct]' passing l.spl_xml
          columns
            name varchar2(500)
              path './(manufacturedMedicine|manufacturedProduct)/normalize-space(string-join(name//text()," "))'
           ,generic_name varchar2(500)
              path './(manufacturedMedicine|manufacturedProduct)/asEntityWithGeneric/genericMedicine/normalize-space(string-join(name//text()," "))'
           ,mfr_org_name varchar2(500)
              path 'manufacturerOrganization/normalize-space(string-join(name//text()," "))'
           ,ndc_code varchar2(50)
              path './(manufacturedMedicine|manufacturedProduct)/code[@codeSystem="2.16.840.1.113883.6.69" or not(@codeSystem)]/normalize-space(@code)'
           ,ncit_form_code varchar2(6)
              path './(manufacturedMedicine|manufacturedProduct)/formCode[@codeSystem="2.16.840.1.113883.3.26.1.1" or not(@codeSystem)]/normalize-space(@code)'
           ,ncit_form_name varchar2(200)
              path './(manufacturedMedicine|manufacturedProduct)/formCode[@codeSystem="2.16.840.1.113883.3.26.1.1" or not(@codeSystem)]/@displayName'
           ,gen_mat_kinds_xml xmltype
              path '<generalized-material-kinds>{./(manufacturedMedicine|manufacturedProduct)/asSpecializedKind/(generalizedMaterialKind|generalizedPharmaceuticalClass)}</generalized-material-kinds>'
           ,equiv_to_ndc_code varchar2(50)
              path './(manufacturedMedicine|manufacturedProduct)/asEquivalentEntity/definingMaterialKind/code[@codeSystem="2.16.840.1.113883.6.69" or not(@codeSystem)]/normalize-space(@code)'
           ,packagings_xml xmltype
              path '<packaging-list>{./(manufacturedMedicine|manufacturedProduct)/asContent}</packaging-list>'
           ,ingredients_xml xmltype
              path '<ingredient-list>{./(manufacturedMedicine|manufacturedProduct|manufacturedMedicine/part/partMedicine|manufacturedProduct/part/partProduct)/(activeIngredient|ingredient)}</ingredient-list>'
           ,characteristics_xml xmltype
              path '<characteristic-list>{subjectOf/characteristic}</characteristic-list>'
           ,approvals_xml xmltype
              path '<approval-list>{subjectOf/approval|(manufacturedProduct|manufacturedMedicine)/subjectOf/approval}</approval-list>'
           ,marketing_acts_xml xmltype
              path '<marketing-act-list>{subjectOf/marketingAct}</marketing-act-list>'
           ,issues_xml xmltype
              path '<issue-list>{subjectOf/issue}</issue-list>'
           ,consumed_ins_xml xmltype
              path '<consumed-in-list>{consumedIn}</consumed-in-list>'
           ,participations_xml xmltype
              path '<participation-list>{participation}</participation-list>'
           ,ncit_dea_code varchar2(6)
              path './subjectOf/policy/code[@codeSystem="2.16.840.1.113883.3.26.1.1"][1]/@code'
           ,ncit_dea_name varchar2(10)
              path './subjectOf/policy/code[@codeSystem="2.16.840.1.113883.3.26.1.1"][1]/@displayName'
        ) xt
      where l.id = p_spl_id;


    -- Insert spl products.
    insert into spl_prod (id, spl_id, normd_generic_name, name, generic_name, ndc_code, ncit_form_code, ncit_form_name, doc_order)
      select id, spl_id, normd_generic_name, name, generic_name, ndc_code, ncit_form_code, ncit_form_name, doc_order
      from spl_prod_staging
      where spl_id = p_spl_id;

    -- insert prod_dea
    insert into prod_dea(product_id, ncit_dea_code, ncit_dea_name, spl_id)
      select id, ncit_dea_code, ncit_dea_name, spl_id
      from spl_prod_staging
      where spl_id=p_spl_id and ncit_dea_code is not null;

    -- Insert product routes.
    insert into prod_route (spl_id, product_id, ncit_route_of_admin_code)
      select distinct /*+ NO_XML_QUERY_REWRITE */
          p.spl_id,
          p.id product_id,
          xt.*
        from
          spl_prod_staging p,
          xmltable(xmlnamespaces(default 'urn:hl7-org:v3'),
            '/consumed-in-list/consumedIn/substanceAdministration' passing p.consumed_ins_xml
            columns
              ncit_route_of_admin_code varchar2(6)
                /* This fn:substring call should not be necessary, there seems to be an Oracle bug triggering ORA-00932 (inconsistent datatypes)
                  for no valid reason without it though (even for a single test row with a code of length 6). Removing the bracketed condition
                  avoids the error but would potentially insert codes of the wrong type. */
                path 'fn:substring(routeCode[@codeSystem="2.16.840.1.113883.3.26.1.1" or not(@codeSystem)]/@code,1,6)'
                --original: path 'routeCode[@codeSystem="2.16.840.1.113883.3.26.1.1" or not(@codeSystem)]/@code'
          ) xt
        where p.spl_id = p_spl_id and xt.ncit_route_of_admin_code is not null;


    -- Insert product ingredients.

    insert into prod_ingr
     select /*+ NO_XML_QUERY_REWRITE */
        s_prod_ingr.nextval id
       ,p.spl_id
       ,p.id product_id
       ,xt.*
     from
       spl_prod_staging p,
       xmltable(
         xmlnamespaces(default 'urn:hl7-org:v3'),
         '/ingredient-list/*' passing p.ingredients_xml
         columns
           -- substance identification fields
           substance_name varchar2(500)
             path './(activeIngredientSubstance|ingredientSubstance)/normalize-space(string(name))' -- "name" element can have mixed content but seems to not have in practice.
          ,unii varchar2(50)
             path './(activeIngredientSubstance|ingredientSubstance)/code[@codeSystem="2.16.840.1.113883.4.9"]/normalize-space(@code)'
          ,is_active int
             path 'if ( local-name(.) = "activeIngredient" or starts-with(@classCode, "ACTI") ) then 1 else 0'
          ,basis_of_strength_code varchar2(1)
             path 'fn:substring(@classCode,4,1)' -- B=basis is active ingredient itself, R=basis is reference drug, M=basis is active moeity
           -- quantity fields
          ,qty_numer_val number
             path 'quantity/numerator/@value'
          ,qty_numer_unit varchar2(200)
             path 'quantity/numerator/@unit'
          ,qty_numer_ncit_units_val number
             path 'quantity/numerator/translation[@codeSystem="2.16.840.1.113883.3.26.1.1"]/@value'
          ,qty_numer_ncit_unit_name varchar2(200)
             path 'quantity/numerator/translation[@codeSystem="2.16.840.1.113883.3.26.1.1"]/@displayName'
          ,qty_numer_ncit_unit_code varchar2(50)
             path 'quantity/numerator/translation[@codeSystem="2.16.840.1.113883.3.26.1.1"]/@code'
          ,qty_denom_val number
             path 'quantity/denominator/@value'
          ,qty_denom_unit varchar2(200)
             path 'quantity/denominator/@unit'
          ,qty_denom_ncit_units_val number
             path 'quantity/denominator/translation[@codeSystem="2.16.840.1.113883.3.26.1.1"]/@value'
          ,qty_denom_ncit_unit_name varchar2(200)
             path 'quantity/denominator/translation[@codeSystem="2.16.840.1.113883.3.26.1.1"]/@displayName'
          ,qty_denom_ncit_unit_code varchar2(50)
             path 'quantity/denominator/translation[@codeSystem="2.16.840.1.113883.3.26.1.1"]/@code'
          ,gen_mat_kinds_xml xmltype
             path '<generalized-material-kinds>{(activeIngredientSubstance|ingredientSubstance)/asSpecializedKind/(generalizedMaterialKind|generalizedPharmaceuticalClass)}</generalized-material-kinds>'
          ,active_moeities_xml xmltype
             path '<activeMoeities>{(activeIngredientSubstance|ingredientSubstance)/activeMoiety/activeMoiety}</activeMoeities>'
       ) xt
     where p.spl_id = p_spl_id;


    -- Insert product approvals.

    insert into prod_appr
      select /*+ NO_XML_QUERY_REWRITE */
        s_prod_appr.nextval id,
        p.id product_id,
        p.spl_id,
        xt.*
      from
        spl_prod_staging p,
        xmltable(
          xmlnamespaces(default 'urn:hl7-org:v3'),
          '/approval-list/approval' passing p.approvals_xml
          columns
            approval_num varchar2(200)
              path 'id[@root="2.16.840.1.113883.3.150"]/@extension'
           ,marketing_cat_ncit_code varchar2(10)
              path 'code[@codeSystem="2.16.840.1.113883.3.26.1.1" or not(@codeSystem)]/@code'
           ,marketing_cat_description varchar2(200)
              path 'code/@displayName'
           ,status_code varchar2(10)
               path 'statusCode/@code' -- no system
           -- ignoring "holder"
           ,date_granted varchar2(50)
               path 'author/time/@value'
           ,territory_country_code varchar2(20)
               path 'author/territorialAuthority/territory/code[@codeSystem="2.16.840.1.113883.5.28" or not(@codeSystem)]/@code'
           ,territory_country_name varchar2(200)
              path 'author/territorialAuthority/territory/name'
           ,agency_name varchar2(200)
               path 'author/territorialAuthority/governingAgency/name'
        ) xt
      where p.spl_id = p_spl_id;


    -- Insert product marketing acts.
    insert into prod_mkt_act
      select /*+ NO_XML_QUERY_REWRITE */
        s_prod_mkt_act.nextval id,
        p.id product_id,
        p.spl_id,
        xt.*
      from
        spl_prod_staging p,
        xmltable(
          xmlnamespaces(default 'urn:hl7-org:v3'),
          '/marketing-act-list/marketingAct' passing p.marketing_acts_xml
          columns
            act_ncit_code varchar2(10)
              path 'code[@codeSystem="2.16.840.1.113883.3.26.1.1" or not(@codeSystem)]/@code'
           ,status_code varchar2(10)
              path 'statusCode/@code' -- no system
           ,low_date varchar2(50)
              path 'effectiveTime/low/@value'
           ,low_date_inclusive varchar2(5)
              path 'effectiveTime/low/@inclusive'
           ,high_date varchar2(50)
              path 'effectiveTime/high/@value'
           ,high_date_inclusive varchar2(5)
              path 'effectiveTime/high/@inclusive'
           ,center_date varchar2(50)
              path 'effectiveTime/center/@value'
           ,date_width varchar2(50)
              path 'effectiveTime/width/@value'
           ,date_width_unit varchar2(50)
              path 'effectiveTime/width/@unit'
        ) xt
      where p.spl_id = p_spl_id;


      -- Insert product generic name components.
      declare
        l_gen_name_comps varchars_ntt;
        l_gen_name varchar2(500);
      begin
        for prod in (select id, normd_generic_name from spl_prod where spl_id = p_spl_id)
        loop

          l_gen_name := regexp_replace(prod.normd_generic_name, '\([^)]*\)', ''); -- remove parenthesized text
          l_gen_name := replace(l_gen_name, ' AND ', ',');                        -- Normalize separators before splitting.
          l_gen_name := replace(l_gen_name, ' - ',   ',');                        -- "
          l_gen_name := regexp_replace(l_gen_name, '-$', '');                     -- Remove dangling separator.
          l_gen_name := trim(l_gen_name);

          select extract_regexp(l_gen_name,'[^,]+') into l_gen_name_comps from dual;

          for i in 1 .. l_gen_name_comps.count
          loop
            begin
              if trim(l_gen_name_comps(i)) is not null then
                insert into prod_gen_name_comp values(prod.id, p_spl_id, trim(l_gen_name_comps(i)));
              end if;
            exception when DUP_VAL_ON_INDEX then
              null; -- OK
            end;
          end loop;
        end loop;
      end;

    if p_include_dgv = 1 then
      dgv.insert_spl_prod_deps(p_spl_id);
    end if;

  end insert_spl_prod_deps;


  -- Insert spls and records which are derived from the spl itself, including spl html, sections, products, and product dependents.
  procedure insert_spl_and_deps(p_spl_xml_txt clob,
                                p_spl_html clob,
                                p_spl_entry_dir varchar2,
                                p_spl_xml_file_name varchar2,
                                p_spl_xml_md5 varchar2,
                                p_load_num integer,
                                p_include_dgv integer,
                                po_spl_id out integer) is
  begin

    insert_spl(xmltype(p_spl_xml_txt),
               p_spl_entry_dir,
               p_spl_xml_file_name,
               p_spl_xml_md5,
               p_load_num,
               p_include_dgv,
               po_spl_id);

    insert_spl_html(po_spl_id, p_spl_html);

    insert_spl_secs(po_spl_id);

    insert_spl_prod_deps(po_spl_id, p_include_dgv);

  end insert_spl_and_deps;



  procedure insert_spl_att(p_spl_id integer,
                           p_att_name varchar2,
                           p_att_data blob) is
  begin

    insert into spl_att(spl_id, name, data)
      values(p_spl_id,
             p_att_name,
             p_att_data);

  end insert_spl_att;


  procedure delete_summary_data(p_include_dgv integer) is
  begin
    delete from sum_spl_gen_prod_ndc;

    delete from sum_spl_gen_prod_act_ingr_unii;

    delete from sum_spl_gen_prod_mkt_act;

    delete from sum_spl_gen_prod;

    delete from sum_spl_ndc;

    delete from sum_spl_prodname;

    delete from sum_spl_dosageform;

    delete from sum_spl_route;

    delete from sum_spl_mkt_cat;

    delete from sum_spl_act_moiety_unii;

    delete from sum_spl_act_moiety_name;

    delete from sum_spl_act_ingr_unii;

    delete from sum_spl_act_ingr_name;

    delete from sum_spl_mkt_act;

    delete from sum_spl_epc;

    delete from sum_spl;

    delete from spl_section_count;

    delete from document_type_count;

    delete from marketing_cat_count;

    delete from medrt_concept_spl_count;

    if p_include_dgv = 1 then
  	  dgv.delete_summary_data();
  	end if;

  end delete_summary_data;


  procedure archive_spl(p_spl_id integer,
                        p_load_num integer,
                        p_include_dgv integer) is
  begin
      insert into archived_spl
        select p_load_num, l.* from spl l where l.id = p_spl_id;

      insert into archived_spl_att
        select la.* from spl_att la where la.spl_id = p_spl_id;

      insert into archived_spl_html
        select lh.* from spl_html lh where lh.spl_id = p_spl_id;

      insert into archived_spl_tag
        select lt.* from spl_tag lt where lt.spl_id = p_spl_id;

      insert into archived_spl_ann
        select la.* from spl_ann la where la.spl_id = p_spl_id;

      delete_spl(p_spl_id, p_include_dgv);

  end archive_spl;


  procedure archive_spls_not_in_load(p_load_num integer,
                                     p_include_dgv integer,
                                     p_max_archived integer default -1) is
    l_archive_count integer;
  begin
      select count(*) into l_archive_count
        from (select l.id spl_id from spl l where l.spl_md5 not in
               (select lmd5.spl_md5 from load_md5 lmd5 where lmd5.load_num = p_load_num));

      if p_max_archived > 0 and l_archive_count > p_max_archived then
        raise_application_error(-20101, 'Maximum allowable number ' || p_max_archived || ' of archived spls would be exceeded by this archive operation.');
      end if;

      for splid_rec in (select l.id spl_id from spl l where l.spl_md5 not in
                         (select lmd5.spl_md5 from load_md5 lmd5 where lmd5.load_num = p_load_num))
      loop

        archive_spl(splid_rec.spl_id, p_load_num, p_include_dgv);

      end loop;

  end archive_spls_not_in_load;


procedure remove_spls_not_in_load(p_load_num integer,
                                  p_include_dgv integer,
                                  p_max_removed integer default -1) is
    l_remove_count integer;
  begin
      select count(*) into l_remove_count
        from (select l.id spl_id from spl l where l.spl_md5 not in
               (select lmd5.spl_md5 from load_md5 lmd5 where lmd5.load_num = p_load_num));

      if p_max_removed > 0 and l_remove_count > p_max_removed then
        raise_application_error(-20101, 'Maximum allowable number ' || p_max_removed || ' of removed spls would be exceeded by this operation.');
      end if;

      for splid_rec in (select l.id spl_id from spl l where l.spl_md5 not in
                         (select lmd5.spl_md5 from load_md5 lmd5 where lmd5.load_num = p_load_num))
      loop

        delete_spl(splid_rec.spl_id, p_include_dgv);

      end loop;

  end remove_spls_not_in_load;

  procedure dubiously_patch_prod_titles is
    begin
      update sum_spl ss set PRODUCT_TITLE=(
        select xt.title
        from
          (
            select spl_id, xmltype(replace(replace(replace(trim(title),'<br/>',' / '),'<br>',' / '),'</br>',' ')) tt
            from dgv_spl_title
          ) l,
              xmltable(
                  xmlnamespaces(default 'urn:hl7-org:v3'),
                  '/title'
                  passing l.tt
                  columns
                    title varchar2(4000)
                    path 'fn:string-join(./title//normalize-space(string()),"  ")'
              ) xt where l.spl_id=ss.spl_id);
  end;

  -- Refresh sum_* tables.   These tables should be mviews but mview refreshes were causing Oracle internal errors (11.2), plus the
  -- refreshes in dbms_mview cannot be done without commiting which is not the desired behavior.
  procedure create_summary_data(p_include_dgv integer) is
  begin

    insert into sum_spl_gen_prod_ndc
      select distinct
        p.spl_id,
        p.normd_generic_name product_normd_generic_name,
        p.ndc_code
      from spl_prod p
      where p.ndc_code is not null and p.normd_generic_name is not null;


    insert into sum_spl_gen_prod_act_ingr_unii
      select distinct
        p.spl_id,
        p.normd_generic_name product_normd_generic_name,
        pi.unii
      from spl_prod p
      join prod_ingr pi on pi.product_id = p.id
      where pi.unii is not null and pi.is_active = 1 and p.normd_generic_name is not null;


    insert into sum_spl_gen_prod_mkt_act
      select distinct
        p.spl_id
       ,p.normd_generic_name product_normd_generic_name
       ,(case when pma.status_code = 'active' then '' else pma.status_code || ':' end ||
         case when pma.low_date is not null then
           substr(pma.low_date,1,4) || '/' || substr(pma.low_date,5,2) || '/' || substr(pma.low_date,7,2)
         else ''
         end ||
         '-' ||
         case when pma.high_date is not null then substr(pma.high_date,1,4) || '/' || substr(pma.high_date,5,2) || '/' || substr(pma.high_date,7,2)
         else ''
         end
        ) marketing_act
      from spl_prod p
      join prod_mkt_act pma on pma.product_id = p.id and p.normd_generic_name is not null;


    insert into sum_spl_gen_prod
      select
         l.id spl_id
        ,p.normd_generic_name product_normd_generic_name
        ,l.normd_org
        ,l.eff_time doc_eff_time
        ,l.version_num
        ,l.archive_dir
        ,l.guid spl_guid
        ,max(l.filename) filename
        ,max(dt.loinc_name) doc_type
        -- ndc's
        ,(select listagg(lgpndc.ndc_code,'; ') within group (order by lgpndc.ndc_code)
          from sum_spl_gen_prod_ndc lgpndc
          where lgpndc.spl_id = l.id and lgpndc.product_normd_generic_name = p.normd_generic_name and rownum <= 100) ndc_codes
        -- active ingredient unii's
        ,(select listagg(lgpai.unii,'; ') within group (order by lgpai.unii)
          from sum_spl_gen_prod_act_ingr_unii lgpai
          where lgpai.spl_id = l.id and lgpai.product_normd_generic_name = p.normd_generic_name and rownum <= 100) act_ingr_uniis
        -- marketing acts
        ,(select listagg(lgpma.marketing_act,'; ') within group (order by lgpma.marketing_act)
          from sum_spl_gen_prod_mkt_act lgpma
          where lgpma.spl_id = l.id and lgpma.product_normd_generic_name = p.normd_generic_name and rownum <= 100) marketing_acts
        -- rank by label doc effective time and version number (1 being latest) among product-in-label records having the same org/product pair.
        ,rank() over (partition by p.normd_generic_name, l.normd_org order by l.eff_time desc, l.version_num desc, l.archive_dir desc) genprodorg_eff_time_rank
        ,rank() over (partition by p.normd_generic_name order by l.eff_time desc, l.archive_dir desc) genprod_eff_time_rank
        ,rank() over (partition by l.normd_org order by l.eff_time desc, l.version_num desc, l.archive_dir desc) org_eff_time_rank
      from spl_prod p
      join spl l on p.spl_id = l.id
      left join document_type dt on l.doc_type_loinc_code = dt.loinc_code
      where p.normd_generic_name is not null
      group by l.id, p.normd_generic_name  -- groups are generic products within labels
              ,l.normd_org, l.guid, l.eff_time, l.version_num, l.archive_dir;  -- these are functionally dependent on l.id so won't really change the groups


    insert into sum_spl_ndc
      select distinct
        p.spl_id,
        p.ndc_code product_ndc_code
      from spl_prod p
      where p.ndc_code is not null;

    insert into sum_spl_prodname
      select distinct
        p.spl_id,
        p.name product_name
      from spl_prod p
      where p.name is not null;

    insert into sum_spl_dosageform
      select distinct
        p.spl_id,
        p.ncit_form_code product_ncit_form_code,
        df.spl_acceptable_term product_dosage_form_term
      from spl_prod p
      left join dosage_form df on p.ncit_form_code = df.nci_thesaurus_code
      where p.ncit_form_code is not null;

    insert into sum_spl_route
      select distinct
        pr.spl_id,
        pr.ncit_route_of_admin_code,
        r.spl_acceptable_term route_spl_acceptable_term
      from prod_route pr
      left join route_of_admin r on pr.ncit_route_of_admin_code = r.nci_thesaurus_code
      where pr.ncit_route_of_admin_code is not null;

    insert into sum_spl_mkt_cat
      select distinct
        a.spl_id,
        a.marketing_cat_ncit_code,
        mc.spl_acceptable_term category_spl_acceptable_term
      from prod_appr a
      left join marketing_category mc on a.marketing_cat_ncit_code = mc.nci_thesaurus_code
      where a.marketing_cat_ncit_code is not null;

    insert into sum_spl_act_ingr_unii
      select distinct
        pi.spl_id,
        pi.unii
      from prod_ingr pi
      where pi.unii is not null and pi.is_active = 1;

    insert into sum_spl_act_ingr_name
      select distinct
        pi.spl_id,
        pi.substance_name
      from prod_ingr pi
      where pi.substance_name is not null and pi.is_active = 1;

    insert into sum_spl_act_moiety_unii
      select distinct
        pi.spl_id,
        xt.*
      from prod_ingr pi,
        xmltable(
        xmlnamespaces(default 'urn:hl7-org:v3'),
        '/activeMoeities/*' passing pi.active_moeities_xml
        columns
          active_moiety_unii varchar2(50)
          path 'code[@codeSystem="2.16.840.1.113883.4.9"]/normalize-space(@code)'
        )xt
      where xt.active_moiety_unii is not null;

    insert into sum_spl_act_moiety_name
      select distinct
      pi.spl_id,
      xt.*
      from prod_ingr pi,
      xmltable(
        xmlnamespaces(default 'urn:hl7-org:v3'),
        '/activeMoeities/*' passing pi.active_moeities_xml
        columns
          active_moiety_name varchar2(500)
          path 'normalize-space(string(name))'
        )xt
      where xt.active_moiety_name is not null;

    insert into sum_spl_mkt_act
      select distinct
        pma.spl_id,
        (case when pma.status_code = 'active' then '' else pma.status_code || ':' end ||
         case when pma.low_date is not null then
           substr(pma.low_date,1,4) ||
           '/' ||
           substr(pma.low_date,5,2) ||
           '/' ||
           substr(pma.low_date,7,2)
         else ''
         end ||
         '-' ||
         case
           when pma.high_date is not null then
             substr(pma.high_date,1,4) ||
             '/' ||
             substr(pma.high_date,5,2) ||
             '/' ||
             substr(pma.high_date,7,2)
         else ''
         end
        ) marketing_act
      from prod_mkt_act pma;

    insert into sum_spl(spl_id,
                        spl_guid,
                        title,
                        spl_md5,
                        document_type_loinc_code,
                        document_type,
                        author_org_normd_name,
                        num_products,
                        num_generic_products,
                        marketing_acts,
                        product_names,
                        product_normd_generic_names,
                        market_categories,
                        routes_of_administration,
                        ndc_codes,
                        dosage_forms,
                        num_act_ingrs,
                        act_ingr_uniis,
                        act_ingr_names,
                        act_moiety_uniis,
                        act_moiety_names,
                        dailymed_ndc_links,
                        archive_dir,
                        filename,
                        eff_time,
                        version_num,
                        archive_dir_date,
                        set_id,
                        org_currency_rank)
      select
         l.id spl_id
        ,l.guid spl_guid
        ,l.title
        ,l.spl_md5
        ,l.doc_type_loinc_code document_type_loinc_code
        ,(select dt.loinc_name from document_type dt where dt.loinc_code = l.doc_type_loinc_code) document_type
        ,l.normd_org author_org_normd_name
        ,(select count(*) from spl_prod p where p.spl_id = l.id) num_products
        ,(select count(*) from sum_spl_gen_prod gp where gp.spl_id = l.id) num_generic_products
        ,(select listagg(lma.marketing_act, '; ') within group (order by lma.marketing_act)
          from sum_spl_mkt_act lma
          where lma.spl_id = l.id and rownum <= 5) marketing_acts
        ,(select listagg(substr(lp.product_name,1,200), '; ') within group (order by lp.product_name)
          from sum_spl_prodname lp where lp.spl_id = l.id and rownum <= 10) product_names
        ,(select listagg(substr(gp.product_normd_generic_name,1,200), '; ') within group (order by gp.product_normd_generic_name)
          from sum_spl_gen_prod gp where gp.spl_id = l.id and rownum <= 10) product_normd_generic_names
        ,(select listagg(substr(mc.category_spl_acceptable_term,1,200), '; ') within group (order by mc.category_spl_acceptable_term)
          from sum_spl_mkt_cat mc where mc.spl_id = l.id and rownum <= 10) market_categories
        ,(select listagg(substr(r.route_spl_acceptable_term,1,200), '; ') within group (order by r.route_spl_acceptable_term)
          from sum_spl_route r where r.spl_id = l.id and rownum <= 20) routes_of_administration
        ,(select listagg(substr(ndc.product_ndc_code,1,20), '; ') within group (order by ndc.product_ndc_code)
          from sum_spl_ndc ndc where ndc.spl_id = l.id and rownum <= 20) ndc_codes
        ,(select listagg(substr(df.product_dosage_form_term,1,200), '; ') within group (order by df.product_dosage_form_term)
          from sum_spl_dosageform df where df.spl_id = l.id and rownum <= 10) dosage_forms
        ,(select count(*) from sum_spl_act_ingr_name ai where ai.spl_id = l.id) num_act_ingrs
        ,(select listagg(substr(ai.unii,1,20), '; ') within group (order by ai.unii)
          from sum_spl_act_ingr_unii ai where ai.spl_id = l.id and rownum <= 20) act_ingr_uniis
        ,(select listagg(substr(ai.substance_name,1,200), '; ') within group (order by ai.substance_name)
          from sum_spl_act_ingr_name ai where ai.spl_id = l.id and rownum <= 10) act_ingr_names
        ,(select listagg(substr(amu.active_moiety_unii,1,20), '; ') within group (order by amu.active_moiety_unii)
          from sum_spl_act_moiety_unii amu where amu.spl_id = l.id and rownum <= 20) act_moiety_uniis
        ,(select listagg(substr(amn.active_moiety_name,1,200), '; ') within group (order by amn.active_moiety_name)
          from sum_spl_act_moiety_name amn where amn.spl_id = l.id and rownum <= 10) act_moiety_names
        ,(select listagg('http://dailymed.nlm.nih.gov/dailymed/lookup.cfm?ndc='||substr(pc.product_ndc_code,1,20), ';') within group (order by pc.product_ndc_code) from sum_spl_ndc pc where pc.spl_id = l.id and rownum <= 5) dailymed_ndc_links
        ,l.archive_dir
        ,l.filename
        ,l.eff_time
        ,l.version_num
        ,substr(l.archive_dir, 1, 8) archive_dir_date
        ,l.set_id
        ,rank() over (partition by l.normd_org order by l.eff_time desc, l.version_num desc, l.archive_dir desc) org_currency_rank
      from spl l
      order by l.id;
      update sum_spl set product_Title=upper(PRODUCT_NAMES)||' - ' ||lower(ACT_INGR_NAMES) ||' ' ||lower(DOSAGE_FORMS);
      update sum_spl set initial_Approval_Year=REGEXP_SUBSTR(case when instr(title,'Approval')>0 then substr(title,instr(title,'Approval')+9, 17) else '' end,'\d{4}');
      update sum_spl set revised_date=
        case when ( instr(title,'Issued')>0 or instr(title,'Revised')>0 ) then
          case when instr(title,'January')>0 then substr(title,instr(title,'January'), 12)
             when instr(title,'February')>0 then substr(title,instr(title,'February'), 13)
             when instr(title,'March')>0 then substr(title,instr(title,'March'), 10)
             when instr(title,'April')>0 then substr(title,instr(title,'April'), 10)
             when instr(title,'May')>0 then substr(title,instr(title,'May'), 8)
             when instr(title,'June')>0 then substr(title,instr(title,'June'), 9)
             when instr(title,'July')>0 then substr(title,instr(title,'July'), 9)
             when instr(title,'August')>0 then substr(title,instr(title,'August'), 11)
             when instr(title,'September')>0 then substr(title,instr(title,'September'), 14)
             when instr(title,'October')>0 then substr(title,instr(title,'October'), 12)
             when instr(title,'November')>0 then substr(title,instr(title,'November'),13)
             when instr(title,'December')>0 then substr(title,instr(title,'December'), 13)
            else '' end
        else '' end ;

    update sum_spl set REVISED_DATE= REGEXP_REPLACE(to_char(to_date(substr(eff_time,1,6),'yyyymm'),'Month yyyy'),'( ){2,}', ' ')
      where REVISED_DATE is null or length(trim(REVISED_DATE))=0;

    insert into spl_section_count
      select count(distinct spl_id) count, loinc_code
      from spl_sec
      group by loinc_code;

    insert into document_type_count
      select document_type_loinc_code code, count(distinct spl_id) count from sum_spl
      where document_type_loinc_code is not null
      group by document_type_loinc_code ;

    insert into marketing_cat_count
      select marketing_cat_ncit_code code, count(distinct spl_id) count
      from sum_spl_mkt_cat
      where marketing_cat_ncit_code is not null
      group by marketing_cat_ncit_code;

    insert into medrt_concept_spl_count
      select pi.nui, count(distinct pm.spl_setid)
      from pharm_index pi
      join medrt_concept c on pi.nui = c.nui
      join pharma_mapping pm on pm.pharma_setid = pi.pharma_setid
      join sum_spl s on s.set_id = pm.spl_setid
      group by pi.nui;

    insert into sum_spl_epc
    select
      s.spl_id,
      s.set_id,
      (  select listagg(pharma_name, '; ') within group (order by pharma_name)
         from
           ( select distinct pm.spl_setid, c.name pharma_name
             from pharma_mapping pm
             join pharm_index pi on pi.pharma_setid = pm.pharma_setid
             join medrt_concept c on c.nui = pi.nui
             where c.tlevel = '[EPC]'
           ) ph
         where ph.spl_setid = s.set_id
      ) epc
    from  sum_spl s;

    -- TODO: These two fields should have just been set correctly in the initial insert into sum_spl.
    update sum_spl s
      set
        epc=(select epc from sum_spl_epc se where s.spl_id=se.spl_id),
        initial_Approval_Year=REGEXP_SUBSTR(case when instr(lower(PRODUCT_TITLE),'approval')>0 then substr(PRODUCT_TITLE,instr(lower(PRODUCT_TITLE),'approval')+9, 17) else '' end,'\d{4}');

	  if p_include_dgv = 1 then
	    dgv.create_summary_data();
      dubiously_patch_prod_titles;
	  end if;

  end create_summary_data;

  procedure write_record_counts(p_load_num integer) is
    l_count integer;
  begin

    for uobj in (select distinct object_name from user_objects
                 where (object_type = 'TABLE' or object_type = 'VIEW' or object_type = 'MATERIALIZED VIEW')
                   and secondary = 'N')
    loop

      if uobj.object_name <> 'LOAD_RECORD_COUNT' then

        execute immediate 'select count(*) from ' || uobj.object_name into l_count;

        insert into load_record_count(load_num, object_name, count_type, record_count, count_ts)
          values(p_load_num, uobj.object_name, 'total', l_count, systimestamp);

      end if;

    end loop;

    -- newly added spls, which do not have set ids matching that of any others in the system whether archived or current
    insert into load_record_count(load_num, object_name, count_type, record_count, count_ts)
        values(p_load_num,
               'SPL',
               'new',
               (select count(*)
                from spl l
                where l.created_load_num = p_load_num
                  and not exists (select 1 from archived_spl al where al.set_id = l.set_id)
                  and not exists (select 1 from spl dl where dl.id <> l.id and dl.set_id = l.set_id)),
               systimestamp);

    -- normal updated spls, previous versions of which were present prior to the current load, and which have no current duplicates
    insert into load_record_count(load_num, object_name, count_type, record_count, count_ts)
        values(p_load_num,
               'SPL',
               'updated',
               (select count(*)
                from spl l
                where l.created_load_num = p_load_num
                  and exists (select 1 from archived_spl al where al.set_id = l.set_id and al.archived_load_num = p_load_num)
                  and not exists (select 1 from spl dl where dl.id <> l.id and dl.set_id = l.set_id)),
               systimestamp);

    -- "resurrected" spls - spls which are updates of versions which had been retired prior to this load, and which have no current duplicates
    insert into load_record_count(load_num, object_name, count_type, record_count, count_ts)
        values(p_load_num,
               'SPL',
               'resurrected',
               (select count(*) from spl l
                where l.created_load_num = p_load_num
                  and p_load_num > (select max(al.archived_load_num) from archived_spl al where al.set_id = l.set_id)
                  and not exists (select 1 from spl dl where dl.id <> l.id and dl.set_id = l.set_id)),
               systimestamp);


    -- removed spls, which have a version removed in this load but no current version
    insert into load_record_count(load_num, object_name, count_type, record_count, count_ts)
        values(p_load_num,
               'SPL',
               'removed',
               (select count(*)
                from archived_spl al
                where al.archived_load_num = p_load_num
                  and not exists (select 1 from spl l where l.set_id = al.set_id)),
               systimestamp);

     -- duplicated spls - spls added during this load for which another version is also current
     insert into load_record_count(load_num, object_name, count_type, record_count, count_ts)
        values(p_load_num,
               'SPL',
               'duplicated',
               (select count(*) from (
                  select l.id from spl l
                  where l.created_load_num = p_load_num
                    and l.set_id in (
                      select lg.set_id from spl lg
                      group by lg.set_id
                      having count(*) > 1))),
               systimestamp);

  end write_record_counts;

  -- Sync context indexes via ctx_dll.sync_index. NOTE: Each call to sync_index() causes a COMMIT.
  procedure sync_context_indexes is
  begin
    for ctxix in (select index_name from user_indexes where ityp_owner='CTXSYS' and ityp_name = 'CONTEXT')
    loop
      ctx_ddl.sync_index(ctxix.index_name, '500M');
    end loop;
  end sync_context_indexes;


  procedure optimize_context_indexes is
  begin
    for ctxix in (select index_name from user_indexes where ityp_owner='CTXSYS' and ityp_name = 'CONTEXT')
    loop
      ctx_ddl.optimize_index(ctxix.index_name, 'FULL');
    end loop;
  end optimize_context_indexes;


  procedure generate_stats is
  begin
    dbms_stats.gather_schema_stats('DRUGLABEL',
                                    method_opt => 'FOR ALL COLUMNS SIZE 1',
                                    options => 'GATHER',
                                    estimate_percent => dbms_stats.auto_sample_size,
                                    cascade => true);
    -- NOTE: The above generates no histograms that can be used by the CBO.
    --       Add histogram generation here on specific columns as needed.
  end generate_stats;

  procedure refresh_stats is
  begin
    dbms_stats.gather_schema_stats('DRUGLABEL',
                                    method_opt => 'FOR COLUMNS REPEAT',
                                    options => 'GATHER',
                                    estimate_percent => dbms_stats.auto_sample_size,
                                    cascade => true);
  end refresh_stats;


end spl_load_2_0;
/
