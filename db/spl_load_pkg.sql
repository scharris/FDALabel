create or replace package spl_load as

  procedure insert_spl_and_deps(p_spl_xml_txt clob,
                                p_spl_html clob,
                                p_spl_entry_dir varchar2,
                                p_spl_xml_file_name varchar2,
                                p_spl_xml_md5 varchar2,
                                p_load_num integer,
                                po_spl_id out integer);

  procedure insert_spl(p_spl_xml xmltype,
                       p_spl_entry_dir varchar2,
                       p_spl_xml_file_name varchar2,
                       p_spl_xml_md5 varchar2,
                       p_load_num integer,
                       po_spl_id out integer);


  procedure insert_spl_att(p_spl_id integer,
                           p_att_name varchar2,
                           p_att_data blob);

  procedure delete_summary_data;

  procedure delete_new_summary_data;

  procedure archive_spl(p_spl_id integer, p_load_num integer);

  procedure delete_spl(p_spl_id integer);

  procedure archive_spls_not_in_load(p_load_num integer, p_max_archived integer default -1);

  procedure remove_spls_not_in_load(p_load_num integer, p_max_removed integer default -1);

  procedure create_summary_data;

  procedure create_new_summary_data;

  procedure write_record_counts(p_load_num integer);


  procedure sync_context_indexes;

  procedure optimize_context_indexes;


  procedure generate_stats;

  procedure refresh_stats;

end spl_load;
