create or replace package spl_load_2_0 as

  procedure insert_spl_and_deps(p_spl_xml_txt clob,
                                p_spl_html clob,
                                p_spl_entry_dir varchar2,
                                p_spl_xml_file_name varchar2,
                                p_spl_xml_md5 varchar2,
                                p_load_num integer,
                                p_include_dgv integer,
                                po_spl_id out integer);

  procedure delete_summary_data(p_include_dgv integer);

  procedure create_summary_data(p_include_dgv integer);

  procedure archive_spls_not_in_load(p_load_num integer, p_include_dgv integer, p_max_archived integer default -1);

  procedure remove_spls_not_in_load(p_load_num integer, p_include_dgv integer, p_max_removed integer default -1);

  procedure sync_context_indexes;

  procedure write_record_counts(p_load_num integer);

  procedure optimize_context_indexes;

  procedure generate_stats;

  procedure refresh_stats;

end spl_load_2_0;
