create or replace package body dgv as

  procedure insert_spl_prod_deps(p_spl_id integer) is
  begin

	  insert into dgv_spl_sec_highlights
      select
        l.id spl_id,
        xt.*
      from
        spl l,
        xmltable(
          xmlnamespaces(default 'urn:hl7-org:v3'),
          '/document/component/structuredBody/component/section'
          passing l.spl_xml
          columns
            section_loinc_code VARCHAR2(80)
              path './code/normalize-space(@code)',
            highlight_content xmltype
              path '<highlight-content>{./excerpt/highlight/text}</highlight-content>' ,
             highlight_text VARCHAR2(4000)
              path './excerpt/highlight/text'
        ) xt where l.id = p_spl_id;

    delete from dgv_spl_sec_highlights where highlight_text is null and spl_id = p_spl_id;

    insert into dgv_spl_title
      select
        l.id spl_id,
        xt.*
      from
        spl l,
        xmltable(
          xmlnamespaces(default 'urn:hl7-org:v3'),
          '/document/title'
          passing l.spl_xml
          columns
            title  xmltype
              path '<title>{.}</title>'
        ) xt  where l.id = p_spl_id;

    insert into dgv_spl_prod
      select
        s_dgv_spl_prod.nextval id,
        l.id spl_id,
        trim(upper(xt.generic_name)) normd_generic_name,
        xt.*
      from
        spl l,
        xmltable(xmlnamespaces(default 'urn:hl7-org:v3'),
          '/document/component/structuredBody/component/section/subject/manufacturedProduct[manufacturedMedicine|manufacturedProduct]' passing l.spl_xml
          columns
            name varchar2(500)
              path './(manufacturedMedicine|manufacturedProduct)/normalize-space(string-join(name//text()," "))'
           ,generic_name varchar2(500)
              path './(manufacturedMedicine|manufacturedProduct)/asEntityWithGeneric/genericMedicine/normalize-space(string-join(name//text()," "))'
            ,ndc_code varchar2(50)
              path './(manufacturedMedicine|manufacturedProduct)/code[@codeSystem="2.16.840.1.113883.6.69" or not(@codeSystem)]/normalize-space(@code)'
           ,ncit_form_code varchar2(6)
              path './(manufacturedMedicine|manufacturedProduct)/formCode[@codeSystem="2.16.840.1.113883.3.26.1.1" or not(@codeSystem)]/normalize-space(@code)'
           ,ncit_form_name varchar2(200)
              path './(manufacturedMedicine|manufacturedProduct)/formCode[@codeSystem="2.16.840.1.113883.3.26.1.1" or not(@codeSystem)]/@displayName'
            ,equiv_to_ndc_code varchar2(50)
              path './(manufacturedMedicine|manufacturedProduct)/asEquivalentEntity/definingMaterialKind/code[@codeSystem="2.16.840.1.113883.6.69" or not(@codeSystem)]/normalize-space(@code)'
		     	,prod_xml xmltype
          path '<prod-list>{.}</prod-list>'
        ) xt
      where l.id = p_spl_id;

	  update dgv_spl_prod set EQUIV_TO_NDC_CODE=regexp_substr(EQUIV_TO_NDC_CODE, '[^-]+', 1, 1) ||'-'||regexp_substr(EQUIV_TO_NDC_CODE, '[^-]+', 1, 2)
      where EQUIV_TO_NDC_CODE is not null and spl_id = p_spl_id;

    update dgv_spl_prod set NDC_CODE=regexp_substr(NDC_CODE, '[^-]+', 1, 1) ||'-'||regexp_substr(NDC_CODE, '[^-]+', 1, 2)
     where NDC_CODE is not null and spl_id = p_spl_id;

    insert into dgv_prod_route
      select distinct /*+ NO_XML_QUERY_REWRITE */
        p.spl_id,
        p.id product_id,
        xt.*
      from
        dgv_spl_prod p,
        xmltable(xmlnamespaces(default 'urn:hl7-org:v3'),
          'prod-list/manufacturedProduct/consumedIn/substanceAdministration' passing p.prod_xml
          columns
            ncit_route_of_admin_code varchar2(6)
              /* This fn:substring call should not be necessary, there seems to be an Oracle bug triggering ORA-00932 (inconsistent datatypes)
                for no valid reason without it though (even for a single test row with a code of length 6). Removing the bracketed condition
                avoids the error but would potentially insert codes of the wrong type. */
              path 'fn:substring(routeCode[@codeSystem="2.16.840.1.113883.3.26.1.1" or not(@codeSystem)]/@code,1,6)'
              --original: path 'routeCode[@codeSystem="2.16.840.1.113883.3.26.1.1" or not(@codeSystem)]/@code'
        ) xt
		  where p.spl_id = p_spl_id;

    insert into dgv_prod_ingr
    select /*+ NO_XML_QUERY_REWRITE */
      s_dgv_prod_ingr.nextval id,
      p.spl_id,
      p.id product_id,
      xt.*
    from
      dgv_spl_prod p,
      xmltable(
         xmlnamespaces(default 'urn:hl7-org:v3'),
         'for $ingr in /prod-list/manufacturedProduct/(manufacturedMedicine|manufacturedProduct)//(activeIngredient|ingredient)
           return $ingr' passing p.prod_xml
         columns
           -- substance identification fields
           substance_name varchar2(500)
             path './(activeIngredientSubstance|ingredientSubstance)[1]/normalize-space(string(name))' -- "name" element can have mixed content but seems to not have in practice.
          ,unii varchar2(50)
             path './(activeIngredientSubstance|ingredientSubstance)[1]/code[@codeSystem="2.16.840.1.113883.4.9"]/normalize-space(@code)'
          ,is_active int
             path 'if ( local-name(.) = "activeIngredient" or starts-with(@classCode, "ACTI") ) then 1 else 0'
          ,active_activeMoiety_name varchar2(500)
             path './(activeIngredientSubstance|ingredientSubstance)/activeMoiety[1]/activeMoiety/normalize-space(string(name))'
           ,active_activeMoiety_unii varchar2(50)
             path './(activeIngredientSubstance|ingredientSubstance)/activeMoiety[1]/activeMoiety/code[@codeSystem="2.16.840.1.113883.4.9"]/normalize-space(@code)'
          ,basis_of_strength_code varchar2(1)
             path 'fn:substring(@classCode,4,1)' -- B=basis is active ingredient itself, R=basis is reference drug, M=basis is active moeity
           -- quantity fields
          ,qty_numer_val varchar2(50)
             path './quantity/numerator[1]/@value'
          ,qty_numer_unit varchar2(200)
             path './quantity/numerator[1]/@unit'
          ,qty_numer_ncit_units_val number
             path './quantity/numerator/translation[@codeSystem="2.16.840.1.113883.3.26.1.1"][1]/@value'
          ,qty_numer_ncit_unit_name varchar2(200)
             path './quantity/numerator/translation[@codeSystem="2.16.840.1.113883.3.26.1.1"][1]/@displayName'
          ,qty_numer_ncit_unit_code varchar2(50)
             path './quantity/numerator/translation[@codeSystem="2.16.840.1.113883.3.26.1.1"][1]/@code'
          ,qty_denom_val varchar2(50)
             path './quantity/denominator[1]/@value'
          ,qty_denom_unit varchar2(200)
             path './quantity/denominator[1]/@unit'
          ,qty_denom_ncit_units_val number
             path './quantity/denominator/translation[@codeSystem="2.16.840.1.113883.3.26.1.1"][1]/@value'
          ,qty_denom_ncit_unit_name varchar2(200)
             path './quantity/denominator/translation[@codeSystem="2.16.840.1.113883.3.26.1.1"][1]/@displayName'
          ,qty_denom_ncit_unit_code varchar2(50)
             path './quantity/denominator/translation[@codeSystem="2.16.840.1.113883.3.26.1.1"][1]/@code'
          ,gen_mat_kinds_xml xmltype
             path '<generalized-material-kinds>{(activeIngredientSubstance|ingredientSubstance)/asSpecializedKind/(generalizedMaterialKind|generalizedPharmaceuticalClass)}</generalized-material-kinds>'
      ) xt  where p.spl_id = p_spl_id;

    update dgv_prod_ingr
      set ACTIVE_ACTIVEMOIETY_NAME=lower(ACTIVE_ACTIVEMOIETY_NAME), SUBSTANCE_NAME=lower(SUBSTANCE_NAME)
      where spl_id = p_spl_id;

    insert into dgv_prod_appr
      select
        s_dgv_prod_appr.nextval id,
        p.id product_id,
        p.spl_id,
        xt.*
      from
        dgv_spl_prod p,
        xmltable(
          xmlnamespaces(default 'urn:hl7-org:v3'),
          ' /prod-list/manufacturedProduct/subjectOf/approval' passing p.prod_xml
          columns
            approval_num varchar2(200)
              path 'id[@root="2.16.840.1.113883.3.150"]/@extension'
           ,marketing_cat_ncit_code varchar2(10)
              path 'code[@codeSystem="2.16.840.1.113883.3.26.1.1" or not(@codeSystem)]/@code'
           ,marketing_cat_description varchar2(200)
              path 'code/@displayName'
           ,status_code varchar2(10)
               path 'statusCode/@code' -- no system
           -- ignoring "holder"
           ,date_granted varchar2(50)
               path 'author/time/@value'
           ,territory_country_code varchar2(20)
               path 'author/territorialAuthority/territory/code[@codeSystem="2.16.840.1.113883.5.28" or not(@codeSystem)]/@code'
           ,territory_country_name varchar2(200)
              path 'author/territorialAuthority/territory/name'
           ,agency_name varchar2(200)
               path 'author/territorialAuthority/governingAgency/name'
        ) xt where p.spl_id = p_spl_id;

	  insert into dgv_prod_deapolicy
      select
        p.id product_id,
        p.spl_id,
        xt.*
      from
        dgv_spl_prod p,
        xmltable(
          xmlnamespaces(default 'urn:hl7-org:v3'),
          ' /prod-list/manufacturedProduct/subjectOf/policy' passing p.prod_xml
          columns
            policy_class varchar2(100)
              path '@classCode'
           ,policy_code varchar2(10)
              path 'code[@codeSystem="2.16.840.1.113883.3.26.1.1" or not(@codeSystem)]/@code'
           ,policy_name varchar2(200)
              path 'code/@displayName'

        ) xt where p.spl_id = p_spl_id;

    insert into dgv_prod_mkt_act
      select /*+ NO_XML_QUERY_REWRITE */
        s_dgv_prod_mkt_act.nextval id,
        p.id product_id,
        p.spl_id,
        xt.*
      from
        dgv_spl_prod p,
        xmltable(
          xmlnamespaces(default 'urn:hl7-org:v3'),
          ' /prod-list/manufacturedProduct/subjectOf/marketingAct' passing p.prod_xml
          columns
            act_ncit_code varchar2(10)
              path 'code[@codeSystem="2.16.840.1.113883.3.26.1.1" or not(@codeSystem)]/@code'
           ,status_code varchar2(10)
              path 'statusCode/@code' -- no system
           ,low_date varchar2(50)
              path 'effectiveTime/low/@value'
           ,low_date_inclusive varchar2(5)
              path 'effectiveTime/low/@inclusive'
           ,high_date varchar2(50)
              path 'effectiveTime/high/@value'
           ,high_date_inclusive varchar2(5)
              path 'effectiveTime/high/@inclusive'
           ,center_date varchar2(50)
              path 'effectiveTime/center/@value'
           ,date_width varchar2(50)
              path 'effectiveTime/width/@value'
           ,date_width_unit varchar2(50)
              path 'effectiveTime/width/@unit'
        ) xt   where p.spl_id = p_spl_id;

  end insert_spl_prod_deps;

  procedure delete_summary_data is
    begin
      delete from dgv_sum_spl_prod_ndc;
      delete from dgv_sum_spl_prod_act_ingr_unii;
      delete from dgv_sum_spl_prod_mkt_act;
      delete from dgv_sum_spl_prod;
      delete from dgv_sum_spl_ndc;
      delete from dgv_sum_spl_prodname;
      delete from dgv_sum_spl_dosageform;
      delete from dgv_sum_spl_route;
      delete from dgv_sum_spl_mkt_cat;
      delete from dgv_sum_spl_act_ingr_unii;
      delete from dgv_sum_spl_act_ingr_name;
      delete from dgv_sum_spl_mkt_act;
      delete from dgv_sum_spl;
      delete from dgv_sum_spl_epc;
      delete from dgv_sum_rx_spl ;
      delete from dgv_format_document_type_count ;
      delete from dgv_spl_sect_count;
      delete from dgv_marketing_cat_count;
      delete from dgv_medrt_concept_spl_count;
    end delete_summary_data;

  -- Sync context indexes via ctx_dll.sync_index. NOTE: Each call to sync_index() causes a COMMIT.
  procedure sync_context_indexes is
    begin
      for ctxix in (select index_name from user_indexes where ityp_owner='CTXSYS' and ityp_name = 'CONTEXT')
      loop
        ctx_ddl.sync_index(ctxix.index_name, '500M');
      end loop;
  end sync_context_indexes;

  procedure update_section_counts is
    begin
      insert into dgv_spl_sect_count
        select rx.FORMAT_GROUP groupno,ss.LOINC_CODE sect_code, count(distinct ss.spl_id ) counts from spl_sec ss
          join dgv_sum_rx_spl rx on rx.spl_id=ss.spl_id
        group by rx.FORMAT_GROUP,ss.LOINC_CODE ;

      insert into dgv_spl_sect_count
        select 0,'99999-1', count(distinct spl_id ) counts from dgv_sum_rx_spl where initial_Approval_Year is not null and FORMAT_GROUP=1;

      insert into dgv_spl_sect_count
        select 0,'99999-2', count(distinct spl_id ) counts from dgv_sum_rx_spl where PRODUCT_TITLE is not null and FORMAT_GROUP=1;

      insert into dgv_spl_sect_count
        select 3,'otc99-01',count(distinct spl_id )  from spl_sec ss
        where loinc_code='34071-1' and (contains(CONTENT_XML,'External use')>0 or contains(CONTENT_XML,'Rectal use')>0 or contains(CONTENT_XML,'Vaginal use')>0) and  spl_id in (select spl_id from dgv_sum_rx_spl where FORMAT_GROUP=3);

      insert into dgv_spl_sect_count
        select 3,'otc99-02',count(distinct spl_id )  from spl_sec ss
        where loinc_code='34071-1' and contains(CONTENT_XML,'Reye''s Syndrome')>0 and  spl_id in (select spl_id from dgv_sum_rx_spl where FORMAT_GROUP=3);

      insert into dgv_spl_sect_count
        select 3,'otc99-03',count(distinct spl_id )  from spl_sec ss
        where loinc_code='34071-1' and (contains(CONTENT_XML,'Allergy Alert')>0 ) and  spl_id in (select spl_id from dgv_sum_rx_spl where FORMAT_GROUP=3);

      insert into dgv_spl_sect_count
        select 3,'otc99-04',count(distinct spl_id )  from spl_sec ss
        where loinc_code='34071-1' and (contains(CONTENT_XML,'Flammability Warning')>0 ) and  spl_id in (select spl_id from dgv_sum_rx_spl where FORMAT_GROUP=3);

      insert into dgv_spl_sect_count
        select 3,'otc99-05',count(distinct spl_id )  from spl_sec ss
        where loinc_code='34071-1' and (contains(CONTENT_XML,'Choking')>0 ) and  spl_id in (select spl_id from dgv_sum_rx_spl where FORMAT_GROUP=3);

      insert into dgv_spl_sect_count
        select 3,'otc99-06',count(distinct spl_id )  from spl_sec ss
        where loinc_code='34071-1' and (contains(CONTENT_XML,'Alcohol Warning')>0 ) and  spl_id in (select spl_id from dgv_sum_rx_spl where FORMAT_GROUP=3);

      insert into dgv_spl_sect_count
        select 3,'otc99-07',count(distinct spl_id )  from spl_sec ss
        where loinc_code='34071-1' and (contains(CONTENT_XML,'Sore Throat Warning')>0 )  and  spl_id in (select spl_id from dgv_sum_rx_spl where FORMAT_GROUP=3);

      insert into dgv_spl_sect_count
        select 3,'otc99-08',count(distinct spl_id )  from spl_sec ss
        where loinc_code='34071-1' and (contains(CONTENT_XML,'Dosage Warning')>0 ) and  spl_id in (select spl_id from dgv_sum_rx_spl where FORMAT_GROUP=3);

      insert into dgv_spl_sect_count
        select 3,'otc99-11',count(distinct spl_id )  from spl_sec ss
        where loinc_code='34071-1' and (contains(CONTENT_XML,'Asthma Alert')>0 ) and  spl_id in (select spl_id from dgv_sum_rx_spl where FORMAT_GROUP=3);

      insert into dgv_spl_sect_count
        select 3,'otc99-12',count(distinct spl_id )  from spl_sec ss
        where loinc_code='34071-1' and (contains(CONTENT_XML,'Liver Warning')>0 ) and  spl_id in (select spl_id from dgv_sum_rx_spl where FORMAT_GROUP=3);

      insert into dgv_spl_sect_count
        select 3,'otc99-13',count(distinct spl_id )  from spl_sec ss
        where loinc_code='34071-1' and (contains(CONTENT_XML,'Stomach Bleeding Warning')>0 ) and  spl_id in (select spl_id from dgv_sum_rx_spl where FORMAT_GROUP=3);
  end;

  procedure create_summary_data is
    begin
      insert into dgv_sum_spl_prod_ndc
        select distinct
          p.spl_id,
          p.normd_generic_name product_normd_generic_name,
          p.ndc_code
        from dgv_spl_prod p
        where p.ndc_code is not null and p.normd_generic_name is not null;

      insert into dgv_sum_spl_prod_act_ingr_unii
        select distinct
          p.spl_id,
          p.normd_generic_name product_normd_generic_name,
          pi.unii
        from dgv_spl_prod p
          join dgv_prod_ingr pi on pi.product_id = p.id
        where pi.unii is not null and pi.is_active = 1 and p.normd_generic_name is not null;

      insert into dgv_sum_spl_prod_mkt_act
        select distinct
          p.spl_id
          ,p.normd_generic_name product_normd_generic_name
          ,(case when pma.status_code = 'active' then '' else pma.status_code || ':' end ||
            case when pma.low_date is not null then
              substr(pma.low_date,1,4) || '/' || substr(pma.low_date,5,2) || '/' || substr(pma.low_date,7,2)
            else ''
            end ||
            '-' ||
            case when pma.high_date is not null then substr(pma.high_date,1,4) || '/' || substr(pma.high_date,5,2) || '/' || substr(pma.high_date,7,2)
            else ''
            end
           ) marketing_act
        from dgv_spl_prod p
          join dgv_prod_mkt_act pma on pma.product_id = p.id and p.normd_generic_name is not null;

      insert into dgv_sum_spl_prod
        select
           l.id spl_id
          ,p.normd_generic_name product_normd_generic_name
          ,l.normd_org
          ,l.eff_time doc_eff_time
          ,l.version_num
          ,l.archive_dir
          ,l.guid spl_guid
          ,max(l.filename) filename
          ,max(dt.loinc_name) doc_type
          -- TODO: list of specific product names for this generic product - add subquery on new table sum_spl_genprod_prodname (TBD)
          -- ndc's
          ,(select listagg(lgpndc.ndc_code,'; ') within group (order by lgpndc.ndc_code)
            from sum_spl_gen_prod_ndc lgpndc
            where lgpndc.spl_id = l.id and lgpndc.product_normd_generic_name = p.normd_generic_name and rownum <= 100) ndc_codes
          -- active ingredient unii's
          ,(select listagg(lgpai.unii,'; ') within group (order by lgpai.unii)
            from sum_spl_gen_prod_act_ingr_unii lgpai
            where lgpai.spl_id = l.id and lgpai.product_normd_generic_name = p.normd_generic_name and rownum <= 100) act_ingr_uniis
          -- marketing acts
          ,(select listagg(lgpma.marketing_act,'; ') within group (order by lgpma.marketing_act)
            from sum_spl_gen_prod_mkt_act lgpma
            where lgpma.spl_id = l.id and lgpma.product_normd_generic_name = p.normd_generic_name and rownum <= 100) marketing_acts
          -- rank by label doc effective time and version number (1 being latest) among product-in-label records having the same org/product pair.
          ,rank() over (partition by p.normd_generic_name, l.normd_org order by l.eff_time desc, l.version_num desc, l.archive_dir desc) genprodorg_eff_time_rank
          ,rank() over (partition by p.normd_generic_name order by l.eff_time desc, l.archive_dir desc) genprod_eff_time_rank
          ,rank() over (partition by l.normd_org order by l.eff_time desc, l.version_num desc, l.archive_dir desc) org_eff_time_rank
        from dgv_spl_prod p
          join spl l on p.spl_id = l.id
          left join document_type dt on l.doc_type_loinc_code = dt.loinc_code
        where p.normd_generic_name is not null
        group by l.id, p.normd_generic_name  -- groups are generic products within labels
          ,l.normd_org, l.guid, l.eff_time, l.version_num, l.archive_dir;  -- these are functionally dependent on l.id so won't really change the groups

      insert into dgv_sum_spl_ndc
        select distinct
          p.spl_id,
          p.ndc_code product_ndc_code
        from dgv_spl_prod p
        where p.ndc_code is not null;

      insert into dgv_sum_spl_prodname
        select distinct
          p.spl_id,
          p.name product_name
        from dgv_spl_prod p
        where p.name is not null;

      insert into dgv_sum_spl_dosageform
        select distinct
          p.spl_id,
          p.ncit_form_code product_ncit_form_code,
          df.spl_acceptable_term product_dosage_form_term
        from dgv_spl_prod p
          left join dosage_form df on p.ncit_form_code = df.nci_thesaurus_code
        where p.ncit_form_code is not null;

      insert into dgv_sum_spl_route
        select distinct
          pr.spl_id,
          pr.ncit_route_of_admin_code,
          r.spl_acceptable_term route_spl_acceptable_term
        from dgv_prod_route pr
          left join route_of_admin r on pr.ncit_route_of_admin_code = r.nci_thesaurus_code
        where pr.ncit_route_of_admin_code is not null;

      insert into dgv_sum_spl_mkt_cat
        select distinct
          a.spl_id,
          a.marketing_cat_ncit_code,
          mc.spl_acceptable_term category_spl_acceptable_term
        from dgv_prod_appr a
          left join marketing_category mc on a.marketing_cat_ncit_code = mc.nci_thesaurus_code
        where a.marketing_cat_ncit_code is not null;

      insert into dgv_sum_spl_act_ingr_unii
        select distinct
          pi.spl_id,
          pi.unii
        from dgv_prod_ingr pi
        where pi.unii is not null and pi.is_active = 1;

      insert into dgv_sum_spl_act_ingr_name
        select distinct
          pi.spl_id,
          pi.substance_name
        from dgv_prod_ingr pi
        where pi.substance_name is not null and pi.is_active = 1;

      insert into dgv_sum_spl_mkt_act
        select distinct
          pma.spl_id,
          (case when pma.status_code = 'active' then '' else pma.status_code || ':' end ||
           case when pma.low_date is not null then
             substr(pma.low_date,1,4) ||
             '/' ||
             substr(pma.low_date,5,2) ||
             '/' ||
             substr(pma.low_date,7,2)
           else ''
           end ||
           '-' ||
           case
           when pma.high_date is not null then
             substr(pma.high_date,1,4) ||
             '/' ||
             substr(pma.high_date,5,2) ||
             '/' ||
             substr(pma.high_date,7,2)
           else ''
           end
          ) marketing_act
        from dgv_prod_mkt_act pma;

      insert into dgv_sum_spl(spl_id,
                              spl_guid,
                              title,
                              spl_md5,
                              document_type_loinc_code,
                              document_type,
                              author_org_normd_name,
                              num_products,
                              num_generic_products,
                              marketing_acts,
                              product_names,
                              product_normd_generic_names,
                              market_categories,
                              routes_of_administration,
                              ndc_codes,
                              dosage_forms,
                              num_act_ingrs,
                              act_ingr_uniis,
                              act_ingr_names,
                              dailymed_ndc_links,
                              archive_dir,
                              filename,
                              eff_time,
                              version_num,
                              archive_dir_date,
                              set_id,
                              org_currency_rank)
        select
           l.id spl_id
          ,l.guid spl_guid
          ,l.title
          ,l.spl_md5
          ,l.doc_type_loinc_code document_type_loinc_code
          ,(select dt.loinc_name from document_type dt where dt.loinc_code = l.doc_type_loinc_code) document_type
          ,l.normd_org author_org_normd_name
          ,(select count(*) from dgv_spl_prod p where p.spl_id = l.id) num_products
          ,(select count(*) from dgv_sum_spl_prod gp where gp.spl_id = l.id) num_generic_products
          ,(select listagg(lma.marketing_act, '; ') within group (order by lma.marketing_act)
            from dgv_sum_spl_mkt_act lma
            where lma.spl_id = l.id and rownum <= 5) marketing_acts
          ,(select listagg(substr(lp.product_name,1,200), '; ') within group (order by lp.product_name)
            from dgv_sum_spl_prodname lp where lp.spl_id = l.id and rownum <= 10) product_names
          ,(select listagg(substr(gp.product_normd_generic_name,1,200), '; ') within group (order by gp.product_normd_generic_name)
            from dgv_sum_spl_prod gp where gp.spl_id = l.id and rownum <= 10) product_normd_generic_names
          ,(select listagg(substr(mc.category_spl_acceptable_term,1,200), '; ') within group (order by mc.category_spl_acceptable_term)
            from dgv_sum_spl_mkt_cat mc where mc.spl_id = l.id and rownum <= 10) market_categories
          ,(select listagg(substr(r.route_spl_acceptable_term,1,200), '; ') within group (order by r.route_spl_acceptable_term)
            from dgv_sum_spl_route r where r.spl_id = l.id and rownum <= 20) routes_of_administration
          ,(select listagg(substr(ndc.product_ndc_code,1,20), '; ') within group (order by ndc.product_ndc_code)
            from dgv_sum_spl_ndc ndc where ndc.spl_id = l.id and rownum <= 20) ndc_codes
          ,(select listagg(substr(df.product_dosage_form_term,1,200), '; ') within group (order by df.product_dosage_form_term)
            from dgv_sum_spl_dosageform df where df.spl_id = l.id and rownum <= 10) dosage_forms
          ,(select count(*) from sum_spl_act_ingr_name ai where ai.spl_id = l.id) num_act_ingrs
          ,(select listagg(substr(ai.unii,1,20), '; ') within group (order by ai.unii)
            from dgv_sum_spl_act_ingr_unii ai where ai.spl_id = l.id and rownum <= 20) act_ingr_uniis
          ,(select listagg(substr(ai.substance_name,1,200), '; ') within group (order by ai.substance_name)
            from dgv_sum_spl_act_ingr_name ai where ai.spl_id = l.id and rownum <= 10) act_ingr_names
          ,(select listagg('http://dailymed.nlm.nih.gov/dailymed/lookup.cfm?ndc='||substr(pc.product_ndc_code,1,20), '; ') within group (order by pc.product_ndc_code)
            from dgv_sum_spl_ndc pc where pc.spl_id = l.id and rownum <= 5) dailymed_ndc_links
          ,l.archive_dir
          ,l.filename
          ,l.eff_time
          ,l.version_num
          ,substr(l.archive_dir, 1, 8) archive_dir_date
          ,l.set_id
          ,rank() over (partition by l.normd_org order by l.eff_time desc, l.version_num desc, l.archive_dir desc) org_currency_rank
        from spl l
        order by l.id;


      update dgv_sum_spl ss set PRODUCT_TITLE=(
        select xt.title from (
                               select spl_id, xmltype(replace(replace(replace(trim(title),'<br/>',' / '),'<br>',' / '),'</br>',' ')) tt from dgv_spl_title ) l,
              xmltable(
                  xmlnamespaces(default 'urn:hl7-org:v3'),
                  '/title'
                  passing l.tt
                  columns
                    title varchar2(4000)
                    path 'fn:string-join(./title//normalize-space(string()),"  ")'
              ) xt where l.spl_id=ss.spl_id);

      update dgv_sum_spl set initial_Approval_Year=REGEXP_SUBSTR(case when instr(lower(PRODUCT_TITLE),'approval')>0 then substr(PRODUCT_TITLE,instr(lower(PRODUCT_TITLE),'approval')+9, 17) else '' end,'\d{4}');
      update dgv_sum_spl set revised_date=
      case when ( instr(PRODUCT_TITLE,'Issued')>0 or instr(PRODUCT_TITLE,'Revised')>0 ) then
        case when instr(PRODUCT_TITLE,'January')>0 then substr(PRODUCT_TITLE,instr(PRODUCT_TITLE,'January'), 12)
        when instr(PRODUCT_TITLE,'February')>0 then substr(PRODUCT_TITLE,instr(PRODUCT_TITLE,'February'), 13)
        when instr(PRODUCT_TITLE,'March')>0 then substr(PRODUCT_TITLE,instr(PRODUCT_TITLE,'March'), 10)
        when instr(PRODUCT_TITLE,'April')>0 then substr(PRODUCT_TITLE,instr(PRODUCT_TITLE,'April'), 10)
        when instr(PRODUCT_TITLE,'May')>0 then substr(PRODUCT_TITLE,instr(PRODUCT_TITLE,'May'), 8)
        when instr(PRODUCT_TITLE,'June')>0 then substr(PRODUCT_TITLE,instr(PRODUCT_TITLE,'June'), 9)
        when instr(PRODUCT_TITLE,'July')>0 then substr(PRODUCT_TITLE,instr(PRODUCT_TITLE,'July'), 9)
        when instr(PRODUCT_TITLE,'August')>0 then substr(PRODUCT_TITLE,instr(PRODUCT_TITLE,'August'), 11)
        when instr(PRODUCT_TITLE,'September')>0 then substr(PRODUCT_TITLE,instr(PRODUCT_TITLE,'September'), 14)
        when instr(PRODUCT_TITLE,'October')>0 then substr(PRODUCT_TITLE,instr(PRODUCT_TITLE,'October'), 12)
        when instr(PRODUCT_TITLE,'November')>0 then substr(PRODUCT_TITLE,instr(PRODUCT_TITLE,'November'),13)
        when instr(PRODUCT_TITLE,'December')>0 then substr(PRODUCT_TITLE,instr(PRODUCT_TITLE,'December'), 13)
        else '' end
      else '' end ;

      update dgv_sum_spl set REVISED_DATE= REGEXP_REPLACE(to_char(to_date(substr(eff_time,1,6),'yyyymm'),'Month yyyy'),'( ){2,}', ' ')
        where REVISED_DATE is null or length(trim(REVISED_DATE))=0;

      --create SPL's pharmacological class

      insert into dgv_sum_spl_epc
        select
          s.spl_id,
          s.set_id,
          (  select listagg(pharma_name, '; ') within group (order by pharma_name)
             from
               ( select distinct pm.spl_setid, c.name pharma_name
                 from pharma_mapping pm
                 join pharm_index pi on pi.pharma_setid = pm.pharma_setid
                 join medrt_concept c on c.nui = pi.nui
                 where c.tlevel = '[EPC]'
               ) ph
             where ph.spl_setid = s.set_id
          ) epc
        from  dgv_sum_spl s;

      update dgv_sum_spl s set epc=(select epc from dgv_sum_spl_epc se where s.spl_id=se.spl_id);

      ----Human Prescription Drug and Biological Products - PLR Format

      insert into dgv_sum_rx_spl
        select 1 FORMAT_GROUP,dgv_sum_spl.*
        from dgv_sum_spl
          join (select distinct spl_id from DGV_SPL_SEC_HIGHLIGHTS) splwh on splwh.spl_id= dgv_sum_spl.spl_id
        where DOCUMENT_TYPE_LOINC_CODE in ('34391-3','45129-4','60684-8','53408-1','53405-7','60682-2','60683-0','53404-0');
      ----Human Prescription Drug and Biological Products - Non-PLR Format
      insert into dgv_sum_rx_spl
        select 2 FORMAT_GROUP,dgv_sum_spl.*
        from dgv_sum_spl
        where DOCUMENT_TYPE_LOINC_CODE in ('34391-3','45129-4','60684-8','53408-1','53405-7','60682-2','60683-0','53404-0')
              and spl_id not in (select spl_id from dgv_sum_rx_spl);
      ----Human OTC Drugs
      insert into dgv_sum_rx_spl
        select 3 FORMAT_GROUP,dgv_sum_spl.*
        from dgv_sum_spl
        where DOCUMENT_TYPE_LOINC_CODE in ('34390-5');

      ----labeling  counts

      insert into dgv_format_document_type_count
        select FORMAT_GROUP format,DOCUMENT_TYPE_LOINC_CODE code,count(spl_id) counts
        from dgv_sum_rx_spl group by FORMAT_GROUP ,DOCUMENT_TYPE_LOINC_CODE ;
      insert into dgv_format_document_type_count
        select FORMAT_GROUP format,'*',count(spl_id) counts
        from dgv_sum_rx_spl  where FORMAT_GROUP=1 or FORMAT_GROUP=2  group by FORMAT_GROUP;


      ---market category count
      insert into dgv_MARKETING_CAT_COUNT
        select MARKETING_CAT_NCIT_CODE code,count(distinct mc.spl_id) count from dgv_SUM_SPL_MKT_CAT mc
          join dgv_sum_rx_spl rx on mc.spl_id=rx.spl_id
        where MARKETING_CAT_NCIT_CODE is not null group by MARKETING_CAT_NCIT_CODE;

      insert into dgv_medrt_concept_spl_count
        select pi.nui, count(distinct pm.spl_setid)
        from pharm_index pi
        join medrt_concept c on pi.nui = c.nui
        join pharma_mapping pm on pm.pharma_setid = pi.pharma_setid
        join dgv_sum_rx_spl s on s.set_id = pm.spl_setid
        group by pi.nui;

      sync_context_indexes();

      update_section_counts();

  end  create_summary_data;


  procedure delete_spl(p_spl_id integer) is
  begin
      delete dgv_prod_mkt_act where spl_id = p_spl_id;

      delete dgv_prod_deapolicy where spl_id = p_spl_id;

      delete dgv_prod_appr where spl_id = p_spl_id;

      delete dgv_prod_ingr  where spl_id = p_spl_id;

      delete dgv_prod_route where spl_id = p_spl_id;

      delete dgv_SPL_PROD where spl_id = p_spl_id;

  	  delete dgv_spl_title where spl_id = p_spl_id;

	    delete from dgv_spl_sec_highlights where spl_id = p_spl_id;
  end delete_spl;

end dgv;


