create or replace type numbers_ntt as table of number;
/
create or replace type ints_ntt as table of int;
/
create type varchars_ntt is table of varchar2(2000);
/

create or replace function extract_regexp(items_str_in varchar2, item_regexp_in varchar2) return varchars_ntt deterministic is
  l_tok varchar2(2000);
  l_pos number;
  l_items varchars_ntt := varchars_ntt();
begin

  l_pos := 1;

  loop
    select regexp_substr(items_str_in, item_regexp_in, 1, l_pos) into l_tok from dual;
    exit when l_tok is null;

    l_items.extend(1);
    l_items(l_pos) := l_tok;

    l_pos := l_pos + 1;
  end loop;

  return l_items;
end;
/


create or replace procedure reset_seq(p_seq_name in varchar2) as
  l_val number;
begin
  execute immediate
    'select ' || p_seq_name || '.nextval from dual' INTO l_val;

  execute immediate
    'alter sequence ' || p_seq_name || ' increment by -' || l_val || ' minvalue 0';

  execute immediate
    'select ' || p_seq_name || '.nextval from dual' INTO l_val;

  execute immediate
    'alter sequence ' || p_seq_name || ' increment by 1 minvalue 0';
end;
/




/*

create or replace procedure make_table_indexes_unusable(schema_name_in varchar2, table_name_in varchar2) as
  cursor c_usr_idxs is select * from user_indexes where table_name = table_name_in and table_owner = schema_name_in;
  l_cur_idx  c_usr_idxs%rowtype;
begin
  open c_usr_idxs;

  loop
    fetch c_usr_idxs into l_cur_idx;
    exit when not c_usr_idxs%found;

    execute immediate 'alter index ' || l_cur_idx.index_name || ' unusable';
  end loop;

  close c_usr_idxs;

end;
/

create or replace procedure rebuild_table_indexes(schema_name_in varchar2, table_name_in varchar2) as
  cursor c_usr_idxs is select * from user_indexes where table_name = table_name_in and table_owner = schema_name_in;
  l_cur_idx  c_usr_idxs%rowtype;
begin
  open c_usr_idxs;

  loop
    fetch c_usr_idxs into l_cur_idx;
    exit when not c_usr_idxs%found;

    execute immediate 'alter index ' || l_cur_idx.index_name || ' rebuild';
  end loop;

  close c_usr_idxs;

end;
/


create or replace function split_regexp(items_str_in varchar2, item_regexp_in varchar2) return varchars_ntt deterministic pipelined is
  l_tok varchar2(2000);
  l_pos number := 1;

begin

  loop
    select regexp_substr(items_str_in, item_regexp_in, 1, l_pos) into l_tok from dual;
    exit when (l_tok is null);

    l_pos := l_pos + 1;

    pipe row(l_tok);
  end loop;

  return;
end;
/
*/

/*
create or replace function normd_drug_names(combined_names varchar2) return varchars_ntt is
begin
  -- split by "(space)+and(space)+", "(nondigit),(nondigit)" (using existing utils fn)

  -- for each item:

    -- remove parenthesized sections

    -- remove (digit)+(ws)*%

    -- remove "(ws)+tablets(ws|$)"

    -- replace (ws){2,} with single space

    -- lowercase and cap-first

end;
/
*/