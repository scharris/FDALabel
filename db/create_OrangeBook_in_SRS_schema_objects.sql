/* 
Products.txt

    Ingredient
    The active ingredient(s) for the product. Multiple ingredients are in alphabetical order, separated by a semicolon.
    Dosage form;Route of Administration
    The product dosage form and route separated by a semi-colon.  The format is not all uppercase.
    Trade Name
    The trade name of the product as shown on the labeling.
    Applicant
    The firm name holding legal responsibility for the new drug application.  The firm name is condensed to a maximum twenty character unique string.
    Strength
    The potency of the active ingredient.  May repeat for multiple part products.
    New Drug Application Type
    The type of new drug application approval.  New Drug Applications (NDA or innovator)  are ”N”. Abbreviated New Drug Applications (ANDA or generic) are “A”.
    New Drug Application (NDA) Number
    The FDA assigned number to the application. Format is nnnnnn. 
    Product Number
    The FDA assigned number to identify the application products. Each strength is a separate product.  May repeat for multiple part products. Format is nnn.
    Therapeutic Equivalence (TE) Code
    The TE Code indicates the therapeutic equivalence rating of generic to innovator Rx products.
    Approval Date
    The date the product was approved as stated in the FDA approval letter to the applicant.  The format is Mmm dd, yyyy.  Products approved prior to the January 1, 1982 contain the phrase: "Approved prior to Jan 1, 1982".
    Reference Listed Drug (RLD)
    The pioneer or innovator of the drug.  The RLD identifies the product Abbreviated New Drug Applications (ANDA) use as a reference.  Format is Yes or No.
    Type
    The group or category of approved drugs.  Format is RX, OTC, DISCN.
    Applicant Full Name
    The full name of the firm holding legal responsibility for the new drug application. 
 */
drop table products;
create table products (
  Ingredient varchar2(250) not null,
  DF_Route varchar2(200) not null,
  Trade_Name varchar2(100) not null,
  Applicant varchar2(50) not null,
  Strength varchar2(250) not null,
  Appl_Type varchar2(5) not null,
  Appl_No number,
  Product_No number,
  TE_Code varchar2(100),
  Approval_Date_text varchar2(50),
  RLD varchar2(3),
  Type varchar2(10),
  Applicant_Full_Name varchar2(200)
 );
												

/* Patent.txt
    New Drug Application Type
    The type of new drug application approval.  New Drug Applications (NDA or innovator)  are ”N”.   Abbreviated New Drug Applications (ANDA or generic) are “A”.
    New Drug Application (NDA) Number
    The FDA assigned number to the application. Format is nnnnnn.
    Product Number
    The FDA assigned number to identify the application products.  Each strength is a separate product.  May repeat for multiple part products. Format is nnn.
    Patent Number
    Patent numbers as submitted by the applicant holder for patents covered by the statutory provisions.  May repeat for multiple applications and multiple products. Includes pediatric exclusivity granted by the agency.  Format is nnnnnnnnnnn.
    Patent Expire Date
    The date the patent expires as submitted by the applicant holder including applicable extensions.  The format is MMM DD, YYYY.
    Drug Substance Flag
    Patents submitted on FDA Form 3542 and listed after August 18, 2003 may have a drug substance flag indicating the sponsor submitted the patent as claiming the drug substance.   Format is Y or null.
    Drug Product Flag
    Patents submitted on FDA Form 3542 and listed after August 18, 2003 may have a drug product flag indicating the sponsor submitted the patent as claiming the drug product.   Format is Y or null.
    Patent Use Code
    Code to designate a use patent that covers the approved indication or use of a drug product.  May repeat for multiple applications, multiple products and multiple patents.  Format is nnnnnnnnnn.
    Patent Delist Request Flag
    Sponsor has requested patent be delisted.  This patent has remained listed because, under Section 505(j)(5)(D)(i) of the Act, a first applicant may retain eligibility for 180-day exclusivity based on a paragraph IV certification to this patent for a certain period.  Applicants under Section 505(b)(2) are not required to certify to patents where this flag is set to Y.  Format is Y or null. 
*/

drop table Patent;
create table Patent (
  Appl_Type varchar2(5) not null,
  ApplNo number not null,
  ProductNo number not null,
  Patent_No varchar2(12) not null,
  Patent_Expire_Date date,
  ProductMktStatus integer,
  Drug_Substance_Flag varchar2(3),
  Drug_Product_Flag varchar2(3),
  Patent_Use_Code varchar2(12),
  Delist_Flag varchar2(3)
);
	
/* 
Exclusivity.txt

    New Drug Application Type
    The type of new drug application approval. New Drug Applications (NDA or innovator) are ”N”. Abbreviated New Drug Applications (ANDA or generic) are “A”.
    New Drug Application (NDA) Number
    The FDA assigned number to the application.  Format is nnnnnn.
    Product Number
    The FDA assigned number to identify the application products. Each strength is a separate product.  May repeat for multiple part products.  Format is nnn.
    Exclusivity Code
    Code to designate exclusivity granted by the FDA to a drug product.  Format is nnnnnnnnnn.
    Exclusivity Date
    The date the exclusivity expires. Format is MMM DD, YYYY.
	*/
drop table exclusivity;
create table exclusivity (
  Appl_Type varchar2(5) not null,
  ApplNo number not null,
  ProductNo number not null,
  Exclusivity_Code varchar2(50) not null,
  Exclusivity_Date date
);								

		
grant select, references on exclusivity to DrugLabel;
grant select, references on Patent to DrugLabel;
grant select, references on products to DrugLabel;
drop table NDSE;
create table NDSE (
  Item_Code varchar2(20) not null,
  NDC11 varchar2(20) not null,
  Proprietary_Name varchar2(500) ,
  Dosage_Form varchar2(150) ,
  Marketing_Category varchar2(120) ,
  Application_Number_or_Citation varchar2(120) ,
  Product_Type varchar2(120) ,
  Marketing_Start_Date  date,
  Marketing_End_Date date,
  Billing_Unit  varchar2(50),
);	
commit;
/* FDA labeling-Drugs@FDA relation table spl_ndc_appl_v

*/ 
drop materialized view spl_ndc_appl_v;
create  materialized view spl_ndc_appl_v as
select distinct sp.SPL_ID,sp.PRODUCT_NDC_CODE,an.APPLNO
from sum_spl_ndc sp
join sum_spl spl on sp.spl_id=spl.SPL_ID
left join app_ndse an on an.ndc9=sp.PRODUCT_NDC_CODE and upper(an.PROPRIETARY_NAME)=upper(spl.PRODUCT_NAMES);
commit;

/* query the drugs@fda data with SPL set_id 

*/ 
select distinct sp.set_id,sp.PRODUCT_NAMES,sp.NDC_CODES,
       apv.APPLNO,apv.appltype,apv.sponsorapplicant,apv.actiontype,rg.actiondate,ad.DOCURL
from 	spl_ndc_appl_v sna
join srs.application apv on sna.applno=apv.applno
join sum_spl sp on sna.spl_id=sp.spl_id
join srs.REGACTIONDATE rg on rg.applno=sna.APPLNO
join SRS.APPDOC ad on ad.applno=rg.APPLNO and ad.SEQNO=rg.INDOCTYPESEQNO
where sp.set_id in (
'84e9de8f-27c3-4177-827a-6992495511ac'
)
order by set_id,actiondate,applno				
/* query the application's action with appdoc drugs@fda data by SPL set_id 

*/ 
select ap.sponsorapplicant,ap.APPLNO,to_char(rg.ACTIONDATE,'yyyy-mm-dd') ACTIONDATE,rg.INDOCTYPESEQNO,look.DOCTYPEDESC ApprovalType,
   (select LISTAGG(trim(doc.doctype)||':='||doc.DOCURL, ';') WITHIN GROUP (ORDER BY adtl.SORTORDER) as Action_Doc 
      from SRS.APPDOC doc 
      join srs.appdoctype_lookup adtl on upper(trim(adtl.APPDOCTYPE))=upper(trim(doc.DOCTYPE))
      where doc.APPLNO=rg.APPLNO and doc.SEQNO=rg.INDOCTYPESEQNO
       ) ActionDoc
 from SRS.REGACTIONDATE rg 
 join srs.application ap on ap.applno=rg.APPLNO
 join srs.doctype_lookup look on look.DOCTYPE=rg.DOCTYPE
 where rg.APPLNO in ( select distinct appl_no from SPL_approval spv
                            join sum_spl sp on sp.SPL_ID=spv.SPL_ID
                            where sp.SPL_ID ='93926'                      
   )
 order by APPLNO,ACTIONDATE desc 
 
 /* query the patent data Orange Book data by SPL set_id 
upper(sp.AUTHOR_ORG_NORMD_NAME) like upper(substr(trim(p.APPLICANT_FULL_NAME),1,instr(trim(p.APPLICANT_FULL_NAME),' ')))||'%' 
*/ 
select p.APPL_NO,p.APPLICANT_FULL_NAME ,p.PRODUCT_NO,p.TRADE_NAME propriety_Name,p.INGREDIENT Active_INGREDIENT,p.TE_CODE,p.RLD Reference_listed_drug,p.TYPE,p.APPROVAL_DATE_TEXT APPROVAL_DATE,
  pa.PATENT_NO,to_char(pa.PATENT_EXPIRE_DATE,'yyyy-mm-dd') PATENT_EXPIRE_DATE,pa.PATENT_USE_CODE,pa.DRUG_SUBSTANCE_FLAG DRUG_SUBSTANCE_Claim
 from SRS.products p 
left join SRS.PATENT pa on pa.APPLNO=p.APPL_NO and pa.PRODUCTNO=p.PRODUCT_NO
 where p.APPL_NO in ( select distinct appl_no from SPL_approval spv
                            join sum_spl sp on sp.SPL_ID=spv.SPL_ID
                            where sp.SPL_ID ='93926'      
                   )
 order by p.APPL_NO,PRODUCT_NO,PATENT_NO
  select p.APPL_NO,p.APPLICANT_FULL_NAME ,p.PRODUCT_NO,p.TRADE_NAME propriety_Name,p.INGREDIENT Active_INGREDIENT,p.TE_CODE,p.RLD Reference_listed_drug,p.TYPE,p.APPROVAL_DATE_TEXT APPROVAL_DATE,
  ex.EXCLUSIVITY_CODE,  ex.EXCLUSIVITY_DATE
 from SRS.products p 
left join SRS.EXCLUSIVITY ex on ex.APPLNO=p.APPL_NO and ex.PRODUCTNO=p.PRODUCT_NO
 where p.APPL_NO in ( select distinct appl_no from SPL_approval spv
                            join sum_spl sp on sp.SPL_ID=spv.SPL_ID
                            where sp.SPL_ID ='94037'         )
 order by p.APPL_NO,PRODUCT_NO
 