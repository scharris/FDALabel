create table tmp_single_unii_spl_uniis as 
select lu.spl_id, l.set_id, lu.unii
    from sum_spl_act_ingr_unii lu 
    join spl l on (l.id = lu.spl_id)
    where lu.spl_id in (
      select spl_id
      from sum_spl_act_ingr_unii
      group by spl_id
      having count(*) = 1
    )
;
create index tmp_su_lu_splid on tmp_single_unii_spl_uniis(spl_id);
create index tmp_su_lu_setid on tmp_single_unii_spl_uniis(set_id);

select
  sect.loinc_name section, 
  mh.pt_name, 
  ptcounts.distinct_act_ingr_uniis num_drugs, 
  mh.hlt_name, 
  mh.hlgt_name, 
  mh.soc_name, 
  mh.soc_abbrev
from ( 
  select 
    ms.section_type, 
    llt.pt_code, 
    count(distinct(select lu.unii from tmp_single_unii_spl_uniis lu where lu.set_id = ms.spl_set_id)) distinct_act_ingr_uniis
  from meddra_llt_spl_coded_sec ms 
  join sum_spl l on l.SET_ID= ms.SPL_SET_ID 
  join meddra.low_level_term llt on ms.llt_code = llt.llt_code 
  where ms.section_type in ( '34066-1' /*BW*/)  and l.DOCUMENT_TYPE_LOINC_CODE='34391-3' 
  group by llt.pt_code, ms.section_type 
) ptcounts 
join meddra.meddra_hierarchy mh on ptcounts.pt_code = mh.pt_code and mh.primary_soc_fg = 'Y' 
join section_type sect on sect.loinc_code = ptcounts.section_type 
order by sect.loinc_name, mh.pt_name
;

--  (export results)

drop table tmp_single_unii_spl_uniis;

/*
-- (This version should be equivalent to the above without requiring the tmp table, but takes too long to run.)
with
  single_unii_splids as ( -- ids of labels containing only a single active ingredient across all contained products (~61K labels as of 7 Sept 2016)
    select spl_id
    from sum_spl_act_ingr_unii lu
    group by spl_id
    having count(*) = 1
  ),
  single_unii_spl_uniis as (
    select l.set_id, lu.unii
    from sum_spl_act_ingr_unii lu 
    join spl l on (l.id = lu.spl_id)
    where lu.spl_id in (select spl_id from single_unii_splids)
    order by l.set_id, lu.unii
  )
select 
  sect.loinc_name section, 
  mh.pt_name, 
  ptcounts.distinct_act_ingr_uniis num_drugs,
  mh.hlt_name, 
  mh.hlgt_name, 
  mh.soc_name, 
  mh.soc_abbrev
from ( 
  select 
    ms.section_type, 
    llt.pt_code, 
    count(distinct(select lu.unii from single_unii_spl_uniis lu where lu.set_id = ms.spl_set_id)) distinct_act_ingr_uniis
  from meddra_llt_spl_coded_sec ms 
  join sum_spl l on l.SET_ID= ms.SPL_SET_ID 
  join meddra.low_level_term llt on ms.llt_code = llt.llt_code 
  where ms.section_type in ( '34066-1')  and l.DOCUMENT_TYPE_LOINC_CODE='34391-3' 
  group by llt.pt_code, ms.section_type 
) ptcounts 
join meddra.meddra_hierarchy mh on ptcounts.pt_code = mh.pt_code and mh.primary_soc_fg = 'Y' 
join section_type sect on sect.loinc_code = ptcounts.section_type 
order by sect.loinc_name, mh.pt_name
;
*/
