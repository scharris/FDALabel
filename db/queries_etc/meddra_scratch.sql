
-- all low level terms
select * from meddra_llt_hier_spl_coded_sec
--order by section_code, llt
;

-- Nearly all labels include meddra terms.
select count(distinct set_id) from meddra_llt_hier_spl_coded_sec;
-- 50281 (of 50620).

-- SPL counts for given pairing of section and llt
select min(section), llt, count(distinct set_id) containing_spls
from meddra_llt_hier_spl_coded_sec
group by section_code, llt
order by section_code, llt
;

-- How many meddra llt's are not in an spl section?
select count(*)
from meddra.low_level_term llt
where llt.llt_code not in (
  select llt_sec.llt_code from meddra_llt_spl_coded_sec llt_sec
);
-- 59600

select count(*) from meddra.low_level_term llt; -- 71326

-- How many meddra pt's are in an spl section?
select count(*)
from meddra.low_level_term llt
where llt.llt_code not in (
  select llt_sec.llt_code from meddra_llt_spl_coded_sec llt_sec
);

-- How many preferred terms occur in spls directly?
select count(*)
from meddra.preferred_term pt
where pt.pt_code in (
  select llt_sec.llt_code from meddra_llt_spl_coded_sec llt_sec
);
-- 4667 (of 20057)
select count(*) from meddra.preferred_term pt; -- 20057

