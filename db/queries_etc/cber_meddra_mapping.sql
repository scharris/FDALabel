/* CBER MedDRA Queries */

-- Set id's for BLA's and vaccines.
select llt.llt_name MedDRA_LLT, case when llt.llt_code = llt.pt_code then 'Y' else 'N' end is_pt,
       st.loinc_name section_type, 
       l.set_id spl_id, l.document_type, l.author_org_normd_name, l.num_products, l.marketing_acts, l.product_names,
       l.product_normd_generic_names, l.market_categories, l.routes_of_administration, l.ndc_codes, l.dosage_forms,
       l.num_act_ingrs, l.act_ingr_uniis, l.act_ingr_names
from sum_spl l
join meddra_llt_spl_coded_sec m on m.spl_set_id = l.set_id
join meddra.low_level_term llt on m.llt_code = llt.llt_code
join section_type st on m.section_type = st.loinc_code
where
  l.spl_id in (select spl_id from prod_appr where marketing_cat_ncit_code = 'C73585') -- BLA
  or l.DOCUMENT_TYPE_LOINC_CODE = '53404-0' -- Vaccine
;

-- Set id's for BLA's and vaccines.
select llt.llt_name MedDRA_LLT, case when llt.llt_code = llt.pt_code then 'Y' else 'N' end is_pt,
       st.loinc_name section_type, 
       l.set_id spl_id, l.document_type, l.author_org_normd_name, l.num_products, l.marketing_acts, l.product_names,
       l.product_normd_generic_names, l.market_categories, l.routes_of_administration, l.ndc_codes, l.dosage_forms,
       l.num_act_ingrs, l.act_ingr_uniis, l.act_ingr_names
from sum_spl l
join meddra_llt_spl_coded_sec m on m.spl_set_id = l.set_id
join meddra.low_level_term llt on m.llt_code = llt.llt_code
join section_type st on m.section_type = st.loinc_code
where
  l.spl_id in (select spl_id from prod_appr where marketing_cat_ncit_code = 'C73585') -- BLA
  or l.DOCUMENT_TYPE_LOINC_CODE = '53404-0' -- Vaccine
;


