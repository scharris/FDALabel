/* This first part only needs to be done once to create the base tables for the term occurrences.
-- (Table es_set_ids loaded from file produced by Elasticsearch loader.)

-- Some labels failed to load for various reasons in each system, so we keep track of
-- those set ids so we can eliminate any differences in term occurrences caused by these.

-- druglabel set ids not in ES
create table es_missing_set_ids (
  set_id varchar(36) primary key
);
insert into es_missing_set_ids
  select distinct dl.set_id from druglabel.spl dl where dl.set_id not in (select set_id from es_set_ids);
-- (6 set ids)

-- ES set ids not in Druglabel
create table dl_missing_set_ids (
  set_id varchar(36) primary key
);
insert into dl_missing_set_ids
  select es.set_id from es_set_ids es where es.set_id not in (select set_id from druglabel.spl);
-- (46 set ids)

-- Create a local copy of druglabel's term occurrences here.
create table dl_llt_occs as select * from druglabel.meddra_llt_spl_coded_sec;
alter table dl_llt_occs modify (llt_code not null);
alter table dl_llt_occs modify (spl_set_id not null);
alter table dl_llt_occs modify (section_type not null);
create unique index ix_dllltoccs on dl_llt_occs(llt_code, spl_set_id, section_type);

-- The ES term occurrences are loaded from file produced by the ES loader.
create table es_llt_occs as select * from dl_llt_occs where rownum < 1;
alter table es_llt_occs modify (llt_code not null);
alter table es_llt_occs modify (spl_set_id not null);
alter table es_llt_occs modify (section_type not null);
create unique index ix_eslltoccs on es_llt_occs(llt_code, spl_set_id, section_type);
*/

-----------------------------------------------------------------------------------
-- [Start here when reloading ES occurrences after making changes to its loader.]
-----------------------------------------------------------------------------------
-- Import es_llt_occs data here from the meddra-llt-spl-coded-sec.csv output
-- file produced by Elasticsearch loader. Also import meddra-llt-details.csv
-- into table es_meddra_llt_details.
-----------------------------------------------------------------------------------

-- We now have the term occurrence tables for both the elasticsearch (ES) and druglabel (DL) sides:
select count(*) from dl_llt_occs;
-- 13,756,167
select count(*) from es_llt_occs;
-- 13,547,186


-- Create views showing all MedDRA term matches in DL and ES, with helpful information added (term and section names, etc).
-- Some of the information in the ES view like "normalized" form of the terms and containing terms is specific to the
-- ES system.

create or replace view es_llt_occs_v as
  select o.spl_set_id, t.llt_name llt, s.loinc_name section, o.section_type section_code, t.llt_code, t.pt_code, t.llt_currency is_current, o.only_as_part_of_larger_term
  from es_llt_occs o
    left join meddra.low_level_term t on t.llt_code = o.llt_code
    left join druglabel.section_type s on s.loinc_code = o.SECTION_TYPE
;

create or replace view dl_llt_occs_v as
  select o.spl_set_id, t.llt_name llt, s.loinc_name section, o.section_type section_code, t.llt_code, t.pt_code, t.llt_currency is_current
  from dl_llt_occs o
    left join meddra.low_level_term t on t.llt_code = o.llt_code
    left join druglabel.section_type s on s.loinc_code = o.SECTION_TYPE
;

-- Example lookup of MedDRA term matches in both systems for a single label.
select * from es_llt_occs_v where spl_set_id = '0a86028e-9b4f-4d28-8b2f-412fb54b1f9c' order by llt;
select * from dl_llt_occs_v where spl_set_id = '0a86028e-9b4f-4d28-8b2f-412fb54b1f9c' order by llt;


-- Find and examine differences between the term occurrences found in DL (FDALabel/Druglabel Oracle db) and ES (new system).


-- Compute term occurrences that are present in one system but not the other, both ways.

drop table llt_occs_es_minus_dl;
create table llt_occs_es_minus_dl as
  select * from es_llt_occs es
  where (es.llt_code, es.spl_set_id, es.section_type) not in
    (select dl.llt_code, dl.spl_set_id, dl.section_type from dl_llt_occs dl)
;
select count(*) from llt_occs_es_minus_dl;
-- 247,994

drop table llt_occs_dl_minus_es;
create table llt_occs_dl_minus_es as
  select * from dl_llt_occs dl
  where (dl.llt_code, dl.spl_set_id, dl.section_type) not in
    (select es.llt_code, es.spl_set_id, es.section_type from es_llt_occs es)
;
select count(*) from llt_occs_dl_minus_es;
-- 456,975


-- The codes are not easily interpreted, so create variants of the above showing the term and section type texts.

drop table llt_occs_dl_minus_es_full;
create table llt_occs_dl_minus_es_full as
select occ.llt_code, llt.llt_name, occ.spl_set_id, st.loinc_name section_type, llt.llt_currency
from llt_occs_dl_minus_es occ
join meddra.low_level_term llt on occ.llt_code = llt.llt_code
join druglabel.section_type st on st.loinc_code = occ.section_type
order by occ.llt_code, occ.spl_set_id, occ.section_type
;

drop table llt_occs_es_minus_dl_full;
create table llt_occs_es_minus_dl_full as
select occ.llt_code, llt.llt_name, occ.spl_set_id, st.loinc_name section_type, llt.llt_currency
from llt_occs_es_minus_dl occ
join meddra.low_level_term llt on occ.llt_code = llt.llt_code
join druglabel.section_type st on st.loinc_code = occ.section_type
order by occ.llt_code, occ.spl_set_id, occ.section_type
;

-- DL did not find terms in sections coded as unclassified, how many of the es-minus-dl occurrences
-- are due to this? A: most
select count(*) from (
  select * from llt_occs_es_minus_dl_full
  where section_type = 'SPL UNCLASSIFIED SECTION'
);
-- 182,236



-- The above two tables show the full differences between the two methods of finding Meddra terms.
-- But we know a couple of sources for differences on both sides (ES minus DL and DL minus ES) already,
-- which we want to filter out so we can investigate causes of the remainder.
-- 1) For the ES minus DL case, most of the 3.5 million records (all but ~50K) in llt_occs_es_minus_dl_full
--    are there because the druglabel table did not include occurrences in sections coded as unclassified, so
--    we will eliminate those.  Also we don't need to investigate occurrences involving the set ids missing
--    from druglabel (a few import failures).
-- 2) For the llt_occs_dl_minus_es_full case, we know that 46 labels could not be
--    loaded into druglabel so we will eliminate those.
-- In each case the remaining records will be put in an "_inv" table for further investigation.
drop table llt_occs_es_minus_dl_inv;
create table llt_occs_es_minus_dl_inv as
  select * from llt_occs_es_minus_dl_full
  where section_type <> 'SPL UNCLASSIFIED SECTION'
    and spl_set_id not in (select m.set_id from dl_missing_set_ids m);

select count(*) from llt_occs_es_minus_dl_inv;
-- 53,673

drop table llt_occs_dl_minus_es_inv;
create table llt_occs_dl_minus_es_inv as
  select * from llt_occs_dl_minus_es_full
  where spl_set_id not in (select m.set_id from es_missing_set_ids m);

select count(*) from llt_occs_dl_minus_es_inv;
-- 456,960


-- Investigate occurrences found in ES but not DL:
select * from llt_occs_es_minus_dl_inv order by llt_code, spl_set_id, section_type;
/*
10000018	1st degree heart block	01f9fb69-2438-481c-b5ec-adebe7825253	PRECAUTIONS SECTION	Y
  Occurs as:
    1<sup>st </sup> degree heart block
    Proper match, DL does not know to match "1st" in the MedDRA term against occurrence with superscript "st",
    which it sees as "1 st" after stripping the markup (sup tags).

10000081	Abdominal pain	00a746cd-5c7e-4b41-b4ac-3cfec36a5e2d	ADVERSE REACTIONS SECTION	Y
  Occurs with trailing digit intended (but not actually written) as a superscript:
     heartburn1, abdominal pain1, nausea1
  ES is able to match these because it will separate trailing numbers from otherwise alphabetical words.

10000210	Abortion	9b73e222-bb58-49b4-a27a-484738d42ba6	ADVERSE REACTIONS SECTION	Y
  Occurs with trailing digit intended as superscript:
    abortion3, albuminuria, amenorrhea3,
  As above, in the new system trailing digits are specially separated so ES is able to do the match.

10000369	Accident	26769008-c635-4b16-b8db-d2a3c9f1cc2b	ADVERSE REACTIONS SECTION	Y
  Occurs with trailing digit intended as superscript:
    cerebrovascular accident1,
  As above.

10000398	Acetaminophen	6ea12e67-e164-4c2b-b208-f85718697c21	PACKAGE LABEL.PRINCIPAL DISPLAY PANEL	Y
  Lacks space between name and dosage:
    Acetaminophen250 mg
  New system is able to separate the trailing digits anyway.

10000607	ACTH	8a58561e-790c-4115-824b-dedfaa10eec5	WARNINGS AND PRECAUTIONS SECTION	Y
  Occurs with trailing digit
    cosyntropin (ACTH1-24)

10001118	ADD	01ec50a1-6c6c-46f0-8c74-73542b0a59c3	INFORMATION FOR PATIENTS SECTION	Y
  Contained within SPL_UNCLASSIFIED_SECTION subsection of INFORMATION FOR PATIENTS SECTION.

10001434	AF	0ad55984-43f0-4b1e-b306-158531dfcb10	PACKAGE LABEL.PRINCIPAL DISPLAY PANEL	N
  Occurs with leading digits:
    041.005/041AF-AG

10001770	ALP	4c619ed9-fe3e-4158-9938-80c6c3493d55	INFORMATION FOR PATIENTS SECTION	N
  Occurs with trailing digits:
    ALP008 R8

10001869	Aluminum	716d0321-fa5e-45c1-9a49-6476453e57a4	INACTIVE INGREDIENT SECTION	Y
  Occurs with leading digits:
    DandC Red 27Aluminum Lake

10002979	Apo B	509b83b6-cde6-393b-8560-557793b6a529	SPL LISTING DATA ELEMENTS SECTION	Y
  Occurs with trailing digits on the 'B':
    <code code="SPLIMPRINT" codeSystem="2.16.840.1.113883.1.11.19255"/>
    <value xsi:type="ST">APO;B750</value>

10002981	Apo-B	509b83b6-cde6-393b-8560-557793b6a529	SPL LISTING DATA ELEMENTS SECTION	Y
  Matches via normalized form "apo b".

10003076	ARC	04c98e46-5704-4e8d-a855-dfce02561b25	PACKAGE LABEL.PRINCIPAL DISPLAY PANEL	Y
  Occurs with trailing digit:
    ARC3

10003658	Atrial fibrillation	26769008-c635-4b16-b8db-d2a3c9f1cc2b	ADVERSE REACTIONS SECTION	Y
  Occurs with trailing digit intended as superscript:
    atrial fibrillation1

10003960	B12 deficiency anemia	e8ac5d36-8116-456f-8274-3defac54bb24	ADVERSE REACTIONS SECTION	Y
  Occurs with subscript "12":
    B<sub>12</sub> deficiency anemia

10003960	B12 deficiency anemia	09b48fda-460a-496b-8644-db205465d06c	ADVERSE REACTIONS SECTION	Y
  Occurs with superscript "12" in its own paragraph:
    B</paragraph>
    <paragraph>
    <sub>12</sub>
    </paragraph>
    <paragraph> deficiency

10007845	CD8 lymphocytes	77108624-4771-4886-a3e2-b2625c444d1b	CLINICAL PHARMACOLOGY SECTION	Y
  Occurs as:
    and CD
    (some lines removed)
      <sub>8</sub> lymphocytes
*/

/* [This section no longer applies because we're including subterm occurrences in the ES results now.]
-- Found in druglabel but not elasticsearch:
-- select * from llt_occs_dl_minus_es_inv order by llt_code, spl_set_id, section_type;
10000081	Abdominal pain	0081f751-e2e5-4d28-9053-306accab3561	ADVERSE REACTIONS SECTION	Y
  Occurs only as part of larger term "Abdominal pain upper".

10000081	Abdominal pain	02727941-c5ea-4f85-beb4-2a00eec73dd0	ADVERSE REACTIONS SECTION	Y
  Occurs only as part of larger term "Abdominal pain upper".

10000081	Abdominal pain	04ca474c-798e-43dd-be7a-1b36720e6a3d	ADVERSE REACTIONS SECTION	Y
  Occurs as "abdominal pain (upper)," which is recognized as the larger term "Abdominal pain upper".

10000081	Abdominal pain	04ca474c-798e-43dd-be7a-1b36720e6a3d	CLINICAL STUDIES SECTION	Y
  Occurs as "Abdominal pain, upper" which is recognized as the larger term "Abdominal pain upper".

10000210	Abortion	028312dc-d155-4fd5-8abd-6bb9f011d3cc	PREGNANCY SECTION	Y
  Occurs as "resulted in abortion, complete resorption of fetuses, or ...".
  Recognized as part of larger term "abortion complete".
  This isn't really the intention of the text ("complete" was part of another phrase here), but the words
  do occur together so it's not practical to prevent.

10000210	Abortion	ab802261-3eeb-4eea-af6a-31c830ac91f8	USE IN SPECIFIC POPULATIONS SECTION	Y
  Occurs as part of larger term "spontaneous abortion", and as "abortions" (plural so non-match).

10000269	Abscess	173bb299-f774-e888-0ecf-7520ac220d78	ADVERSE REACTIONS SECTION	Y
  Occurs only as part of larger term "breast abscess".

...

-- Most of the DL minus ES differences above are caused by terms only occurring as a part of a
-- larger MedDRA term, which ES recognizes but DL does not. So we pull out the subset of DL
-- minus ES which involves only terms which aren't contained in a larger term.
*/

select * from llt_occs_dl_minus_es_inv;
/*
10006069	Br-	00add4a1-377c-4971-8924-65bb1aa25f2a	REFERENCES SECTION	Y
  DL ignores all dashes, so erronously matches "Br J Clin ...", so DL is at fault here, term should not have matched.

10006850	Ca	06eeaab8-a00d-4b41-b46b-40163f09ecb8	CLINICAL PHARMACOLOGY SECTION	N
  Occurs as "Ca++-transport".  ES does not ignore the ++ but DL does. DL should not have matched this.

10009226	Cl	005bfa1f-b9fd-4d63-a926-515d341667dd	CLINICAL PHARMACOLOGY SECTION	Y
  Occurs as "(Cl-)" so DL should not have matched this but "Cl-" instead.

10009226	Cl	2056833d-707b-41e6-80af-357732da78e7	CLINICAL PHARMACOLOGY SECTION	Y
  Same as above, DL's match is in error.

10009228	Cl- decreased	09daf48f-1d44-4b7b-bb53-bd5635b93ca6	CLINICAL PHARMACOLOGY SECTION	Y
  Occurs as "The mean Cl decreased approximately", DL should not have matched this.

10011838	D & C	00baca82-57bd-4af1-95cb-eaa0ba8a3a19	INACTIVE INGREDIENT SECTION	Y
  Occurs as "D+C red no 33" (dye name), DL should not have matched this as the '&' vs. '+' helps to distinguish the meaning.

10016272	Fe	123c61ec-d930-499e-a27a-0cd82d2bd8ce	CLINICAL PHARMACOLOGY SECTION	Y
  (label no longer exists in DL - it was probably removed subsequent to the copy of DL occurrences into this schema)

10016272	Fe	ee775b56-0575-41e4-a948-c92a0bcd69b6	MECHANISM OF ACTION SECTION	Y
  Occurs as "Fe+3" which probably should ideally match "Fe+++" (had it been written differently), not "Fe" as DL is
  matching, so DL should not have matched this.

10016274	Fe+++	00aa4948-6ff6-48ed-9e45-ecbb7a65452c	OVERDOSAGE SECTION	Y
  Occurs as "<sup>59</sup>Fe-ferrokinetic studies".  Dl is only matching the "Fe" and ignoring the "+++" which is not
  correct. DL is at fault, term should not have matched.

10033338	P+	03afce7c-48fd-4798-818a-d778684a2154	PHARMACOKINETICS SECTION	Y
  Occurs as "(p<0.032)", DL ignored the '+' and should not have matched this.

[Almost all of the remaining entries (about 46K) are false matches for "P+" like the above.]

10034175	Pb++	0291eca5-7a1d-4a79-30be-252224d96509	CLINICAL STUDIES SECTION	Y
  Matches "PB" as separate word in several places, without plusses. DL should not have matched this.
*/

