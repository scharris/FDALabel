/* TODO
There should be fk's to MARKETING_CATEGORY and DOCUMENT_TYPE.
Should be fk from MEDDRA_TERM_SPL_COUNT to CODING_AUTHORITY.
*/

select count(id)
from spl
where 
-- NDA's only
id in (
 select spl_id from prod_appr where marketing_cat_ncit_code = 'C73594'
);
-- 5027


-- All act. ingr. unii's.
select count(distinct unii) from sum_spl_act_ingr_unii;
-- 4544

-- Act ingr. unii's for NDA's.
select count(distinct unii) from sum_spl_act_ingr_unii
where spl_id in (
 select spl_id from prod_appr where marketing_cat_ncit_code = 'C73594'
);
-- 1275

-- Act ingr unii's for spl's with only one associated act ingr. unii.
select count(distinct unii) from sum_spl_act_ingr_unii
where spl_id in (
 select spl_id from sum_spl_act_ingr_unii
 group by spl_id
 having count(*) = 1
);
-- 2484

-- Act ingr unii's for NDA spl's with only one associated act ingr. unii.
select count(distinct unii) from sum_spl_act_ingr_unii
where spl_id in (
 select spl_id from sum_spl_act_ingr_unii
 group by spl_id
 having count(*) = 1
)
and spl_id in (
 select spl_id from prod_appr where marketing_cat_ncit_code = 'C73594'
);
-- 1085 uniis

select count(id)
from spl
where 
-- one active ingredient
id in (
 select spl_id from sum_spl_act_ingr_unii
 group by spl_id
 having count(*) = 1
)
-- NDA's only
and id in (
 select spl_id from prod_appr where marketing_cat_ncit_code = 'C73594'
);
-- 4179 spls


-- Total distinct product names, no restrictions.
select count(distinct name) from spl_prod;
-- 31322

-- How many products are in more than one label?
select count(*) from (
select distinct product_name
from sum_spl_prodname
group by product_name
having count(*) > 1
);
-- 6680
-- Between 1/4 and 1/5 of product names occur in multiple labels.

-- Total distinct generic names, no restrictions.
select count(distinct normd_generic_name) from spl_prod;
-- 10759

-- Distinct product names among NDA's.
select count(distinct name) from spl_prod
where spl_id in (
 select spl_id from prod_appr where marketing_cat_ncit_code = 'C73594'
);
-- 2566

-- Distinct generic names among NDA's.
select count(distinct normd_generic_name) from spl_prod
where spl_id in (
 select spl_id from prod_appr where marketing_cat_ncit_code = 'C73594'
);
-- 1523

-- Distinct product names among products with a single active ingredient unii.
select count(distinct name) from spl_prod
where spl_id in (
 select spl_id from sum_spl_act_ingr_unii
 group by spl_id
 having count(*) = 1
);
-- 17111


-- Distinct product names among products with a single active ingredient unii and which are in NDA's.
select count(distinct name) from spl_prod
where spl_id in (
 select spl_id from sum_spl_act_ingr_unii
 group by spl_id
 having count(*) = 1
) and spl_id in (
 select spl_id from prod_appr where marketing_cat_ncit_code = 'C73594'
);
-- 2051


-- Distinct generic names among products with a single active ingredient unii.
select count(distinct normd_generic_name) from spl_prod
where spl_id in (
 select spl_id from sum_spl_act_ingr_unii
 group by spl_id
 having count(*) = 1
);
-- 3418


-- Distinct generic names among products with a single active ingredient unii and which are in NDA's.
select count(distinct normd_generic_name) from spl_prod
where spl_id in (
 select spl_id from sum_spl_act_ingr_unii
 group by spl_id
 having count(*) = 1
) and spl_id in (
 select spl_id from prod_appr where marketing_cat_ncit_code = 'C73594'
);
-- 1146

select count(distinct ndc_code) from spl_prod;
-- 58585

-- all labels
select count(*) from spl;
-- 53731

-- NDA labels.
select count(*) from spl where id in (
 select spl_id from prod_appr where marketing_cat_ncit_code = 'C73594'
);
-- 4857

-- labels with one associated act. ingr. unii.
select count(*) from spl where id in (
 select spl_id from sum_spl_act_ingr_unii
 group by spl_id
 having count(*) = 1
);
-- 37256

-- NDA labels with one associated act. ingr. unii.
select count(*) from spl where id in (
 select spl_id from sum_spl_act_ingr_unii
 group by spl_id
 having count(*) = 1
) and id in (
 select spl_id from prod_appr where marketing_cat_ncit_code = 'C73594'
);
-- 4034
-- So about 4/5 of NDA labels have just 1 active ingr.
