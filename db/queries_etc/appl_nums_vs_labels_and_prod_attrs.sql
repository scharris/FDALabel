----------------------------------------------------------------------------
-- Application numbers commonly appear in multiple labels.
----------------------------------------------------------------------------
--
select pa.approval_num, count(distinct pa.spl_id) spls
from prod_appr pa
where pa.approval_num like 'NDA%' -- (can remove this condition - illustrating that this holds even for NDA's for this example)
group by pa.approval_num
;
/*
appl #          # spls
-----------------------
NDA021997	1
NDA019999	1
NDA021555	3
NDA019699	1
NDA020065	10
NDA020405	19
NDA017010	2
NDA021143	3
...
*/

----------------------------------------------------------------------------
-- Some labels contain multiple product application/approval numbers.
----------------------------------------------------------------------------
--
select count(distinct spl_id) from (
  select pa.spl_id, count(distinct pa.approval_num) appl_nums
  from prod_appr pa
  where pa.approval_num is not null
    --and pa.approval_num like 'NDA%'
  group by pa.spl_id
  having count(distinct pa.approval_num) > 1
)
;
-- 1124 of 6048 labels with an approved product have multiple approval numbers in the same SPL.


--------------------------------------------------------------------------
-- Many labels contain multiple products having the same approval numbers.
--------------------------------------------------------------------------
--
select pa.spl_id, pa.approval_num, count(*) prods
from prod_appr pa
where pa.approval_num like 'NDA%'
group by pa.spl_id, pa.approval_num
;
/*
249912	NDA209387	1
291243	NDA021117	1
237421	NDA022100	4
249950	NDA021163	1
136195	NDA020076	1
291266	NDA020699	3
255330	NDA020357	3
*/

--------------------------------------------------------------------------------
-- Many labels contain products with the same approval numbers but differing
-- characteristics.
-- Here multiple dosage forms are listed for fixed combinations of 
-- (spl set id, application-num).
--------------------------------------------------------------------------------
--
select
  (select sl.set_id from sum_spl sl where sl.spl_id = pa.spl_id) set_id,
  pa.approval_num,
  count(distinct p.ncit_form_code) num_form_codes,
  listagg(p.ncit_form_name, '|') within group (order by p.ncit_form_name) dosage_forms
from prod_appr pa
join spl_prod p on (p.id = pa.product_id)
where pa.approval_num like 'NDA%' -- (can remove this condition - illustrating that this holds even for NDA's for this example)
group by pa.spl_id, pa.approval_num
having count(distinct p.ncit_form_code) > 1
;
/*
SET_ID  APPROVAL_NUM  NUM_FORM_CODES  dosage_forms
befa91b7-ea2b-4fbb-9e4c-45993bfd7c18	NDA019824	2	KIT|SUSPENSION
a0949773-c75f-4ef0-a69c-1abd978340d0	NDA012806	2	CREAM|OINTMENT
fc2a08dd-4fb6-4ac4-9082-f99552fae25c	NDA017962	2	CAPSULE, GELATIN COATED|TABLET
26af3f22-90c0-4caf-9335-9d7c1d028529	NDA205829	2	GAS|LIQUID|LIQUID|LIQUID
8c8acad6-44cc-43aa-966b-027e053be8f5	NDA201849	2	INJECTION, POWDER, LYOPHILIZED, FOR SOLUTION|KIT
96fc24b5-7385-4e01-a969-9f0deacc6db4	NDA018748	2	CREAM|KIT
e4be1c0e-0fe6-45a0-9229-5cde98434b84	NDA201849	2	INJECTION, POWDER, LYOPHILIZED, FOR SOLUTION|KIT
33f60b40-3fca-11de-8f56-0002a5d5c51b	NDA022192	2	KIT|TABLET|TABLET|TABLET|TABLET|TABLET|TABLET|TABLET
66b69c1e-b25c-44d3-b5ff-1c1de9a516fa	NDA203441	2	INJECTION, POWDER, LYOPHILIZED, FOR SOLUTION|KIT|KIT
616224ff-a925-4b38-9ca2-00fbf669380f	NDA022291	2	POWDER, FOR SUSPENSION|TABLET, FILM COATED|TABLET, FILM COATED|TABLET, FILM COATED|TABLET, FILM COATED|TABLET, FILM COATED
939e28c5-f3a9-42c0-9a2d-8d471d82a6e0	NDA021829	2	KIT|PATCH, EXTENDED RELEASE|PATCH, EXTENDED RELEASE|PATCH, EXTENDED RELEASE|PATCH, EXTENDED RELEASE|PATCH, EXTENDED RELEASE|PATCH, EXTENDED RELEASE
db16d6b9-3b89-431d-97f9-9b4a8300e913	NDA021549	2	CAPSULE|KIT
*/

--------------------------------------------------------------------------------
-- Many labels contain products with the same approval numbers but differing
-- characteristics.
-- Here multiple generic names are listed for fixed combinations of
-- (spl set id, application-num).  The names have been normalized a bit already
-- to prevent gratuitous differences in capitalization and punctuation. 
--------------------------------------------------------------------------------
--
select
  (select sl.set_id from sum_spl sl where sl.spl_id = pa.spl_id) set_id,
  pa.approval_num,
  count(distinct p.normd_generic_name) num_est_names,
  cast(collect(distinct p.normd_generic_name) as varchars_ntt) est_names
  --listagg(p.normd_generic_name, '|') within group (order by p.normd_generic_name) est_names
from prod_appr pa
join spl_prod p on (p.id = pa.product_id)
where pa.approval_num like 'NDA%' or pa.approval_num like 'ANDA%'
group by pa.spl_id, pa.approval_num
having count(distinct p.normd_generic_name) > 1
;
/*
SET_ID  APPROVAL_NUM  NUM_EST_NAMES EST_NAMES
98a1caaf-aaf0-47bf-8bdc-1eae486444f9	NDA820915	2	('ADDITIVE SOLUTION - 3','ANTICOAGULANT CITRATE PHOSPHATE DOUBLE DEXTROSE')
480199c3-9524-475f-897c-5fadab7f9c27	ANDA040659	2	('MECLIZINE HYDROCHLORIDE','MECLIZINE HYDROCLORIDE')
563036ee-90e0-4cef-2c80-d6776ed93591	NDA017789	2	('ISOLEUCINE, LEUCINE, LYSINE ACETATE, METHIONINE, PHENYLALANINE, THREONINE, TRYPTOPHAN, VALINE, ALANINE, ARGININE, HISTIDINE, PROLINE, SERINE, TYROSINE, GLYCINE, SODIUM CHLORIDE, MAGNESIUM CHLORIDE, SODIUM PHOSPHATE, DIBASIC, AND POTASSIUM CHLORIDE','ISOLEUCINE, LEUCINE, LYSINE ACETATE, METHIONINE, PHENYLALANINE, THREONINE, TRYPTOPHAN, VALINE, ALANINE, ARGININE, HISTIDINE, PROLINE, SERINE, TYROSINE, GLYCINE, SODIUM CHLORIDE, POTASSIUM ACETATE, PHOSPHORIC ACID, AND MAGNESIUM ACETATE')
9921249f-e15d-46e1-b4ad-4d6866f30ca6	ANDA079025	2	('FOSINOPRIL SODIUM AND HYDROCHLOROTHIAZIDE','FOSINOPRIL SODIUM AND HYDROCHLOROTHIAZIDE TABLET')
0e382487-a6ee-4d29-90f8-4467d6cbdff3	NDA021703	4	('CALCIUM CHLORIDE, MAGNESIUM CHLORIDE, DEXTROSE MONOHYDRATE, LACTIC ACID, SODIUM CHLORIDE, AND SODIUM BICARBONATE','CALCIUM CHLORIDE, MAGNESIUM CHLORIDE, DEXTROSE MONOHYDRATE, LACTIC ACID, SODIUM CHLORIDE, SODIUM BICARBONATE, AND POTASSIUM CHLORIDE','MAGNESIUM CHLORIDE, DEXTROSE MONOHYDRATE, LACTIC ACID, SODIUM CHLORIDE, SODIUM BICARBONATE, AND POTASSIUM CHLORIDE','MAGNESIUM CHLORIDE, LACTIC ACID, SODIUM CHLORIDE, AND SODIUM BICARBONATE')
05cf0ab1-293c-4740-84b8-9a4a1cc3e9d9	ANDA090545	2	('NAPROXEN SODIUM TABLET, COATED','NAPROXEN SODIUM, COATED TABLETS')
163dfa7c-aad3-4290-9fe9-d9592b0ba317	ANDA078171	3	('LEVALBUTEROL INHALATION 0.31MG/3ML','LEVALBUTEROL INHALATION 0.63MG/3ML','LEVALBUTEROL INHALATION 1.25MG/3ML')
af5d0c9b-a997-8033-afca-3c04221f2e76	ANDA201451	2	('MECLIZINE','MECLIZINE HYDROCHLORIDE')
3911a46c-80fe-4b70-a4ba-416de597481b	ANDA076418	2	('AMLODIPINE','AMLODIPINE BESYLATE')
01cca91e-0374-4b89-a25a-be05e8b64346	ANDA074532	2	('CAPTOPRIL','CAPTORPIL')
27c6d7d9-78f1-485f-adbb-506fd1aa8e5c	ANDA040185	2	('PROCHLORPERAZINE MALEATE','PROCHLORPERAZINE MEALEATE')
d132d29d-723e-4357-87f2-0d760afa50a4	ANDA202052	2	('PANTOOPRAZOLE SODIUM','PANTOPRAZOLE SODIUM')
25c4d41f-bdcc-479e-a49f-96d7a2d46fc1	NDA207026	2	('CALCIUM CHLORIDE, MAGNESIUM CHLORIDE, SODIUM CHLORIDE, POTASSIUM CHLORIDE, SODIUM PHOSPHATE, DIBASIC, AND SODIUM BICARBONATE','MAGNESIUM CHLORIDE, SODIUM CHLORIDE, POTASSIUM CHLORIDE, SODIUM PHOSPHATE, DIBASIC, AND SODIUM BICARBONATE')
4b80d087-eaa7-448f-967e-344de7f16436	ANDA091211	2	('ATOVAQUONE AND PROGUANIL HYDROCHLORIDE','ATOVAQUONE AND PROGUANIL HYDROCHLORIDE PEDIATRIC')
*/