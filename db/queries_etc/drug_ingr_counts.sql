-- Count drugs in human rx and otc.
select
  count(distinct (select LISTAGG(lu.unii, ',') WITHIN GROUP (ORDER BY lu.unii) as uniis
    from sum_spl_act_ingr_unii lu
    where lu.spl_id = l.id)) distinct_act_ingr_unii_sets
from spl l
where l.doc_type_loinc_code in (
  '45129-4', '34391-3',  -- human rx
  '34390-5'              -- human otc
)
;
-- human rx: 2524
-- human otc: 5056
-- human rx or otc: 7318

-- Count act ingr unii's (alone) in human rx and otc labels.
select count(distinct unii)
from sum_spl_act_ingr_unii
where spl_id in (
  select id
  from spl l
  where l.doc_type_loinc_code in (
  '45129-4', '34391-3', -- human rx
  '34390-5'             -- human otc
  )
)
;
-- human rx: 3040
-- human otc: 2785
-- all: 4595