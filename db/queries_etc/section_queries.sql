-- contraceptive or hormone (no "hormone replacement" results)
select unique 'http://dailymed.nlm.nih.gov/dailymed/lookup.cfm?setid='||l.set_id link, p.normd_generic_name generic_name, p.name trade_name, l.author_org mfr
from spl_prod p
join spl l on l.id = p.spl_id
where exists
  (select * from spl_sec s
   where s.spl_id = l.id
     and s.loinc_code in ('34066-1', -- boxed warning
                          '48780-1', -- warnings
                          --'34084-4', -- adverse reactions
                          --'34070-3', -- contraindications
                          --'34073-7', -- drug interactions
                          '34072-9', -- general precautions
                          --'34076-0', -- information for patients
                          --'34080-2', -- nursing mothers
                          '42232-9')  -- precautions
                          
     and contains(s.content_xml, 'contraceptive or hormone') > 0)
and exists
  (select * from prod_appr pa
   where pa.spl_id = l.id
     and pa.MARKETING_CAT_NCIT_CODE = 'C73594')
order by p.normd_generic_name
;

-- pregnancy or breastfeeding
select unique 'http://dailymed.nlm.nih.gov/dailymed/lookup.cfm?setid='||l.set_id link, p.normd_generic_name generic_name, p.name trade_name, l.author_org mfr
from spl_prod p
join spl l on l.id = p.spl_id
where exists
  (select * from spl_sec s
   where s.spl_id = l.id
     and s.loinc_code in ('34066-1', -- boxed warning
                          '48780-1', -- warnings
                          --'34084-4', -- adverse reactions
                          --'34070-3', -- contraindications
                          --'34073-7', -- drug interactions
                          '34072-9', -- general precautions
                          '34076-0', -- information for patients
                          '34080-2', -- nursing mothers
                          '42232-9')  -- precautions
                          
     and contains(s.content_xml, 'pregnancy or breastfeeding') > 0)
and exists
  (select * from prod_appr pa
   where pa.spl_id = l.id
     and pa.MARKETING_CAT_NCIT_CODE = 'C73594')
order by p.normd_generic_name
;
