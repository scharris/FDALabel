
-- 10 Biologics
select llt.llt_name MedDRA_LLT, case when llt.llt_code = llt.pt_code then 'Y' else 'N' end is_pt,
       st.loinc_name section_type, 
       l.set_id spl_id, l.document_type, l.author_org_normd_name, l.num_products, l.marketing_acts, l.product_names,
       l.product_normd_generic_names, l.market_categories, l.routes_of_administration, l.ndc_codes, l.dosage_forms,
       l.num_act_ingrs, l.act_ingr_uniis, l.act_ingr_names
from sum_spl l
join meddra_llt_spl_coded_sec m on m.spl_set_id = l.set_id
join meddra.low_level_term llt on m.llt_code = llt.llt_code
join section_type st on m.section_type = st.loinc_code
where
  l.set_id in (
    '0ba769ff-5d91-47b5-b3dc-cade2f25565f','4f96c8e4-3050-457a-bac9-8ac3bfbd4354','e8aa934c-d74e-4c7c-b2fc-d3b8b46f4529',
    '61d4995d-92be-4678-74b9-79b3e12e5e30','ebeb1c66-deab-4358-a5c0-d4f58c8d8ae7', 'dccfb747-8f64-46ec-3993-95195b585581',
    'a9806027-8323-4abf-849f-46fb10984f13', '030dfb30-589d-4335-85fe-d609644088e0', '64f300d3-e1ba-40bc-a25f-4203ffdb27cf'
  )
;

-- 10 drugs, meddra term occurrences
select llt.llt_name MedDRA_LLT, case when llt.llt_code = llt.pt_code then 'Y' else 'N' end is_pt, mh.pt_name, mh.hlt_name, mh.hlgt_name, mh.soc_name, mh.primary_soc_fg is_primary_soc_for_pt,
       st.loinc_name section_type, 
       l.set_id spl_id, l.document_type, l.author_org_normd_name, l.num_products, l.marketing_acts, l.product_names,
       l.product_normd_generic_names, l.market_categories, l.routes_of_administration, l.ndc_codes, l.dosage_forms,
       l.num_act_ingrs, l.act_ingr_uniis, l.act_ingr_names
from sum_spl l
join meddra_llt_spl_coded_sec ml on ml.spl_set_id = l.set_id
join meddra.low_level_term llt on ml.llt_code = llt.llt_code
join section_type st on ml.section_type = st.loinc_code
join meddra.meddra_hierarchy mh on llt.pt_code = mh.pt_code
where
  l.set_id in (
'10db92f9-2300-4a80-836b-673e1ae91610',
'5fe9c118-c44b-48d7-a142-9668ae3df0c6',
'f7b3f443-e83d-4bf2-0e96-023448fed9a8',
'7fa41601-7fb5-4155-8e50-2ae903f0d2d6',
'5526617c-c7b9-4556-886d-729bbabbc566',
'75b16bfc-38c1-4133-bd7d-13258d54edec',
'90550274-6605-44de-8c25-c5591080270f',
'd7f569dc-6bed-42dc-9bec-940a9e6b090d',
'fe0f5dcb-65dd-4d31-80d6-8b97eb040063',
'ba74e3cd-b06f-4145-b284-5fd6b84ff3c9'
)
;

-- 10 biologics, meddra term occurrences
select llt.llt_name MedDRA_LLT, case when llt.llt_code = llt.pt_code then 'Y' else 'N' end is_pt, mh.pt_name, mh.hlt_name, mh.hlgt_name, mh.soc_name, mh.primary_soc_fg is_primary_soc_for_pt,
       st.loinc_name section_type, 
       l.set_id spl_id, l.document_type, l.author_org_normd_name, l.num_products, l.marketing_acts, l.product_names,
       l.product_normd_generic_names, l.market_categories, l.routes_of_administration, l.ndc_codes, l.dosage_forms,
       l.num_act_ingrs, l.act_ingr_uniis, l.act_ingr_names
from sum_spl l
join meddra_llt_spl_coded_sec ml on ml.spl_set_id = l.set_id
join meddra.low_level_term llt on ml.llt_code = llt.llt_code
join section_type st on ml.section_type = st.loinc_code
join meddra.meddra_hierarchy mh on llt.pt_code = mh.pt_code
where
  l.set_id in (
    '0ba769ff-5d91-47b5-b3dc-cade2f25565f','4f96c8e4-3050-457a-bac9-8ac3bfbd4354','e8aa934c-d74e-4c7c-b2fc-d3b8b46f4529',
    '61d4995d-92be-4678-74b9-79b3e12e5e30','ebeb1c66-deab-4358-a5c0-d4f58c8d8ae7', 'dccfb747-8f64-46ec-3993-95195b585581',
    'a9806027-8323-4abf-849f-46fb10984f13', '030dfb30-589d-4335-85fe-d609644088e0', '64f300d3-e1ba-40bc-a25f-4203ffdb27cf'
  )
;

-- Lookup preferred terms for a given set of low level terms.
select llt.llt_name llt, llt.llt_code, case when llt.llt_code = llt.pt_code then 'Y' else 'N' end is_pt, mh.pt_name, mh.pt_code, mh.hlt_name, mh.hlgt_name, mh.soc_name, mh.primary_soc_fg is_primary_soc_for_pt
from meddra.low_level_term llt
join meddra.meddra_hierarchy mh on llt.pt_code = mh.pt_code
where
  llt.llt_code in (
  '10068879',
  '10065117',
  '10069620',
  '10059079',
  '10019211',
  '10028411',
  '10059078',
  '10002199',
  '10022057',
  '10016256',
  '10008531',
  '10003239',
  '10042661',
  '10016558',
  '10022004',
  '10034844',
  '10043554',
  '10010741',
  '10015985',
  '10015963',
  '10012727',
  '10028813',
  '10047700',
  '10000081',
  '10057664',
  '10022098',
  '10069624',
  '10069480',
  '10003549',
  '10016025',
  '10020756',
  '10043519',
  '10054496',
  '10002218',
  '10042772',
  '10013573',
  '10029223',
  '10033775',
  '10010904',
  '10028524',
  '10014619',
  '10028527',
  '10029328',
  '10029240',
  '10073002',
  '10033799',
  '10004223',
  '10061908',
  '10018766',
  '10047115',
  '10038546',
  '10060800',
  '10021414',
  '10020197',
  '10002424',
  '10001705',
  '10042931',
  '10014625',
  '10030942',
  '10061323',
  '10016062',
  '10063344'
)
;

drop table tmp;

create table tmp (
  sheet_tag varchar(100),
  rown number,
  label_verbatim_term varchar(200),
  meddra_term varchar(200),
  meddra_code varchar(20),
  meddra_hierarchy varchar(20),
  meddra_url varchar(500)
);

select * from tmp where meddra_code like '% or %';

-- Lookup preferred terms for a given set of low level terms in tmp table.
select xl.*, llt.llt_name llt, llt.llt_code, case when llt.llt_code = llt.pt_code then 'Y' else 'N' end is_pt, pt.pt_name, pt.pt_code
from tmp xl
left join meddra.low_level_term llt on xl.meddra_code = llt.llt_code
left join meddra.preferred_term pt on llt.pt_code = pt.pt_code
where xl.meddra_code not like '% or %'
order by xl.rown;

