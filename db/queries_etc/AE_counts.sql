-- Meddra PT counts for boxed warning section occurrences.
select
  llt.llt_name,
  mh.pt_name,
  ms.spl_set_id,
  (select LISTAGG(lu.unii, ',') WITHIN GROUP (ORDER BY lu.unii) as uniis
    from sum_spl_act_ingr_unii lu
    where lu.spl_id = (select l.id from spl l where l.set_id = ms.spl_set_id)) act_ingr_uniis,
  mh.hlt_name,
  mh.hlgt_name,
  mh.soc_name,
  mh.soc_abbrev,
  mh.primary_soc_fg is_primary_soc_for_pt
from meddra_llt_spl_coded_sec ms
join meddra.low_level_term llt on ms.llt_code = llt.llt_code
join meddra.meddra_hierarchy mh on llt.pt_code = mh.pt_code
where
  ms.section_type = '34066-1' -- boxed warning section
;


-- Meddra LLT occurrences for all sections.
select
  llt.llt_name llt,
  sect.loinc_name section,
  count(*) documents
/*
  mh.pt_name,
  ms.spl_set_id,
  (select LISTAGG(lu.unii, ',') WITHIN GROUP (ORDER BY lu.unii) as uniis
    from sum_spl_act_ingr_unii lu
    where lu.spl_id = (select l.id from spl l where l.set_id = ms.spl_set_id)) act_ingr_uniis,
  mh.hlt_name,
  mh.hlgt_name,
  mh.soc_name,
  mh.soc_abbrev,
  mh.primary_soc_fg is_primary_soc_for_pt
*/
from meddra_llt_spl_coded_sec ms
join meddra.low_level_term llt on ms.llt_code = llt.llt_code
join meddra.meddra_hierarchy mh on llt.pt_code = mh.pt_code
join section_type sect on sect.loinc_code = ms.section_type
group by llt.llt_name, sect.loinc_name
;



-- Meddra term occurrences in select sections, with primary SOC information (raw data).
select
  ms.spl_set_id,
  llt.llt_name llt,
  sect.loinc_name section,
  mh.pt_name,
  (select LISTAGG(lu.unii, ',') WITHIN GROUP (ORDER BY lu.unii) as uniis
    from sum_spl_act_ingr_unii lu
    where lu.spl_id = (select l.id from spl l where l.set_id = ms.spl_set_id)) drug,
  mh.hlt_name,
  mh.hlgt_name,
  mh.soc_name,
  mh.soc_abbrev
from meddra_llt_spl_coded_sec ms
join meddra.low_level_term llt on ms.llt_code = llt.llt_code
join meddra.meddra_hierarchy mh on llt.pt_code = mh.pt_code and mh.primary_soc_fg = 'Y'
join section_type sect on sect.loinc_code = ms.section_type
where ms.section_type in ('34066-1' /*BW*/, '43685-7' /*WP*/, '34084-4' /*AE*/)
;

-- Lists preferred terms and label sections for which an LLT under the PT occurs in the section text,
-- together with a count of drugs for all such occurrences across labels. Here the drug assigned to a
-- label is the set of unii's for its active ingredients.
select
  sect.loinc_name section,
  mh.pt_name,
  ptcounts.distinct_act_ingr_unii_sets num_drugs,
  mh.hlt_name,
  mh.hlgt_name,
  mh.soc_name,
  mh.soc_abbrev
from (
  select
    ms.section_type,
    llt.pt_code,
    count(distinct (select LISTAGG(lu.unii, ',') WITHIN GROUP (ORDER BY lu.unii) as uniis
      from sum_spl_act_ingr_unii lu
      where lu.spl_id = (select l.id from spl l where l.set_id = ms.spl_set_id))) distinct_act_ingr_unii_sets
  from meddra_llt_spl_coded_sec ms
  join meddra.low_level_term llt on ms.llt_code = llt.llt_code
  where ms.section_type in ('34066-1' /*BW*/, '43685-7' /*WP*/, '34084-4' /*AE*/)
  group by llt.pt_code, ms.section_type
) ptcounts
join meddra.meddra_hierarchy mh on ptcounts.pt_code = mh.pt_code and mh.primary_soc_fg = 'Y'
join section_type sect on sect.loinc_code = ptcounts.section_type
order by sect.loinc_name, mh.pt_name
;

-- Count drugs in human rx and otc.
select
  count(distinct (select LISTAGG(lu.unii, ',') WITHIN GROUP (ORDER BY lu.unii) as uniis
    from sum_spl_act_ingr_unii lu
    where lu.spl_id = l.id)) distinct_act_ingr_unii_sets
from spl l
where l.doc_type_loinc_code in (
  '45129-4', '34391-3',  -- human rx
  '34390-5'              -- human otc
)
;
-- human rx: 2524
-- human otc: 5056
-- human rx or otc: 7318

-- Count act ingr unii's (alone) in human rx and otc labels.
select count(distinct unii)
from sum_spl_act_ingr_unii
where spl_id in (
  select id
  from spl l
  where l.doc_type_loinc_code in (
  '45129-4', '34391-3', -- human rx
  '34390-5'             -- human otc
  )
)
;
-- human rx: 3040
-- human otc: 2785
-- all: 4595
