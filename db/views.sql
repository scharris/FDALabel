--------------------------------------------------------
--  DDL for View DOC_TYPE_SEALDS
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE VIEW "DOC_TYPE_SEALDS" ("CODE", "DISPLAY_NAME", "VIEW_ORDER") AS
  select distinct loinc_code as code, loinc_name as display_name,view_order from document_type d join spl s on s.DOC_TYPE_LOINC_CODE=d.LOINC_CODE where d.LOINC_CODE NOT IN('60685-5','64124-1','72090-4','75030-7','77573-4') and view_order>0 order by view_order,loinc_name;
--------------------------------------------------------
--  DDL for View MARKET_CATEGORY_SEALDS
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE VIEW "MARKET_CATEGORY_SEALDS" ("CODE", "DISPLAY_NAME", "ISLDT", "VIEW_NO") AS
  select distinct nci_thesaurus_code as code, spl_acceptable_term as display_name ,isldt, view_no
  from marketing_category m
    join SUM_SPL_MKT_CAT ss on m.NCI_THESAURUS_CODE=ss.MARKETING_CAT_NCIT_CODE
  where m.nci_thesaurus_code NOT in ('C73626','C95601','C95602')
  order by view_no;
--------------------------------------------------------
--  DDL for View MEDDRA_LLT_HIER_SPL_CODED_SEC
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE VIEW "MEDDRA_LLT_HIER_SPL_CODED_SEC" ("SECTION", "LLT", "SET_ID", "IS_LLT_CURRENT", "LLT_CODE", "SECTION_CODE", "PT", "HLT", "HLGT", "SOC", "IS_PRIMARY_SOC_FOR_PT", "PT_CODE", "HLT_CODE", "HLGT_CODE", "SOC_CODE", "PT_PRIMARY_SOC_CODE") AS
  select sect.loinc_name section, llt.llt_name llt, llt_sec.spl_set_id set_id, llt.llt_currency is_llt_current, llt_sec.llt_code, llt_sec.section_type section_code,
         mhier.pt_name pt, mhier.hlt_name hlt, mhier.hlgt_name hlgt, mhier.soc_name soc, mhier.primary_soc_fg is_primary_soc_for_pt,
    mhier.pt_code, mhier.hlt_code, mhier.hlgt_code, mhier.soc_code, mhier.pt_soc_code pt_primary_soc_code
  from meddra_llt_spl_coded_sec llt_sec
    join meddra.low_level_term llt on llt_sec.llt_code = llt.llt_code
    join section_type sect on llt_sec.section_type = sect.loinc_code
    join meddra.meddra_hierarchy mhier on llt.pt_code = mhier.pt_code;
--------------------------------------------------------
--  DDL for View MEDDRA_LLT_VIEW
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE VIEW "MEDDRA_LLT_VIEW" ("MEDDRA_LLT", "IS_PT", "MEDDRA_PT", "MEDDRA_HLT", "MEDDRA_HLGT", "MEDDRA_SOC", "IS_PRIMARY_SOC_FOR_PT", "SECTION_TYPE", "SPL_ID", "DOCUMENT_TYPE", "AUTHOR_ORG_NORMD_NAME", "NUM_PRODUCTS", "MARKETING_ACTS", "PRODUCT_NAMES", "PRODUCT_NORMD_GENERIC_NAMES", "MARKET_CATEGORIES", "ROUTES_OF_ADMINISTRATION", "NDC_CODES", "DOSAGE_FORMS", "NUM_ACT_INGRS", "ACT_INGR_UNIIS", "ACT_INGR_NAMES") AS
  select llt.llt_name MedDRA_LLT, case when llt.llt_code = llt.pt_code then 'Y' else 'N' end is_pt, mh.pt_name MedDRA_PT, mh.hlt_name MedDRA_HLT,
         mh.hlgt_name MedDRA_HLGT, mh.soc_name MedDRA_SOC, mh.primary_soc_fg is_primary_soc_for_pt,
         st.loinc_name section_type,
         l.set_id spl_id, l.document_type, l.author_org_normd_name, l.num_products, l.marketing_acts, l.product_names,
    l.product_normd_generic_names, l.market_categories, l.routes_of_administration, l.ndc_codes, l.dosage_forms,
    l.num_act_ingrs, l.act_ingr_uniis, l.act_ingr_names
  from sum_spl l
    join meddra_llt_spl_coded_sec ml on ml.spl_set_id = l.set_id
    join meddra.low_level_term llt on ml.llt_code = llt.llt_code
    join section_type st on ml.section_type = st.loinc_code
    join meddra.meddra_hierarchy mh on llt.pt_code = mh.pt_code
  order by act_ingr_uniis,marketing_acts;
--------------------------------------------------------
--  DDL for View NDA_SINGLE_SAI
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE VIEW "NDA_SINGLE_SAI" ("FORMAT_GROUP", "SPL_ID", "LABELING_TYPE", "SUBMITTER", "PRODUCT_NAMES", "PRODUCT_NORMD_GENERIC_NAMES", "ACT_INGR_NAMES", "ACT_INGR_UNIIS", "NDC_CODES", "DOSAGE_FORMS", "ROUTES_OF_ADMINISTRATION", "MARKETING_ACTS", "MARKET_CATEGORIES", "INITIAL_APPROVAL_YEAR", "SPL_EFF_TIME", "EPC", "SET_ID", "ACTIVEMOIETY_NAME", "ACTIVEMOIETY_UNII", "APPLICATION_NUM") AS
  (
    select distinct l.format_group,l.spl_id,
      l.document_type Labeling_type, author_org_normd_name Submitter,
      l.PRODUCT_NAMES, l.PRODUCT_NORMD_GENERIC_NAMES,
      l.act_ingr_names, l.act_ingr_uniis,
      l.ndc_codes, l.dosage_forms, l.routes_of_administration,
      l.marketing_acts, l.market_categories, l.initial_approval_year,
      l.eff_time SPL_EFF_TIME, l.epc,
      l.set_id,
      (select listagg(active_activemoiety_name,'; ') within group (order by active_activemoiety_name) from (select distinct active_activemoiety_name from dgv_prod_ingr where spl_id = l.spl_id)) ActiveMoiety_Name,
      (select listagg(active_activemoiety_unii,'; ') within group (order by active_activemoiety_unii) from (select distinct active_activemoiety_unii from dgv_prod_ingr where spl_id = l.spl_id)) ActiveMoiety_UNII,
      (select listagg(APPROVAL_NUM,'; ') within group (order by APPROVAL_NUM) from (select distinct APPROVAL_NUM from dgv_prod_appr where spl_id = l.spl_id)) Application_Num
    from dgv_sum_rx_spl l
      join dgv_sum_spl_mkt_cat dmc on dmc.spl_id = l.spl_id
    where l.DOCUMENT_TYPE = 'HUMAN PRESCRIPTION DRUG LABEL'
          and dmc.MARKETING_CAT_NCIT_CODE = 'C73594'
          and (select count(distinct unii) from DGV_prod_ingr where spl_id = l.spl_id and is_active = 1) =1
  );
--------------------------------------------------------
--  DDL for View NDFRT_CLASS_SPL
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE VIEW "NDFRT_CLASS_SPL" ("NUI", "CLASS_NAME", "CLASS_TYPE", "SPL_COUNT") AS
  select nui,c.name class_name, case when c.TLEVEL='[MoA]' then 'MoA' when c.TLEVEL='[PE]' then 'PE' when c.TLEVEL='[Chemical/Ingredient]' then 'CI' when c.TLEVEL='[EPC]' then 'EPC' else c.TLEVEL
                                end class_type, cc.spl_count from ndfrt_conceptinf c  join ndfrt_concept_spl_count cc on c.nui = cc.ndfrt_nui;
--------------------------------------------------------
--  DDL for View SECTION_TYPE_SEALDS_VIEW
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE VIEW "SECTION_TYPE_SEALDS_VIEW" ("TYPEFORMAT", "VIEW_NO", "ISFISRTROWOFTYPE", "CODE", "DISPLAY_NAME", "SECTION_LEVEL", "GROUPNO", "COUNT") AS
  select "TYPEFORMAT",to_number(view_no,'99.99') VIEW_NO,"ISFISRTROWOFTYPE","CODE","DISPLAY_NAME","SECTION_LEVEL","GROUPNO",c.count
  from section_type_sealds s
    join spl_section_count c on c.loinc_code=s.code
  where display_name<>'PATIENT COUNSELING INFORMATION'
  union select  "TYPEFORMAT",to_number(view_no,'99.99') VIEW_NO,"ISFISRTROWOFTYPE","CODE","DISPLAY_NAME","SECTION_LEVEL","GROUPNO",(select count(distinct spl_id) from spl_sec where contains(content_xml,'PATIENT COUNSELING INFORMATION')>0 and LOINC_CODE='34076-0') count
        from section_type_sealds where display_name='PATIENT COUNSELING INFORMATION'
  union select  "TYPEFORMAT",to_number(view_no,'99.99') VIEW_NO,"ISFISRTROWOFTYPE","CODE","DISPLAY_NAME","SECTION_LEVEL","GROUPNO",(select count(distinct spl_id) count from sum_spl where INITIAL_APPROVAL_YEAR is not null) count
        from section_type_sealds where code='99999-1'
  union select  "TYPEFORMAT",to_number(view_no,'99.99') VIEW_NO,"ISFISRTROWOFTYPE","CODE","DISPLAY_NAME","SECTION_LEVEL","GROUPNO",(select count(distinct spl_id) count from sum_spl where PRODUCT_TITLE is not null) count
        from section_type_sealds where code='99999-2'
  order by GROUPNO,view_no,section_level;
--------------------------------------------------------
--  DDL for View SPL_APPL_V
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE VIEW "SPL_APPL_V" ("SPL_ID", "MARKETING_CAT_DESCRIPTION", "APPL_NO") AS
  select distinct spl_id, MARKETING_CAT_DESCRIPTION,
    substr(trim(APPROVAL_NUM),length(trim(APPROVAL_NUM))-5,length(trim(APPROVAL_NUM))) appl_no
  from dgv_prod_appr
  where trim(MARKETING_CAT_NCIT_CODE) in( 'C73583','C73584','C73585','C73588','C73593','C73594','C73605','C75302','C780438','C80440','C80441','C80442','C92556') and APPROVAL_NUM is not null
  order by spl_id;
--------------------------------------------------------
--  DDL for View SPL_LOADER_COUNT_VIEW
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE VIEW "SPL_LOADER_COUNT_VIEW" ("LOAD_DATE", "ADDED", "RESURRECTED", "REMOVED", "UPDATED", "TOTAL") AS
  select to_char(COUNT_TS,'yyyy-MM-dd') load_date,sum(case when count_type='new' then RECORD_COUNT else 0 end) added,
         sum(case when count_type='resurrected' then RECORD_COUNT else 0 end) resurrected,
         sum(case when count_type='removed' then RECORD_COUNT else 0 end) removed,
         sum(case when count_type='updated' then RECORD_COUNT else 0 end) updated,
         sum(case when count_type='total' then RECORD_COUNT else 0 end) total

  from load_record_count
  where object_name='SPL' and COUNT_TS>add_months(sysdate, -12 * 6) and COUNT_TYPE in('new','removed','updated','resurrected','total')
  group by  to_char(COUNT_TS,'yyyy-MM-dd')
  order by to_char(COUNT_TS,'yyyy-MM-dd') desc;
--------------------------------------------------------
--  DDL for View SPL_LOADER_COUNT_VIEW_LDT
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE VIEW "SPL_LOADER_COUNT_VIEW_LDT" ("LOAD_DATE", "TOTAL") AS
  select to_char(COUNT_TS,'yyyy-MM-dd') load_date,
         sum(case when count_type='total' then RECORD_COUNT else 0 end) total
  from load_record_count
  where object_name='DGV_SUM_RX_SPL' and COUNT_TS>add_months(sysdate, -12 * 6) and COUNT_TYPE in('total')
  group by  to_char(COUNT_TS,'yyyy-MM-dd')
  order by to_char(COUNT_TS,'yyyy-MM-dd') desc;
--------------------------------------------------------
--  DDL for View SPL_SUMMARY
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE VIEW "SPL_SUMMARY" ("SPL_ID", "SPL_GUID", "TITLE", "DOCUMENT_TYPE", "AUTHOR_ORG_NORMD_NAME", "PRODUCT_NAMES", "PRODUCT_NORMD_GENERIC_NAMES", "NUM_PRODUCTS", "MARKET_CATEGORIES", "MARKETING_ACTS", "ROUTES_OF_ADMINISTRATION", "NDC_CODES", "ACT_INGR_UNIIS", "DOSAGE_FORMS", "EFF_TIME", "VERSION_NUM", "ORG_CURRENCY_RANK") AS
  select
    spl_id
    ,spl_guid
    ,title
    ,document_type
    ,author_org_normd_name
    ,product_names
    ,product_normd_generic_names
    ,num_products
    ,market_categories
    ,marketing_acts
    ,routes_of_administration
    ,ndc_codes
    ,act_ingr_uniis
    ,dosage_forms
    ,eff_time
    ,version_num
    ,org_currency_rank
  from
    sum_spl
;


create view meddra_llt_spl_sum_v as
select 
  llt.llt_name MedDRA_LLT,
  case when llt.llt_code = llt.pt_code then 'Y' else 'N' end is_pt,
  mh.pt_name,
  mh.hlt_name,
  mh.hlgt_name,
  mh.soc_name,
  mh.primary_soc_fg is_primary_soc_for_pt,
  st.loinc_name section_type,
  tls.only_as_part_of_larger_term,
  l.set_id spl_id,
  l.document_type,
  l.author_org_normd_name,
  l.num_products,
  l.marketing_acts,
  l.product_names,
  l.product_normd_generic_names,
  l.market_categories,
  l.routes_of_administration,
  l.ndc_codes,
  l.dosage_forms,
  l.num_act_ingrs,
  l.act_ingr_uniis,
  l.act_ingr_names
from meddra_llt_spl_coded_sec tls
join sum_spl l on tls.spl_set_id = l.set_id
join meddra.low_level_term llt on tls.llt_code = llt.llt_code
join section_type st on tls.section_type = st.loinc_code
join meddra.meddra_hierarchy mh on llt.pt_code = mh.pt_code
;

