create table srs.unii_data (
  unii varchar(10 byte), 
  pt varchar(2000 byte) not null, 
  rn varchar(500 byte), 
  mf varchar(500 byte), 
  inchikey varchar(500 byte), 
  einecs varchar(500 byte), 
  ncit varchar(500 byte), 
  itis varchar(500 byte), 
  ncbi varchar(10 byte), 
  plants varchar(10 byte), 
  smiles varchar(2000 byte),
  constraint pk_uniidata primary key (unii)
) pctfree 0;


create index srs.uniidata_rn_ix on unii_data(rn);


create table srs.unii_name (
  unii varchar(10 byte),
  name varchar(2000 byte),
  "TYPE" varchar(2 byte),
  pt varchar(2000 byte) not null,
  constraint pk_uniiname primary key (unii, "TYPE", name)
) pctfree 0;

create index uniidata_smiles_ix on srs.unii_data(smiles) indextype is bingo.moleculeindex;

grant select, references on srs.unii_data to DrugLabel;
grant select, references on srs.unii_name to DrugLabel;
